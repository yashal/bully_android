package com.nexgen.bullybeware.pojo;

/**
 * Created by quepplin1 on 9/21/2016.
 */
public class ImageUploadObjectPojo {

    private String File_Id;
    private String File;
    private String date_added;

    public String getFile_Id() {
        return File_Id;
    }

    public void setFile_Id(String file_Id) {
        File_Id = file_Id;
    }

    public String getFile() {
        return File;
    }

    public void setFile(String file) {
        File = file;
    }

    public String getDate_added() {
        return date_added;
    }

    public void setDate_added(String date_added) {
        this.date_added = date_added;
    }


}
