package com.nexgen.bullybeware.pojo;

/**
 * Created by umax desktop on 2/16/2017.
 */

public class SelfHarmObjectPojo {

    private String id;
    private String student_id;
    private String friend_name;
    private String harm_description;
    private String submitted_on;

    public String getStudent_id() {
        return student_id;
    }

    public void setStudent_id(String student_id) {
        this.student_id = student_id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFriend_name() {
        return friend_name;
    }

    public void setFriend_name(String friend_name) {
        this.friend_name = friend_name;
    }

    public String getHarm_description() {
        return harm_description;
    }

    public void setHarm_description(String harm_description) {
        this.harm_description = harm_description;
    }

    public String getSubmitted_on() {
        return submitted_on;
    }

    public void setSubmitted_on(String submitted_on) {
        this.submitted_on = submitted_on;
    }


}
