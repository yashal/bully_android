package com.nexgen.bullybeware.pojo;

/**
 * Created by quepplin1 on 8/23/2016.
 */
public class SecurityQueListPojo {

    private String id;
    private String question;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }


}
