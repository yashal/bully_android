package com.nexgen.bullybeware.pojo;

import java.util.ArrayList;

/**
 * Created by quepplin1 on 11/2/2016.
 */
public class ReportSubmissionPojo {

    private String USER_ID;
    private String file_type;
    private String SUBMIT_ID;
    private String type;
    private String subject;
    private String bully_name;
    private String date;
    private String Time;
    private String name;
    private String adult;
    private String description;

    public String getBully_name() {
        return bully_name;
    }

    public void setBully_name(String bully_name) {
        this.bully_name = bully_name;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }



    public ArrayList<ReportSubmissionFilesPojo> getFilelist() {
        return filelist;
    }

    public void setFilelist(ArrayList<ReportSubmissionFilesPojo> filelist) {
        this.filelist = filelist;
    }

    private ArrayList<ReportSubmissionFilesPojo> filelist;

    public String getTime() {
        return Time;
    }

    public void setTime(String time) {
        Time = time;
    }

    public String getUSER_ID() {
        return USER_ID;
    }

    public void setUSER_ID(String USER_ID) {
        this.USER_ID = USER_ID;
    }

    public String getFile_type() {
        return file_type;
    }

    public void setFile_type(String file_type) {
        this.file_type = file_type;
    }

    public String getSUBMIT_ID() {
        return SUBMIT_ID;
    }

    public void setSUBMIT_ID(String SUBMIT_ID) {
        this.SUBMIT_ID = SUBMIT_ID;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAdult() {
        return adult;
    }

    public void setAdult(String adult) {
        this.adult = adult;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
