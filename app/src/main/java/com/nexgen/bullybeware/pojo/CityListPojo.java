package com.nexgen.bullybeware.pojo;

/**
 * Created by quepplin1 on 8/23/2016.
 */
public class CityListPojo {

    private String id;
    private String city_name;

    public String getCity_name() {
        return city_name;
    }

    public void setCity_name(String city_name) {
        this.city_name = city_name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
