package com.nexgen.bullybeware.pojo;

import java.util.ArrayList;

/**
 * Created by quepplin1 on 8/26/2016.
 */
public class ForgotPasswordPojo {

    private String Title;
    private String Message;
    private Boolean Status;

    public ForgotPasswordObjectPojo getObject() {
        return Object;
    }

    public void setObject(ForgotPasswordObjectPojo object) {
        Object = object;
    }

    private ForgotPasswordObjectPojo Object;

    public Boolean getStatus() {
        return Status;
    }

    public void setStatus(Boolean status) {
        Status = status;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }


}
