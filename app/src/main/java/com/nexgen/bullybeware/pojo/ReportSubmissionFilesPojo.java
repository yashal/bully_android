package com.nexgen.bullybeware.pojo;

/**
 * Created by quepplin1 on 11/3/2016.
 */
public class ReportSubmissionFilesPojo {

    private String Submit_id;
    private String Image_url;
    private String Audio_url;
    private String Video_url;

    public String getSubmit_id() {
        return Submit_id;
    }

    public void setSubmit_id(String Submit_id) {
        this.Submit_id = Submit_id;
    }

    public String getImage_url() {
        return Image_url;
    }

    public void setImage_url(String image_url) {
        Image_url = image_url;
    }

    public String getAudio_url() {
        return Audio_url;
    }

    public void setAudio_url(String audio_url) {
        Audio_url = audio_url;
    }

    public String getVideo_url() {
        return Video_url;
    }

    public void setVideo_url(String video_url) {
        Video_url = video_url;
    }
}
