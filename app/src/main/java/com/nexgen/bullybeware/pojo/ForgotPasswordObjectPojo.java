package com.nexgen.bullybeware.pojo;

/**
 * Created by quepplin1 on 8/26/2016.
 */
public class ForgotPasswordObjectPojo {

    private String student_id;
    private String name;
    private String id;
    private String OTP;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStudent_id() {
        return student_id;
    }

    public void setStudent_id(String student_id) {
        this.student_id = student_id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getOTP() {
        return OTP;
    }

    public void setOTP(String OTP) {
        this.OTP = OTP;
    }
}
