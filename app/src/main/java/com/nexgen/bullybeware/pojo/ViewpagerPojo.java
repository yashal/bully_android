package com.nexgen.bullybeware.pojo;

import android.content.Context;

/**
 * Created by quepplin1 on 9/8/2016.
 */
public class ViewpagerPojo {

    private Context context;

    public ViewpagerPojo(Context context){

        this.context = context;

    }

    private String fname;

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

}
