package com.nexgen.bullybeware.pojo;

import java.util.ArrayList;

/**
 * Created by quepplin1 on 9/28/2016.
 */
public class GetSubmissionPojo {

    private String Title;
    private String Message;
    private Boolean Status;
    private ArrayList<GetSubmissionObjectPojo> Object;

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public Boolean getStatus() {
        return Status;
    }

    public void setStatus(Boolean status) {
        Status = status;
    }

    public ArrayList<GetSubmissionObjectPojo> getObject() {
        return Object;
    }

    public void setObject(ArrayList<GetSubmissionObjectPojo> object) {
        Object = object;
    }
}
