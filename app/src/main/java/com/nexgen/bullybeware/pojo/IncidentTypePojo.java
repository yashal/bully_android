package com.nexgen.bullybeware.pojo;

import java.util.ArrayList;

/**
 * Created by quepplin1 on 9/20/2016.
 */
public class IncidentTypePojo {

    private String Title;
    private String Message;
    private Boolean Status;
    private ArrayList<IncidentTypeListPojo>Object;

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public Boolean getStatus() {
        return Status;
    }

    public void setStatus(Boolean status) {
        Status = status;
    }

    public ArrayList<IncidentTypeListPojo> getObject() {
        return Object;
    }

    public void setObject(ArrayList<IncidentTypeListPojo> object) {
        Object = object;
    }
}
