package com.nexgen.bullybeware.pojo;

/**
 * Created by quepplin1 on 8/26/2016.
 */
public class ChangePasswordPojo {

    private String Title;
    private String Message;
    private Boolean Status;
    private ChangePasswordObjectPojo Object;

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public Boolean getStatus() {
        return Status;
    }

    public void setStatus(Boolean status) {
        Status = status;
    }

    public ChangePasswordObjectPojo getObject() {
        return Object;
    }

    public void setObject(ChangePasswordObjectPojo object) {
        Object = object;
    }
}
