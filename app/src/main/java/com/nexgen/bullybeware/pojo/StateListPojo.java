package com.nexgen.bullybeware.pojo;

/**
 * Created by quepplin1 on 8/12/2016.
 */

public class StateListPojo {

    private String id;
    private String state_name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getState_name() {
        return state_name;
    }

    public void setState_name(String state_name) {
        this.state_name = state_name;
    }
}
