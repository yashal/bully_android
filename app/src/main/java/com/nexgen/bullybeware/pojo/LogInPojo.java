package com.nexgen.bullybeware.pojo;

import java.util.ArrayList;

/**
 * Created by quepplin1 on 8/12/2016.
 */

public class LogInPojo {

    private String Title;
    private String Message;
    private Boolean Status;
    private LogInObjectPojo Object;

    public LogInObjectPojo getObject() {
        return Object;
    }

    public void setObject(LogInObjectPojo object) {
        Object = object;
    }



    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public Boolean getStatus() {
        return Status;
    }

    public void setStatus(Boolean status) {
        Status = status;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

}
