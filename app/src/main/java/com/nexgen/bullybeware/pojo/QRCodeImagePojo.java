package com.nexgen.bullybeware.pojo;

/**
 * Created by umax desktop on 2/1/2017.
 */

public class QRCodeImagePojo {

    private String Title;
    private String Message;
    private Boolean Status;
    private String Object;

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public Boolean getStatus() {
        return Status;
    }

    public void setStatus(Boolean status) {
        Status = status;
    }

    public String getObject() {
        return Object;
    }

    public void setObject(String object) {
        Object = object;
    }
}
