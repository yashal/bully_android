package com.nexgen.bullybeware.pojo;

import java.util.ArrayList;

/**
 * Created by quepplin1 on 9/20/2016.
 */
public class IncidentSubmissionPojo {

    private String Title;
    private String Message;
    private Boolean Status;
    private IncidentSubmissionListPojo Object;

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public Boolean getStatus() {
        return Status;
    }

    public void setStatus(Boolean status) {
        Status = status;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public IncidentSubmissionListPojo getObject() {
        return Object;
    }

    public void setObject(IncidentSubmissionListPojo object) {
        Object = object;
    }

}
