package com.nexgen.bullybeware.pojo;

import java.util.ArrayList;

/**
 * Created by umax desktop on 2/9/2017.
 */

public class GetLatLong {

    private String Title;
    private String Message;
    private Boolean Status;
    private ArrayList<GetLatLongObject> Object;



    public ArrayList<GetLatLongObject> getObject() {
        return Object;
    }

    public void setObject(ArrayList<GetLatLongObject> object) {
        Object = object;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public Boolean getStatus() {
        return Status;
    }

    public void setStatus(Boolean status) {
        Status = status;
    }
}
