package com.nexgen.bullybeware.pojo;

/**
 * Created by quepplin1 on 9/20/2016.
 */
public class IncidentSubmissionListPojo {

    private String From_id;
    private String Type;
    private String date_added;

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }

    public String getFrom_id() {
        return From_id;
    }

    public void setFrom_id(String from_id) {
        From_id = from_id;
    }

    public String getDate_added() {
        return date_added;
    }

    public void setDate_added(String date_added) {
        this.date_added = date_added;
    }
}
