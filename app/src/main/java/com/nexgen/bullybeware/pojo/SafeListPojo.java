package com.nexgen.bullybeware.pojo;

import java.util.ArrayList;

/**
 * Created by yash on 7/18/2017.
 */

public class SafeListPojo extends BasePojo {

    private ArrayList<SafeListDataPojo> Object;

    public ArrayList<SafeListDataPojo> getObject() {
        return Object;
    }

    public void setObject(ArrayList<SafeListDataPojo> object) {
        Object = object;
    }
}
