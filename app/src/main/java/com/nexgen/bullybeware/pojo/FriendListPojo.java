package com.nexgen.bullybeware.pojo;

import java.util.ArrayList;

/**
 * Created by umax desktop on 2/1/2017.
 */

public class FriendListPojo {

    private String Title;
    private String Message;
    private Boolean Status;
    private ArrayList<FriendListObjectPojo> Object;

    public Boolean getStatus() {
        return Status;
    }

    public void setStatus(Boolean status) {
        Status = status;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public ArrayList<FriendListObjectPojo> getObject() {
        return Object;
    }

    public void setObject(ArrayList<FriendListObjectPojo> object) {
        Object = object;
    }
}
