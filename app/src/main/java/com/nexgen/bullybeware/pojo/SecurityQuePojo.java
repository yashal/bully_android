package com.nexgen.bullybeware.pojo;

import java.util.ArrayList;

/**
 * Created by quepplin1 on 8/23/2016.
 */
public class SecurityQuePojo {

    private String Title;
    private String Message;
    private Boolean Status;
    private ArrayList<SecurityQueListPojo> Object;

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public Boolean getStatus() {
        return Status;
    }

    public void setStatus(Boolean status) {
        Status = status;
    }

    public ArrayList<SecurityQueListPojo> getObject() {
        return Object;
    }

    public void setObject(ArrayList<SecurityQueListPojo> object) {
        Object = object;
    }
}
