package com.nexgen.bullybeware.pojo;

/**
 * Created by quepplin1 on 9/20/2016.
 */
public class IncidentTypeListPojo {

    private String id;
    private String incident;

    public String getIncident() {
        return incident;
    }

    public void setIncident(String incident) {
        this.incident = incident;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
