package com.nexgen.bullybeware.pojo;

/**
 * Created by quepplin1 on 8/24/2016.
 */
public class SignUpObjectPojo {

    private String student_id;
    private String school_id;
    private String email;
    private String school_email;
    private String mobile;
    private String last_login_time;



    public String getSchool_email() {
        return school_email;
    }

    public void setSchool_email(String school_email) {
        this.school_email = school_email;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getStudent_id() {
        return student_id;
    }

    public void setStudent_id(String student_id) {
        this.student_id = student_id;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getLast_login_time() {
        return last_login_time;
    }

    public void setLast_login_time(String last_login_time) {
        this.last_login_time = last_login_time;
    }

    public String getSchool_id(){
        return school_id;
    }

    public void setSchool_id(String School_id){
        this.school_id = School_id;


    }
}
