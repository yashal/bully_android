package com.nexgen.bullybeware.pojo;

import java.io.Serializable;

/**
 * Created by hp on 10/25/2016.
 */

public class VideolistStatusPojo implements Serializable {

    private Boolean status = false;
    private boolean requeststaus=false;
    private String path;
    private String video_id;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public boolean getRequeststaus() {
        return requeststaus;
    }

    public void setRequeststaus(boolean requeststaus) {
        this.requeststaus = requeststaus;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getVideo_id() {
        return video_id;
    }

    public void setVideo_id(String video_id) {
        this.video_id = video_id;
    }
}
