package com.nexgen.bullybeware.pojo;

import java.util.ArrayList;

/**
 * Created by yash on 7/18/2017.
 */

public class RequestResponsePojo extends BasePojo {

    private ArrayList<RequestResponseDataPojo> Object;

    public ArrayList<RequestResponseDataPojo> getObject() {
        return Object;
    }

    public void setObject(ArrayList<RequestResponseDataPojo> object) {
        Object = object;
    }
}
