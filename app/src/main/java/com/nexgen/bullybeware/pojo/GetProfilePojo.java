package com.nexgen.bullybeware.pojo;

import java.util.ArrayList;

/**
 * Created by quepplin1 on 10/26/2016.
 */
public class GetProfilePojo {

    private String Title;
    private String Message;
    private Boolean Status;
    private GetProfileObjectPojo Object;

    public GetProfileObjectPojo getObject() {
        return Object;
    }

    public void setObject(GetProfileObjectPojo object) {
        Object = object;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public Boolean getStatus() {
        return Status;
    }

    public void setStatus(Boolean status) {
        Status = status;
    }
}
