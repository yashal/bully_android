package com.nexgen.bullybeware.pojo;

/**
 * Created by yash on 7/18/2017.
 */

public class RequestResponseDataPojo {

    private String question_id;
    private String situation;
    private String response;
    public boolean isExpanded = false;

    public String getQuestion_id() {
        return question_id;
    }

    public void setQuestion_id(String question_id) {
        this.question_id = question_id;
    }

    public String getSituation() {
        return situation;
    }

    public void setSituation(String situation) {
        this.situation = situation;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public boolean isExpanded() {
        return isExpanded;
    }

    public void setExpanded(boolean expanded) {
        isExpanded = expanded;
    }
}
