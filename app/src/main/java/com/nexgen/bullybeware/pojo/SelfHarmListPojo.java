package com.nexgen.bullybeware.pojo;

import java.util.ArrayList;

/**
 * Created by umax desktop on 2/16/2017.
 */

public class SelfHarmListPojo {

    private String Title;
    private String Message;
    private Boolean Status;
    private ArrayList<SelfHarmObjectPojo>Object;

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public Boolean getStatus() {
        return Status;
    }

    public void setStatus(Boolean status) {
        Status = status;
    }

    public ArrayList<SelfHarmObjectPojo> getObject() {
        return Object;
    }

    public void setObject(ArrayList<SelfHarmObjectPojo> object) {
        Object = object;
    }
}
