package com.nexgen.bullybeware.pojo;

/**
 * Created by quepplin1 on 8/23/2016.
 */
public class RelationListPojo {

    private String id;
    private String relation;

    public String getRelation() {
        return relation;
    }

    public void setRelation(String relation) {
        this.relation = relation;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
