package com.nexgen.bullybeware.pojo;

import java.util.ArrayList;

/**
 * Created by quepplin1 on 9/28/2016.
 */
public class GetSubmissionObjectPojo {

    private String id;
    private String type;
    private String user_id;
    private String incident_id;
    private String date;
    private String time;
    private String subject;
    private String bully_name;
    private String victim;
    private String adult_name;
    private String description;
    private ArrayList<GetSubmissionFilesPojo>files;
    private String status;
    private String date_added;
    private String date_update;
    private String incident;

    public String getBully_name() {
        return bully_name;
    }

    public void setBully_name(String bully_name) {
        this.bully_name = bully_name;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getIncident() {
        return incident;
    }

    public void setIncident(String incident) {
        this.incident = incident;
    }



    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getIncident_id() {
        return incident_id;
    }

    public void setIncident_id(String incident_id) {
        this.incident_id = incident_id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }


    public String getVictim() {
        return victim;
    }

    public void setVictim(String victim) {
        this.victim = victim;
    }

    public String getAdult_name() {
        return adult_name;
    }

    public void setAdult_name(String adult_name) {
        this.adult_name = adult_name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ArrayList<GetSubmissionFilesPojo> getFiles() {
        return files;
    }

    public void setFiles(ArrayList<GetSubmissionFilesPojo> files) {
        this.files = files;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDate_added() {
        return date_added;
    }

    public void setDate_added(String date_added) {
        this.date_added = date_added;
    }

    public String getDate_update() {
        return date_update;
    }

    public void setDate_update(String date_update) {
        this.date_update = date_update;
    }
}
