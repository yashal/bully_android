package com.nexgen.bullybeware.pojo;

import java.util.ArrayList;

/**
 * Created by quepplin1 on 8/17/2016.
 */

public class SignUpPojo {

    private String Title;
    private String Message;
    private Boolean Status;
    private SignUpObjectPojo Object;

    public SignUpObjectPojo getObject() {
        return Object;
    }

    public void setObject(SignUpObjectPojo object) {
        Object = object;
    }



    public Boolean getStatus() {
        return Status;
    }

    public void setStatus(Boolean status) {
        Status = status;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

}
