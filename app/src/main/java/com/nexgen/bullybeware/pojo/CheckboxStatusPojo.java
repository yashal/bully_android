package com.nexgen.bullybeware.pojo;

/**
 * Created by quepplin1 on 9/23/2016.
 */
public class CheckboxStatusPojo {

    private Boolean status;
    private String path;

    public CheckboxStatusPojo(Boolean status,String path){

        this.status = status;
        this.path = path;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

}
