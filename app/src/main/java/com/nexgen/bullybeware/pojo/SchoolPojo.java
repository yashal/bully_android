package com.nexgen.bullybeware.pojo;

import java.util.ArrayList;

/**
 * Created by quepplin1 on 8/23/2016.
 */
public class SchoolPojo {

    private String Title;
    private String Message;
    private Boolean Status;
    private SchoolListpojo Object;

    public Boolean getStatus() {
        return Status;
    }

    public void setStatus(Boolean status) {
        Status = status;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public SchoolListpojo getObject() {
        return Object;
    }

    public void setObject(SchoolListpojo object) {
        Object = object;
    }
}
