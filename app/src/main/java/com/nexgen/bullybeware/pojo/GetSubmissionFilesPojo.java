package com.nexgen.bullybeware.pojo;

import java.io.Serializable;

/**
 * Created by quepplin1 on 9/28/2016.
 */
public class GetSubmissionFilesPojo implements Serializable {

    private String id;
    private String file;
    private String thumbnail;

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
