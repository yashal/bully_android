package com.nexgen.bullybeware.pojo;

/**
 * Created by quepplin1 on 9/21/2016.
 */
public class ImageUploadPojo {

    private String Title;
    private String Message;
    private Boolean Status;
    private ImageUploadObjectPojo Object;

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public Boolean getStatus() {
        return Status;
    }

    public void setStatus(Boolean status) {
        Status = status;
    }

    public ImageUploadObjectPojo getObject() {
        return Object;
    }

    public void setObject(ImageUploadObjectPojo object) {
        Object = object;
    }
}
