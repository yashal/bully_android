package com.nexgen.bullybeware.pojo;

import java.util.ArrayList;

/**
 * Created by quepplin1 on 8/12/2016.
 */

public class StatePojo {

    private String Title;
    private String Message;

    public Boolean getStatus() {
        return Status;
    }

    public void setStatus(Boolean status) {
        Status = status;
    }

    private Boolean Status;
    private ArrayList<StateListPojo> Object;

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public ArrayList<StateListPojo> getObject() {
        return Object;
    }

    public void setObject(ArrayList<StateListPojo> object) {
        Object = object;
    }
}
