package com.nexgen.bullybeware.pojo;

/**
 * Created by quepplin1 on 10/26/2016.
 */
public class GetProfileObjectPojo {

    private String id;
    private String school_id;
    private String name;
    private String country;
    private String state;
    private String city;
    private String zip;
    private String address;
    private String school_email;
    private String last_name;
    private String profile_image;
    private String thumbnail;
    private String email;
    private String password;
    private String principal_mobile;
    private String class1;
    private String con_name;
    private String con_lastname;
    private String con_email;
    private String con_relation;
    private String con_mobile;
    private String security_question;
    private String security_answer;
    private String last_login_time;
    private String socialId;
    private String date_added;
    private String date_update;
    private String school_name;
    private String school_address;
    private String school_principal_name;
    private String school_mobile;
    private String school_code;
    private String city_name;
    private String state_name;
    private String country_name;
    private String relation_name;

    public String getPrincipal_name() {
        return school_principal_name;
    }

    public void setPrincipal_name(String principal_name) {
        this.school_principal_name = principal_name;
    }



    public String getRelation_name() {
        return relation_name;
    }

    public void setRelation_name(String relation_name) {
        this.relation_name = relation_name;
    }




    public String getSchool_address() {
        return school_address;
    }

    public void setSchool_address(String school_address) {
        this.school_address = school_address;
    }

    public String getSchool_mobile() {
        return school_mobile;
    }

    public void setSchool_mobile(String school_mobile) {
        this.school_mobile = school_mobile;
    }

    public String getSchool_code() {
        return school_code;
    }

    public void setSchool_code(String school_code) {
        this.school_code = school_code;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSchool_id() {
        return school_id;
    }

    public void setSchool_id(String school_id) {
        this.school_id = school_id;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getSchool_email() {
        return school_email;
    }

    public void setSchool_email(String school_email) {
        this.school_email = school_email;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getProfile_image() {
        return profile_image;
    }

    public void setProfile_image(String profile_image) {
        this.profile_image = profile_image;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMobile() {
        return principal_mobile;
    }

    public void setMobile(String mobile) {
        this.principal_mobile = mobile;
    }

    public String getClass1() {
        return class1;
    }

    public void setClass1(String class1) {
        this.class1 = class1;
    }

    public String getCon_name() {
        return con_name;
    }

    public void setCon_name(String con_name) {
        this.con_name = con_name;
    }

    public String getCon_lastname() {
        return con_lastname;
    }

    public void setCon_lastname(String con_lastname) {
        this.con_lastname = con_lastname;
    }

    public String getCon_email() {
        return con_email;
    }

    public void setCon_email(String con_email) {
        this.con_email = con_email;
    }

    public String getCon_relation() {
        return con_relation;
    }

    public void setCon_relation(String con_relation) {
        this.con_relation = con_relation;
    }

    public String getCon_mobile() {
        return con_mobile;
    }

    public void setCon_mobile(String con_mobile) {
        this.con_mobile = con_mobile;
    }

    public String getSecurity_question() {
        return security_question;
    }

    public void setSecurity_question(String security_question) {
        this.security_question = security_question;
    }

    public String getSecurity_answer() {
        return security_answer;
    }

    public void setSecurity_answer(String security_answer) {
        this.security_answer = security_answer;
    }

    public String getLast_login_time() {
        return last_login_time;
    }

    public void setLast_login_time(String last_login_time) {
        this.last_login_time = last_login_time;
    }

    public String getSocialId() {
        return socialId;
    }

    public void setSocialId(String socialId) {
        this.socialId = socialId;
    }

    public String getDate_added() {
        return date_added;
    }

    public void setDate_added(String date_added) {
        this.date_added = date_added;
    }

    public String getDate_update() {
        return date_update;
    }

    public void setDate_update(String date_update) {
        this.date_update = date_update;
    }

    public String getSchool_name() {
        return school_name;
    }

    public void setSchool_name(String school_name) {
        this.school_name = school_name;
    }

    public String getState_name() {
        return state_name;
    }

    public void setState_name(String state_name) {
        this.state_name = state_name;
    }

    public String getCity_name() {
        return city_name;
    }

    public void setCity_name(String city_name) {
        this.city_name = city_name;
    }

    public String getCountry_name() {
        return country_name;
    }

    public void setCountry_name(String country_name) {
        this.country_name = country_name;
    }

}
