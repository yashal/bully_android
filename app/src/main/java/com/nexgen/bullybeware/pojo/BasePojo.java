package com.nexgen.bullybeware.pojo;

/**
 * Created by yash on 7/18/2017.
 */

public class BasePojo {

    private String Title;
    private String Message;
    private Boolean Status;

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public Boolean getStatus() {
        return Status;
    }

    public void setStatus(Boolean status) {
        Status = status;
    }
}
