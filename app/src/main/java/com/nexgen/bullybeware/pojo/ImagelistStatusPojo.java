package com.nexgen.bullybeware.pojo;

import android.os.Parcelable;

import java.io.Serializable;

/**
 * Created by quepplin1 on 10/24/2016.
 */
public class ImagelistStatusPojo implements Serializable {

    private Boolean status = false;
    private boolean requeststaus=false;
    private String path;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public boolean getRequeststaus() {
        return requeststaus;
    }

    public void setRequeststaus(boolean requeststaus) {
        this.requeststaus = requeststaus;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
