package com.nexgen.bullybeware.pojo;

/**
 * Created by quepplin1 on 9/23/2016.
 */
public class ChangePasswordObjectPojo {

    private String student_id;
    private String email;
    private String mobile;
    private String last_login_time;

    public String getStudent_id() {
        return student_id;
    }

    public void setStudent_id(String student_id) {
        this.student_id = student_id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getLast_login_time() {
        return last_login_time;
    }

    public void setLast_login_time(String last_login_time) {
        this.last_login_time = last_login_time;
    }
}
