package com.nexgen.bullybeware.pojo;

import java.util.ArrayList;

/**
 * Created by quepplin1 on 8/23/2016.
 */
public class FbProfileInfoPojo {

    private String Title;
    private String Message;
    private Boolean Status;
    private FbprofileObjectPojo Object;

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public Boolean getStatus() {
        return Status;
    }

    public void setStatus(Boolean status) {
        Status = status;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }
    public FbprofileObjectPojo getObject() {
        return Object;
    }

    public void setObject(FbprofileObjectPojo object) {
        Object = object;
    }
}
