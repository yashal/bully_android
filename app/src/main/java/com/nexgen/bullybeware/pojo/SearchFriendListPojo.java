package com.nexgen.bullybeware.pojo;

import java.util.ArrayList;

/**
 * Created by yash on 7/19/2017.
 */

public class SearchFriendListPojo extends BasePojo {

    private ArrayList<SearchFriendDataPojo> Object;

    public ArrayList<SearchFriendDataPojo> getObject() {
        return Object;
    }

    public void setObject(ArrayList<SearchFriendDataPojo> object) {
        Object = object;
    }
}
