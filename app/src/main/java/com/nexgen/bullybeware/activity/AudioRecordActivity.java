package com.nexgen.bullybeware.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioRecord;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.GlideDrawableImageViewTarget;
import com.newventuresoftware.waveform.WaveformView;
import com.nexgen.bullybeware.R;
import com.nexgen.bullybeware.util.AudioDataReceivedListener;
import com.nexgen.bullybeware.util.BullyPreferences;
import com.nexgen.bullybeware.util.BullyToast;
import com.nexgen.bullybeware.util.RecordingThread;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.RECORD_AUDIO;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

/**
 * Created by quepplin1 on 9/2/2016.
 */
public class AudioRecordActivity extends BaseActivity {

    private TextView tv_audio_timer;
    private ImageView iView_start,iView_stop;
    private String for_val;
    private WaveformView mRealtimeWaveformView;
    private RecordingThread mRecordingThread;
    private LinearLayout ll_record_main;
    long lastClickTime = 0;
    private TextView tv_recording;

    private static final long DOUBLE_CLICK_TIME_DELTA = 2000;//milliseconds

    private int cnt;
    CountDownTimer t;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_audiorecord);

        setDrawerCrossIcon("Record Audio");

        init();
        setOnClickListner();

    }

    private void init(){
        tv_audio_timer = (TextView) findViewById(R.id.tv_audio);
        iView_start = (ImageView) findViewById(R.id.iView_start_audio);
        iView_stop  = (ImageView) findViewById(R.id.iView_stop_audio);

        tv_recording = (TextView) findViewById(R.id.tv_recording);

        ll_record_main = (LinearLayout) findViewById(R.id.ll_record_main);

        GlideDrawableImageViewTarget imageViewTarget = new GlideDrawableImageViewTarget(iView_stop);
        Glide.with(this).load(R.raw.btn_pause).into(imageViewTarget);


       /*  mode = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        //Silent Mode Programatically
        mode.setRingerMode(AudioManager.RINGER_MODE_SILENT);*/

        BullyToast.showToast(AudioRecordActivity.this,getResources().getString(R.string.emergency_alaram));

        for_val = getIntent().getStringExtra("for");


        // waveform set

        mRealtimeWaveformView = (WaveformView) findViewById(R.id.waveformView);
        mRecordingThread = new RecordingThread(AudioRecordActivity.this,new AudioDataReceivedListener() {
            @Override
            public void onAudioDataReceived(short[] data) {

                mRealtimeWaveformView.setSamples(data);
            }
        });
    }

    private void setOnClickListner(){
        iView_start.setOnClickListener(new View.OnClickListener() {
         @Override
         public void onClick(View view) {

             if (!mRecordingThread.recording()) {

                 mRecordingThread.startRecording();

                 statTimer();

                 iView_start.setVisibility(View.GONE);
                 iView_stop.setVisibility(View.VISIBLE);

                 tv_recording.setVisibility(View.VISIBLE);

             } else {
                 mRecordingThread.stopRecording(for_val);
             }
         }
     });

        iView_stop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                iView_stop.setSoundEffectsEnabled(false);
                mRecordingThread.stopRecording(for_val);

                iView_start.setVisibility(View.VISIBLE);
                iView_stop.setVisibility(View.GONE);

                tv_recording.setVisibility(View.INVISIBLE);

       //         mode.setRingerMode(AudioManager.RINGER_MODE_NORMAL);

                t.cancel();
            }
        });
        ll_record_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                long clickTime = System.currentTimeMillis();
                if (clickTime - lastClickTime < DOUBLE_CLICK_TIME_DELTA){
                    try {
                        playStopClickListner();
                    } catch (Exception e){
                        e.printStackTrace();
                    }
                    lastClickTime = 0;
                } else {
                    lastClickTime = clickTime;
                }
            }
        });
    }

    private void statTimer(){
        t = new CountDownTimer( 120000 , 1000) {

            @Override
            public void onTick(long millisUntilFinished) {

                cnt++;

                long millis = cnt;
                int seconds = (int) (millis);
                int minutes = seconds / 60;
                seconds     = seconds % 60;

                tv_audio_timer.setText(String.format("%02d:%02d", minutes, seconds));

            }

            @Override
            public void onFinish() {
                mRecordingThread.stopRecording(for_val);

                iView_start.setVisibility(View.VISIBLE);
                iView_stop.setVisibility(View.GONE);

                t.cancel();

            }
        }.start();
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();

        Intent intent = new Intent(AudioRecordActivity.this,MainActivity.class);
        startActivity(intent);
        finish();

    }
}
