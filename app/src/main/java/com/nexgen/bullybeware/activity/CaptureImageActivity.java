package com.nexgen.bullybeware.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.hardware.Camera;
import android.media.ExifInterface;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.nexgen.bullybeware.R;
import com.nexgen.bullybeware.util.BullyPreferences;
import com.nexgen.bullybeware.util.BullyToast;
import com.nexgen.bullybeware.util.CameraView;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ExecutionException;

/**
 * Created by quepplin1 on 9/2/2016.
 */
public class CaptureImageActivity extends BaseActivity {

    private Camera mCamera;
    private CameraView cameraView;
    private String for_val;
    Bitmap realImage;
    private FrameLayout fm_layout;
    private static final long DOUBLE_CLICK_TIME_DELTA = 2000;//milliseconds
    File pictureFile;
    private BullyPreferences preferences;


    long lastClickTime = 0;
    private MediaPlayer mPlayer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_captureimage);

        setDrawerCrossIcon("Take a Picture");

        BullyToast.showToast(CaptureImageActivity.this,getResources().getString(R.string.emergency_alaram));

        preferences = new BullyPreferences(this);

        ImageView captureButton = (ImageView) findViewById(R.id.button_capture);
        mCamera = getCameraInstance();
        cameraView = new CameraView(this, mCamera);

        for_val = getIntent().getStringExtra("for");

        FrameLayout preview = (FrameLayout) findViewById(R.id.camera_preview);
        preview.addView(cameraView);
      //  preview.addView(captureButton);

        captureButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               /* try {

                } catch (Exception e){
                    e.printStackTrace();
                }*/

                Timer timer = new Timer();
                timer.schedule(new TimerTask()
                {
                    @Override
                    public void run()
                    {
//                        mCamera.startPreview();
                        if (mCamera != null)
                         mCamera.takePicture(null, null, mPicture);
                    }
                }, 0, 5000);

              /*  final Handler h = new Handler();
                h.post(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            mCamera.takePicture(null, null, mPicture);
                            h.postDelayed(this, 5000);
                        } catch (Exception e){
                            e.printStackTrace();
                        }

                    }
                });*/
            }
        });

        fm_layout = (FrameLayout) findViewById(R.id.camera_preview);


        fm_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                long clickTime = System.currentTimeMillis();
                if (clickTime - lastClickTime < DOUBLE_CLICK_TIME_DELTA){
                    try {
                        playStopClickListner();
                    } catch (Exception e){
                        e.printStackTrace();
                    }
                    lastClickTime = 0;
                } else {
                    lastClickTime = clickTime;
                }
            }
        });
    }


    @Override
    protected void onResume() {
        super.onResume();

    }

    private Camera getCameraInstance() {
        Camera camera = null;
        try {
            camera = Camera.open();

            //Parameters object to get the parameters of the camera
            Camera.Parameters params = mCamera.getParameters();

            List<Camera.Size> sizes = params.getSupportedPictureSizes();
            Camera.Size size = sizes.get(0);
            for(int i=0;i<sizes.size();i++)
            {
                if(sizes.get(i).width > size.width)
                    size = sizes.get(i);
            }
            params.setPictureSize(size.width, size.height);
            mCamera.setParameters(params);

        } catch (Exception e) {
            // cannot get camera or does not exist
        }
        return camera;
    }

    Camera.PictureCallback mPicture = new Camera.PictureCallback() {
        @Override
        public void onPictureTaken(byte[] data, Camera camera) {
            pictureFile = getOutputMediaFile();
            if (pictureFile == null) {
                return;
            }
            try {
                FileOutputStream fos = new FileOutputStream(pictureFile);
                //             fos.write(data);
                realImage = BitmapFactory.decodeByteArray(data, 0, data.length);
                ExifInterface exif=new ExifInterface(pictureFile.toString());
                Log.d("EXIF value", exif.getAttribute(ExifInterface.TAG_ORIENTATION));
                if(exif.getAttribute(ExifInterface.TAG_ORIENTATION).equalsIgnoreCase("6")){
                    realImage= rotate(realImage, 90);
                } else if(exif.getAttribute(ExifInterface.TAG_ORIENTATION).equalsIgnoreCase("8")){
                    realImage= rotate(realImage, 270);
                } else if(exif.getAttribute(ExifInterface.TAG_ORIENTATION).equalsIgnoreCase("3")){
                    realImage= rotate(realImage, 180);
                } else if(exif.getAttribute(ExifInterface.TAG_ORIENTATION).equalsIgnoreCase("0")){
                    realImage= rotate(realImage, 90);
                }
                boolean bo = realImage.compress(Bitmap.CompressFormat.JPEG, 100, fos);
                fos.close();

                Log.d("Info", bo + "");

              /*  if (for_val == null) {
                    Intent intent = new Intent(CaptureImageActivity.this, SavedImageActivity.class);
                    intent.putExtra("image_file", String.valueOf((pictureFile)));
                    startActivity(intent);
                } else {
                    Intent returnIntent = new Intent();
                    returnIntent.putExtra("image_file", String.valueOf((pictureFile)));
                    setResult(Activity.RESULT_OK, returnIntent);
                    finish();
                }*/

                Toast.makeText(CaptureImageActivity.this,"image clicked",Toast.LENGTH_LONG).show();

         //     releaseCamera();
                mCamera.startPreview();

            } catch (FileNotFoundException e) {

            } catch (IOException e) {
            }
        }
    };
    public static Bitmap rotate(Bitmap bitmap, int degree) {
        int w = bitmap.getWidth();
        int h = bitmap.getHeight();

        Matrix mtx = new Matrix();
        //       mtx.postRotate(degree);
        mtx.setRotate(degree);

        return Bitmap.createBitmap(bitmap, 0, 0, w, h, mtx, true);
    }


    private static File getOutputMediaFile() {
       /* File mediaStorageDir = new File(
                Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                "MyCameraApp");*/
        String filepath = Environment.getExternalStorageDirectory().getPath();
        File file = new File(filepath,"/BullyApp/Image");
        if (!file.exists()) {
            if (!file.mkdirs()) {
                Log.d("MyCameraApp", "failed to create directory");
                return null;
            }
        }
        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss")
                .format(new Date());
        File mediaFile;
        mediaFile = new File(file.getPath() + File.separator
                + "Bully_" + timeStamp + ".jpg");

        return mediaFile;
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(mCamera != null) {
            //        releaseCamera();
            cameraView.getHolder().removeCallback(cameraView);
            //mCamera.stopPreview();
            //          mCameraView = null;
        }
    }

    private void releaseCamera() {
        if (mCamera != null) {

            mCamera.release();
            mCamera = null;
        }
    }

    @Override
    protected void onStop() {
        super.onStop();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        releaseCamera();

        if (for_val == null){
            Intent intent = new Intent(CaptureImageActivity.this,MainActivity.class);
            startActivity(intent);
            finish();
        } else {
            Intent returnIntent = new Intent();
            returnIntent.putExtra("image_file", String.valueOf((pictureFile)));
            setResult(Activity.RESULT_OK, returnIntent);
            finish();
        }
    }
}
