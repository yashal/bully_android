package com.nexgen.bullybeware.activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nexgen.bullybeware.R;
import com.nexgen.bullybeware.adapter.FriendListAdapter;
import com.nexgen.bullybeware.adapter.SelfHarmAdapter;
import com.nexgen.bullybeware.model.RestClient;
import com.nexgen.bullybeware.pojo.DeleteSelfharmPojo;
import com.nexgen.bullybeware.pojo.FriendListObjectPojo;
import com.nexgen.bullybeware.pojo.SelfHarmListPojo;
import com.nexgen.bullybeware.pojo.SelfHarmObjectPojo;
import com.nexgen.bullybeware.util.BullyConstants;
import com.nexgen.bullybeware.util.BullyDialogs;
import com.nexgen.bullybeware.util.BullyPreferences;
import com.nexgen.bullybeware.util.BullyToast;
import com.nexgen.bullybeware.util.ConnectionDetector;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by umax desktop on 2/16/2017.
 */

public class SelfHarmListActivity extends BaseActivity implements View.OnClickListener {

    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private SelfHarmAdapter adapter;
    private ArrayList<SelfHarmObjectPojo>selfharmlist;
    private LinearLayout ll_noDatafound;
    private BullyPreferences preferences;
    private LinearLayout ll_selfharm;
    private static final long DOUBLE_CLICK_TIME_DELTA = 2000;//milliseconds
    long lastClickTime = 0;
    private LinearLayout ll_main_selfharm;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selfharm_list);

        setDrawerbackIcon("Self Harm List");

        init();
        setOnClick();

    }
    private void init(){
        ll_noDatafound = (LinearLayout) findViewById(R.id.selfharmList_ll_NoData);
     //   ll_noDatafound.setVisibility(View.VISIBLE);

        ll_selfharm = (LinearLayout) findViewById(R.id.ll_selfharm);
        ll_selfharm.setVisibility(View.VISIBLE);

        ll_main_selfharm = (LinearLayout) findViewById(R.id.ll_main_selfharm);

        preferences = new BullyPreferences(SelfHarmListActivity.this);

        BullyToast.showToast(SelfHarmListActivity.this, getResources().getString(R.string.emergency_alaram));

        cd = new ConnectionDetector(getApplicationContext());
        // get Internet status
        isInternetPresent = cd.isConnectingToInternet();

        selfharmlist = new ArrayList<SelfHarmObjectPojo>();

        mRecyclerView = (RecyclerView) findViewById(R.id.selfharmlist_recycler_view);

        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(SelfHarmListActivity.this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        getSelfHarmList();
    }

    private void setOnClick(){
        ll_selfharm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SelfharmDialog();
            }
        });

        ll_main_selfharm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                long clickTime = System.currentTimeMillis();
                if (clickTime - lastClickTime < DOUBLE_CLICK_TIME_DELTA) {
                    try {
                        playStopClickListner();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    lastClickTime = 0;
                } else {
                    lastClickTime = clickTime;
                }
            }
        });
    }

    private void getSelfHarmList(){
        if (isInternetPresent){
            final ProgressDialog d = BullyDialogs.showLoading(SelfHarmListActivity.this);
            d.setCanceledOnTouchOutside(false);

            RestClient.get().getSelfharmList(preferences.getStudentId(), new Callback<SelfHarmListPojo>() {
                @Override
                public void success(SelfHarmListPojo selfHarmListPojo, Response response) {

                    if (selfHarmListPojo != null){
                        if (selfHarmListPojo.getStatus()){

                            selfharmlist = selfHarmListPojo.getObject();

                            ll_noDatafound.setVisibility(View.GONE);

                            adapter = new SelfHarmAdapter(SelfHarmListActivity.this,selfharmlist);
                            mRecyclerView.setAdapter(adapter);
                            adapter.notifyDataSetChanged();
                        } else {

                           ll_noDatafound.setVisibility(View.VISIBLE);

                        }
                    }
                    else
                    {
                        d.dismiss();
                        BullyToast.showToast(SelfHarmListActivity.this, BullyConstants.SERVER_NOT_RESPOND);
                    }
                    d.dismiss();
                }

                @Override
                public void failure(RetrofitError error) {
                       d.dismiss();
                }
            });
        } else {
            ll_noDatafound.setVisibility(View.VISIBLE);
        }
    }

    private void SelfharmDialog() {
        dialogLogout = new Dialog(SelfHarmListActivity.this, android.R.style.Theme_Translucent_NoTitleBar);
        dialogLogout.setContentView(R.layout.logout_dialog);
        dialogLogout.show();


        TextView dialog_header = (TextView) dialogLogout.findViewById(R.id.logout_dialog_header);
        dialog_header.setVisibility(View.VISIBLE);
        dialog_header.setText("Self Harm");
        LinearLayout ll_submit = (LinearLayout) dialogLogout.findViewById(R.id.ll_dialog_submit);
        LinearLayout ll_cancel = (LinearLayout) dialogLogout.findViewById(R.id.ll_dialog_cancel);
        ImageView iView_cross = (ImageView) dialogLogout.findViewById(R.id.dialog_header_cross);
        TextView tv_submit = (TextView) dialogLogout.findViewById(R.id.dialog_submit);
        tv_submit.setText("YES");
        TextView dialog_text = (TextView) dialogLogout.findViewById(R.id.dialog_text);
        dialog_text.setVisibility(View.VISIBLE);
        dialog_text.setText("Only use this button to confidentially tell the Principal that you believe your friend may hurt themselves. Your name will be submitted with this report.");
        
        ll_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SelfHarmListActivity.this, SelfHarmActivity.class);
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);

            }
        });
        iView_cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogLogout.dismiss();
            }
        });

        ll_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogLogout.dismiss();
            }
        });
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();


        finish();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.selfharm_delete:

            String selfHarm_id = (String) view.getTag(R.string.self_harm_id);

                int position = (int) view.getTag(R.string.self_harm_position);

                DeleteSelfHarmdialog(selfHarm_id,position);
                break;

            case R.id.selfharm:
                long clickTime = System.currentTimeMillis();
                if (clickTime - lastClickTime < DOUBLE_CLICK_TIME_DELTA) {
                    try {
                        playStopClickListner();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    lastClickTime = 0;
                } else {
                    lastClickTime = clickTime;
                }
            }

    }

    public void DeleteSelfHarmdialog(final String self_ham_id, final int position) {
        dialogLogout = new Dialog(SelfHarmListActivity.this, android.R.style.Theme_Translucent_NoTitleBar);
        dialogLogout.setContentView(R.layout.logout_dialog);
        dialogLogout.show();

        TextView dialog_header = (TextView) dialogLogout.findViewById(R.id.logout_dialog_header);
        dialog_header.setVisibility(View.VISIBLE);
        dialog_header.setText("Self Harm List");
        LinearLayout ll_submit = (LinearLayout) dialogLogout.findViewById(R.id.ll_dialog_submit);
        LinearLayout ll_cancel = (LinearLayout) dialogLogout.findViewById(R.id.ll_dialog_cancel);
        ImageView iView_cross = (ImageView) dialogLogout.findViewById(R.id.dialog_header_cross);
        TextView tv_submit = (TextView) dialogLogout.findViewById(R.id.dialog_submit);
        tv_submit.setText("YES");
        TextView tv_cancel = (TextView) dialogLogout.findViewById(R.id.dialog_cancel);
        TextView dialog_text = (TextView) dialogLogout.findViewById(R.id.dialog_text);
        dialog_text.setVisibility(View.VISIBLE);
        dialog_text.setText("Are you sure you want to delete?");


        ll_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                deletefile(self_ham_id,position);

                dialogLogout.dismiss();
            }
        });
        iView_cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogLogout.dismiss();
            }
        });

        ll_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogLogout.dismiss();
            }
        });
    }

    private void deletefile(String selhamr_id, final int pos){
        final ProgressDialog d = BullyDialogs.showLoading(SelfHarmListActivity.this);
        d.setCanceledOnTouchOutside(false);

        RestClient.get().deleteSelfHarm(selhamr_id, new Callback<DeleteSelfharmPojo>() {
            @Override
            public void success(DeleteSelfharmPojo deleteSelfharmPojo, Response response) {
                if (deleteSelfharmPojo != null){
                    if (deleteSelfharmPojo.getStatus()){

                        selfharmlist.remove(pos);

                        adapter.notifyDataSetChanged();

                        BullyToast.showToast(SelfHarmListActivity.this,deleteSelfharmPojo.getMessage());

                    } else {

                    }

                    d.dismiss();
                }
                else
                {
                    d.dismiss();
                    BullyToast.showToast(SelfHarmListActivity.this, BullyConstants.SERVER_NOT_RESPOND);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                d.dismiss();
            }
        });
    }

}
