package com.nexgen.bullybeware.activity;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Dialog;
import android.bluetooth.le.ScanRecord;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.media.ExifInterface;
import android.media.Image;
import android.media.MediaPlayer;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nexgen.bullybeware.R;
import com.nexgen.bullybeware.adapter.CustomImageAdapter;
import com.nexgen.bullybeware.adapter.GridAdapter;
import com.nexgen.bullybeware.adapter.SavedImageAdapter;
import com.nexgen.bullybeware.pojo.CheckboxStatusPojo;
import com.nexgen.bullybeware.util.BullyPreferences;
import com.nexgen.bullybeware.util.BullyToast;
import com.nexgen.bullybeware.util.ImageLoadingUtils;
import com.nexgen.bullybeware.util.ScaleBitmap;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by quepplin1 on 9/2/2016.
 */
public class SavedImageActivity extends BaseActivity {

    private GridView gridview;
    private String image_file;
    private GridAdapter adapter;
    private ArrayList<String> imagelist = new ArrayList<String>();
    private ArrayList<String> checkedimagelist = new ArrayList<String>();
    File[] listFile;

    private String imageUri;
    private String photoPath = "";

    private TextView tv_reportIncident;
    private ArrayList<CheckboxStatusPojo> imagestatus = new ArrayList<CheckboxStatusPojo>();
    private CheckboxStatusPojo pojo;
    private final int SELECT_PHOTO = 1;
    private LinearLayout ll_image_main;

    private static final long DOUBLE_CLICK_TIME_DELTA = 2000;//milliseconds

    long lastClickTime = 0;
    private MediaPlayer mPlayer;

    private String mCurrentPhotoPath;
    private final int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 456;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_saved_image);

        setDrawerbackIcon("Preview");

        BullyToast.showToast(SavedImageActivity.this, getResources().getString(R.string.emergency_alaram));

        imageUri = "android.resource://com.nexgen.bullybeware/" + R.mipmap.take_pic;
        init();
        setOnClickListner();
        getFromSdcard();
        gridView();

    }

    private void init() {
        gridview = (GridView) findViewById(R.id.gridview);
        tv_reportIncident = (TextView) findViewById(R.id.tv_report_incident);

        ll_image_main = (LinearLayout) findViewById(R.id.ll_image_main);

        image_file = getIntent().getStringExtra("image_file");
        CheckboxStatusPojo statusPojo = new CheckboxStatusPojo(true, image_file);
        CheckboxStatusPojo statusPojo1 = new CheckboxStatusPojo(false, imageUri);
        imagestatus.add(statusPojo);
        imagestatus.add(statusPojo1);

        imagelist.add(image_file);
        imagelist.add(imageUri);

    }

    private void setOnClickListner() {
        tv_reportIncident.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkedimagelist.clear();
                for (int i = 0; i < imagestatus.size(); i++) {
                    Boolean status = imagestatus.get(i).getStatus();

                    if (status)
                        checkedimagelist.add(imagestatus.get(i).getPath());
                }

                Intent intent = new Intent(SavedImageActivity.this, ReportIncidentActivity.class);
                Bundle args = new Bundle();
                args.putSerializable("ARRAYLIST", (Serializable) checkedimagelist);
                intent.putExtra("BUNDLE", args);
                startActivity(intent);
            }
        });
        ll_image_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("hh click");
                long clickTime = System.currentTimeMillis();
                if (clickTime - lastClickTime < DOUBLE_CLICK_TIME_DELTA) {
                    try {
                        playStopClickListner();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    lastClickTime = 0;
                } else {
                    lastClickTime = clickTime;
                }
            }
        });

    }

    private void gridView() {
        adapter = new GridAdapter(this, imagestatus, imageUri);
        gridview.setAdapter(adapter);
    }

    public void getFromSdcard() {
        String path = Environment.getExternalStorageDirectory() + image_file;
        File file = new File(path);

        if (file.isDirectory()) {
            listFile = file.listFiles();

            for (int i = 0; i < listFile.length; i++) {

                imagelist.add(listFile[i].getAbsolutePath());
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {

            if (requestCode == SELECT_PHOTO) {

                Uri selectedImage = data.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};

                Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                String imagePath1 = cursor.getString(columnIndex);
                System.out.println("hh image path =" + imagePath1);

                File f = new File(compressImage(imagePath1, 1));

                photoPath = f.getAbsolutePath();

                imagelist.remove(imagelist.size() - 1);
                imagestatus.remove(imagestatus.size() - 1);
                image_file = photoPath;
                CheckboxStatusPojo statusPojo = new CheckboxStatusPojo(false, image_file);
                CheckboxStatusPojo statusPojo1 = new CheckboxStatusPojo(false, imageUri);

                imagestatus.add(statusPojo);
                imagestatus.add(statusPojo1);

                imagelist.add(image_file);
                imagelist.add(imageUri);

                getFromSdcard();
                gridView();

            } else if (requestCode == CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE) {

                File f = new File(compressImage(mCurrentPhotoPath, 0));
                photoPath = f.getAbsolutePath();

                imagelist.remove(imagelist.size() - 1);
                imagestatus.remove(imagestatus.size() - 1);
                image_file = photoPath;
                CheckboxStatusPojo statusPojo = new CheckboxStatusPojo(false, image_file);
                CheckboxStatusPojo statusPojo1 = new CheckboxStatusPojo(false, imageUri);

                imagestatus.add(statusPojo);
                imagestatus.add(statusPojo1);

                imagelist.add(image_file);
                imagelist.add(imageUri);

                getFromSdcard();
                gridView();
            }
        }
        if (resultCode == Activity.RESULT_CANCELED) {
            //Write your code if there's no result
        }
    }

    public void Photogallery() {
        Intent i = new Intent(
                Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, SELECT_PHOTO);
    }

    private void Photocamera() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        try {

            String filepath = Environment.getExternalStorageDirectory().getPath();
            File file = new File(filepath, "/BullyApp/Image");
            if (!file.exists()) {
                if (!file.mkdirs()) {
                    Log.d("MyCameraApp", "failed to create directory");

                }
            }
            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss")
                    .format(new Date());
            File mediaFile;
            mediaFile = new File(file.getPath() + File.separator
                    + "Bully_" + timeStamp + ".jpg");

            mCurrentPhotoPath = mediaFile.getAbsolutePath();
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(mediaFile));
        } catch (Exception e) {
            e.printStackTrace();
        }

        startActivityForResult(takePictureIntent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
    }

    public String compressImage(String imageUri, int flag) {
        String filePath;
        if (flag == 1) {
            filePath = getRealPathFromURI(imageUri);
        }

        filePath = imageUri;
        Bitmap scaledBitmap = null;

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        Bitmap bmp = BitmapFactory.decodeFile(filePath, options);

        int actualHeight = options.outHeight;
        int actualWidth = options.outWidth;
        float maxHeight = 816.0f;
        float maxWidth = 612.0f;
        float imgRatio = actualWidth / actualHeight;
        float maxRatio = maxWidth / maxHeight;

        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            if (imgRatio < maxRatio) {
                imgRatio = maxHeight / actualHeight;
                actualWidth = (int) (imgRatio * actualWidth);
                actualHeight = (int) maxHeight;
            } else if (imgRatio > maxRatio) {
                imgRatio = maxWidth / actualWidth;
                actualHeight = (int) (imgRatio * actualHeight);
                actualWidth = (int) maxWidth;
            } else {
                actualHeight = (int) maxHeight;
                actualWidth = (int) maxWidth;

            }
        }

        ImageLoadingUtils utils = new ImageLoadingUtils(this);
        options.inSampleSize = utils.calculateInSampleSize(options, actualWidth, actualHeight);
        options.inJustDecodeBounds = false;
        options.inDither = false;
        options.inTempStorage = new byte[16 * 1024];

        try {
            bmp = BitmapFactory.decodeFile(filePath, options);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        }
        try {
            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        }
        float ratioX = actualWidth / (float) options.outWidth;
        float ratioY = actualHeight / (float) options.outHeight;
        float middleX = actualWidth / 2.0f;
        float middleY = actualHeight / 2.0f;

        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2, middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));
        ExifInterface exif;
        try {
            exif = new ExifInterface(filePath);
            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 0);
            Log.d("EXIF", "Exif: " + orientation);
            Matrix matrix = new Matrix();
            if (orientation == 6) {
                matrix.postRotate(90);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 3) {
                matrix.postRotate(180);
                Log.d("EXIF", "Exif: " + orientation);
            } else if  (orientation == 8) {
                matrix.postRotate(270);
                Log.d("EXIF", "Exif: " + orientation);
            }
            scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix, true);
        } catch (IOException e) {
            e.printStackTrace();
        }
        FileOutputStream out = null;
        String filename = getFilename();
        try {
            out = new FileOutputStream(filename);
            scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 80, out);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return filename;
    }

    public void Camera() {
        dialogLogout = new Dialog(SavedImageActivity.this, android.R.style.Theme_Translucent_NoTitleBar);
        dialogLogout.setContentView(R.layout.camera_dialog);
        dialogLogout.show();

        LinearLayout ll_dialog = (LinearLayout) dialogLogout.findViewById(R.id.ll_dialog);
        ll_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogLogout.dismiss();
            }
        });

        TextView tv_camera = (TextView) dialogLogout.findViewById(R.id.tv_camera);
        TextView tv_gallety = (TextView) dialogLogout.findViewById(R.id.tv_gallery);

        tv_camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Photocamera();

                dialogLogout.dismiss();
            }
        });

        tv_gallety.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Photogallery();

                dialogLogout.dismiss();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        /*Intent intent = new Intent(SavedImageActivity.this, MainActivity.class);
        startActivity(intent);*/
        finish();
    }
}
