package com.nexgen.bullybeware.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.nexgen.bullybeware.R;
import com.nexgen.bullybeware.adapter.SafeListAdapter;
import com.nexgen.bullybeware.adapter.SearchResultAdapter;
import com.nexgen.bullybeware.model.RestClient;
import com.nexgen.bullybeware.pojo.BasePojo;
import com.nexgen.bullybeware.pojo.SafeListPojo;
import com.nexgen.bullybeware.pojo.SearchFriendListPojo;
import com.nexgen.bullybeware.util.BullyConstants;
import com.nexgen.bullybeware.util.BullyDialogs;
import com.nexgen.bullybeware.util.BullyPreferences;
import com.nexgen.bullybeware.util.BullyToast;
import com.nexgen.bullybeware.util.ConnectionDetector;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class SearchByNameActivity extends BaseActivity implements View.OnClickListener {

    private SearchByNameActivity ctx = this;
    private EditText searchEdit;
    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerView.Adapter mAdapter;
    private LinearLayout no_data_ll;
    private ConnectionDetector cd;
    private BullyPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_by_name);
        setDrawerbackIcon("Search Friend by Name");
        preferences = new BullyPreferences(ctx);
        cd = new ConnectionDetector(getApplicationContext());
        searchEdit = (EditText)findViewById(R.id.search_by_name);
        mRecyclerView = (RecyclerView)findViewById(R.id.friend_list);
        no_data_ll = (LinearLayout) findViewById(R.id.no_data_ll);

        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(ctx);
        mRecyclerView.setLayoutManager(mLayoutManager);

        searchEdit.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    if (searchEdit.getText().toString().length() > 0) {
                        searchFrienByName(searchEdit.getText().toString());
                    }
                    else if(searchEdit.getText().toString().length()==0)
                    {
                        Toast.makeText(ctx, "Search field should not be empty", Toast.LENGTH_SHORT).show();
                    }
                    return true;
                }
                return false;
            }
        });

    }

    private void searchFrienByName(String studentName)
    {
        if (cd.isConnectingToInternet()) {
            final ProgressDialog d = BullyDialogs.showLoading(ctx);
            d.setCanceledOnTouchOutside(false);
            RestClient.get().searchFriendByName(studentName,preferences.getStudentId(), new Callback<SearchFriendListPojo>() {
                @Override
                public void success(SearchFriendListPojo basePojo, Response response) {

                    if (basePojo != null) {
                        if (basePojo.getStatus()) {
                            if (basePojo.getObject() != null) {
                                if (basePojo.getObject().size() > 0) {
                                    mAdapter = new SearchResultAdapter(ctx, basePojo.getObject());
                                    mRecyclerView.setAdapter(mAdapter);
                                    no_data_ll.setVisibility(View.GONE);
                                } else {
                                    no_data_ll.setVisibility(View.VISIBLE);
                                }
                            } else {
                                no_data_ll.setVisibility(View.VISIBLE);
                            }

                        }
                        else
                        {
                            if (basePojo.getMessage()!= null)
                            {
                                customAlertDialog(getResources().getString(R.string.safe_friend_list), basePojo.getMessage());

                            }
                        }
                    } else {
                        BullyToast.showToast(ctx, BullyConstants.SERVER_NOT_RESPOND);
                    }
                    d.dismiss();
                }

                @Override
                public void failure(RetrofitError error) {
                    d.dismiss();
                }
            });
        } else {
            customAlertDialog("BullyBeware", BullyConstants.NO_INTERNET_CONNECTED);
        }
    }

    @Override
    public void onClick(View v) {

        String first_name =(String) v.getTag(R.string.key_state);
        String last_name =(String) v.getTag(R.string.key);
        String phone_number =(String) v.getTag(R.string.key_delete);
        String email_address =(String) v.getTag(R.string.delete_position);
        String thumbnail =(String) v.getTag(R.string.edit_position);
        String friend_id =(String) v.getTag(R.string.dial_number);

        System.out.println("hh yashal first_name " + first_name + " last_name " + last_name + " phone_number " + phone_number + " email_address " + email_address + " thumbnail " + thumbnail + " friend_id " + friend_id);

        Intent intent = new Intent(ctx,AddfriendInfoActivity.class);
        intent.putExtra("friend_id",friend_id);
        intent.putExtra("first_name",first_name);
        intent.putExtra("last_name",last_name);
        intent.putExtra("email",email_address);
        intent.putExtra("mobile",phone_number);
        intent.putExtra("thumbnail",thumbnail);
        intent.putExtra("from","SearchFriend");
        startActivity(intent);
        finish();

    }

    @Override
    public void onBackPressed() {

        Intent intent = new Intent(ctx, FriendListActivity.class);
        intent.putExtra("from","main");
        startActivity(intent);
        finish();
        overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
    }
}
