package com.nexgen.bullybeware.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.nexgen.bullybeware.R;
import com.nexgen.bullybeware.adapter.SubmissionPagerAdapter;
import com.nexgen.bullybeware.pojo.GetSubmissionFilesPojo;
import com.nexgen.bullybeware.util.BullyToast;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by quepplin1 on 9/30/2016.
 */
public class MySubmissionDetaileActivity extends BaseActivity {


    private TextView tv_incident_type, tv_title, tv_date, tv_victim_name, tv_nearest_adult, tv_description, tv_bullyName;
    private String type, subject, bully_name, date, victim_name, nearest_adult, description, incident;
    private ViewPager mViewPager;
    private SubmissionPagerAdapter adapter;
    ArrayList<GetSubmissionFilesPojo> object;
    private String file;

    private LinearLayout ll_description, ll_nearest_adult, ll_no_data, ll_title, ll_bully_name;

    private FrameLayout fl_record_audio;
    private ImageView iv_incident;
    private LinearLayout detaile_main_ll;
    private static final long DOUBLE_CLICK_TIME_DELTA = 2000; //milliseconds

    long lastClickTime = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_submission);

        setDrawerbackIcon("My Submission");

        init();
        setData();

    }

    private void init() {
        tv_incident_type = (TextView) findViewById(R.id.submission_incident_type);
        tv_title = (TextView) findViewById(R.id.submission_title);
        tv_date = (TextView) findViewById(R.id.submission_date);
        tv_bullyName = (TextView) findViewById(R.id.submission_bullyname);
        tv_victim_name = (TextView) findViewById(R.id.submisiion_victim_name);
        tv_nearest_adult = (TextView) findViewById(R.id.submission_nearest_adult);
        tv_description = (TextView) findViewById(R.id.submission_description);

        ll_nearest_adult = (LinearLayout) findViewById(R.id.ll_nearest_adult);
        ll_description = (LinearLayout) findViewById(R.id.ll_description);
        ll_no_data = (LinearLayout) findViewById(R.id.ll_no_data);
        ll_title = (LinearLayout) findViewById(R.id.ll_title);
        ll_bully_name = (LinearLayout) findViewById(R.id.ll_bullyName);

        fl_record_audio = (FrameLayout) findViewById(R.id.fl_record_audio);
        iv_incident = (ImageView) findViewById(R.id.incident_photo);

        mViewPager = (ViewPager) findViewById(R.id.submission_pager);
        type = getIntent().getStringExtra("type");
        incident = getIntent().getStringExtra("incident");
        subject = getIntent().getStringExtra("title");
        bully_name = getIntent().getStringExtra("bully_name");
        date = getIntent().getStringExtra("date");
        victim_name = getIntent().getStringExtra("Victim_name");
        nearest_adult = getIntent().getStringExtra("nearest_adult");
        description = getIntent().getStringExtra("description");

        BullyToast.showToast(MySubmissionDetaileActivity.this, getResources().getString(R.string.emergency_alaram));

        detaile_main_ll = (LinearLayout) findViewById(R.id.detaile_main_ll);

        Intent intent = getIntent();
        Bundle args = intent.getBundleExtra("BUNDLE");
        try {
            if ((ArrayList<GetSubmissionFilesPojo>) args.getSerializable("file") != null) {
                object = (ArrayList<GetSubmissionFilesPojo>) args.getSerializable("file");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if ((object != null)) {
            ll_no_data.setVisibility(View.GONE);
            mViewPager.setVisibility(View.VISIBLE);

            adapter = new SubmissionPagerAdapter(this, object, type);
            mViewPager.setAdapter(adapter);
            adapter.notifyDataSetChanged();

            mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                    adapter.onPageChanged();
                }

                @Override
                public void onPageSelected(int position) {

                }

                @Override
                public void onPageScrollStateChanged(int state) {

                }
            });

        } else {
            file = getIntent().getStringExtra("file");
            mViewPager.setVisibility(View.GONE);
            ll_no_data.setVisibility(View.VISIBLE);

            if (type.equalsIgnoreCase("2")) {
                iv_incident.setVisibility(View.GONE);
                fl_record_audio.setVisibility(View.VISIBLE);
            } else if (type.equalsIgnoreCase("4")) {
                iv_incident.setVisibility(View.GONE);
                fl_record_audio.setVisibility(View.GONE);
            } else {
                fl_record_audio.setVisibility(View.GONE);
                iv_incident.setVisibility(View.VISIBLE);
            }
        }

        if (nearest_adult.length() > 0) {
            ll_nearest_adult.setVisibility(View.VISIBLE);
        } else {
            ll_nearest_adult.setVisibility(View.GONE);
        }
        if (description.length() > 0) {
            ll_description.setVisibility(View.VISIBLE);
        } else {
            ll_description.setVisibility(View.GONE);
        }
        if (subject.length() > 0) {
            ll_title.setVisibility(View.VISIBLE);
        } else {
            ll_title.setVisibility(View.GONE);
        }
        if (bully_name.length() > 0) {
            ll_bully_name.setVisibility(View.VISIBLE);
        } else {
            ll_bully_name.setVisibility(View.GONE);
        }

        detaile_main_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                long clickTime = System.currentTimeMillis();
                if (clickTime - lastClickTime < DOUBLE_CLICK_TIME_DELTA) {
                    try {
                        playStopClickListner();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    lastClickTime = 0;
                } else {
                    lastClickTime = clickTime;
                }
            }
        });
    }

    private void setData() {

        tv_incident_type.setText(incident);
        tv_title.setText(subject);
        tv_bullyName.setText(bully_name);
        tv_victim_name.setText(victim_name);
        tv_date.setText(date);
        tv_nearest_adult.setText(nearest_adult);
        tv_description.setText(description);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (adapter != null) {
            adapter.onPageChanged();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        if (adapter != null) {
            adapter.onPageChanged();
        }
    }
}
