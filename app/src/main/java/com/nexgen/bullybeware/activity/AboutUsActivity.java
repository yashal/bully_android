package com.nexgen.bullybeware.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nexgen.bullybeware.R;
import com.nexgen.bullybeware.util.BullyConstants;
import com.nexgen.bullybeware.util.BullyDialogs;
import com.nexgen.bullybeware.util.BullyPreferences;
import com.nexgen.bullybeware.util.BullyToast;
import com.nexgen.bullybeware.util.ConnectionDetector;

/**
 * Created by quepplin1 on 9/14/2016.
 */
public class AboutUsActivity extends BaseActivity {

    private WebView webview;
    private AboutUsActivity ctx = this;
    private static final long DOUBLE_CLICK_TIME_DELTA = 2000;//milliseconds

    long lastClickTime = 0;
    private MediaPlayer mPlayer;
    private BullyPreferences preferences;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aboutus);

        setDrawerbackIcon("About Us");

        webview = (WebView) findViewById(R.id.webview_about);

        final ProgressDialog alertDialog = BullyDialogs.showLoading(ctx);

        webview.setOnTouchListener(new View.OnTouchListener() {

            public boolean onTouch(View v, MotionEvent event) {
                WebView.HitTestResult hr = ((WebView) v).getHitTestResult();

                long clickTime = System.currentTimeMillis();
                if (clickTime - lastClickTime < DOUBLE_CLICK_TIME_DELTA) {
                    try {

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    lastClickTime = 0;
                } else {
                    lastClickTime = clickTime;
                }
                return false;
            }
        });


      /*  webview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("hh click");

            }
        });*/

        webview.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                if (progress >= 85) {
                    if (alertDialog != null && alertDialog.isShowing())
                        alertDialog.dismiss();
                }
            }
        });

        webview.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (url.toLowerCase().startsWith("http") || url.toLowerCase().startsWith("https") || url.toLowerCase().startsWith("file")) {
                    view.loadUrl(url);
                } else {
                    try {
                        Uri uri = Uri.parse(url);
                        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                        startActivity(intent);
                    } catch (Exception e) {
                        Log.d("JSLogs", "Webview Error:" + e.getMessage());
                    }
                }
                return (true);
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
            }
        });

        ConnectionDetector cd = new ConnectionDetector(getApplicationContext());
        if (cd.isConnectingToInternet()) {
            openURL("http://139.162.60.231/bully/about_us_mobile.php");
        } else {
            if (alertDialog != null && alertDialog.isShowing())
                alertDialog.dismiss();
            customAlertDialog("BullyBeware",BullyConstants.NO_INTERNET_CONNECTED);
        }
    }


    private void openURL(String url) {
        webview.loadUrl(url);
        webview.requestFocus();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        finish();
    }
}
