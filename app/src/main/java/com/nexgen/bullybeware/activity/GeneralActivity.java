package com.nexgen.bullybeware.activity;

import android.content.Intent;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.nexgen.bullybeware.R;
import com.nexgen.bullybeware.util.BullyToast;

/**
 * Created by umax desktop on 2/15/2017.
 */

public class GeneralActivity extends BaseActivity {

    private ImageView iView_contactUs,iView_aboutUs,iView_rateUs,iView_terms;
    private LinearLayout ll_general;
    private static final long DOUBLE_CLICK_TIME_DELTA = 2000;//milliseconds
    long lastClickTime = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_general);

        setDrawerbackIcon("General");

        init();
        setOnClick();
    }

    private void init(){
        iView_contactUs = (ImageView) findViewById(R.id.iv_contactUs);
        iView_aboutUs = (ImageView) findViewById(R.id.iv_aboutUs);
        iView_rateUs = (ImageView) findViewById(R.id.iv_rateUs);
        iView_terms = (ImageView) findViewById(R.id.iv_terms);

        ll_general = (LinearLayout) findViewById(R.id.ll_general);

        BullyToast.showToast(GeneralActivity.this, getResources().getString(R.string.emergency_alaram));
    }

    private void setOnClick(){
        iView_contactUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent favIntent = new Intent(GeneralActivity.this,ContactUsActivity.class);
                startActivity(favIntent);
            }
        });

        iView_aboutUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent aboutIntent = new Intent(GeneralActivity.this,AboutUsActivity.class);
                startActivity(aboutIntent);
            }
        });

        iView_rateUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String appPackageName = getPackageName();

                System.out.println("package"+ appPackageName);
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                }
            }
        });

        iView_terms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent ftermsIntent = new Intent(GeneralActivity.this,TermsConditionsActivity.class);
                startActivity(ftermsIntent);
            }
        });

        ll_general.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                long clickTime = System.currentTimeMillis();
                if (clickTime - lastClickTime < DOUBLE_CLICK_TIME_DELTA) {
                    try {
                        playStopClickListner();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    lastClickTime = 0;
                } else {
                    lastClickTime = clickTime;
                }
            }
        });
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();

        finish();
    }
}
