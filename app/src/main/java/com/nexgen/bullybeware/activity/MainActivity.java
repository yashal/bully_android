package com.nexgen.bullybeware.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.database.Cursor;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.os.Bundle;
import android.support.v4.content.FileProvider;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.nexgen.bullybeware.BuildConfig;
import com.nexgen.bullybeware.R;
import com.nexgen.bullybeware.service.AudioUploadService;
import com.nexgen.bullybeware.service.SinchService;
import com.nexgen.bullybeware.util.BullyPreferences;
import com.nexgen.bullybeware.util.BullyToast;
import com.nexgen.bullybeware.util.FilePahUtils;
import com.sinch.android.rtc.SinchError;
import java.io.File;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;

import wseemann.media.FFmpegMediaMetadataRetriever;

public class MainActivity extends BaseActivity implements SinchService.StartFailedListener {

    private ImageView iv_recordvideo, iv_recordAudio, iv_takePicture, iv_text, iV_emergency_alarm, my_activity;
    private Dialog dialogLogout;
    private final int SELECT_PHOTO = 1;
    private final int REQUEST_TAKE_GALLERY_VIDEO = 2;
    private String photoPath = "";
    String filemanagerstring = "";
    private BullyPreferences preferences;
    private LinearLayout ll_notification;
    private String mCurrentPhotoPath;
    private final int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 456;
    private static final int VIDEO_CAPTURE = 101;
    Uri videoUri;
    boolean doubleBackToExitPressedOnce = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setDrawerAndToolbar("Bully Beware");

        init();
        setOnClickListner();
    }


    private void init() {
        iv_recordvideo = (ImageView) findViewById(R.id.iv_record_video);
        iv_recordAudio = (ImageView) findViewById(R.id.iv_record_audio);
        iv_text = (ImageView) findViewById(R.id.iv_text);

        iv_takePicture = (ImageView) findViewById(R.id.iv_take_pic);

        iV_emergency_alarm = (ImageView) findViewById(R.id.iv_emergency_alarm);

        my_activity = (ImageView) findViewById(R.id.myactivity);

        ll_notification = (LinearLayout) findViewById(R.id.ll_Notification);
        ll_notification.setVisibility(View.GONE);

        preferences = new BullyPreferences(MainActivity.this);

        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "com.nexgen.bullybeware",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                followclicked();
            }
        }, 1000);


    }

    private void setOnClickListner() {
        iv_recordvideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                VideoDialog();

            }
        });

        iv_recordAudio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startAudioRecordingSafe();
            }
        });

        iv_takePicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                CameraDialog();
            }
        });

        iV_emergency_alarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    playStopClickListner();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        my_activity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent activityIntent = new Intent(MainActivity.this, MyRecordActivity.class);
                activityIntent.putExtra("from_activity", "Main_screen");
                startActivity(activityIntent);
                overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
            }
        });

        iv_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*openFriendlistActivity();*/

                Intent activityIntent = new Intent(MainActivity.this, TextReportIncident.class);
                startActivity(activityIntent);
                overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        iV_emergency_alarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    playStopClickListner();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void CameraDialog() {
        dialogLogout = new Dialog(MainActivity.this, android.R.style.Theme_Translucent_NoTitleBar);
        dialogLogout.setContentView(R.layout.camera_dialog);
        dialogLogout.show();

        LinearLayout ll_dialog = (LinearLayout) dialogLogout.findViewById(R.id.ll_dialog);

        TextView tv_camera = (TextView) dialogLogout.findViewById(R.id.tv_camera);
        TextView tv_gallety = (TextView) dialogLogout.findViewById(R.id.tv_gallery);

        ll_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogLogout.dismiss();
            }
        });

        tv_camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                camera();

                dialogLogout.dismiss();
            }
        });

        tv_gallety.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                goAhead();

                dialogLogout.dismiss();
            }
        });
    }

    private void VideoDialog() {
        dialogLogout = new Dialog(MainActivity.this, android.R.style.Theme_Translucent_NoTitleBar);
        dialogLogout.setContentView(R.layout.customvideo_dialog);
        dialogLogout.show();

        LinearLayout ll_dialog = (LinearLayout) dialogLogout.findViewById(R.id.ll_dialog);

        TextView tv_camera = (TextView) dialogLogout.findViewById(R.id.tv_camera);
        TextView tv_gallety = (TextView) dialogLogout.findViewById(R.id.tv_gallery);

        tv_camera.setText("Video Camera");

        ll_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogLogout.dismiss();
            }
        });

        tv_camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                video();

                dialogLogout.dismiss();
            }
        });

        tv_gallety.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                goAheadVideo();

                dialogLogout.dismiss();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_PHOTO) {

                Uri selectedImage = data.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};

                Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                String imagePath1 = cursor.getString(columnIndex);
                System.out.println("hh image path =" + imagePath1);

                File f = new File(compressImage(imagePath1, 1));
                photoPath = f.getAbsolutePath();

                Intent intent = new Intent(MainActivity.this, SavedImageActivity.class);
                intent.putExtra("image_file", photoPath);
                startActivity(intent);
                overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);

            } else if (requestCode == CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE) {

                System.out.println("hh image path = " + mCurrentPhotoPath);

                File f = new File(compressImage(mCurrentPhotoPath, 0));
                photoPath = f.getAbsolutePath();

                Intent intent = new Intent(MainActivity.this, SavedImageActivity.class);
                intent.putExtra("image_file", photoPath);
                startActivity(intent);
                overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);

            } else if (requestCode == VIDEO_CAPTURE) {
                if (resultCode == RESULT_OK) {
                    photoPath = getPath1(videoUri);
                    if (photoPath != null) {

                        Intent intent = new Intent(MainActivity.this,
                                SavedVideoActivity.class);
                        intent.putExtra("video_file", photoPath);
                        startActivity(intent);
                        overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
                    }

                } else if (resultCode == RESULT_CANCELED) {
                    Toast.makeText(this, "Video recording cancelled.",
                            Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(this, "Failed to record video",
                            Toast.LENGTH_LONG).show();
                }
            } else if (requestCode == REQUEST_TAKE_GALLERY_VIDEO) {
                Uri selectedVideoUri = data.getData();

                // OI FILE Manager
                filemanagerstring = selectedVideoUri.getPath();

                // MEDIA GALLERY
                photoPath = getPath(selectedVideoUri);

                if (photoPath != null) {
                    File video_file = new File(photoPath);

                    long sizeInBytes = video_file.length();

                    long sizeInMb = sizeInBytes / (1024 * 1024);

                    System.out.println("tttt"+sizeInMb);

//                    MediaMetadataRetriever retriever = new MediaMetadataRetriever();
                    FFmpegMediaMetadataRetriever retriever = new FFmpegMediaMetadataRetriever ();
                    try {
                        retriever.setDataSource(MainActivity.this, selectedVideoUri);
                        String time = retriever.extractMetadata(FFmpegMediaMetadataRetriever.METADATA_KEY_DURATION);
                        long timeInMillisec = Long.parseLong(time);

                        if (timeInMillisec > 120033){
                            BullyToast.showToast(MainActivity.this,"Video length cannot be greater then 2 min");
                        } else if (sizeInMb > 200){
                            BullyToast.showToast(MainActivity.this,"Video size cannot be greater then 200 mb");
                        }  else {
                            if (photoPath != null) {

                                Intent intent = new Intent(MainActivity.this,
                                        SavedVideoActivity.class);
                                intent.putExtra("video_file", photoPath);
                                startActivity(intent);
                                overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                    }
                    retriever.release();
                }
            }
        }
    }

    // UPDATED!
    public String getPath(Uri uri) {

        if (uri != null) {

            return FilePahUtils.getPath(getApplicationContext(), uri);
        } else return null;
    }

    @SuppressWarnings("deprecation")
    private String getPath1(Uri selectedImaeUri) {
        String[] projection = {MediaStore.Images.Media.DATA};

        Cursor cursor = managedQuery(selectedImaeUri, projection, null, null,
                null);

        if (cursor != null) {
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);

            return cursor.getString(columnIndex);
        }

        return selectedImaeUri.getPath();
    }

    private void goAhead() {
        Intent i = new Intent(
                Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, SELECT_PHOTO);
    }

    private void goAheadVideo() {
        Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Video.Media.INTERNAL_CONTENT_URI);
        intent.setType("video/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.putExtra("return-data", true);
        startActivityForResult(intent, REQUEST_TAKE_GALLERY_VIDEO);
    }

    private void camera() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        try {

            String filepath = Environment.getExternalStorageDirectory().getPath();
            File file = new File(filepath, "/BullyApp/Image");
            if (!file.exists()) {
                if (!file.mkdirs()) {
                    Log.d("MyCameraApp", "failed to create directory");

                }
            }
            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss")
                    .format(new Date());
            File mediaFile;
            mediaFile = new File(file.getPath() + File.separator
                    + "Bully_" + timeStamp + ".jpg");

            mCurrentPhotoPath = mediaFile.getAbsolutePath();
//            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(mediaFile));
            Uri photoURI = FileProvider.getUriForFile(MainActivity.this,
                    BuildConfig.APPLICATION_ID + ".provider",
                    mediaFile);
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
        } catch (Exception e) {
            e.printStackTrace();
        }

        startActivityForResult(takePictureIntent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
    }

    private void video() {
        String filepath = Environment.getExternalStorageDirectory().getPath();
        File file = new File(filepath, "/BullyApp/Video");
        if (!file.exists()) {
            if (!file.mkdirs()) {
                Log.d("MyCameraApp", "failed to create directory");

            }
        }
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss")
                .format(new Date());
        File mediaFile;
        mediaFile = new File(file.getPath() + File.separator
                + "Bully_" + timeStamp + ".mp4");

        Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            videoUri = FileProvider.getUriForFile(MainActivity.this,
                    BuildConfig.APPLICATION_ID + ".provider",
                    mediaFile);
        }
        else
        {
            videoUri = Uri.fromFile(mediaFile);
        }

        intent.putExtra(MediaStore.EXTRA_OUTPUT, videoUri);
        intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 0);
        intent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, 120);
        startActivityForResult(intent, VIDEO_CAPTURE);
    }

    private void startAudioRecordingSafe() {
        Intent intent = new Intent(MainActivity.this, AudioRecordActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
    }

    @Override
    protected void onStop() {
        super.onStop();

    }

    /*@Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);

    }
*/    @Override
    protected void onServiceConnected() {
        getSinchServiceInterface().setStartListener(this);
    }

    @Override
    public void onStartFailed(SinchError error) {
        Toast.makeText(this, error.toString(), Toast.LENGTH_LONG).show();
        System.out.println("xxx" + error.toString());
    }

    @Override
    public void onStarted() {
        //  openFriendlistActivity();
    }

    private void followclicked() {

        String userName = preferences.getEmail();

        System.out.println("xy" + userName);

        if (!getSinchServiceInterface().isStarted()) {
            getSinchServiceInterface().startClient(userName);

            System.out.println("xyxyx create ");

        } else {
            //      openFriendlistActivity();

            System.out.println("xyxyx alreay ");
        }
    }

    private void openFriendlistActivity() {
        Intent activityIntent = new Intent(MainActivity.this, FriendListActivity.class);
        activityIntent.putExtra("from", "follow_me");
        startActivity(activityIntent);
        overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
    }

    public void sinchStop() {

        getSinchServiceInterface().stopClient();
    }
}
