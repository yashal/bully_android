package com.nexgen.bullybeware.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nexgen.bullybeware.R;
import com.nexgen.bullybeware.model.RestClient;
import com.nexgen.bullybeware.pojo.ChangePasswordPojo;
import com.nexgen.bullybeware.util.BullyConstants;
import com.nexgen.bullybeware.util.BullyDialogs;
import com.nexgen.bullybeware.util.BullyPreferences;
import com.nexgen.bullybeware.util.BullyToast;
import com.nexgen.bullybeware.util.ConnectionDetector;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by quepplin1 on 8/26/2016.
 */
public class ChangePasswordActivity extends BaseActivity {

    private EditText edtxt_otp,edtxt_oldPassword,edtxt_newPassword,edtxt_con_password;
    private TextInputLayout inputlayoutotp,inputlayoutOldpassword,inputlayoutNewpassword;
    private TextView tv_submit;
    private String otp,from;
    private BullyPreferences preferences;
    private static String old_password,new_password;
    LinearLayout ll_forgotPassword;
    private static final long DOUBLE_CLICK_TIME_DELTA = 2000; //milliseconds

    long lastClickTime = 0;
    private MediaPlayer mPlayer;

    // flag for Internet connection status
    Boolean isInternetPresent = false;

    // Connection detector class
    ConnectionDetector cd;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_changepassword);

        init();
        setOnClickListner();
    }

    private void init(){

        edtxt_otp = (EditText) findViewById(R.id.cp_edtxt_otp);
        edtxt_oldPassword = (EditText) findViewById(R.id.cp_edtxt_oldpassword);
        edtxt_newPassword = (EditText) findViewById(R.id.cp_edtxt_newpassword);
        edtxt_con_password = (EditText) findViewById(R.id.cp_edtxt_con_password);

        inputlayoutotp = (TextInputLayout) findViewById(R.id.input_layout_otp);
        inputlayoutOldpassword = (TextInputLayout) findViewById(R.id.input_layout_old_password);
        inputlayoutNewpassword = (TextInputLayout) findViewById(R.id.input_layout_new_password);

        tv_submit = (TextView) findViewById(R.id.changepassword_submit);

        ll_forgotPassword = (LinearLayout) findViewById(R.id.ll_forgotPassword);

        cd = new ConnectionDetector(getApplicationContext());
        // get Internet status
        isInternetPresent = cd.isConnectingToInternet();

        preferences = new BullyPreferences(this);

        from = getIntent().getExtras().getString("from");

        BullyToast.showToast(ChangePasswordActivity.this, getResources().getString(R.string.emergency_alaram));


        if ( from.equalsIgnoreCase("login")){
            edtxt_oldPassword.setVisibility(View.GONE);

            setDrawerbackIcon("Forgot Password");
        } else {
            inputlayoutOldpassword.setVisibility(View.VISIBLE);
            inputlayoutotp.setVisibility(View.GONE);

            setDrawerbackIcon("Change Password");
        }
    }

    private void setOnClickListner(){
        tv_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (from.equalsIgnoreCase("login")){
                    if (edtxt_otp.getText().toString().length() == 0){
                        customAlertDialog("CHANGE PASSWORD",getResources().getString(R.string.err_otp));
                    } else if (edtxt_newPassword.getText().toString().trim().isEmpty()){
                        customAlertDialog("CHANGE PASSWORD",getResources().getString(R.string.err_new_password));
                    } else if (edtxt_newPassword.getText().toString().trim().length() < 6){
                        customAlertDialog("CHANGE PASSWORD",getResources().getString(R.string.err_New_password_length));
                    } else if (edtxt_con_password.getText().toString().trim().isEmpty()){
                        customAlertDialog("CHANGE PASSWORD",getResources().getString(R.string.err_con_password));
                    } else if (edtxt_con_password.getText().toString().trim().length() < 6){
                        customAlertDialog("CHANGE PASSWORD",getResources().getString(R.string.err_con_password_length));
                    } else if (!edtxt_newPassword.getText().toString().equalsIgnoreCase(edtxt_con_password.getText().toString())){
                        customAlertDialog("CHANGE PASSWORD",getResources().getString(R.string.err_password_not_match));
                    }
                    else {
                        setOtpPasswordData();
                    }

                } else {
                    if (edtxt_oldPassword.getText().toString().trim().isEmpty()){
                        customAlertDialog("CHANGE PASSWORD",getResources().getString(R.string.err_old_password));
                    } else if (edtxt_oldPassword.getText().toString().length() < 6){
                        customAlertDialog("CHANGE PASSWORD",getResources().getString(R.string.err_old_password_length));
                    } else if (edtxt_newPassword.getText().toString().trim().isEmpty()){
                        customAlertDialog("CHANGE PASSWORD",getResources().getString(R.string.err_new_password));
                    }else if (edtxt_newPassword.getText().toString().trim().length() < 6){
                        customAlertDialog("CHANGE PASSWORD",getResources().getString(R.string.err_New_password_length));
                    } else if (edtxt_con_password.getText().toString().trim().isEmpty()){
                        customAlertDialog("CHANGE PASSWORD",getResources().getString(R.string.err_con_password));
                    }else if (edtxt_con_password.getText().toString().trim().length() < 6){
                        customAlertDialog("CHANGE PASSWORD",getResources().getString(R.string.err_con_password_length));
                    } else if (!edtxt_newPassword.getText().toString().equalsIgnoreCase(edtxt_con_password.getText().toString())){
                        customAlertDialog("CHANGE PASSWORD",getResources().getString(R.string.err_password_not_match));
                    } else {
                        setoldPasswordData();
                    }
                }
            }
        });
        ll_forgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                long clickTime = System.currentTimeMillis();
                if (clickTime - lastClickTime < DOUBLE_CLICK_TIME_DELTA){
                    try {
                        playStopClickListner();
                    } catch (Exception e){
                        e.printStackTrace();
                    }
                    lastClickTime = 0;
                } else {
                    lastClickTime = clickTime;
                }
            }
        });
    }
    private void setOtpPasswordData(){
        if (isInternetPresent){
            final ProgressDialog d = BullyDialogs.showLoading(ChangePasswordActivity.this);
            d.setCanceledOnTouchOutside(false);

            new_password = edtxt_newPassword.getText().toString();

            md(new_password);

            RestClient.get().otpChangepassword(preferences.getStudentId(), edtxt_otp.getText().toString(),new_password , new Callback<ChangePasswordPojo>() {
                @Override
                public void success(ChangePasswordPojo changePasswordPojo, Response response) {
                     if (changePasswordPojo != null){
                         if (changePasswordPojo.getStatus()){
                             BullyToast.showToast(ChangePasswordActivity.this,changePasswordPojo.getMessage());

                             Intent intent = new Intent(ChangePasswordActivity.this, LoginActivity.class);
                             startActivity(intent);
                             finish();

                             preferences.setStudentId("");

                         } else {
                             customAlertDialog("CHANGE PASSWORD",changePasswordPojo.getMessage());
                         }
                      d.dismiss();
                     }
                     else
                     {
                         d.dismiss();
                         BullyToast.showToast(ChangePasswordActivity.this,BullyConstants.SERVER_NOT_RESPOND);
                     }
                }

                @Override
                public void failure(RetrofitError error) {
                    d.dismiss();
                }
            });
        }else {
            customAlertDialog("BullyBeware",BullyConstants.NO_INTERNET_CONNECTED);
        }
    }
    private void setoldPasswordData(){
        if (isInternetPresent){
            final ProgressDialog d = BullyDialogs.showLoading(ChangePasswordActivity.this);
            d.setCanceledOnTouchOutside(false);


            old_password = edtxt_oldPassword.getText().toString();
            new_password = edtxt_newPassword.getText().toString();

            md5(old_password);
            md(new_password);


            RestClient.get().oldChangePassword(preferences.getStudentId(), old_password,new_password , new Callback<ChangePasswordPojo>() {
                @Override
                public void success(ChangePasswordPojo changePasswordPojo, Response response) {
                    if (changePasswordPojo != null){
                        if (changePasswordPojo.getStatus()){
                            BullyToast.showToast(ChangePasswordActivity.this,changePasswordPojo.getMessage());

                            Intent intent = new Intent(ChangePasswordActivity.this, MainActivity.class);
                            startActivity(intent);
                            finish();
                        } else {
                            customAlertDialog("CHANGE PASSWORD!",changePasswordPojo.getMessage());
                        }
                        d.dismiss();
                    }
                    else
                    {
                        d.dismiss();
                        BullyToast.showToast(ChangePasswordActivity.this, BullyConstants.SERVER_NOT_RESPOND);
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    d.dismiss();
                }
            });
        }else {
            customAlertDialog("BullyBeware",BullyConstants.NO_INTERNET_CONNECTED);
        }
    }
    public static final String md5(final String s) {
        final String MD5 = "MD5";
        try {
            // Create MD5 Hash
            MessageDigest digest = java.security.MessageDigest
                    .getInstance(MD5);
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuilder hexString = new StringBuilder();
            for (byte aMessageDigest : messageDigest) {
                String h = Integer.toHexString(0xFF & aMessageDigest);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
                old_password = hexString.toString();
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }
    public static final String md(final String s) {
        final String MD5 = "MD5";
        try {
            // Create MD5 Hash
            MessageDigest digest = java.security.MessageDigest
                    .getInstance(MD5);
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuilder hexString = new StringBuilder();
            for (byte aMessageDigest : messageDigest) {
                String h = Integer.toHexString(0xFF & aMessageDigest);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
                new_password = hexString.toString();
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        if ( from.equalsIgnoreCase("login")){

            preferences.setStudentId("");

            Intent intent = new Intent(ChangePasswordActivity.this, LoginActivity.class);
            startActivity(intent);
            finish();

        } else {

            finish();

        }
    }
}
