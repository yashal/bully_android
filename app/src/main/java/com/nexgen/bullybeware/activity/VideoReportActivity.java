package com.nexgen.bullybeware.activity;

import android.accounts.AccountManager;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.google.android.gms.auth.GoogleAuthException;
import com.google.android.gms.auth.GoogleAuthUtil;
import com.google.android.gms.auth.UserRecoverableAuthException;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.googleapis.extensions.android.gms.auth.UserRecoverableAuthIOException;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.client.util.ExponentialBackOff;
import com.google.api.services.youtube.YouTube;
import com.google.api.services.youtube.YouTubeScopes;
import com.google.api.services.youtube.model.Video;
import com.google.api.services.youtube.model.VideoSnippet;
import com.nexgen.bullybeware.R;
import com.nexgen.bullybeware.YoutubeVideoUpload.Auth;
import com.nexgen.bullybeware.YoutubeVideoUpload.Constants;
import com.nexgen.bullybeware.YoutubeVideoUpload.UploadService;
import com.nexgen.bullybeware.YoutubeVideoUpload.UploadsListFragment;
import com.nexgen.bullybeware.adapter.AudioReportAdapter;
import com.nexgen.bullybeware.adapter.IncidentTypeAdapter;
import com.nexgen.bullybeware.adapter.VideoReportAdapter;
import com.nexgen.bullybeware.database.DatabaseHandler;
import com.nexgen.bullybeware.model.RestClient;
import com.nexgen.bullybeware.pojo.ImageUploadPojo;
import com.nexgen.bullybeware.pojo.ImagelistStatusPojo;
import com.nexgen.bullybeware.pojo.IncidentSubmissionPojo;
import com.nexgen.bullybeware.pojo.IncidentTypeListPojo;
import com.nexgen.bullybeware.pojo.IncidentTypePojo;
import com.nexgen.bullybeware.pojo.ReportSubmissionFilesPojo;
import com.nexgen.bullybeware.pojo.ReportSubmissionPojo;
import com.nexgen.bullybeware.pojo.VideolistStatusPojo;
import com.nexgen.bullybeware.service.ImageUploadService;
import com.nexgen.bullybeware.util.BullyConstants;
import com.nexgen.bullybeware.util.BullyDialogs;
import com.nexgen.bullybeware.util.BullyPreferences;
import com.nexgen.bullybeware.util.BullyToast;
import com.nexgen.bullybeware.util.ConnectionDetector;
import com.nexgen.bullybeware.util.Upload;
import com.nexgen.bullybeware.util.VideoData;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import static android.Manifest.permission.GET_ACCOUNTS;

/**
 * Created by quepplin1 on 10/6/2016.
 */
public class VideoReportActivity extends BaseActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, View.OnClickListener {

    private VideoReportActivity ctx = this;
    private EditText edtxt_title, edtxt_date, edtxt_victinName,edtext_bullyName, edtxt_nearestAdult, edtxt_description;
    private ViewPager viewPager;
    private Spinner type_spineer;
    private ArrayList<IncidentTypeListPojo> arraylist;
    private IncidentTypeAdapter adapter;
    private VideoReportAdapter videoAdapter;
    private String bully_name;
    private String incident_id = "video";
    private TextView tv_submit;
    private Calendar calendar;
    private int year, month, day;
    private String date;
    GoogleAccountCredential credential;
    public static final String ACCOUNT_KEY = "accountName";
    public static final String REQUEST_AUTHORIZATION_INTENT = "com.google.example.yt.RequestAuth";
    public static final String REQUEST_AUTHORIZATION_INTENT_PARAM = "com.google.example.yt.RequestAuth.param";
    public static final String YOUTUBE_ID = "youtubeId";
    private static final String TAG = "VideoReportActivity";
    private DatabaseHandler db;
    private String image,audio,type,title,victim_name,nearest_adult,description,incident,Submit_id,school_id;
    private Dialog dialogLogout;
    private static final int REQUEST_AUTHORIZATION = 3;


    // flag for Internet connection status
    Boolean isInternetPresent = false;
    private UploadsListFragment mUploadsListFragment;

    // Connection detector class
    ConnectionDetector cd;
    private BullyPreferences preferences;
    private String form_id;
    ArrayList<String> object;
    private ArrayList<VideolistStatusPojo> videostatus= new ArrayList<VideolistStatusPojo>();;
    String SCOPES;


    // for external permission
    private final static int GET_ACCOUNTS_RESULT = 102;
    private SharedPreferences sharedPreferences;
    private ArrayList<String> permissionsToRequest;
    private ArrayList<String> permissionsRejected;

    private GoogleApiClient mGoogleApiClient;
    private boolean mSignInClicked;
    private ConnectionResult mConnectionResult;
    private boolean mIntentInProgress;
    private static final int RC_SIGN_IN = 0;
    private String token, uid, googleId,incident_type;

    private CallbackManager callbackManager;
    private String mChosenAccountName;
    private UploadBroadcastReceiver broadcastReceiver;
    private VideolistStatusPojo statusPojo;
    private static final long DOUBLE_CLICK_TIME_DELTA = 2000;//milliseconds
    long lastClickTime = 0;
    private MediaPlayer mPlayer;
    private LinearLayout ll_video_preview;
    private ImageView terms_checkbox;
    private boolean status_check = false;
    private boolean isChecked = true;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_incident);

        setDrawerDataCross("Report Incident");

        init();
        setData();
        setIncidentTypeData();
        setOnClickListner();

        credential = GoogleAccountCredential.usingOAuth2(
                getApplicationContext(), Arrays.asList(Auth.SCOPES));
        // set exponential backoff policy
        credential.setBackOff(new ExponentialBackOff());

    }

    private void init() {
        edtxt_title = (EditText) findViewById(R.id.report_edtxt_title);
        edtxt_date = (EditText) findViewById(R.id.report_edtxt_date);
        edtxt_victinName = (EditText) findViewById(R.id.report_edtxt_victim_name);
        edtext_bullyName = (EditText) findViewById(R.id.report_edtxt_Bully_name);
        edtxt_nearestAdult = (EditText) findViewById(R.id.report_edtxt_nearest_adult);
        edtxt_description = (EditText) findViewById(R.id.report_edtxt_description);
        tv_submit = (TextView) findViewById(R.id.report_tv_submit);

        terms_checkbox = (ImageView) findViewById(R.id.terms_checkbox);

        type_spineer = (Spinner) findViewById(R.id.type_spinner);

        viewPager = (ViewPager) findViewById(R.id.report_pager);

        arraylist = new ArrayList<IncidentTypeListPojo>();

        ll_video_preview = (LinearLayout) findViewById(R.id.ll_image_report);

        preferences = new BullyPreferences(VideoReportActivity.this);

        cd = new ConnectionDetector(getApplicationContext());
        // get Internet status
        isInternetPresent = cd.isConnectingToInternet();

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        System.out.println("zzz" +preferences.getString(ACCOUNT_KEY));
        if (preferences.getString(ACCOUNT_KEY) != null){

            mChosenAccountName = preferences.getString(ACCOUNT_KEY);
        } else {
            mGoogleApiClient = new GoogleApiClient.Builder(this).addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this).addApi(Plus.API, Plus.PlusOptions.builder().build())
                    .addScope(Plus.SCOPE_PLUS_LOGIN)
                    .addScope(new Scope(YouTubeScopes.YOUTUBE)).build();
            mGoogleApiClient.connect();
        }

        db = new DatabaseHandler(VideoReportActivity.this);

        // set date and time

        calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);

        month = calendar.get(Calendar.MONTH) +1;
        day = calendar.get(Calendar.DAY_OF_MONTH);
        showDate(year, month, day);

        LinearLayout drawer_cross = (LinearLayout) findViewById(R.id.drawerCross);

        drawer_cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Submit_id != null){
                    finish();
                } else {
                    NotSubmitedData();
                }
            }
        });

        terms_checkbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isChecked)
                {
                    terms_checkbox.setImageDrawable(ContextCompat.getDrawable(ctx, R.mipmap.check_box_unchecked));
                    isChecked = false;
                    status_check = true;
                    System.out.println("jhvbjhvjhvjhvjhvhuyj A"); // unchecked
                    showMessageDialog(ctx);
                }
                else
                {
                    terms_checkbox.setImageDrawable(ContextCompat.getDrawable(ctx, R.mipmap.check_box_checked));
                    isChecked = true;
                    status_check = false;
                    System.out.println("jhvbjhvjhvjhvjhvhuyj B");  // checked

                }
            }
        });
    }


    private void setData() {

        try {
            Intent intent = getIntent();
            Bundle args = intent.getBundleExtra("BUNDLE");
            if (args != null){
                object = (ArrayList<String>) args.getSerializable("ARRAYLIST");

                videostatus.clear();

                for (int i = 0; i < object.size(); i++) {
                    statusPojo = new VideolistStatusPojo();
                    statusPojo.setPath(object.get(i));
                    videostatus.add(statusPojo);
                }
            }
        } catch (Exception e){
            e.printStackTrace();
        }

        if (getIntent().getStringExtra("type") !=null){
            type = getIntent().getStringExtra("type");
        } if (getIntent().getStringExtra("incident") != null){
            incident = getIntent().getStringExtra("incident");
        }if (getIntent().getStringExtra("title") != null){
            title = getIntent().getStringExtra("title");
            edtxt_title.setText(title);
        } if (getIntent().getStringExtra("Submit_id") != null){
            Submit_id = getIntent().getStringExtra("Submit_id");
        }if (getIntent().getStringExtra("bully_name") != null){
            bully_name = getIntent().getStringExtra("bully_name");
            edtext_bullyName.setText(bully_name);}
        if (getIntent().getStringExtra("date") !=null){
            date = getIntent().getStringExtra("date");
            edtxt_date.setText(date);
        } if (getIntent().getStringExtra("Victim_name") != null){
            victim_name = getIntent().getStringExtra("Victim_name");
            edtxt_victinName.setText(victim_name);
        } if (getIntent().getStringExtra("nearest_adult") != null){
            nearest_adult = getIntent().getStringExtra("nearest_adult");
            edtxt_nearestAdult.setText(nearest_adult);
        } if (getIntent().getStringExtra("description") != null){
            description = getIntent().getStringExtra("description");
            edtxt_description.setText(description);
        }

        videoAdapter = new VideoReportAdapter(VideoReportActivity.this, videostatus);

        viewPager.setAdapter(videoAdapter);
        videoAdapter.notifyDataSetChanged();

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                videoAdapter.onPageChanged();
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private boolean isCorrectlyConfigured() {
        // This isn't going to internationalize well, but we only really need
        // this for the sample app.
        // Real applications will remove this section of code and ensure that
        // all of these values are configured.
        if (Auth.KEY.startsWith("Replace")) {
            return false;
        }
        if (Constants.UPLOAD_PLAYLIST.startsWith("Replace")) {
            return false;
        }
        return true;
    }

    private void setOnClickListner() {
        type_spineer.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (position == 0) {

                    incident_id = "video";

                    LinearLayout parent1 = (LinearLayout) view;

                    ((TextView) parent1.getChildAt(0)).setTextColor(getResources().getColor(R.color.song_name_text_color));

                } else {
                    incident_id = arraylist.get(position).getId();

                    incident_type = arraylist.get(position).getIncident();

                    LinearLayout paren = (LinearLayout) view;
                    ((TextView) paren.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        tv_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (preferences.getSchool_id().length() > 0){
                    school_id = preferences.getSchool_id();
                } else {
                    school_id = preferences.getSchool_email();
                }
                // get Internet status
                isInternetPresent = cd.isConnectingToInternet();

                if (isInternetPresent){
                if (incident_id.equalsIgnoreCase("video")) {
                    customAlertDialog(getResources().getString(R.string.submission_failed), getResources().getString(R.string.err_incident_type));
                } else if (date.length() == 0) {
                    customAlertDialog(getResources().getString(R.string.submission_failed), getResources().getString(R.string.err_incident_date));
                } else if (edtxt_victinName.getText().toString().length() == 0) {
                    customAlertDialog(getResources().getString(R.string.submission_failed), getResources().getString(R.string.err_victim_name));
                } else if (school_id.length() == 0){
                    customAlertDialog(getResources().getString(R.string.submission_failed), getResources().getString(R.string.err_school_id));
                }  else {

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            addPermissionDialogMarshMallow();
                        } else {
                            if (preferences.getString(ACCOUNT_KEY) != null){
                                imageservice();
                            } else {
                                googlePlusLogin();
                            }
                        }
                }
                }else {
                    NetworkDialog();
                }
            }
        });
        edtxt_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog(999);
            }
        });

        ll_video_preview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                long clickTime = System.currentTimeMillis();
                if (clickTime - lastClickTime < DOUBLE_CLICK_TIME_DELTA){
                    try {
                        playStopClickListner();
                    } catch (Exception e){
                        e.printStackTrace();
                    }
                    lastClickTime = 0;
                } else {
                    lastClickTime = clickTime;
                }
            }
        });
    }

    private void googlePlusLogin() {
        if (!mGoogleApiClient.isConnecting()) {
            mSignInClicked = true;
            resolveSignInError();
        }
    }

    private void resolveSignInError() {
        if (mConnectionResult!=null&&mConnectionResult.hasResolution()) {
            try {
                mIntentInProgress = true;
                mConnectionResult.startResolutionForResult(this, RC_SIGN_IN);

            } catch (IntentSender.SendIntentException e) {
                mIntentInProgress = false;
                mGoogleApiClient.connect();
            }
        }
    }
   /* @Override
    protected void onPause() {
        super.onPause();
        if (broadcastReceiver != null) {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(
                    broadcastReceiver);
        }
        if (isFinishing()) {
            // mHandler.removeCallbacksAndMessages(null);
        }
    }*/

    @Override
    protected Dialog onCreateDialog(int id) {
        // TODO Auto-generated method stub
        if (id == 999) {
            DatePickerDialog dialog = new DatePickerDialog(this, myDateListener, year, month, day);
            dialog.getDatePicker().setMaxDate(System.currentTimeMillis());
            return dialog;
        }
        return null;
    }

    private DatePickerDialog.OnDateSetListener myDateListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker arg0, int arg1, int arg2, int arg3) {
            // TODO Auto-generated method stub
            // arg1 = year
            // arg2 = month
            // arg3 = day
            showDate(arg1, arg2+1, arg3);
            arg0.setMaxDate(System.currentTimeMillis());
        }
    };

    private void showDate(int year, Integer month, Integer day) {

        edtxt_date.setText(new StringBuilder().append((month.toString().length() == 1 ? "0"+month.toString():month.toString())).append("/")
                .append((day.toString().length() == 1 ? "0"+day.toString():day.toString())).append("/").append(year));

        date = String.valueOf(new StringBuilder().append(year).append("-")
                .append((month.toString().length() == 1 ? "0"+month.toString():month.toString())).append("-").append((day.toString().length() == 1 ? "0"+day.toString():day.toString())));
    }

    private void setIncidentTypeData() {
        if (isInternetPresent) {
            RestClient.get().getIncidentType(new Callback<IncidentTypePojo>() {
                @Override
                public void success(IncidentTypePojo basepojo, Response response) {
                    if (basepojo != null) {
                        if (basepojo.getStatus()) {
                            arraylist = basepojo.getObject();
                            IncidentTypeListPojo typepojo = new IncidentTypeListPojo();
                            typepojo.setIncident("Select Incident Type");
                            arraylist.add(0, typepojo);

                            int pos = -1;
                            for (int j=0;j<arraylist.size();j++) {
                                if (type != null) {
                                    if (type.equalsIgnoreCase(arraylist.get(j).getIncident())) {

                                        pos = j;
                                        break;
                                    }
                                }
                            }
                            adapter = new IncidentTypeAdapter(VideoReportActivity.this, arraylist);
                            type_spineer.setAdapter(adapter);
                            if(pos != -1){
                                type_spineer.setSelection(pos);
                            } else {
                                type_spineer.setSelection(0);
                            }
                        }
                    }
                    else
                    {
                        BullyToast.showToast(VideoReportActivity.this, BullyConstants.SERVER_NOT_RESPOND);
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    System.out.println("xxx" + "failure");
                }
            });
        } else {
            IncidentTypeListPojo typepojo = new IncidentTypeListPojo();
            typepojo.setIncident("Select Incident Type");
            arraylist.add(0, typepojo);
            adapter = new IncidentTypeAdapter(VideoReportActivity.this, arraylist);
            type_spineer.setAdapter(adapter);
        }
    }

    public void uploadVideo() {
            for (int i = 0; i < videostatus.size(); i++) {

                ReportSubmissionFilesPojo pojo = new ReportSubmissionFilesPojo();
                pojo.setSubmit_id(preferences.getSubmit_time());
                pojo.setImage_url("");
                pojo.setAudio_url("");
                pojo.setVideo_url(videostatus.get(i).getPath());

                ArrayList<ReportSubmissionFilesPojo> arrayimage = new ArrayList<ReportSubmissionFilesPojo>();
                arrayimage.add(pojo);

                db.addFiles(arrayimage);
            }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
          mSignInClicked = false;

        getProfileInformation();
    }

    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        if (!connectionResult.hasResolution()) {
            GooglePlayServicesUtil.getErrorDialog(connectionResult.getErrorCode(), this,
                    0).show();
            return;
        }

        if (!mIntentInProgress) {
            // Store the ConnectionResult for later usage
            mConnectionResult = connectionResult;

            if (mSignInClicked) {
                // The user has already clicked 'sign-in' so we attempt to
                // resolve all
                // errors until the user is signed in, or they cancel.
                resolveSignInError();
            }
        }
    }

   /* @Override
    protected void onResume() {
        super.onResume();
        if (broadcastReceiver == null)
            broadcastReceiver = new UploadBroadcastReceiver();
        IntentFilter intentFilter = new IntentFilter(
                REQUEST_AUTHORIZATION_INTENT);
       *//* LocalBroadcastManager.getInstance(this).registerReceiver(
                broadcastReceiver, intentFilter);*//*

    //
    }*/

    private void getProfileInformation() {

        new AsyncTask<Void, Void, Void>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();

                if (Plus.PeopleApi.getCurrentPerson(mGoogleApiClient) != null) {
                  SCOPES = "oauth2:profile email";
                }
            }

            @Override
            protected Void doInBackground(Void... params) {

                try {
                        token = GoogleAuthUtil.getToken(
                                VideoReportActivity.this,
                                Plus.AccountApi.getAccountName(mGoogleApiClient),
                                SCOPES);

                        String email = Plus.AccountApi.getAccountName(mGoogleApiClient);

                        if (email != null) {
                            mChosenAccountName = email;
                            credential.setSelectedAccountName(email);

                        }

                        Person currentPerson = Plus.PeopleApi.getCurrentPerson(mGoogleApiClient);
                        googleId = currentPerson.getId();

                } catch (IOException e) {
                    e.printStackTrace();
                    System.out.println("hh IOException ");
                } catch (UserRecoverableAuthException e) {
                    e.printStackTrace();
                    Intent recover = e.getIntent();
                    startActivityForResult(recover, 125);
                    System.out.println("hh UserRecoverableAuthException ");
                } catch (GoogleAuthException authEx) {
                    // The call is not ever expected to succeed
                    // assuming you have already verified that
                    // Google Play services is installed.
                }

                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);

                preferences.setString(ACCOUNT_KEY,mChosenAccountName);

                System.out.println("zzz"+ preferences.getString(ACCOUNT_KEY));

                imageservice();

            }
        }.execute();
    }

    private void imageservice(){

        try {
            Calendar cal = Calendar.getInstance();
            Date currentLocalTime = cal.getTime();
            DateFormat date2 = new SimpleDateFormat("hh:mm:ss");
            date2.setTimeZone(TimeZone.getTimeZone("EST"));

            final String time = date2.format(currentLocalTime);

            Intent uploadIntent = new Intent(VideoReportActivity.this, UploadService.class);
            uploadIntent.putExtra("student_id", preferences.getStudentId());
            uploadIntent.putExtra("incident_id", incident_id);
            uploadIntent.putExtra("date", date);
            uploadIntent.putExtra("time", time);
            uploadIntent.putExtra("title", edtxt_title.getText().toString());
            uploadIntent.putExtra("bully_name", edtext_bullyName.getText().toString());
            uploadIntent.putExtra("victim_name", edtxt_victinName.getText().toString().trim());
            uploadIntent.putExtra("nearest_adult", edtxt_nearestAdult.getText().toString().trim());
            uploadIntent.putExtra("description", edtxt_description.getText().toString().trim());
            uploadIntent.putExtra("Submit_id",Submit_id);
            uploadIntent.putExtra("ACCOUNT_KEY", mChosenAccountName);
            uploadIntent.putExtra("video_status", videostatus);
            uploadIntent.putExtra("school_id",school_id);
            uploadIntent.putExtra("status_check", status_check);
            startService(uploadIntent);

            BullyToast.showToast(VideoReportActivity.this,"Upload Started");

            Intent intent = new Intent(VideoReportActivity.this, MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        } catch (Exception e){
            e.printStackTrace();
        }
        System.out.println("xhxhxhhxhxhx status_check "+status_check);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                Intent intent = new Intent(VideoReportActivity.this, MainActivity.class);
                startActivity(intent);
            }
            if (resultCode == RESULT_CANCELED) {
            }
        } else if (requestCode == RC_SIGN_IN) {
            if (resultCode == RESULT_OK) {
                mSignInClicked = false;
                if (!mGoogleApiClient.isConnecting()) {
                    mGoogleApiClient.connect();
                }
            }
            mIntentInProgress = false;
        } else {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void addPermissionDialogMarshMallow() {
        ArrayList<String> permissions = new ArrayList<>();
        int resultCode = 0;

        permissions.add(GET_ACCOUNTS);
        resultCode = GET_ACCOUNTS_RESULT;


        //filter out the permissions we have already accepted
        permissionsToRequest = findUnAskedPermissions(permissions, sharedPreferences);
        //get the permissions we have asked for before but are not granted..
        //we will store this in a global list to access later.
        permissionsRejected = findRejectedPermissions(permissions, sharedPreferences);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            if (permissionsToRequest.size() > 0) {//we need to ask for permissions
                //but have we already asked for them?
                requestPermissions(permissionsToRequest.toArray(new String[permissionsToRequest.size()]), resultCode);
                //mark all these as asked..
                for (String perm : permissionsToRequest) {
                    markAsAsked(perm, sharedPreferences);
                }
            } else {
                //show the success banner
                if (permissionsRejected.size() < permissions.size()) {
                    //this means we can show success because some were already accepted.
                    //permissionSuccess.setVisibility(View.VISIBLE);
                    if (preferences.getString(ACCOUNT_KEY) != null){
                        imageservice();
                    } else {
                        googlePlusLogin();
                    }
                }

                if (permissionsRejected.size() > 0) {
                    //we have none to request but some previously rejected..tell the user.
                    //It may be better to show a dialog here in a prod application

                    requestPermissions(permissionsToRequest.toArray(new String[permissionsToRequest.size()]), resultCode);
                    //mark all these as asked..
                    for (String perm : permissionsToRequest) {
                        markAsAsked(perm, sharedPreferences);
                    }
                }
            }
        }
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {
            case GET_ACCOUNTS_RESULT:
                if (hasPermission(GET_ACCOUNTS)) {
                    //        permissionSuccess.setVisibility(View.VISIBLE);
                    if (preferences.getString(ACCOUNT_KEY) != null){
                        imageservice();
                    } else {
                        googlePlusLogin();
                    }
                } else {
                    permissionsRejected.add(GET_ACCOUNTS);
                    clearMarkAsAsked(GET_ACCOUNTS, sharedPreferences);
                    String message = "permission for contact access was rejected. Please allow to run the app.";
                    makePostRequestSnack(message, permissionsRejected.size());
                }
                break;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.tv_status_videos:

                uploadVideo();

                break;
        }
    }

    private class UploadBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(REQUEST_AUTHORIZATION_INTENT)) {
                Log.d(TAG, "Request auth received - executing the intent");
                Intent toRun = intent
                        .getParcelableExtra(REQUEST_AUTHORIZATION_INTENT_PARAM);
                startActivityForResult(toRun, REQUEST_AUTHORIZATION);
            }
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();


        videogooglePlusLogout();
    }

   public void videogooglePlusLogout() {
        System.out.println("hh logout");
        if (mGoogleApiClient != null){
            if (mGoogleApiClient.isConnected()) {
                Plus.AccountApi.clearDefaultAccount(mGoogleApiClient);
                mGoogleApiClient.disconnect();
                mGoogleApiClient.connect();
            }
        }
    }

    private void NetworkDialog() {
        dialogLogout = new Dialog(VideoReportActivity.this, android.R.style.Theme_Translucent_NoTitleBar);
        dialogLogout.setContentView(R.layout.custom_dialog);
        dialogLogout.show();


        EditText fp_edtxt_email = (EditText) dialogLogout.findViewById(R.id.fp_edtxt_email);
        fp_edtxt_email.setVisibility(View.GONE);


        TextView dialog_header = (TextView) dialogLogout.findViewById(R.id.dialog_header);
        dialog_header.setVisibility(View.VISIBLE);
        dialog_header.setText(R.string.out_of_network);
        ImageView iView_cross = (ImageView) dialogLogout.findViewById(R.id.dialog_header_cross);
        TextView tv_submit = (TextView) dialogLogout.findViewById(R.id.dialog_submit);
        tv_submit.setText("OK");
        TextView dialog_text = (TextView) dialogLogout.findViewById(R.id.dialog_text);
        dialog_text.setVisibility(View.VISIBLE);

        Calendar cal = Calendar.getInstance(TimeZone.getDefault());
        Date currentLocalTime = cal.getTime();
        DateFormat date1 = new SimpleDateFormat("hh:mm aa");
        date1.setTimeZone(TimeZone.getTimeZone("EST"));

      final String time = date1.format(currentLocalTime);

        Long tsLong = System.currentTimeMillis()/1000;
        final String localTime = tsLong.toString();

        if (Submit_id != null){
            dialog_text.setText(R.string.offline_submit_title);

            tv_submit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                   finish();
                }
            });
        } else {
            dialog_text.setText(R.string.offline_submit_message);

            tv_submit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    preferences.setSubmit_time(localTime);

                    ReportSubmissionPojo pojo = new ReportSubmissionPojo();
                    pojo.setUSER_ID(preferences.getStudentId());
                    pojo.setFile_type("3");
                    pojo.setSUBMIT_ID(localTime);
                    pojo.setType("");
                    pojo.setSubject(edtxt_title.getText().toString().trim());
                    pojo.setBully_name(edtext_bullyName.getText().toString());
                    pojo.setDate(date);
                    pojo.setTime(time);
                    pojo.setName(edtxt_victinName.getText().toString().trim());
                    pojo.setAdult(edtxt_nearestAdult.getText().toString().trim());
                    pojo.setDescription(edtxt_description.getText().toString().trim());

                    uploadVideo();

                    db.addDetails(pojo);

                    Intent intent = new Intent(VideoReportActivity.this, MyRecordActivity.class);
                    intent.putExtra("from_activity","Report_incident");
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                }
            });
        }

        iView_cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogLogout.dismiss();
            }
        });
    }

    private void NotSubmitedData(){

        Calendar cal = Calendar.getInstance(TimeZone.getDefault());
        Date currentLocalTime = cal.getTime();
        DateFormat date1 = new SimpleDateFormat("hh:mm aa");
        date1.setTimeZone(TimeZone.getTimeZone("EST"));

        final String time = date1.format(currentLocalTime);

        Long tsLong = System.currentTimeMillis()/1000;
        final String localTime = tsLong.toString();

        preferences.setSubmit_time(localTime);


        ReportSubmissionPojo pojo = new ReportSubmissionPojo();
        pojo.setUSER_ID(preferences.getStudentId());
        pojo.setFile_type("3");
        pojo.setSUBMIT_ID(localTime);
        pojo.setType(incident_type);
        pojo.setSubject(edtxt_title.getText().toString().trim());
        pojo.setBully_name(edtext_bullyName.getText().toString());
        pojo.setDate(date);
        pojo.setTime(time);
        pojo.setName(edtxt_victinName.getText().toString().trim());
        pojo.setAdult(edtxt_nearestAdult.getText().toString().trim());
        pojo.setDescription(edtxt_description.getText().toString().trim());

        uploadVideo();

        db.addDetails(pojo);

        BullyToast.showToast(VideoReportActivity.this,getResources().getString(R.string.back_submit_message));

        Intent intent = new Intent(VideoReportActivity.this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    public void showMessageDialog(final Context ctx) {
        final Dialog dialog_message = new Dialog(ctx, android.R.style.Theme_Translucent_NoTitleBar);
        dialog_message.setContentView(R.layout.report_dialog);

        ImageView close_dialog = (ImageView) dialog_message.findViewById(R.id.dialog_header_cross);
        close_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_message.dismiss();
            }
        });

        LinearLayout ll_dialog_submit = (LinearLayout)dialog_message.findViewById(R.id.ll_dialog_submit);
        ll_dialog_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_message.dismiss();
                isChecked = false;
                status_check = true;
            }
        });

        LinearLayout ll_dialog_cancel = (LinearLayout)dialog_message.findViewById(R.id.ll_dialog_cancel);
        ll_dialog_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_message.dismiss();
                isChecked = true;
                status_check = false;
                terms_checkbox.setImageDrawable(ContextCompat.getDrawable(ctx, R.mipmap.check_box_checked));
            }
        });

        dialog_message.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        dialog_message.show();
    }
}
