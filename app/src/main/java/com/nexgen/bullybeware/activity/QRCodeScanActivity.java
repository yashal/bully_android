package com.nexgen.bullybeware.activity;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.google.zxing.Result;
import com.nexgen.bullybeware.R;
import com.nexgen.bullybeware.util.BullyToast;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

/**
 * Created by umax desktop on 1/24/2017.
 */

public class QRCodeScanActivity extends BaseActivity implements ZXingScannerView.ResultHandler {

    private ZXingScannerView mScannerView;
    private String fn_value,ln_value,email_value,mobile_value,thumbail_value,friend_id;
    private String friend_key = "key_value";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.qrcode_scan_activity);

        setDrawerbackIcon("QR Code Scan");


        mScannerView = (ZXingScannerView) findViewById(R.id.qrcode_scan);
    }

    public void QrScanner(){

      /*  mScannerView = new ZXingScannerView(this);   // Programmatically initialize the scanner view
        setContentView(mScannerView);*/

        mScannerView.setResultHandler(this); // Register ourselves as a handler for scan results.
        mScannerView.startCamera();         // Start camera

    }

    @Override
    protected void onStop() {
        super.onStop();

        mScannerView.stopCamera();
    }

   /* @Override
    public void onPause() {
        super.onPause();
        mScannerView.stopCamera();           // Stop camera on pause
    }*/

    @Override
    public void handleResult(Result result) {
        // Do something with the result here

        System.out.println("qrcod"+result.getText()); // Prints scan results
        System.out.println("qrcod1"+result.getBarcodeFormat().toString()); // Prints the scan format (qrcode)


            try {
                String str = result.getText();
                String[] splited = str.split(",");

                String split_one= splited[0];
                String split_second= splited[1];
                String split_three= splited[2];
                String split_four = splited[3];
                String split_five = splited[4];
                String split_six = splited[5];

                String [] sub_id = split_one.split("=");
                friend_key = sub_id[0];
                friend_id = sub_id[1];

                String [] sub_string = split_second.split("=");

                String fn_key = sub_string[0];
                fn_value = sub_string[1];

                String [] sub_string1 = split_three.split("=");

                String ln_key = sub_string1[0];
                ln_value = sub_string1[1];

                String [] sub_string2 = split_four.split("=");

                String email_key = sub_string2[0];
                email_value = sub_string2[1];

                String [] sub_string3 = split_five.split("=");

                String mobile_key = sub_string3[0];
                mobile_value = sub_string3[1];

                if (split_six.length() > 0){
                    String [] sub_string4 = split_six.split("=");
                    if (sub_string4.length > 0){
                        String thumbnail_key = sub_string4[0];
                        thumbail_value = sub_string4[1];
                    }
                }
            } catch (Exception e){
                e.printStackTrace();
            }

            System.out.println("key"+fn_value);
            System.out.println("key1"+ln_value);
            System.out.println("key2"+email_value);
            System.out.println("key3"+mobile_value);
            System.out.println("key4"+thumbail_value);


        if (friend_key.equalsIgnoreCase("Id")){
            Intent intent = new Intent(QRCodeScanActivity.this,AddfriendInfoActivity.class);
            intent.putExtra("friend_id",friend_id);
            intent.putExtra("first_name",fn_value);
            intent.putExtra("last_name",ln_value);
            intent.putExtra("email",email_value);
            intent.putExtra("mobile",mobile_value);
            intent.putExtra("thumbnail",thumbail_value);
            intent.putExtra("from","QrCode");
            startActivity(intent);
            finish();
        } else {
            BullyToast.showToast(QRCodeScanActivity.this,"Sorry! this QRC Code has no information");

            // If you would like to resume scanning, call this method below:
            mScannerView.resumeCameraPreview(this);

        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        QrScanner();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        // If you would like to resume scanning, call this method below:
        mScannerView.resumeCameraPreview(this);

        Intent intent = new Intent(QRCodeScanActivity.this, FriendListActivity.class);
        intent.putExtra("from","main");
        startActivity(intent);
        finish();
    }
}
