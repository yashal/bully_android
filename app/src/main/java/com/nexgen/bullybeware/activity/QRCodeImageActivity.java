package com.nexgen.bullybeware.activity;

import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.nexgen.bullybeware.R;
import com.nexgen.bullybeware.model.RestClient;
import com.nexgen.bullybeware.pojo.QRCodeImagePojo;
import com.nexgen.bullybeware.util.BullyConstants;
import com.nexgen.bullybeware.util.BullyPreferences;
import com.nexgen.bullybeware.util.BullyToast;
import com.nexgen.bullybeware.util.ConnectionDetector;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by umax desktop on 1/24/2017.
 */

public class QRCodeImageActivity extends BaseActivity {

    private BullyPreferences preferences;
    private ImageView iView_qrcode;
    private LinearLayout ll_qrcode;
    private static final long DOUBLE_CLICK_TIME_DELTA = 2000;//milliseconds
    long lastClickTime = 0;
    private LinearLayout ll_qrCodeNodata;

    // Connection detector class
    ConnectionDetector cd;

    // flag for Internet connection status
    Boolean isInternetPresent = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.qrcode_image_activity);

        BullyToast.showToast(QRCodeImageActivity.this, getResources().getString(R.string.emergency_alaram));

        iView_qrcode = (ImageView) findViewById(R.id.qrcode_image);

        ll_qrcode = (LinearLayout) findViewById(R.id.ll_qrcode);

        ll_qrCodeNodata = (LinearLayout) findViewById(R.id.qrcode_ll_NoData);

        cd = new ConnectionDetector(getApplicationContext());
        // get Internet status
        isInternetPresent = cd.isConnectingToInternet();

        preferences = new BullyPreferences(this);

        setDrawerbackIcon("QRC Code");

        setApi();

        ll_qrcode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                long clickTime = System.currentTimeMillis();
                if (clickTime - lastClickTime < DOUBLE_CLICK_TIME_DELTA) {
                    try {
                        playStopClickListner();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    lastClickTime = 0;
                } else {
                    lastClickTime = clickTime;
                }
            }
        });

    }

    private void setApi(){
        if (isInternetPresent){
            RestClient.get().getQrImage(preferences.getStudentId(), new Callback<QRCodeImagePojo>() {
                @Override
                public void success(QRCodeImagePojo qrCodeImagePojo, Response response) {

                    ll_qrCodeNodata.setVisibility(View.GONE);
                    iView_qrcode.setVisibility(View.VISIBLE);

                    if (qrCodeImagePojo != null){
                        if (qrCodeImagePojo.getStatus()){
                            Glide.clear(iView_qrcode);
                            Glide.with(QRCodeImageActivity.this).load(qrCodeImagePojo.getObject())
                                    .thumbnail(0.5f)
                                    .crossFade()
                                    .placeholder(R.mipmap.ic_launcher)
                                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                                    .into(iView_qrcode);
                        }
                    }
                    else
                    {
                        BullyToast.showToast(QRCodeImageActivity.this, BullyConstants.SERVER_NOT_RESPOND);
                    }
                }

                @Override
                public void failure(RetrofitError error) {

                }
            });
        } else {
            ll_qrCodeNodata.setVisibility(View.VISIBLE);
            iView_qrcode.setVisibility(View.GONE);
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        finish();
    }
}
