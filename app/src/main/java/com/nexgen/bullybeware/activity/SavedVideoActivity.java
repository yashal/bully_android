package com.nexgen.bullybeware.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.PixelFormat;
import android.graphics.drawable.ColorDrawable;
import android.media.CamcorderProfile;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.nexgen.bullybeware.R;
import com.nexgen.bullybeware.adapter.CustomImageAdapter;
import com.nexgen.bullybeware.adapter.VideoGridAdapter;
import com.nexgen.bullybeware.pojo.CheckboxStatusPojo;
import com.nexgen.bullybeware.util.BullyPreferences;
import com.nexgen.bullybeware.util.BullyToast;
import com.nexgen.bullybeware.util.FilePahUtils;

import java.io.File;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by hp on 9/17/2016.
 */

public class SavedVideoActivity extends BaseActivity {

    private GridView gridview;
    private ArrayList<CheckboxStatusPojo> videostatus = new ArrayList<CheckboxStatusPojo>();
    private String video_file;
    private String videoUri;
    Uri videoUri1;
    private VideoGridAdapter adapter;
    private final int REQUEST_TAKE_GALLERY_VIDEO = 2;
    private String photoPath = "";
    String filemanagerstring = "";
    private TextView tv_reportIncident;
    private ArrayList<String> checkedvideolist = new ArrayList<String>();
    private LinearLayout ll_video_main;
    private static final long DOUBLE_CLICK_TIME_DELTA = 2000;//milliseconds
    long lastClickTime = 0;
    private static final int VIDEO_CAPTURE = 101;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_saved_image);

        setDrawerbackIcon("Preview");

        init();
        setOnClickListner();
        gridView();
    }

    private void init() {
        gridview = (GridView) findViewById(R.id.gridview);
        tv_reportIncident = (TextView) findViewById(R.id.tv_report_incident);
        ll_video_main = (LinearLayout) findViewById(R.id.ll_image_main);

        video_file = getIntent().getStringExtra("video_file");
        CheckboxStatusPojo statusPojo = new CheckboxStatusPojo(true, video_file);
        CheckboxStatusPojo statusPojo1 = new CheckboxStatusPojo(false, videoUri);
        videostatus.add(statusPojo);
        videostatus.add(statusPojo1);

        videoUri = "android.resource://com.nexgen.bullybeware/" + R.mipmap.icon_camara;

        BullyToast.showToast(SavedVideoActivity.this, getResources().getString(R.string.emergency_alaram));
    }

    private void setOnClickListner() {
        tv_reportIncident.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkedvideolist.clear();
                for (int i = 0; i < videostatus.size(); i++) {
                    Boolean status = videostatus.get(i).getStatus();

                    if (status)
                        checkedvideolist.add(videostatus.get(i).getPath());
                }

                Intent intent = new Intent(SavedVideoActivity.this, VideoReportActivity.class);
                Bundle args = new Bundle();
                args.putSerializable("ARRAYLIST", (Serializable) checkedvideolist);
                intent.putExtra("BUNDLE", args);
                startActivity(intent);
            }
        });

        ll_video_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("hh click");
                long clickTime = System.currentTimeMillis();
                if (clickTime - lastClickTime < DOUBLE_CLICK_TIME_DELTA) {
                    try {
                        playStopClickListner();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    lastClickTime = 0;
                } else {
                    lastClickTime = clickTime;
                }
            }
        });
    }

    private void gridView() {
        adapter = new VideoGridAdapter(this, videostatus);
        gridview.setAdapter(adapter);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_TAKE_GALLERY_VIDEO) {
                Uri selectedVideoUri = data.getData();

                // OI FILE Manager
                filemanagerstring = selectedVideoUri.getPath();

                // MEDIA GALLERY
                photoPath = getPath(selectedVideoUri);

                if (photoPath != null) {
                    File video_file1 = new File(photoPath);

                    long sizeInBytes = video_file1.length();

                    long sizeInMb = sizeInBytes / (1024 * 1024);

                    System.out.println("tttt" + sizeInMb);

                    MediaMetadataRetriever retriever = new MediaMetadataRetriever();
                    retriever.setDataSource(SavedVideoActivity.this, selectedVideoUri);
                    String time = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);
                    long timeInMillisec = Long.parseLong(time);

                    if (timeInMillisec > 120033) {
                        BullyToast.showToast(SavedVideoActivity.this, "Video length cannot be greater then 2 min");
                    } else if (sizeInMb > 200) {
                        BullyToast.showToast(SavedVideoActivity.this, "Video size cannot be greater then 200 mb");
                    } else {
                        if (selectedVideoUri != null) {
                            System.out.println("xx" + selectedVideoUri);
                            videostatus.remove(videostatus.size() - 1);
                            video_file = String.valueOf(selectedVideoUri);
                            CheckboxStatusPojo statusPojo = new CheckboxStatusPojo(false, photoPath);
                            CheckboxStatusPojo statusPojo1 = new CheckboxStatusPojo(false, videoUri);
                            videostatus.add(statusPojo);
                            videostatus.add(statusPojo1);

                            gridView();
                        }
                    }
                 }

                } else if (requestCode == VIDEO_CAPTURE) {
                    if (resultCode == RESULT_OK) {
                        photoPath = getPath1(videoUri1);
                        if (videoUri1 != null) {
                            System.out.println("xx" + videoUri1);
                            videostatus.remove(videostatus.size() - 1);
                            video_file = String.valueOf(videoUri1);
                            CheckboxStatusPojo statusPojo = new CheckboxStatusPojo(false, photoPath);
                            CheckboxStatusPojo statusPojo1 = new CheckboxStatusPojo(false, videoUri);
                            videostatus.add(statusPojo);
                            videostatus.add(statusPojo1);

                            gridView();
                        }

                    } else if (resultCode == RESULT_CANCELED) {
                        Toast.makeText(this, "Video recording cancelled.",
                                Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(this, "Failed to record video",
                                Toast.LENGTH_LONG).show();
                    }
                } else {
                    videostatus.remove(videostatus.size() - 1);
                    video_file = data.getStringExtra("video_file");
                    CheckboxStatusPojo statusPojo = new CheckboxStatusPojo(false, video_file);
                    CheckboxStatusPojo statusPojo1 = new CheckboxStatusPojo(false, videoUri);
                    videostatus.add(statusPojo);
                    videostatus.add(statusPojo1);

                    gridView();
                }
        }
    }

    // UPDATED!
    public String getPath(Uri uri) {
        if (uri != null) {
            return FilePahUtils.getPath(getApplicationContext(), uri);
        } else return null;
    }

    public void video() {
        dialogLogout = new Dialog(SavedVideoActivity.this, android.R.style.Theme_Translucent_NoTitleBar);
        dialogLogout.setContentView(R.layout.customvideo_dialog);
        dialogLogout.show();

        LinearLayout ll_dialog = (LinearLayout) dialogLogout.findViewById(R.id.ll_dialog);
        ll_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogLogout.dismiss();
            }
        });

        TextView tv_camera = (TextView) dialogLogout.findViewById(R.id.tv_camera);
        TextView tv_gallety = (TextView) dialogLogout.findViewById(R.id.tv_gallery);

        tv_camera.setText("Video Camera");

        tv_camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                videoStorage();

                dialogLogout.dismiss();
            }
        });

        tv_gallety.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                videogallery();

                dialogLogout.dismiss();
            }
        });
    }

    private void videoStorage() {
        String filepath = Environment.getExternalStorageDirectory().getPath();
        File file = new File(filepath, "/BullyApp/Video");
        if (!file.exists()) {
            if (!file.mkdirs()) {
                Log.d("MyCameraApp", "failed to create directory");
            }
        }
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss")
                .format(new Date());
        File mediaFile;
        mediaFile = new File(file.getPath() + File.separator
                + "Bully_" + timeStamp + ".mp4");

        Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);

        videoUri1 = Uri.fromFile(mediaFile);

        intent.putExtra(MediaStore.EXTRA_OUTPUT, videoUri1);
        intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 0);
        intent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, 120);
        startActivityForResult(intent, VIDEO_CAPTURE);
    }


    public void videogallery() {
        Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Video.Media.INTERNAL_CONTENT_URI);
        intent.setType("video/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.putExtra("return-data", true);
        startActivityForResult(intent, REQUEST_TAKE_GALLERY_VIDEO);
    }

    @SuppressWarnings("deprecation")
    private String getPath1(Uri selectedImaeUri) {
        String[] projection = {MediaStore.Images.Media.DATA};

        Cursor cursor = managedQuery(selectedImaeUri, projection, null, null,
                null);

        if (cursor != null) {
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            return cursor.getString(columnIndex);
        }
        return selectedImaeUri.getPath();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        finish();
    }
}
