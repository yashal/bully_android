package com.nexgen.bullybeware.activity;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.nexgen.bullybeware.R;
import com.nexgen.bullybeware.adapter.AlermListAdapter;
import com.nexgen.bullybeware.util.BullyPreferences;
import com.nexgen.bullybeware.util.BullyToast;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by quepplin1 on 10/3/2016.
 */
public class SoundConfigActivity extends BaseActivity {

    private Spinner soundConfig;
    String value;
    private BullyPreferences preferences;
    int i;
    private LinearLayout ll_soundConfig;
    private static final long DOUBLE_CLICK_TIME_DELTA = 2000;//milliseconds
    long lastClickTime = 0;
    private AlermListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sound_config);

        setDrawerbackIcon("Sound Config");

        init();
    }

    private void init() {
        soundConfig = (Spinner) findViewById(R.id.soundConfig_spinner);

        ll_soundConfig = (LinearLayout) findViewById(R.id.ll_soundConfig);

        BullyToast.showToast(SoundConfigActivity.this, getResources().getString(R.string.emergency_alaram));

        preferences = new BullyPreferences(SoundConfigActivity.this);

        // Spinner Drop down elements
        ArrayList<String> categories = new ArrayList<String>();
        categories.add("Emergency");
        categories.add("Alert Sound");

        adapter = new AlermListAdapter(SoundConfigActivity.this,categories);
        soundConfig.setAdapter(adapter);

        /*// Creating adapter for spinner
        final ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, categories);
        soundConfig.setAdapter(dataAdapter);*/

        if (preferences.getSound_config().equalsIgnoreCase("Emergency")) {
            soundConfig.setSelection(0);
        } else if (preferences.getSound_config().equalsIgnoreCase("Alert Sound")) {
            soundConfig.setSelection(1);
        }

        soundConfig.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                value = (String) adapter.getItem(position);
                preferences.setSound_config(value);

                System.out.println("value" + value);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        ll_soundConfig.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                long clickTime = System.currentTimeMillis();
                if (clickTime - lastClickTime < DOUBLE_CLICK_TIME_DELTA) {
                    try {
                        playStopClickListner();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    lastClickTime = 0;
                } else {
                    lastClickTime = clickTime;
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        finish();
    }
}
