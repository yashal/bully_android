package com.nexgen.bullybeware.activity;

import android.Manifest;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.nexgen.bullybeware.R;
import com.nexgen.bullybeware.adapter.FriendListAdapter;
import com.nexgen.bullybeware.adapter.MyRecordingsAdapter;
import com.nexgen.bullybeware.model.RestClient;
import com.nexgen.bullybeware.pojo.DeleteLatLongPojo;
import com.nexgen.bullybeware.pojo.FriendListObjectPojo;
import com.nexgen.bullybeware.pojo.FriendListPojo;
import com.nexgen.bullybeware.pojo.RemoveFriendPojo;
import com.nexgen.bullybeware.service.SinchService;
import com.nexgen.bullybeware.util.BullyConstants;
import com.nexgen.bullybeware.util.BullyDialogs;
import com.nexgen.bullybeware.util.BullyPreferences;
import com.nexgen.bullybeware.util.BullyToast;
import com.nexgen.bullybeware.util.ConnectionDetector;
import com.sinch.android.rtc.SinchError;
import com.sinch.android.rtc.calling.Call;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by umax desktop on 12/21/2016.
 */

public class FriendListActivity extends BaseActivity implements View.OnClickListener {

    private FriendListActivity ctx = this;
    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private FriendListAdapter adapter;
    private ArrayList<FriendListObjectPojo> friendlist;
    private LinearLayout ll_addfriends,ll_noDatafound;
    String from;
    private BullyPreferences preferences;
    String userName;
    private int friend_limit = 0;
    private static final long DOUBLE_CLICK_TIME_DELTA = 1000;//milliseconds
    long lastClickTime = 0;
    private LinearLayout ll_friendlist;



    // flag for Internet connection status
    Boolean isInternetPresent = false;

    // Connection detector class
    ConnectionDetector cd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.friendlist_activity);

        init();
        setOnClickListner();

        setDrawerbackIcon("Pick a Friend");

    }

    private void init() {

        ll_addfriends = (LinearLayout) findViewById(R.id.ll_addfriends);

        ll_noDatafound = (LinearLayout) findViewById(R.id.friendlist_ll_NoData);

        ll_friendlist = (LinearLayout) findViewById(R.id.ll_friendlist);


        BullyToast.showToast(FriendListActivity.this, getResources().getString(R.string.emergency_alaram));


        preferences = new BullyPreferences(FriendListActivity.this);

        cd = new ConnectionDetector(getApplicationContext());
        // get Internet status
        isInternetPresent = cd.isConnectingToInternet();

        from = getIntent().getStringExtra("from");

        if (from != null) {
            if (from.equalsIgnoreCase("main")) {
                ll_addfriends.setVisibility(View.VISIBLE);
            } else {
                ll_addfriends.setVisibility(View.GONE);
            }
        }
        friendlist = new ArrayList<FriendListObjectPojo>();

        mRecyclerView = (RecyclerView) findViewById(R.id.friendlist_recycler_view);

        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(FriendListActivity.this);
        mRecyclerView.setLayoutManager(mLayoutManager);

    }

    @Override
    protected void onResume() {
        super.onResume();
        getFriendList();
    }

    private void setOnClickListner() {

        ll_addfriends.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (friend_limit >= 10){
                  BullyToast.showToast(FriendListActivity.this,"You cannot add more than 10 friends.");
                } else {
                    addFriendDialog();
                }
            }
        });

        ll_friendlist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                long clickTime = System.currentTimeMillis();
                if (clickTime - lastClickTime < DOUBLE_CLICK_TIME_DELTA) {
                    try {
                        playStopClickListner();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    lastClickTime = 0;
                } else {
                    lastClickTime = clickTime;
                }
            }
        });

    }

    private void getFriendList(){
        if (isInternetPresent) {
            final ProgressDialog d = BullyDialogs.showLoading(FriendListActivity.this);
            d.setCanceledOnTouchOutside(false);

            RestClient.get().getFriendlist(preferences.getStudentId(), new Callback<FriendListPojo>() {
                @Override
                public void success(FriendListPojo friendListPojo, Response response) {
                    if (friendListPojo != null){
                        if (friendListPojo.getStatus()){

                            friendlist = friendListPojo.getObject();

                            friend_limit = friendlist.size();

                            if (friendlist.size()>0)
                            {
                                adapter = new FriendListAdapter(FriendListActivity.this, friendlist, from);
                                mRecyclerView.setAdapter(adapter);
                                adapter.notifyDataSetChanged();
                                ll_noDatafound.setVisibility(View.GONE);
                            }
                            else
                            {
                                ll_noDatafound.setVisibility(View.VISIBLE);
                            }

                        } else {
//                            followMeDialog();
                            ll_noDatafound.setVisibility(View.VISIBLE);
                        }
                        d.dismiss();
                    }
                    else
                    {
                        d.dismiss();
                        BullyToast.showToast(FriendListActivity.this, BullyConstants.SERVER_NOT_RESPOND);
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    d.dismiss();
                }
            });
        } else {
            ll_noDatafound.setVisibility(View.VISIBLE);
            customAlertDialog("BullyBeware", BullyConstants.NO_INTERNET_CONNECTED);
        }
    }

    private void removeFriend(String friend_id, final int pos){
        if (isInternetPresent) {
            final ProgressDialog d = BullyDialogs.showLoading(FriendListActivity.this);
            d.setCanceledOnTouchOutside(false);

            RestClient.get().removeFriend(preferences.getStudentId(), friend_id , new Callback<RemoveFriendPojo>() {
                @Override
                public void success(RemoveFriendPojo removeFriendPojo, Response response) {
                    if (removeFriendPojo != null){
                        if (removeFriendPojo.getStatus()){
                            BullyToast.showToast(FriendListActivity.this,removeFriendPojo.getMessage());

                            friendlist.remove(pos);

                            adapter.notifyDataSetChanged();

                            friend_limit = friendlist.size();
                            if (friendlist.size() == 0)
                            {
                                ll_noDatafound.setVisibility(View.VISIBLE);
                            }

                        }else{

                        }
                        d.dismiss();
                    }
                    else
                    {
                        d.dismiss();
                        BullyToast.showToast(FriendListActivity.this, BullyConstants.SERVER_NOT_RESPOND);
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    d.dismiss();
                }
            });
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.video_call:
/*
                String email = (String) view.getTag(R.string.key);

                        try {
                            if (email != null){
                                userName = email;

                                Call call = getSinchServiceInterface().callUserVideo(userName);
                                String callId = call.getCallId();

                                TrackEvent("Follow Me",userName,preferences.getEmail());

                                Intent callScreen = new Intent(this, CallScreenActivity.class);
                                callScreen.putExtra(SinchService.CALL_ID, callId);
                                startActivity(callScreen);
                                overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
                            } else {
                                BullyToast.showToast(FriendListActivity.this,"User not found");
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                }*/
                break;
            case R.id.dial:

                String dial_call = (String) view.getTag(R.string.dial_number);

                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse("tel:"+dial_call));
                    if (ActivityCompat.checkSelfPermission(FriendListActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        return;
                    }
                    startActivity(callIntent);

                break;
            case R.id.edit:

                int position = (int) view.getTag(R.string.edit_position);

                String fn = friendlist.get(position).getFirst_name();
                String ln = friendlist.get(position).getLast_name();
                String email_id = friendlist.get(position).getEmail();
                String mobile = friendlist.get(position).getMobile();
                String thumbnail = friendlist.get(position).getThumbnail();
                String friend = friendlist.get(position).getFriend_id();

                Intent intent = new Intent(FriendListActivity.this,AddfriendInfoActivity.class);
                intent.putExtra("first_name",fn);
                intent.putExtra("last_name",ln);
                intent.putExtra("email",email_id);
                intent.putExtra("mobile",mobile);
                intent.putExtra("thumbnail",thumbnail);
                intent.putExtra("friend_id",friend);
                intent.putExtra("from","edit");
                startActivity(intent);

                finish();
                overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);

                break;

            case R.id.delete:

                String friend_id = (String) view.getTag(R.string.key_delete);
                int pos = (int) view.getTag(R.string.delete_position);

                DeleteFrienddialog(friend_id,pos);
                break;

            case R.id.ll_friendlist:
                if (from != null) {
                    if (from.equalsIgnoreCase("main")) {
                    } else {
                        String email = (String) view.getTag(R.string.key);

                        try {
                            if (email != null){
                                userName = email;

                                Call call = getSinchServiceInterface().callUserVideo(userName);
                                String callId = call.getCallId();

                                TrackEvent("Follow Me",userName,preferences.getEmail());

                                Intent callScreen = new Intent(this, CallScreenActivity.class);
                                callScreen.putExtra(SinchService.CALL_ID, callId);
                                startActivity(callScreen);
                                overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
                            } else {
                                BullyToast.showToast(FriendListActivity.this,"User not found");
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        finish();
    }

    private void DeleteFrienddialog(final String friend_id1, final int position) {
        dialogLogout = new Dialog(FriendListActivity.this, android.R.style.Theme_Translucent_NoTitleBar);
        dialogLogout.setContentView(R.layout.logout_dialog);
        dialogLogout.show();

        TextView dialog_header = (TextView) dialogLogout.findViewById(R.id.logout_dialog_header);
        dialog_header.setVisibility(View.VISIBLE);
        dialog_header.setText("Remove friend");
        LinearLayout ll_submit = (LinearLayout) dialogLogout.findViewById(R.id.ll_dialog_submit);
        LinearLayout ll_cancel = (LinearLayout) dialogLogout.findViewById(R.id.ll_dialog_cancel);
        ImageView iView_cross = (ImageView) dialogLogout.findViewById(R.id.dialog_header_cross);
        TextView tv_submit = (TextView) dialogLogout.findViewById(R.id.dialog_submit);
        tv_submit.setText("YES");
        TextView dialog_text = (TextView) dialogLogout.findViewById(R.id.dialog_text);
        dialog_text.setVisibility(View.VISIBLE);
        dialog_text.setText("Are you sure you want to delete?");


        ll_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                removeFriend(friend_id1,position);

                dialogLogout.dismiss();
            }
        });
        iView_cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogLogout.dismiss();
            }
        });

        ll_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogLogout.dismiss();
            }
        });
    }

    /*private void followMeDialog() {
        dialogLogout = new Dialog(FriendListActivity.this, android.R.style.Theme_Translucent_NoTitleBar);
        dialogLogout.setContentView(R.layout.logout_dialog);
        dialogLogout.show();

        TextView dialog_header = (TextView) dialogLogout.findViewById(R.id.logout_dialog_header);
        dialog_header.setVisibility(View.VISIBLE);
        dialog_header.setText("Follow Me");
        LinearLayout ll_submit = (LinearLayout) dialogLogout.findViewById(R.id.ll_dialog_submit);
        LinearLayout ll_cancel = (LinearLayout) dialogLogout.findViewById(R.id.ll_dialog_cancel);
        ImageView iView_cross = (ImageView) dialogLogout.findViewById(R.id.dialog_header_cross);
        TextView tv_submit = (TextView) dialogLogout.findViewById(R.id.dialog_submit);
        tv_submit.setText("YES");
        TextView dialog_text = (TextView) dialogLogout.findViewById(R.id.dialog_text);
        dialog_text.setVisibility(View.VISIBLE);
        dialog_text.setText("Before you can use Follow-Me, you must identify a friend.Have your friend go to setup menu and select QRC Code? You go to setup menu and hit Friends and add friend by scanning their code.");

        ll_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addFriendDialog();
                dialogLogout.dismiss();
            }
        });
        iView_cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogLogout.dismiss();
            }
        });

        ll_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogLogout.dismiss();
            }
        });
    }*/

    private void addFriendDialog() {
        final Dialog addFriendDialog = new Dialog(ctx, android.R.style.Theme_Translucent_NoTitleBar);
        addFriendDialog.setContentView(R.layout.add_friend_dialog);
        addFriendDialog.show();

        LinearLayout ll_dialog = (LinearLayout) addFriendDialog.findViewById(R.id.ll_dialog);

        TextView by_name = (TextView) addFriendDialog.findViewById(R.id.by_name);
        TextView by_scan = (TextView) addFriendDialog.findViewById(R.id.by_scan);

        ll_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addFriendDialog.dismiss();
            }
        });

        by_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(FriendListActivity.this, SearchByNameActivity.class);
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
                addFriendDialog.dismiss();
            }
        });

        by_scan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(FriendListActivity.this, QRCodeScanActivity.class);
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
                addFriendDialog.dismiss();
            }
        });
    }
}
