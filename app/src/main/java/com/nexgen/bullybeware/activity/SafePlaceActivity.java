package com.nexgen.bullybeware.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;

import com.nexgen.bullybeware.R;
import com.nexgen.bullybeware.adapter.SafeListAdapter;
import com.nexgen.bullybeware.model.RestClient;
import com.nexgen.bullybeware.pojo.SafeListPojo;
import com.nexgen.bullybeware.util.BullyConstants;
import com.nexgen.bullybeware.util.BullyDialogs;
import com.nexgen.bullybeware.util.BullyPreferences;
import com.nexgen.bullybeware.util.BullyToast;
import com.nexgen.bullybeware.util.ConnectionDetector;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class SafePlaceActivity extends BaseActivity implements View.OnClickListener {

    private SafePlaceActivity ctx = this;
    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerView.Adapter mAdapter;
    private BullyPreferences preferences;
    private String school_id;
    private ConnectionDetector cd;
    private LinearLayout no_safe_list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_safe_list);

        setDrawerbackIcon("Safe Place List");
        preferences = new BullyPreferences(ctx);
        mRecyclerView = (RecyclerView)findViewById(R.id.safeplace_recycler_view);
        cd = new ConnectionDetector(getApplicationContext());
        no_safe_list = (LinearLayout)findViewById(R.id.no_safe_list);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(ctx);
        mRecyclerView.setLayoutManager(mLayoutManager);
        if (preferences.getSchool_id().length() > 0) {
            school_id = preferences.getSchool_id();
        } else {
            school_id = preferences.getSchool_email();
        }
        System.out.println("hhhh yashal school is is "+school_id);
        if (school_id.length() >0) {
            getSafeFriendList();
        }
        else
        {
            customAlertDialog(getResources().getString(R.string.safe_friend_list), getResources().getString(R.string.err_school_id));
        }
    }

    public void getSafeFriendList() {
        if (cd.isConnectingToInternet()) {
            final ProgressDialog d = BullyDialogs.showLoading(ctx);
            d.setCanceledOnTouchOutside(false);
            RestClient.get().getSafePlace(school_id, new Callback<SafeListPojo>() {
                @Override
                public void success(SafeListPojo basePojo, Response response) {

                    if (basePojo != null) {
                        if (basePojo.getStatus()) {
                            if (basePojo.getObject() != null) {
                                if (basePojo.getObject().size() > 0) {
                                    mAdapter = new SafeListAdapter(ctx, basePojo.getObject());
                                    mRecyclerView.setAdapter(mAdapter);
                                    no_safe_list.setVisibility(View.GONE);
                                } else {
                                    no_safe_list.setVisibility(View.VISIBLE);
                                }
                            } else {
                                no_safe_list.setVisibility(View.VISIBLE);
                            }

                        }
                        else
                        {
                            if (basePojo.getMessage()!= null)
                            {
                                customAlertDialog(getResources().getString(R.string.safe_friend_list), basePojo.getMessage());
                            }
                        }
                    } else {
                        BullyToast.showToast(ctx, BullyConstants.SERVER_NOT_RESPOND);
                    }
                    d.dismiss();
                }

                @Override
                public void failure(RetrofitError error) {
                    d.dismiss();
                }
            });
        } else {
            customAlertDialog("BullyBeware", BullyConstants.NO_INTERNET_CONNECTED);
        }
    }


    @Override
    public void onClick(View v) {
        String place_name =(String) v.getTag(R.string.key_state);
        String contact_name =(String) v.getTag(R.string.key);
        String phone_number =(String) v.getTag(R.string.key_delete);
        String email_address =(String) v.getTag(R.string.delete_position);
        System.out.println("hh yashal place_name " + place_name + "contact_name " + contact_name + "phone_number " + phone_number + "email_address " + email_address);

        Intent mIntent = new Intent(ctx, SafePlaceDetailActivity.class);
        mIntent.putExtra("placeName", place_name);
        mIntent.putExtra("contactName", contact_name);
        mIntent.putExtra("phoneNumber", phone_number);
        mIntent.putExtra("emailAddress", email_address);
        startActivity(mIntent);
    }
}
