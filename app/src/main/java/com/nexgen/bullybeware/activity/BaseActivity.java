package com.nexgen.bullybeware.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.Messenger;
import android.os.PersistableBundle;
import android.os.RemoteException;
import android.provider.MediaStore;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.plus.People;
import com.google.android.gms.plus.Plus;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.services.youtube.YouTubeScopes;
import com.nexgen.bullybeware.R;
import com.nexgen.bullybeware.YoutubeVideoUpload.UploadService;
import com.nexgen.bullybeware.database.DatabaseHandler;
import com.nexgen.bullybeware.fragment.DrawerFragment;
import com.nexgen.bullybeware.model.RestClient;
import com.nexgen.bullybeware.pojo.ImageUploadPojo;
import com.nexgen.bullybeware.pojo.ImagelistStatusPojo;
import com.nexgen.bullybeware.pojo.VideolistStatusPojo;
import com.nexgen.bullybeware.service.AlermService;
import com.nexgen.bullybeware.service.AudioUploadService;
import com.nexgen.bullybeware.service.ImageUploadService;
import com.nexgen.bullybeware.service.SinchService;
import com.nexgen.bullybeware.util.BullyApplication;
import com.nexgen.bullybeware.util.BullyConstants;
import com.nexgen.bullybeware.util.BullyPreferences;
import com.nexgen.bullybeware.util.BullyToast;
import com.nexgen.bullybeware.util.ConnectionDetector;
import com.nexgen.bullybeware.util.ImageLoadingUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by quepplin1 on 8/11/2016.
 */

public class BaseActivity extends AppCompatActivity implements ServiceConnection {

    boolean is_playing, is_stopped;
    private Messenger serviceMessenger;
    private ServiceConnection serviceConnection;
    private ArrayList<ImagelistStatusPojo> imagestatus;
    private ArrayList<ImagelistStatusPojo> audiostatus;
    private ArrayList<VideolistStatusPojo> checkedvideolist = new ArrayList<VideolistStatusPojo>();
    private DatabaseHandler db;
    String form_id, ACCOUNT_KEY, Submit_id;
    private BullyPreferences preferences;
    // flag for Internet connection status
    Boolean isInternetPresent = false;
    Dialog dialogLogout;
    boolean flag = false;
    private DrawerLayout mDrawerLayout;

    // Connection detector class
    ConnectionDetector cd;

    private SinchService.SinchServiceInterface mSinchServiceInterface;
    DrawerFragment fragment;


    BroadcastReceiver imagereciver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            imagestatus = (ArrayList<ImagelistStatusPojo>) intent.getSerializableExtra("image_status");

            form_id = intent.getStringExtra("form_id");

            Submit_id = intent.getStringExtra("Submit_id");

            customImageAlertDialog("Report Incident", "Some images failed to upload.Please check your internet connection and click retry");
        }
    };

    BroadcastReceiver audioReciver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            audiostatus = (ArrayList<ImagelistStatusPojo>) intent.getSerializableExtra("audio_status");

            form_id = intent.getStringExtra("form_id");

            Submit_id = intent.getStringExtra("Submit_id");

            customAudioAlertDialog("Report Incident", "Some audios failed to upload.Please check your internet connection and click retry");
        }
    };

    BroadcastReceiver uploadReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.i("VideoReportActivity", "Video_Service");
            System.out.println("xx Video_Service");

            form_id = intent.getStringExtra("form_id");

            ACCOUNT_KEY = intent.getStringExtra("ACCOUNT_KEY");

            Submit_id = intent.getStringExtra("Submit_id");

            checkedvideolist = (ArrayList<VideolistStatusPojo>) intent.getSerializableExtra("video_status");

            customVideoAlertDialog("Report Incident", "Some videos failed to upload.Please check your internet connection and click retry");

        }
    };
    BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            switch (intent.getAction()) {
                case AlermService.ACTION_IS_PLAYING:
                    is_playing = intent.getBooleanExtra("is_playing", false);
                    is_stopped = intent.getBooleanExtra("is_stopped", false);
                    System.out.println("xxxxxxxxxxxxxxxxxx hello is_playing " + is_playing);
                    System.out.println("xxxxxxxxxxxxxxxxxx hello is_stopped " + is_stopped);

                    break;
            }
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);

        cd = new ConnectionDetector(getApplicationContext());
        // get Internet status
        isInternetPresent = cd.isConnectingToInternet();
    }

    public void setDrawerAndToolbar(String text) {
        LinearLayout drawer = (LinearLayout) findViewById(R.id.drawerButton);
        TextView tv_title = (TextView) findViewById(R.id.tv_title);
        tv_title.setText(text);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        fragment = (DrawerFragment) getSupportFragmentManager()
                .findFragmentById(R.id.fragment_drawer);
        fragment.setUp(mDrawerLayout);

        drawer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mDrawerLayout != null && mDrawerLayout.isDrawerOpen(Gravity.LEFT)) {
                    mDrawerLayout.closeDrawer(Gravity.LEFT);
                } else {
                    mDrawerLayout.openDrawer(Gravity.LEFT);
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (mDrawerLayout != null && mDrawerLayout.isDrawerOpen(Gravity.LEFT)) {
            fragment.selectedPos = -1;
            mDrawerLayout.closeDrawers();
        } else {
            super.onBackPressed();
            finish();
            overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_right);
        }
    }

    public void setDrawerbackIcon(String text) {

        LinearLayout drawer_back = (LinearLayout) findViewById(R.id.drawerBack);
        LinearLayout drawer = (LinearLayout) findViewById(R.id.drawerButton);
        LinearLayout drawer_cross = (LinearLayout) findViewById(R.id.drawerCross);

        TextView tv_title = (TextView) findViewById(R.id.tv_title);
        tv_title.setText(text);

        drawer_back.setVisibility(View.VISIBLE);
        drawer.setVisibility(View.GONE);
        drawer_cross.setVisibility(View.GONE);

        drawer_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    public void setDrawerCrossIcon(String text) {
        LinearLayout drawer_back = (LinearLayout) findViewById(R.id.drawerBack);
        LinearLayout drawer = (LinearLayout) findViewById(R.id.drawerButton);
        LinearLayout drawer_cross = (LinearLayout) findViewById(R.id.drawerCross);

        TextView tv_title = (TextView) findViewById(R.id.tv_title);
        tv_title.setText(text);

        drawer_back.setVisibility(View.GONE);
        drawer.setVisibility(View.GONE);
        drawer_cross.setVisibility(View.VISIBLE);

        drawer_cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    public void setDrawerDataCross(String text) {
        LinearLayout drawer_back = (LinearLayout) findViewById(R.id.drawerBack);
        LinearLayout drawer = (LinearLayout) findViewById(R.id.drawerButton);
        LinearLayout drawer_cross = (LinearLayout) findViewById(R.id.drawerCross);

        TextView tv_title = (TextView) findViewById(R.id.tv_title);
        tv_title.setText(text);

        drawer_back.setVisibility(View.GONE);
        drawer.setVisibility(View.GONE);
        drawer_cross.setVisibility(View.VISIBLE);

       /* drawer_cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });*/
    }


    public void hideSoftKeyboard() {
        if (getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }

    public void customAlertDialog(String title, String text) {
        dialogLogout = new Dialog(BaseActivity.this, android.R.style.Theme_Translucent_NoTitleBar);
        dialogLogout.setContentView(R.layout.custom_dialog);
        dialogLogout.show();

        EditText fp_edtxt_email = (EditText) dialogLogout.findViewById(R.id.fp_edtxt_email);
        fp_edtxt_email.setVisibility(View.GONE);

        TextView dialog_header = (TextView) dialogLogout.findViewById(R.id.dialog_header);
        dialog_header.setVisibility(View.VISIBLE);
        dialog_header.setText(title);
        ImageView iView_cross = (ImageView) dialogLogout.findViewById(R.id.dialog_header_cross);
        LinearLayout dialog_submitLL = (LinearLayout) dialogLogout.findViewById(R.id.dialog_submitLL);
        TextView tv_submit = (TextView) dialogLogout.findViewById(R.id.dialog_submit);
        tv_submit.setText("OK");
        TextView dialog_text = (TextView) dialogLogout.findViewById(R.id.dialog_text);
        dialog_text.setVisibility(View.VISIBLE);
        dialog_text.setText(text);


        dialog_submitLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialogLogout.dismiss();
            }
        });
        iView_cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogLogout.dismiss();
            }
        });
    }

    public void customImageAlertDialog(String title, String text) {
        dialogLogout = new Dialog(this, android.R.style.Theme_Translucent_NoTitleBar);
        dialogLogout.setContentView(R.layout.custom_dialog);
        dialogLogout.show();

        EditText fp_edtxt_email = (EditText) dialogLogout.findViewById(R.id.fp_edtxt_email);
        fp_edtxt_email.setVisibility(View.GONE);

        TextView dialog_header = (TextView) dialogLogout.findViewById(R.id.dialog_header);
        dialog_header.setVisibility(View.VISIBLE);
        dialog_header.setText(title);
        ImageView iView_cross = (ImageView) dialogLogout.findViewById(R.id.dialog_header_cross);
        TextView tv_submit = (TextView) dialogLogout.findViewById(R.id.dialog_submit);
        tv_submit.setText("Retry");
        TextView dialog_text = (TextView) dialogLogout.findViewById(R.id.dialog_text);
        dialog_text.setVisibility(View.VISIBLE);
        dialog_text.setText(text);


        tv_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isInternetPresent = cd.isConnectingToInternet();
                if (isInternetPresent) {
                    Intent uploadIntent = new Intent(getApplicationContext(), ImageUploadService.class);
                    uploadIntent.putExtra("form_id", form_id);
                    uploadIntent.putExtra("Submit_id", Submit_id);
                    uploadIntent.putExtra("image_status", imagestatus);
                    startService(uploadIntent);
                } else {
                    BullyToast.showToast(BaseActivity.this, BullyConstants.NO_INTERNET_CONNECTED);
                }
                dialogLogout.dismiss();
            }
        });
        iView_cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogLogout.dismiss();
            }
        });
    }

    public void customAudioAlertDialog(String title, String text) {
        dialogLogout = new Dialog(this, android.R.style.Theme_Translucent_NoTitleBar);
        dialogLogout.setContentView(R.layout.custom_dialog);
        dialogLogout.show();

        EditText fp_edtxt_email = (EditText) dialogLogout.findViewById(R.id.fp_edtxt_email);
        fp_edtxt_email.setVisibility(View.GONE);

        TextView dialog_header = (TextView) dialogLogout.findViewById(R.id.dialog_header);
        dialog_header.setVisibility(View.VISIBLE);
        dialog_header.setText(title);
        ImageView iView_cross = (ImageView) dialogLogout.findViewById(R.id.dialog_header_cross);
        TextView tv_submit = (TextView) dialogLogout.findViewById(R.id.dialog_submit);
        tv_submit.setText("Retry");
        TextView dialog_text = (TextView) dialogLogout.findViewById(R.id.dialog_text);
        dialog_text.setVisibility(View.VISIBLE);
        dialog_text.setText(text);


        tv_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isInternetPresent = cd.isConnectingToInternet();
                if (isInternetPresent) {
                    Intent uploadIntent = new Intent(getApplicationContext(), AudioUploadService.class);
                    uploadIntent.putExtra("form_id", form_id);
                    uploadIntent.putExtra("Submit_id", Submit_id);
                    uploadIntent.putExtra("audio_status", audiostatus);
                    startService(uploadIntent);
                } else {
                    BullyToast.showToast(BaseActivity.this, BullyConstants.NO_INTERNET_CONNECTED);
                }

                dialogLogout.dismiss();
            }
        });
        iView_cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogLogout.dismiss();
            }
        });
    }

    public void customVideoAlertDialog(String title, String text) {
        dialogLogout = new Dialog(this, android.R.style.Theme_Translucent_NoTitleBar);
        dialogLogout.setContentView(R.layout.custom_dialog);
        dialogLogout.show();

        EditText fp_edtxt_email = (EditText) dialogLogout.findViewById(R.id.fp_edtxt_email);
        fp_edtxt_email.setVisibility(View.GONE);

        TextView dialog_header = (TextView) dialogLogout.findViewById(R.id.dialog_header);
        dialog_header.setVisibility(View.VISIBLE);
        dialog_header.setText(title);
        ImageView iView_cross = (ImageView) dialogLogout.findViewById(R.id.dialog_header_cross);
        TextView tv_submit = (TextView) dialogLogout.findViewById(R.id.dialog_submit);
        tv_submit.setText("Retry");
        TextView dialog_text = (TextView) dialogLogout.findViewById(R.id.dialog_text);
        dialog_text.setVisibility(View.VISIBLE);
        dialog_text.setText(text);


        tv_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isInternetPresent = cd.isConnectingToInternet();

                if (isInternetPresent) {
                    Intent uploadIntent = new Intent(getApplicationContext(), UploadService.class);
                    uploadIntent.putExtra("ACCOUNT_KEY", ACCOUNT_KEY);
                    uploadIntent.putExtra("Submit_id", Submit_id);
                    uploadIntent.putExtra("video_status", checkedvideolist);
                    uploadIntent.putExtra("form_id", form_id);
                    startService(uploadIntent);
                } else {
                    BullyToast.showToast(BaseActivity.this, BullyConstants.NO_INTERNET_CONNECTED);
                }
                dialogLogout.dismiss();
            }
        });
        iView_cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogLogout.dismiss();
            }
        });
    }

    public class GetBitmapTask extends AsyncTask<String, Void, Bitmap> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Bitmap doInBackground(String... arg0) {
            Bitmap bitmap = getBitmapFromURL(arg0[0]);
            return bitmap;
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
        }

    }

    public Bitmap getBitmapFromURL(String strURL) {
        try {
            URL url = new URL(strURL);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            // sendBigPictureStyleNotification();
            return myBitmap;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Bitmap createBitmap_ScriptIntrinsicBlur(Bitmap src, float r) {

        //Radius range (0 < r <= 25)
        if (r <= 0) {
            r = 0.1f;
        } else if (r > 25) {
            r = 25.0f;
        }

        Bitmap bitmap = Bitmap.createBitmap(
                src.getWidth(), src.getHeight(),
                Bitmap.Config.ARGB_8888);

        RenderScript renderScript = RenderScript.create(this);

        Allocation blurInput = Allocation.createFromBitmap(renderScript, src);
        Allocation blurOutput = Allocation.createFromBitmap(renderScript, bitmap);

        ScriptIntrinsicBlur blur = ScriptIntrinsicBlur.create(renderScript,
                Element.U8_4(renderScript));
        blur.setInput(blurInput);
        blur.setRadius(r);
        blur.forEach(blurOutput);

        blurOutput.copyTo(bitmap);
        renderScript.destroy();
        return bitmap;
    }

    @Override
    protected void onResume() {
        super.onResume();

        cd = new ConnectionDetector(getApplicationContext());
        // get Internet status
        isInternetPresent = cd.isConnectingToInternet();

        db = new DatabaseHandler(BaseActivity.this);

        preferences = new BullyPreferences(BaseActivity.this);


        LocalBroadcastManager.getInstance(this).registerReceiver(imagereciver, new IntentFilter("Image_Service"));

        LocalBroadcastManager.getInstance(this).registerReceiver(audioReciver, new IntentFilter("Audio_Service"));

        LocalBroadcastManager.getInstance(this).registerReceiver(uploadReceiver, new IntentFilter("Video_Service"));

        Intent intent = new Intent(this, AlermService.class);
        serviceConnection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                serviceMessenger = new Messenger(service);
                Message message = new Message();
                message.what = AlermService.IS_PLAYING;
                try {
                    serviceMessenger.send(message);
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {

            }
        };
        startService(intent);
        bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE | BIND_ADJUST_WITH_ACTIVITY);

        IntentFilter filter = new IntentFilter();
        filter.addAction(AlermService.ACTION_IS_PLAYING);
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver, filter);
    }

    public void playStopClickListner() {
        if (!is_playing) {
            Message message = new Message();
            message.what = AlermService.START;

            try {
                serviceMessenger.send(message);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        } else {
            Message message = new Message();
            message.what = AlermService.STOP;
            try {
                serviceMessenger.send(message);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    public void TrackEvent(String category,String action,String label) {

        Tracker t = ((BullyApplication) this.getApplication())
                .getTracker(BullyApplication.TrackerName.APP_TRACKER);
        t.send(new HitBuilders.EventBuilder()
                .setCategory(category)
                .setAction(action)
                .setLabel(label)
                .build());
    }

    public void stopClickListner() {
        if (is_playing) {
            Message message = new Message();
            message.what = AlermService.STOP;
            try {
                serviceMessenger.send(message);
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
    }

    public String compressImage(String imageUri, int flag) {
        String filePath;
        if (flag == 1) {
            filePath = getRealPathFromURI(imageUri);
        }

        filePath = imageUri;
        Bitmap scaledBitmap = null;

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        Bitmap bmp = BitmapFactory.decodeFile(filePath, options);

        int actualHeight = options.outHeight;
        int actualWidth = options.outWidth;
        float maxHeight = 816.0f;
        float maxWidth = 612.0f;
        float imgRatio = actualWidth / actualHeight;
        float maxRatio = maxWidth / maxHeight;

        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            if (imgRatio < maxRatio) {
                imgRatio = maxHeight / actualHeight;
                actualWidth = (int) (imgRatio * actualWidth);
                actualHeight = (int) maxHeight;
            } else if (imgRatio > maxRatio) {
                imgRatio = maxWidth / actualWidth;
                actualHeight = (int) (imgRatio * actualHeight);
                actualWidth = (int) maxWidth;
            } else {
                actualHeight = (int) maxHeight;
                actualWidth = (int) maxWidth;

            }
        }

        ImageLoadingUtils utils = new ImageLoadingUtils(this);
        options.inSampleSize = utils.calculateInSampleSize(options, actualWidth, actualHeight);
        options.inJustDecodeBounds = false;
        options.inDither = false;
//        options.inPurgeable = true;
//        options.inInputShareable = true;
        options.inTempStorage = new byte[16 * 1024];

        try {
            bmp = BitmapFactory.decodeFile(filePath, options);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();

        }
        try {
            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        }

        float ratioX = actualWidth / (float) options.outWidth;
        float ratioY = actualHeight / (float) options.outHeight;
        float middleX = actualWidth / 2.0f;
        float middleY = actualHeight / 2.0f;

        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2, middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));


        ExifInterface exif;
        try {
            exif = new ExifInterface(filePath);

            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 0);
            Log.d("EXIF", "Exif: " + orientation);
            Matrix matrix = new Matrix();
            if (orientation == 6) {
                matrix.postRotate(90);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 3) {
                matrix.postRotate(180);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 8) {
                matrix.postRotate(270);
                Log.d("EXIF", "Exif: " + orientation);
            }
            scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix, true);
        } catch (IOException e) {
            e.printStackTrace();
        }
        FileOutputStream out = null;
        String filename = getFilename();
        try {
            out = new FileOutputStream(filename);
            scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 80, out);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return filename;
    }

    public String getFilename() {
        File file = new File(Environment.getExternalStorageDirectory().getPath(), "MyFolder/Images");
        if (!file.exists()) {
            file.mkdirs();
        }
        String uriSting = (file.getAbsolutePath() + "/" + System.currentTimeMillis() + ".jpg");
        return uriSting;
    }

    public String getRealPathFromURI(String contentURI) {
        Uri contentUri = Uri.parse(contentURI);
        Cursor cursor = this.getContentResolver().query(contentUri, null, null, null, null);
        if (cursor == null) {
            return contentUri.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(idx);
        }
    }

    public void makePostRequestSnack(String message, int size) {

        Toast.makeText(getApplicationContext(), String.valueOf(size) + " " + message, Toast.LENGTH_SHORT).show();

        //    finish();
    }

    /**
     * method that will return whether the permission is accepted. By default it is true if the user is using a device below
     * version 23
     *
     * @param permission
     * @return
     */

    public boolean hasPermission(String permission) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (canMakeSmores()) {
                return (checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED);
            }
        }

        return true;
    }

    /**
     * method to determine whether we have asked
     * for this permission before.. if we have, we do not want to ask again.
     * They either rejected us or later removed the permission.
     *
     * @param permission
     * @return
     */
    public boolean shouldWeAsk(String permission, SharedPreferences sharedPreferences) {
        return (sharedPreferences.getBoolean(permission, true));
    }

    /**
     * we will save that we have already asked the user
     *
     * @param permission
     */
    public void markAsAsked(String permission, SharedPreferences sharedPreferences) {
        sharedPreferences.edit().putBoolean(permission, false).apply();
    }

    /**
     * We may want to ask the user again at their request.. Let's clear the
     * marked as seen preference for that permission.
     *
     * @param permission
     */
    public void clearMarkAsAsked(String permission, SharedPreferences sharedPreferences) {
        sharedPreferences.edit().putBoolean(permission, true).apply();
    }


    /**
     * This method is used to determine the permissions we do not have accepted yet and ones that we have not already
     * bugged the user about.  This comes in handle when you are asking for multiple permissions at once.
     *
     * @param wanted
     * @return
     */
    public ArrayList<String> findUnAskedPermissions(ArrayList<String> wanted, SharedPreferences sharedPreferences) {
        ArrayList<String> result = new ArrayList<String>();

        for (String perm : wanted) {
            if (!hasPermission(perm) && shouldWeAsk(perm, sharedPreferences)) {
                result.add(perm);
            }
        }

        return result;
    }

    public boolean canMakeSmores() {
        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);
    }

    /**
     * this will return us all the permissions we have previously asked for but
     * currently do not have permission to use. This may be because they declined us
     * or later revoked our permission. This becomes useful when you want to tell the user
     * what permissions they declined and why they cannot use a feature.
     *
     * @param wanted
     * @return
     */
    public ArrayList<String> findRejectedPermissions(ArrayList<String> wanted, SharedPreferences sharedPreferences) {
        ArrayList<String> result = new ArrayList<String>();

        for (String perm : wanted) {
            if (!hasPermission(perm) && !shouldWeAsk(perm, sharedPreferences)) {
                result.add(perm);
            }
        }

        return result;
    }

    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
        if (serviceConnection != null) {
            unbindService(serviceConnection);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (dialogLogout != null) {
            if (dialogLogout.isShowing()) {
                dialogLogout.dismiss();
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        try {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(imagereciver);

            LocalBroadcastManager.getInstance(this).unregisterReceiver(audioReciver);

            LocalBroadcastManager.getInstance(BaseActivity.this).unregisterReceiver(uploadReceiver);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // video calling

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getApplicationContext().bindService(new Intent(this, SinchService.class), this,
                BIND_AUTO_CREATE);
    }


    @Override
    public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        if (SinchService.class.getName().equals(componentName.getClassName())) {
            mSinchServiceInterface = (SinchService.SinchServiceInterface) iBinder;
            onServiceConnected();
        }
    }

    @Override
    public void onServiceDisconnected(ComponentName componentName) {
        if (SinchService.class.getName().equals(componentName.getClassName())) {
            mSinchServiceInterface = null;
            onServiceDisconnected();
        }
    }

    protected void onServiceConnected() {
        // for subclasses
    }

    protected void onServiceDisconnected() {
        // for subclasses
    }

    protected SinchService.SinchServiceInterface getSinchServiceInterface() {
        return mSinchServiceInterface;
    }

    public boolean checkValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }
}
