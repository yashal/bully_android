package com.nexgen.bullybeware.activity;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.hardware.display.DisplayManager;
import android.hardware.display.VirtualDisplay;
import android.location.Location;
import android.location.LocationManager;
import android.media.AudioManager;
import android.media.MediaRecorder;
import android.media.projection.MediaProjection;
import android.media.projection.MediaProjectionManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.SparseIntArray;
import android.view.Surface;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.nexgen.bullybeware.R;
import com.nexgen.bullybeware.model.RestClient;
import com.nexgen.bullybeware.pojo.DeleteLatLongPojo;
import com.nexgen.bullybeware.pojo.GetLatLongObject;
import com.nexgen.bullybeware.pojo.ImagelistStatusPojo;
import com.nexgen.bullybeware.service.AlermService;
import com.nexgen.bullybeware.service.LocationGetService;
import com.nexgen.bullybeware.service.LocationSendService;
import com.nexgen.bullybeware.service.SinchService;
import com.nexgen.bullybeware.util.AudioPlayer;
import com.nexgen.bullybeware.util.BullyPreferences;
import com.nexgen.bullybeware.util.BullyToast;
import com.nexgen.bullybeware.util.ConnectionDetector;
import com.sinch.android.rtc.AudioController;
import com.sinch.android.rtc.PushPair;
import com.sinch.android.rtc.calling.Call;
import com.sinch.android.rtc.calling.CallEndCause;
import com.sinch.android.rtc.calling.CallState;
import com.sinch.android.rtc.video.VideoCallListener;
import com.sinch.android.rtc.video.VideoController;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class CallScreenActivity extends BaseActivity implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

    static final String TAG = CallScreenActivity.class.getSimpleName();
    static final String CALL_START_TIME = "callStartTime";
    static final String ADDED_LISTENER = "addedListener";

    private AudioPlayer mAudioPlayer;
    private Timer mTimer;
    private UpdateCallDurationTask mDurationTask;

    private String mCallId;
    private long mCallStart = 0;
    private boolean mAddedListener = false;
    private boolean mVideoViewsAdded = false;

    private TextView mCallDuration;
    private TextView mCallState;
    private TextView mCallerName;

    private LinearLayout ll_name;
    private GoogleMap googleMap;

    LocationRequest mLocationRequest;
    GoogleApiClient mGoogleApiClient;
    LatLng latLng;
    Marker currLocationMarker;

    private BullyPreferences preferences;
    private Double latitude, longitude;

    ArrayList<LatLng> MarkerPoints;
    private MediaProjection mMediaProjection;
    private VirtualDisplay mVirtualDisplay;
    private MediaProjectionCallback mMediaProjectionCallback;

    private MediaProjectionManager mProjectionManager;

    private static final int DISPLAY_WIDTH = 720;
    private static final int DISPLAY_HEIGHT = 1280;

    private int mScreenDensity;

    Timer timer;

    private ImageView flip_camera;

    private LinearLayout ll_map;

    ArrayList<GetLatLongObject> points1 = new ArrayList<GetLatLongObject>();
    private LinearLayout ll_recieveCall;
    private LinearLayout bottomPanel;
    private TextView recieveCall_lat, recieveCall_long;
    private ImageView recievecall_hangupButton, startRecording;

    // screen video recording Variable Declaration start
    private static final int REQUEST_CODE = 1000;
    private MediaRecorder mMediaRecorder;
    private static final SparseIntArray ORIENTATIONS = new SparseIntArray();
    private static final int REQUEST_PERMISSIONS = 10;
    private boolean flag_for_rec = true;
    private String start_recording = "no";

    static {
        ORIENTATIONS.append(Surface.ROTATION_0, 90);
        ORIENTATIONS.append(Surface.ROTATION_90, 0);
        ORIENTATIONS.append(Surface.ROTATION_180, 270);
        ORIENTATIONS.append(Surface.ROTATION_270, 180);
    }

    BroadcastReceiver locationreciver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            try {
                if ((ArrayList<GetLatLongObject>) intent.getSerializableExtra("LatLongobject") != null) {
                    points1 = (ArrayList<GetLatLongObject>) intent.getSerializableExtra("LatLongobject");
                }

                latitude = points1.get(0).getLatitude();
                longitude = points1.get(0).getLongitude();

                System.out.println("llll lat" + latitude);
                System.out.println("llll long" + longitude);

                recieveCall_long.setText("LG:" + String.valueOf(longitude));
                recieveCall_lat.setText("LA:" + String.valueOf(latitude));

                udpateMapView(latitude, longitude);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        private void udpateMapView(Double latitude, Double longitude) {
            CameraPosition cameraPosition = null;
            if (googleMap != null) {
                googleMap.clear();
                googleMap.addMarker(new MarkerOptions().position(new LatLng(latitude, longitude)).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE))
                        .title("").snippet(""));
                cameraPosition = new CameraPosition.Builder().target(
                        new LatLng(latitude, longitude)).zoom(18).build();
                if (cameraPosition != null) {
                    googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition), 2000, null);
                }

                ArrayList<LatLng> points = new ArrayList<LatLng>();

                for (int j = 0; j < points1.size(); j++) {
                    points.add(new LatLng(points1.get(j).getLatitude(), points1.get(j).getLongitude()));
                }
                PolylineOptions lineOptions = new PolylineOptions();
                lineOptions.addAll(points);
                lineOptions.width(8);
                lineOptions.color(Color.BLUE);
                googleMap.addPolyline(lineOptions);
            }
        }
    };


    @Override
    public void onMapReady(final GoogleMap googleMap) {
        this.googleMap = googleMap;

        //Initialize Google Play Services
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                buildGoogleApiClient();
                //  googleMap.setMyLocationEnabled(true);
            }
        } else {
            buildGoogleApiClient();
            // googleMap .setMyLocationEnabled(true);
        }
    }

    protected synchronized void buildGoogleApiClient() {
        //     Toast.makeText(this, "buildGoogleApiClient", Toast.LENGTH_SHORT).show();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        mGoogleApiClient.connect();
    }

    @Override
    protected void onResume() {
        super.onResume();

        LocalBroadcastManager.getInstance(this).registerReceiver(locationreciver, new IntentFilter("Location_Service"));
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        //    Toast.makeText(this, "onConnected", Toast.LENGTH_SHORT).show();
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(5000); //5 seconds
        mLocationRequest.setFastestInterval(3000); //3 seconds
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        //mLocationRequest.setSmallestDisplacement(0.1F); //1/10 meter

        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, (LocationListener) this);
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {
        //place marker at current position
        //mGoogleMap.clear();
        if (currLocationMarker != null) {
            currLocationMarker.remove();
        }

        System.out.println("yyyy1 lat" + location.getLatitude());
        System.out.println("yyyy1 long" + location.getLongitude());

        try {

            Intent intent = new Intent(CallScreenActivity.this, LocationSendService.class);
            intent.putExtra("student_id", preferences.getStudentId());
            intent.putExtra("student_email", preferences.getEmail());
            intent.putExtra("lat", location.getLatitude());
            intent.putExtra("long", location.getLongitude());
            startService(intent);

            if (getIntent().getStringExtra("from") == null) {
                latLng = new LatLng(location.getLatitude(), location.getLongitude());
                MarkerOptions markerOptions = new MarkerOptions();
                markerOptions.position(latLng);
                markerOptions.title("Current Position");
                markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA));
                currLocationMarker = googleMap.addMarker(markerOptions);
                //move map camera
                googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                //   googleMap.animateCamera(CameraUpdateFactory.zoomTo(18));
                CameraPosition cameraPosition = new CameraPosition.Builder()
                        .target(latLng).zoom(18).build();
                googleMap.animateCamera(CameraUpdateFactory
                        .newCameraPosition(cameraPosition));

            } else {

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void deletelatlong(String student_id) {

        RestClient.get().deletelatlong(student_id, new Callback<DeleteLatLongPojo>() {
            @Override
            public void success(DeleteLatLongPojo deleteLatLongPojo, Response response) {

            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
    }


    private class UpdateCallDurationTask extends TimerTask {

        @Override
        public void run() {
            CallScreenActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    updateCallDuration();
                }
            });
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putLong(CALL_START_TIME, mCallStart);
        savedInstanceState.putBoolean(ADDED_LISTENER, mAddedListener);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        mCallStart = savedInstanceState.getLong(CALL_START_TIME);
        mAddedListener = savedInstanceState.getBoolean(ADDED_LISTENER);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.callscreen);

        mAudioPlayer = new AudioPlayer(this);
        mCallDuration = (TextView) findViewById(R.id.callDuration);
        mCallerName = (TextView) findViewById(R.id.remoteUser);
        mCallState = (TextView) findViewById(R.id.callState);

        preferences = new BullyPreferences(CallScreenActivity.this);

        cd = new ConnectionDetector(getApplicationContext());

        // get Internet status
        isInternetPresent = cd.isConnectingToInternet();

        ll_recieveCall = (LinearLayout) findViewById(R.id.recieve_call_layout);

        flip_camera = (ImageView) findViewById(R.id.flip_camera);

        bottomPanel = (LinearLayout) findViewById(R.id.bottomPanel);

        startRecording = (ImageView) findViewById(R.id.recording);

        recievecall_hangupButton = (ImageView) findViewById(R.id.recievecall_hangupButton);

        recieveCall_lat = (TextView) findViewById(R.id.recieveCall_lat);
        recieveCall_long = (TextView) findViewById(R.id.recieveCall_long);

        ll_map = (LinearLayout) findViewById(R.id.ll_map);

        AudioManager am = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        am.setStreamVolume(
                AudioManager.STREAM_VOICE_CALL,
                am.getStreamMaxVolume(AudioManager.STREAM_VOICE_CALL),
                0);
        am.setMode(AudioManager.STREAM_VOICE_CALL);
        am.setSpeakerphoneOn(true);


        // screen video Variable Initialize recording Start
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        mScreenDensity = metrics.densityDpi;

        deletelatlong(preferences.getStudentId());

        ll_name = (LinearLayout) findViewById(R.id.ll_name_title);

        mMediaRecorder = new MediaRecorder();

        mProjectionManager = (MediaProjectionManager) getSystemService
                (Context.MEDIA_PROJECTION_SERVICE);

        statusCheck();

        Button endCallButton = (Button) findViewById(R.id.end_calling);

        endCallButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                endCall();
            }
        });

        recievecall_hangupButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                endCall();
            }
        });

        startRecording.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (flag_for_rec) {
                    onToggleScreenShare(true);
                } else {
                    onToggleScreenShare(false);
                }
            }
        });

        timer = new Timer();

        if (getIntent().getStringExtra("from") != null) {
            String from = getIntent().getStringExtra("from");
            final String friend_id = getIntent().getStringExtra("friend_id");

            if (from.equalsIgnoreCase("incoming")) {
                ll_map.setVisibility(View.VISIBLE);
                ll_recieveCall.setVisibility(View.VISIBLE);
                bottomPanel.setVisibility(View.GONE);

                timer.scheduleAtFixedRate(new TimerTask() {
                    @Override
                    public void run() {
                        Intent intent1 = new Intent(CallScreenActivity.this, LocationGetService.class);
                        intent1.putExtra("friend_email", friend_id);
                        startService(intent1);
                    }
                }, 0, 5000);
            }
        }

        //       mCallId = getIntent().getStringExtra(SinchService.CALL_ID);

        if (getIntent().getStringExtra(SinchService.CALL_ID) != null) {
            mCallId = getIntent().getStringExtra(SinchService.CALL_ID);
            if (savedInstanceState == null) {
                mCallStart = System.currentTimeMillis();
            }

            // screen video method call
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkLocationPermission();
        }
        // Initializing
        MarkerPoints = new ArrayList<>();

        MapFragment mapFragment = (MapFragment) getFragmentManager()
                .findFragmentById(R.id.userlocation_map);
        mapFragment.getMapAsync(this);
    }

    public void statusCheck() {
        final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            buildAlertMessageNoGps();

        }
    }

    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Your GPS seems to be disabled, do you want to enable it?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }


    @Override
    public void onServiceConnected() {
        if (mCallId != null){
            Call call = getSinchServiceInterface().getCall(mCallId);
            if (call != null) {
                if (!mAddedListener) {
                    call.addCallListener(new SinchCallListener());
              /*  setVolumeControlStream(AudioManager.STREAM_VOICE_CALL);
                AudioController audioController = getSinchServiceInterface().getAudioController();
                audioController.enableSpeaker();*/
                    mAddedListener = true;
                }
            } else {
                Log.e(TAG, "Started with invalid callId, aborting.");
                finish();
            }

            updateUI();
        }
    }

    private void updateUI() {
        if (getSinchServiceInterface() == null) {
            return; // early
        }

        Call call = getSinchServiceInterface().getCall(mCallId);
        if (call != null) {
            mCallerName.setText(call.getRemoteUserId() + "" + " is connected");
            mCallState.setText(call.getState().toString());

            if (call.getState() == CallState.ESTABLISHED) {

                addVideoViews();
            }
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        System.out.println("hh on stop");
        if (mMediaRecorder != null) {
            try {
                mMediaRecorder.stop();
                mMediaRecorder.reset();

            } catch (RuntimeException stopException) {
                //handle cleanup here
                stopException.printStackTrace();
            }
        }
        mDurationTask.cancel();
        mTimer.cancel();
        timer.cancel();
        removeVideoViews();
    }


    @Override
    public void onStart() {
        super.onStart();
        mTimer = new Timer();
        mDurationTask = new UpdateCallDurationTask();
        mTimer.schedule(mDurationTask, 0, 500);
        updateUI();
    }

    @Override
    public void onBackPressed() {
        // User should exit activity by ending call, not by going back.
    }

    private void endCall() {
        mAudioPlayer.stopProgressTone();
        Call call = getSinchServiceInterface().getCall(mCallId);
        if (call != null) {
            call.hangup();
        }
        finish();
    }

    private String formatTimespan(long timespan) {
        long totalSeconds = timespan / 1000;
        long minutes = totalSeconds / 60;
        long seconds = totalSeconds % 60;
        return String.format(Locale.US, "%02d:%02d", minutes, seconds);
    }

    private void updateCallDuration() {
        if (mCallStart > 0) {
            mCallDuration.setText(formatTimespan(System.currentTimeMillis() - mCallStart));
        }
    }

    private void addVideoViews() {
        if (mVideoViewsAdded || getSinchServiceInterface() == null) {
            return; //early
        }

        final VideoController vc = getSinchServiceInterface().getVideoController();
        if (vc != null) {
            ll_name.setVisibility(View.GONE);
            RelativeLayout localView = (RelativeLayout) findViewById(R.id.rl_localVideo);
            localView.addView(vc.getLocalView());
            flip_camera.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    vc.toggleCaptureDevicePosition();
                }
            });
           /* localView.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    vc.toggleCaptureDevicePosition();
                }
            });*/
            LinearLayout view = (LinearLayout) findViewById(R.id.remoteVideo);
            view.addView(vc.getRemoteView());
            mVideoViewsAdded = true;
        }
    }

    private void removeVideoViews() {
        if (getSinchServiceInterface() == null) {
            return; // early
        }

        VideoController vc = getSinchServiceInterface().getVideoController();
        if (vc != null) {
            LinearLayout view = (LinearLayout) findViewById(R.id.remoteVideo);
            view.removeView(vc.getRemoteView());

            RelativeLayout localView = (RelativeLayout) findViewById(R.id.rl_localVideo);
            localView.removeView(vc.getLocalView());
            mVideoViewsAdded = false;
        }
    }

    private class SinchCallListener implements VideoCallListener {

        @Override
        public void onCallEnded(Call call) {
            CallEndCause cause = call.getDetails().getEndCause();
            Log.d(TAG, "Call ended. Reason: " + cause.toString());
            mAudioPlayer.stopProgressTone();
            setVolumeControlStream(AudioManager.USE_DEFAULT_STREAM_TYPE);
            String endMsg = "Call ended: " + call.getDetails().toString();
//            Toast.makeText(CallScreenActivity.this, endMsg, Toast.LENGTH_LONG).show();

            endCall();
        }

        @Override
        public void onCallEstablished(Call call) {
            Log.d(TAG, "Call established");
            isInternetPresent = cd.isConnectingToInternet();
            if (isInternetPresent){
                mAudioPlayer.stopProgressTone();
                mCallState.setText(call.getState().toString());


                mCallStart = System.currentTimeMillis();
                Log.d(TAG, "Call offered video: " + call.getDetails().isVideoOffered());
            } else {
                System.out.println("not gone");
            }

        }

        @Override
        public void onCallProgressing(Call call) {
            Log.d(TAG, "Call progressing");
            mAudioPlayer.playProgressTone();
        }

        @Override
        public void onShouldSendPushNotification(Call call, List<PushPair> pushPairs) {
            // Send a push through your push provider here, e.g. GCM
        }

        @Override
        public void onVideoTrackAdded(Call call) {
            Log.d(TAG, "Video track added");
            addVideoViews();
        }
    }

    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;

    public boolean checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Asking user if explanation is needed
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

                //Prompt the user once explanation has been shown
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);


            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted. Do the
                    // contacts-related task you need to do.
                    if (ContextCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {

                        if (mGoogleApiClient == null) {
                            buildGoogleApiClient();
                        }
                        googleMap.setMyLocationEnabled(true);
                    }

                } else {

                    // Permission denied, Disable the functionality that depends on this permission.
                    Toast.makeText(this, "permission denied", Toast.LENGTH_LONG).show();
                }
            }
            case REQUEST_PERMISSIONS: {
                if ((grantResults.length > 0) && (grantResults[0] +
                        grantResults[1]) == PackageManager.PERMISSION_GRANTED) {
                    flag_for_rec = true;
                    onToggleScreenShare(flag_for_rec);
                    System.out.println("hh yashal1");
                } else {
                    System.out.println("hh yashal2");
                    Snackbar.make(findViewById(android.R.id.content), R.string.label_permissions,
                            Snackbar.LENGTH_INDEFINITE).setAction("ENABLE",
                            new OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent intent = new Intent();
                                    intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                    intent.addCategory(Intent.CATEGORY_DEFAULT);
                                    intent.setData(Uri.parse("package:" + getPackageName()));
                                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
                                    startActivity(intent);
                                }
                            }).show();
                }
                return;
            }
        }

        // other 'case' lines to check for other permissions this app might request.
        // You can add here other case statements according to your requirement.

    }

    // screen capture recording
 /*   private void automaticVideoRecording() {
        if (ContextCompat.checkSelfPermission(CallScreenActivity.this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE) + ContextCompat
                .checkSelfPermission(CallScreenActivity.this,
                        Manifest.permission.RECORD_AUDIO)
                != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale
                    (CallScreenActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) ||
                    ActivityCompat.shouldShowRequestPermissionRationale
                            (CallScreenActivity.this, Manifest.permission.RECORD_AUDIO)) {
                Snackbar.make(findViewById(android.R.id.content), R.string.label_permissions,
                        Snackbar.LENGTH_INDEFINITE).setAction("ENABLE",
                        new OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                ActivityCompat.requestPermissions(CallScreenActivity.this,
                                        new String[]{Manifest.permission
                                                .WRITE_EXTERNAL_STORAGE, Manifest.permission.RECORD_AUDIO},
                                        REQUEST_PERMISSIONS);
                            }
                        }).show();
            } else {
                ActivityCompat.requestPermissions(CallScreenActivity.this,
                        new String[]{Manifest.permission
                                .WRITE_EXTERNAL_STORAGE, Manifest.permission.RECORD_AUDIO},
                        REQUEST_PERMISSIONS);
            }
        } else {
            flag_for_rec =true;
            onToggleScreenShare(flag_for_rec);
        }
    }*/

    public void onToggleScreenShare(Boolean mFlag) {
        if (mFlag) {
            System.out.println("hh  recording start");
            shareScreen();

            start_recording = "start";


        } else {
            System.out.println("hh recording stop");
            mMediaRecorder.stop();
            mMediaRecorder.reset();
            Log.v(TAG, "Stopping Recording");
            stopScreenSharing();

            start_recording = "stop";

        }
    }

    private void shareScreen() {
        if (mMediaProjection == null) {
            startActivityForResult(mProjectionManager.createScreenCaptureIntent(), REQUEST_CODE);
            initRecorder();
            return;
        }
        mVirtualDisplay = createVirtualDisplay();
        //   mMediaRecorder.start();
    }

    private VirtualDisplay createVirtualDisplay() {
        return mMediaProjection.createVirtualDisplay("MainActivity",
                DISPLAY_WIDTH, DISPLAY_HEIGHT, mScreenDensity,
                DisplayManager.VIRTUAL_DISPLAY_FLAG_AUTO_MIRROR,
                mMediaRecorder.getSurface(), null /*Callbacks*/, null
                /*Handler*/);
    }

    private void initRecorder() {
        try {
           /* int audioSource = MediaRecorder.AudioSource.VOICE_CALL;
            mMediaRecorder.setAudioSource(audioSource);*/
            mMediaRecorder.setVideoSource(MediaRecorder.VideoSource.SURFACE);

            mMediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);

            //       mMediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
            mMediaRecorder.setVideoEncoder(MediaRecorder.VideoEncoder.H264);

            mMediaRecorder.setVideoSize(DISPLAY_WIDTH, DISPLAY_HEIGHT);
            mMediaRecorder.setVideoFrameRate(30);

            String filepath = Environment.getExternalStorageDirectory().getPath();
            File file = new File(filepath, "/BullyApp/VideoRecord");
            if (!file.exists()) {
                if (!file.mkdirs()) {
                    Log.d("MyCameraApp", "failed to create directory");
                }
            }
            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss")
                    .format(new Date());
            File mediaFile;
            mediaFile = new File(file.getPath() + File.separator
                    + "Bully_" + timeStamp + ".mp4");

            mMediaRecorder.setOutputFile(mediaFile.getAbsolutePath());

            mMediaRecorder.setVideoEncodingBitRate(512 * 1000);

      /*      int rotation = getWindowManager().getDefaultDisplay().getRotation();
            int orientation = ORIENTATIONS.get(rotation + 90);
            mMediaRecorder.setOrientationHint(orientation);*/

            mMediaRecorder.prepare();
            mMediaRecorder.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private class MediaProjectionCallback extends MediaProjection.Callback {
        @Override
        public void onStop() {
            /*if (mToggleButton.isChecked()) {
                mToggleButton.setChecked(false);
                mMediaRecorder.stop();
                mMediaRecorder.reset();
                Log.v(TAG, "Recording Stopped");
            }*/
            mMediaProjection = null;
            stopScreenSharing();
        }
    }

    private void stopScreenSharing() {
        if (mVirtualDisplay == null) {
            return;
        }
        mVirtualDisplay.release();
        //mMediaRecorder.release(); //If used: mMediaRecorder object cannot
        // be reused again
        destroyMediaProjection();

        startRecording.setImageResource(R.mipmap.start_recording);

        flag_for_rec = true;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        destroyMediaProjection();
        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
    }

    private void destroyMediaProjection() {
        if (mMediaProjection != null) {
            mMediaProjection.unregisterCallback(mMediaProjectionCallback);
            mMediaProjection.stop();
            mMediaProjection = null;
        }
        Log.i(TAG, "MediaProjection Stopped");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_CODE) {
                mMediaProjectionCallback = new MediaProjectionCallback();
                mMediaProjection = mProjectionManager.getMediaProjection(resultCode, data);
                mMediaProjection.registerCallback(mMediaProjectionCallback, null);
                mVirtualDisplay = createVirtualDisplay();

                startRecording.setImageResource(R.mipmap.stop_recording);

                flag_for_rec = false;
            }
        }
    }
}
