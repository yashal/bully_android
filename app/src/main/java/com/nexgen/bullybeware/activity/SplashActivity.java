package com.nexgen.bullybeware.activity;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.PermissionChecker;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.GlideDrawableImageViewTarget;
import com.crashlytics.android.Crashlytics;
import com.nexgen.bullybeware.R;
import com.nexgen.bullybeware.model.RestClient;
import com.nexgen.bullybeware.pojo.GetProfilePojo;
import com.nexgen.bullybeware.util.BullyConstants;
import com.nexgen.bullybeware.util.BullyDialogs;
import com.nexgen.bullybeware.util.BullyPreferences;
import com.nexgen.bullybeware.util.BullyToast;

import io.fabric.sdk.android.Fabric;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.RECORD_AUDIO;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

/**
 * Created by quepplin1 on 9/7/2016.
 */
public class SplashActivity extends BaseActivity implements ActivityCompat.OnRequestPermissionsResultCallback,PermissionResultCallback {

    private ImageView iView_gif;
    private BullyPreferences preferences;
    Handler handler;
    Runnable myRunnable;
    public static int OVERLAY_PERMISSION_REQ_CODE = 1234;
    // list of permissions

    ArrayList<String> permissions=new ArrayList<>();

    PermissionUtils permissionUtils;
    private TextView tv_version;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_splash);

        tv_version = (TextView) findViewById(R.id.tv_version);

        PackageInfo pInfo = null;
        try {
            pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);

            String version =                pInfo.versionName;

            tv_version.setText("Version "+ version);

        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        String packageName = getApplicationContext().getPackageName();
        try {
            pInfo = getPackageManager().getPackageInfo(packageName, PackageManager.GET_SIGNATURES);
            for (Signature signature : pInfo.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                System.out.println("hh hash key " + Base64.encodeToString(md.digest(),
                        Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        permissionUtils=new PermissionUtils(SplashActivity.this);

        permissions.add(Manifest.permission.ACCESS_FINE_LOCATION);
        permissions.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        permissions.add(Manifest.permission.CAMERA);
        permissions.add(Manifest.permission.RECORD_AUDIO);
        permissions.add(Manifest.permission.CALL_PHONE);
        permissions.add(Manifest.permission.GET_ACCOUNTS);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!Settings.canDrawOverlays(this)) {
                Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                        Uri.parse("package:" + getPackageName()));
                startActivityForResult(intent, OVERLAY_PERMISSION_REQ_CODE);
            }else{
                permissionUtils.check_permission(permissions,"All the permissions are required to access the app functionality",1);
            }
        }else {
            permissionUtils.check_permission(permissions,"All the permissions are required to access the app functionality",1);
        }
//        permissionUtils.check_permission(permissions,"All the permissions are required to access the app functionality",1);

        preferences = new BullyPreferences(SplashActivity.this);

        setProfileInfo();

        iView_gif = (ImageView) findViewById(R.id.iView_gif);
        GlideDrawableImageViewTarget imageViewTarget = new GlideDrawableImageViewTarget(iView_gif);
        Glide.with(this).load(R.raw.nexgen).into(imageViewTarget);

        iView_gif.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri uri = Uri.parse("http://www.nexgenusa.com/");
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(intent);
            }
        });
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == OVERLAY_PERMISSION_REQ_CODE) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                permissionUtils.check_permission(permissions,"All the permissions are required to access the app functionality",1);
                if (!Settings.canDrawOverlays(this)) {


                    // SYSTEM_ALERT_WINDOW permission not granted...
  //                  Toast.makeText(SplashActivity.this, "SYSTEM_ALERT_WINDOW permission not granted...", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }
    private void startActivity(){

        handler=  new Handler();
        myRunnable = new Runnable() {
            public void run() {
                if (preferences.getStudentId().length() > 0){
                    Intent intent = new Intent(SplashActivity.this,MainActivity.class);
                    startActivity(intent);
                    finish();
                    overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
                } else {
                    Intent intent = new Intent(SplashActivity.this,LoginActivity.class);
                    startActivity(intent);
                    finish();
                    overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
                }
            }
        };
        handler.postDelayed(myRunnable,2000);
    }

    private void setProfileInfo(){
        RestClient.get().getProfileInfo(preferences.getStudentId(), new Callback<GetProfilePojo>() {
            @Override
            public void success(final GetProfilePojo getProfilePojo, Response response) {
                if (getProfilePojo != null){
                    if (getProfilePojo.getStatus()) {

                        preferences.setSchool_id(getProfilePojo.getObject().getSchool_id());
                        preferences.setSchool_email(getProfilePojo.getObject().getSchool_email());

                    }
                }
                else
                {
                    BullyToast.showToast(SplashActivity.this, BullyConstants.SERVER_NOT_RESPOND);
                }
            }
            @Override
            public void failure(RetrofitError error) {

            }
        });
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();

        try {
            handler.removeCallbacks(myRunnable);
        } catch (Exception e){
            e.printStackTrace();
        }

        finish();

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {

        // redirects to utils

        permissionUtils.onRequestPermissionsResult(requestCode,permissions,grantResults);

    }

    @Override
    public void PermissionGranted(int request_code) {
        Log.i("PERMISSION","GRANTED");
        startActivity();
    }

    @Override
    public void PartialPermissionGranted(int request_code, ArrayList<String> granted_permissions) {
        Log.i("PERMISSION PARTIALLY","GRANTED");

    }

    @Override
    public void PermissionDenied(int request_code) {
        Log.i("PERMISSION","DENIED");

    }

    @Override
    public void NeverAskAgain(int request_code) {
        Log.i("PERMISSION","NEVER ASK AGAIN");

    }
}
