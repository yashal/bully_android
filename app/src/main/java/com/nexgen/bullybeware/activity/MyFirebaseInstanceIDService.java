/**
 * Copyright 2016 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.nexgen.bullybeware.activity;

import android.content.pm.PackageInfo;
import android.provider.Settings;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.nexgen.bullybeware.util.BullyPreferences;
//import com.mirchimurga.ui.model.RestClient;
//import com.mirchimurga.ui.pojo.BasePojo;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {

    private static final String TAG = "MyFirebaseIIDService";
    private BullyPreferences preferences;

    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the InstanceID token
     * is initially generated so this is where you would retrieve the token.
     */
    // [START refresh_token]
    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        try {
            String refreshedToken = FirebaseInstanceId.getInstance().getToken();
            Log.d(TAG, "Refreshed token: " + refreshedToken);
            String m_androidId = Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);
            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            String versionName = pInfo.versionName;
            // If you want to send messages to this application instance or
            // manage this apps subscriptions on the server side, send the
            // Instance ID token to your app server.
    //           sendRegistrationToServer(refreshedToken, m_androidId,versionName);

            System.out.println("yyy token"+ refreshedToken);
            System.out.println("yyy android_id"+ m_androidId);

            preferences = new BullyPreferences(this);

            preferences.setDevice_id(refreshedToken);
            preferences.setImei(m_androidId);

            preferences.setDevice_type(versionName);

        }catch(Exception e)
        {
            Log.d(TAG, "Failed to complete token refresh", e);
        }
    }
    // [END refresh_token]

    /**
     * Persist token to third-party servers.
     *
     * Modify this method to associate the user's FCM InstanceID token with any server-side account
     * maintained by your application.
     *
     * @param token The new token.
     */
//    private void sendRegistrationToServer(String token, String deviceId,String app_version) {
//        // TODO: Implement this method to send token to your app server.
//        RestClient.get().signinAndroid("android",token,deviceId,app_version, new Callback<BasePojo>() {
//            @Override
//            public void success(BasePojo basePojo, Response response) {
//                System.out.println("registration done :"+ basePojo.getMessage());
//            }
//
//            @Override
//            public void failure(RetrofitError error) {
//
//            }
//        });
//    }
}
