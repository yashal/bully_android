package com.nexgen.bullybeware.activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nexgen.bullybeware.R;
import com.nexgen.bullybeware.model.RestClient;
import com.nexgen.bullybeware.pojo.DeleteSubmissionData;
import com.nexgen.bullybeware.pojo.SelfHarmPojo;
import com.nexgen.bullybeware.util.BullyConstants;
import com.nexgen.bullybeware.util.BullyDialogs;
import com.nexgen.bullybeware.util.BullyPreferences;
import com.nexgen.bullybeware.util.BullyToast;
import com.nexgen.bullybeware.util.ConnectionDetector;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by umax desktop on 12/26/2016.
 */

public class SelfHarmActivity extends BaseActivity {

    private SelfHarmActivity ctx = this;
    private EditText edtxt_name, edtxt_description;
    private BullyPreferences preferences;
    private ImageView tv_submit;
    String school_id;
    private LinearLayout ll_selfHarm;
    private static final long DOUBLE_CLICK_TIME_DELTA = 2000; //milliseconds
    long lastClickTime = 0;

    // flag for Internet connection status
    Boolean isInternetPresent = false;

    // Connection detector class
    ConnectionDetector cd;

    private TextView what_should_i_do;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_self_harm);

        setDrawerbackIcon("Self Harm");

        init();
        setClick();
    }

    private void init() {
        edtxt_name = (EditText) findViewById(R.id.sh_edtxt_name);
        edtxt_description = (EditText) findViewById(R.id.sh_edtxt_description);

        tv_submit = (ImageView) findViewById(R.id.sh_submit);
        what_should_i_do = (TextView) findViewById(R.id.what_should_i_do);

        ll_selfHarm = (LinearLayout) findViewById(R.id.ll_selfharmList);

        BullyToast.showToast(SelfHarmActivity.this, getResources().getString(R.string.emergency_alaram));

        preferences = new BullyPreferences(SelfHarmActivity.this);

        cd = new ConnectionDetector(getApplicationContext());
        // get Internet status
        isInternetPresent = cd.isConnectingToInternet();

    }

    private void setClick() {

        tv_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isInternetPresent = cd.isConnectingToInternet();
                if (isInternetPresent){
                    if (edtxt_name.getText().toString().trim().isEmpty()) {
                        customAlertDialog("Self Harm", getResources().getString(R.string.err_self_harm_name));
                    } else if (edtxt_description.getText().toString().isEmpty()) {
                        customAlertDialog("Self Harm", getResources().getString(R.string.err_self_harm_description));
                    } else {
                        SubmitInfo();
                    }
                } else {
                    customAlertDialog("BullyBeware", BullyConstants.NO_INTERNET_CONNECTED);
                }

            }
        });

        ll_selfHarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                long clickTime = System.currentTimeMillis();
                if (clickTime - lastClickTime < DOUBLE_CLICK_TIME_DELTA) {
                    try {
                        playStopClickListner();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    lastClickTime = 0;
                } else {
                    lastClickTime = clickTime;
                }
            }
        });

        what_should_i_do.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mIntent = new Intent(ctx, SituationResponseActivity.class);
                startActivity(mIntent);
            }
        });
    }

    private void SubmitInfo() {

            final ProgressDialog d = BullyDialogs.showLoading(SelfHarmActivity.this);
            d.setCanceledOnTouchOutside(false);

            if (preferences.getSchool_id().length() > 0) {
                school_id = preferences.getSchool_id();
            } else {
                school_id = "0";
            }

            RestClient.get().selfharm(preferences.getStudentId(), school_id, edtxt_name.getText().toString(), edtxt_description.getText().toString(), new Callback<SelfHarmPojo>() {
                @Override
                public void success(SelfHarmPojo selfHarmPojo, Response response) {

                    if (selfHarmPojo != null) {
                        if (selfHarmPojo.getStatus()) {

                            BullyToast.showToast(SelfHarmActivity.this, selfHarmPojo.getMessage());

                            Intent intent = new Intent(SelfHarmActivity.this, SelfHarmListActivity.class);
                            startActivity(intent);
                            finish();

                            TrackEvent("Self Harm",edtxt_name.getText().toString(),preferences.getEmail());

                        } else {
                            customAlertDialog("CHANGE PASSWORD", selfHarmPojo.getMessage());
                        }
                        d.dismiss();
                    }
                    else
                    {
                        d.dismiss();
                        BullyToast.showToast(SelfHarmActivity.this, BullyConstants.SERVER_NOT_RESPOND);
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    d.dismiss();
                }
            });
    }

    public void customAlertDialog(String title, String text) {
        dialogLogout = new Dialog(SelfHarmActivity.this, android.R.style.Theme_Translucent_NoTitleBar);
        dialogLogout.setContentView(R.layout.custom_dialog);
        dialogLogout.show();

        EditText fp_edtxt_email = (EditText) dialogLogout.findViewById(R.id.fp_edtxt_email);
        fp_edtxt_email.setVisibility(View.GONE);

        TextView dialog_header = (TextView) dialogLogout.findViewById(R.id.dialog_header);
        dialog_header.setVisibility(View.VISIBLE);
        dialog_header.setText(title);
        ImageView iView_cross = (ImageView) dialogLogout.findViewById(R.id.dialog_header_cross);
        TextView tv_submit = (TextView) dialogLogout.findViewById(R.id.dialog_submit);
        tv_submit.setText("OK");
        TextView dialog_text = (TextView) dialogLogout.findViewById(R.id.dialog_text);
        dialog_text.setVisibility(View.VISIBLE);
        dialog_text.setText(text);

        tv_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialogLogout.dismiss();
            }
        });
        iView_cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogLogout.dismiss();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        Intent intent = new Intent(SelfHarmActivity.this,SelfHarmListActivity.class);
        startActivity(intent);
        finish();
    }
}
