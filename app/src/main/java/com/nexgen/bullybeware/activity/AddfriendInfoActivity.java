package com.nexgen.bullybeware.activity;

import android.app.ProgressDialog;
import android.app.Service;
import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.nexgen.bullybeware.R;
import com.nexgen.bullybeware.model.FileUploadSer;
import com.nexgen.bullybeware.model.RestClient;
import com.nexgen.bullybeware.model.ServiceGenerator;
import com.nexgen.bullybeware.pojo.AddFriendInfoPojo;
import com.nexgen.bullybeware.util.BullyConstants;
import com.nexgen.bullybeware.util.BullyDialogs;
import com.nexgen.bullybeware.util.BullyPreferences;
import com.nexgen.bullybeware.util.BullyToast;
import com.nexgen.bullybeware.util.UsPhoneNumberFormatter;


import java.io.File;
import java.lang.ref.WeakReference;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedFile;

/**
 * Created by umax desktop on 1/30/2017.
 */

public class AddfriendInfoActivity extends BaseActivity {

    private EditText edtxt_fName,edtxt_lName,edtxt_email,edtxt_number;
    private CircularImageView iView_user;
    private TextView tv_submit;
    private String first_name,last_name,email,mobile,thumbnail,friend_id;
    private String from;

    private BullyPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_friends);

        setDrawerbackIcon("Add Friend");

        init();
        setData();
        setOnClick();

    }

    private void init(){

        iView_user = (CircularImageView) findViewById(R.id.addfriend_circleView);
        tv_submit = (TextView) findViewById(R.id.addFriend_submit);

        edtxt_fName = (EditText) findViewById(R.id.addFriend_edtxt_fname);
        edtxt_lName = (EditText) findViewById(R.id.addFriend_edtxt_lname);
        edtxt_email = (EditText) findViewById(R.id.addFriend_edtxt_email);
        edtxt_number = (EditText) findViewById(R.id.addFriend_edtxt_mobile);

        preferences = new BullyPreferences(AddfriendInfoActivity.this);

//        edtxt_number.addTextChangedListener(new PhoneNumberTextWatcher(edtxt_number));
        UsPhoneNumberFormatter addLineNumberFormatter = new UsPhoneNumberFormatter(
                new WeakReference<EditText>(edtxt_number));
        edtxt_number.addTextChangedListener(addLineNumberFormatter);
    }

    private void setData(){

        if (getIntent().getStringExtra("first_name") != null)
        first_name = getIntent().getStringExtra("first_name");

        if (getIntent().getStringExtra("last_name") != null)
        last_name = getIntent().getStringExtra("last_name");

        if (getIntent().getStringExtra("email") != null)
        email = getIntent().getStringExtra("email");

        if (getIntent().getStringExtra("mobile") != null)
        mobile = getIntent().getStringExtra("mobile");

        if (getIntent().getStringExtra("thumbnail") != null)
        thumbnail = getIntent().getStringExtra("thumbnail");

        if (getIntent().getStringExtra("friend_id") != null)
        friend_id = getIntent().getStringExtra("friend_id");

        if (getIntent().getStringExtra("from") != null)
            from = getIntent().getStringExtra("from");


        edtxt_fName.setText(first_name);
        edtxt_lName.setText(last_name);
        edtxt_email.setText(email);
        edtxt_number.setText(mobile);

        if(thumbnail == null){

        } else if(thumbnail.length() == 0){
            Glide.with(AddfriendInfoActivity.this).load(R.mipmap.user_pic)
                    .crossFade()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .placeholder(R.mipmap.user_pic)
                    .error(R.mipmap.user_pic)
                    .into(iView_user);
        } else {
            Glide.with(AddfriendInfoActivity.this).load(thumbnail)
                    .crossFade()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .placeholder(R.mipmap.user_pic)
                    .error(R.mipmap.user_pic)
                    .into(iView_user);
        }

        if (from.equalsIgnoreCase("edit")){
            tv_submit.setText("Update");
        } else {
            tv_submit.setText("Add Friend");
        }
    }

    private void setOnClick(){
        tv_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isInternetPresent = cd.isConnectingToInternet();
                if (isInternetPresent){
                    addfriend();
                } else {
                    customAlertDialog("BullyBeware", BullyConstants.NO_INTERNET_CONNECTED);
                }
            }
        });
    }

    private void addfriend(){
            final ProgressDialog d = BullyDialogs.showLoading(AddfriendInfoActivity.this);
            d.setCanceledOnTouchOutside(false);

            RestClient.get().addfriend(preferences.getStudentId(),friend_id,edtxt_number.getText().toString(), new Callback<AddFriendInfoPojo>() {
                @Override
                public void success(AddFriendInfoPojo addFriendInfoPojo, Response response) {
                    if (addFriendInfoPojo != null){
                        if (addFriendInfoPojo.getStatus()){

                            d.dismiss();

                            BullyToast.showToast(AddfriendInfoActivity.this,addFriendInfoPojo.getMessage());

                            Intent intent = new Intent(AddfriendInfoActivity.this, FriendListActivity.class);
                            intent.putExtra("from","main");
                            startActivity(intent);
                            finish();
                            overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
                        }else {
                         d.dismiss();
                        }
                        d.dismiss();
                    }
                    else
                    {
                        d.dismiss();
                        BullyToast.showToast(AddfriendInfoActivity.this,BullyConstants.SERVER_NOT_RESPOND);
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                  d.dismiss();
                }
            });
    }

    public class PhoneNumberTextWatcher implements TextWatcher {

        private final String TAG = MyProfileActivity.PhoneNumberTextWatcher.class
                .getSimpleName();
        private EditText edTxt;
        private boolean isDelete;

        public PhoneNumberTextWatcher(EditText edTxtPhone) {
            this.edTxt = edTxtPhone;
            edTxt.setOnKeyListener(new View.OnKeyListener() {

                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    if (keyCode == KeyEvent.KEYCODE_DEL) {
                        isDelete = true;
                    }
                    return false;
                }
            });
        }


        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        public void beforeTextChanged(CharSequence s, int start, int count,
                                      int after) {
        }

        public void afterTextChanged(Editable s) {

            if (isDelete) {
                isDelete = false;
                return;
            }
            String val = s.toString();
            String a = "";
            String b = "";
            String c = "";
            if (val != null && val.length() > 0) {
                val = val.replace("-", "");
                if (val.length() >= 3) {
                    a = val.substring(0, 3);
                } else if (val.length() < 3) {
                    a = val.substring(0, val.length());
                }
                if (val.length() >= 6) {
                    b = val.substring(3, 6);
                    c = val.substring(6, val.length());
                } else if (val.length() > 3 && val.length() < 6) {
                    b = val.substring(3, val.length());
                }
                StringBuffer stringBuffer = new StringBuffer();
                if (a != null && a.length() > 0) {
                    stringBuffer.append(a);
                    if (a.length() == 3) {
                        stringBuffer.append("-");
                    }
                }
                if (b != null && b.length() > 0) {
                    stringBuffer.append(b);
                    if (b.length() == 3) {
                        stringBuffer.append("-");
                    }
                }
                if (c != null && c.length() > 0) {
                    stringBuffer.append(c);
                }
                edTxt.removeTextChangedListener(this);
                edTxt.setText(stringBuffer.toString());
                edTxt.setSelection(edTxt.getText().toString().length());
                edTxt.addTextChangedListener(this);
            } else {
                edTxt.removeTextChangedListener(this);
                edTxt.setText("");
                edTxt.addTextChangedListener(this);
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        if (from.equalsIgnoreCase("edit")){
            Intent intent = new Intent(AddfriendInfoActivity.this, FriendListActivity.class);
            intent.putExtra("from","main");
            startActivity(intent);
            finish();
        } else if (from.equalsIgnoreCase("SearchFriend")) {
            Intent intent = new Intent(AddfriendInfoActivity.this, SearchByNameActivity.class);
            startActivity(intent);
            finish();
        }
        else {
            Intent intent = new Intent(AddfriendInfoActivity.this, QRCodeScanActivity.class);
            startActivity(intent);
            finish();
        }
    }
}
