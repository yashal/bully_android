package com.nexgen.bullybeware.activity;

import android.Manifest;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.nexgen.bullybeware.R;
import com.nexgen.bullybeware.adapter.SafeListAdapter;
import com.nexgen.bullybeware.model.RestClient;
import com.nexgen.bullybeware.pojo.SafeListPojo;
import com.nexgen.bullybeware.pojo.SafePlaceEmailPojo;
import com.nexgen.bullybeware.util.BullyConstants;
import com.nexgen.bullybeware.util.BullyDialogs;
import com.nexgen.bullybeware.util.BullyPreferences;
import com.nexgen.bullybeware.util.BullyToast;
import com.nexgen.bullybeware.util.ConnectionDetector;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class SafePlaceDetailActivity extends BaseActivity {

    private SafePlaceDetailActivity ctx = this;

    private TextView contact_name, phone_number, email_address;
    private LinearLayout contact_nameLayout, phone_numberLayout, email_addressLayout, call_layout, email_layout;
    private String placeName, contactName, phoneNumber, emailAddress, callable_number;
    private View seprator;
    private BullyPreferences preferences;
    private String student_id;
    private ConnectionDetector cd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_safe_place_detail);

        init();

        placeName = getIntent().getStringExtra("placeName");
        contactName = getIntent().getStringExtra("contactName");
        phoneNumber = getIntent().getStringExtra("phoneNumber");
        emailAddress = getIntent().getStringExtra("emailAddress");

        if (!placeName.isEmpty()) {
            setDrawerbackIcon(placeName);
        }

        if (!contactName.isEmpty()) {
            contact_nameLayout.setVisibility(View.VISIBLE);
            contact_name.setText(contactName);
        } else {
            contact_nameLayout.setVisibility(View.GONE);
        }

        if (!phoneNumber.isEmpty()) {
            phone_numberLayout.setVisibility(View.VISIBLE);
            call_layout.setVisibility(View.VISIBLE);
            phone_number.setText(phoneNumber);
        } else {
            phone_numberLayout.setVisibility(View.GONE);
            call_layout.setVisibility(View.GONE);
            seprator.setVisibility(View.GONE);
        }

        if (!emailAddress.isEmpty()) {
            email_addressLayout.setVisibility(View.VISIBLE);
            email_layout.setVisibility(View.VISIBLE);
            email_address.setText(emailAddress);
        } else {
            email_addressLayout.setVisibility(View.GONE);
            email_layout.setVisibility(View.GONE);
            seprator.setVisibility(View.GONE);
        }
        callable_number=String.valueOf(phoneNumber.replace("(", "").replace(") ", "").replace("-", ""));
        System.out.println("hh yashal callable_number "+callable_number);

        call_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_CALL);

                intent.setData(Uri.parse("tel:" + callable_number));
                if (ActivityCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                startActivity(intent);
            }
        });

        email_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showMessageDialog(ctx);
            }
        });

    }

    private void init()
    {
        contact_name = (TextView)findViewById(R.id.contact_name);
        phone_number = (TextView)findViewById(R.id.phone_number);
        email_address = (TextView)findViewById(R.id.email_address);

        contact_nameLayout = (LinearLayout) findViewById(R.id.contact_nameLayout);
        phone_numberLayout = (LinearLayout) findViewById(R.id.phone_numberLayout);
        email_addressLayout = (LinearLayout) findViewById(R.id.email_addressLayout);

        call_layout = (LinearLayout) findViewById(R.id.call_layout);
        email_layout = (LinearLayout) findViewById(R.id.email_layout);

        seprator = (View) findViewById(R.id.seprator);
        preferences = new BullyPreferences(ctx);
        cd = new ConnectionDetector(getApplicationContext());
        student_id = preferences.getStudentId();
    }


    public void showMessageDialog(Context ctx) {
        final Dialog dialog_message = new Dialog(ctx, android.R.style.Theme_Translucent_NoTitleBar);
        dialog_message.setContentView(R.layout.message_dialog);

        ImageView close_dialog = (ImageView) dialog_message.findViewById(R.id.dialog_header_cross);
        final EditText message_description = (EditText) dialog_message.findViewById(R.id.message_description);
        LinearLayout messageLL = (LinearLayout) dialog_message.findViewById(R.id.message);
        close_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_message.dismiss();
            }
        });

        messageLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String message = message_description.getText().toString();
                if (message.length() == 0)
                {
                    customAlertDialog(getResources().getString(R.string.safe_place_submission), getResources().getString(R.string.message_not_empty));
                }
                else
                {
                    dialog_message.dismiss();
                    senEmail(message);

                }
            }
        });

        dialog_message.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        dialog_message.show();
    }

    private void senEmail(String content)
    {
        if (cd.isConnectingToInternet()) {
            final ProgressDialog d = BullyDialogs.showLoading(ctx);
            d.setCanceledOnTouchOutside(false);

            RestClient.get().sendSafePlaceEmail(placeName,contactName,emailAddress,content,student_id,  new Callback<SafePlaceEmailPojo>() {
                @Override
                public void success(SafePlaceEmailPojo basePojo, Response response) {

                    if (basePojo != null) {
                        if (basePojo.getStatus()) {
                            BullyToast.showToast(ctx,  "Email Send Successfully");
                        }
                    } else {
                        BullyToast.showToast(ctx, BullyConstants.SERVER_NOT_RESPOND);
                    }
                    d.dismiss();
                }

                @Override
                public void failure(RetrofitError error) {
                    d.dismiss();
                }
            });
        } else {
            customAlertDialog("BullyBeware", BullyConstants.NO_INTERNET_CONNECTED);
        }
    }
}
