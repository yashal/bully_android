package com.nexgen.bullybeware.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.media.Image;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.design.widget.TextInputLayout;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.nexgen.bullybeware.R;
import com.nexgen.bullybeware.adapter.CityListAdapter;
import com.nexgen.bullybeware.adapter.RelationListAdapter;
import com.nexgen.bullybeware.adapter.SchoolListAdapter;
import com.nexgen.bullybeware.adapter.SecurityQueListAdapter;
import com.nexgen.bullybeware.adapter.StateListAdapter;
import com.nexgen.bullybeware.model.FileUploadSer;
import com.nexgen.bullybeware.model.RestClient;
import com.nexgen.bullybeware.model.ServiceGenerator;
import com.nexgen.bullybeware.pojo.CityListPojo;
import com.nexgen.bullybeware.pojo.CityPojo;
import com.nexgen.bullybeware.pojo.GetProfilePojo;
import com.nexgen.bullybeware.pojo.QRCodeImagePojo;
import com.nexgen.bullybeware.pojo.RelationListPojo;
import com.nexgen.bullybeware.pojo.RelationPojo;
import com.nexgen.bullybeware.pojo.ReportSubmissionPojo;
import com.nexgen.bullybeware.pojo.SchoolListpojo;
import com.nexgen.bullybeware.pojo.SchoolPojo;
import com.nexgen.bullybeware.pojo.SecurityQueListPojo;
import com.nexgen.bullybeware.pojo.SecurityQuePojo;
import com.nexgen.bullybeware.pojo.SignUpPojo;
import com.nexgen.bullybeware.pojo.StateListPojo;
import com.nexgen.bullybeware.pojo.StatePojo;
import com.nexgen.bullybeware.util.BullyConstants;
import com.nexgen.bullybeware.util.BullyDialogs;
import com.nexgen.bullybeware.util.BullyPreferences;
import com.nexgen.bullybeware.util.BullyToast;
import com.nexgen.bullybeware.util.ConnectionDetector;
import com.nexgen.bullybeware.util.ImageLoadingUtils;
import com.nexgen.bullybeware.util.UsPhoneNumberFormatter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedFile;

import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

/**
 * Created by quepplin1 on 10/26/2016.
 */
public class MyProfileActivity extends BaseActivity {

    private TextInputLayout inputlayout_fName,inputlayout_lName,inputlayout_schoolEmail;
    private EditText edtxt_fName,edtxt_lName,edtxt_email,edtxt_schoolEmail,edtxt_cnt_fname,edtxt_cnt_lName,edtxt_telephone;
    private EditText edtxt_school_id,edtxt_schoolAddress,edtxt_principleName,edtxt_mobile,profile_school_name;
    private TextView tv_title_name,tv_title_city;
    private Spinner state_spineer,city_spineer,relation_spinner;
    private ImageView school_search;
    private StateListAdapter state_adapter;
    private CityListAdapter city_adapter;
    private Bitmap largeIcon;


    private ArrayList<CityListPojo> arrayCityList;
    private ArrayList<StateListPojo> arrayStateList;
    private ArrayList<RelationListPojo> arrayRelationList;

    private String state_id,city_id,relation_id;
    private RelationListAdapter relation_adapter;
    private BullyPreferences preferences;
    private ImageView profile_edit_check, bannerImage;
    private String state_name,city_name,relation_name,school_id_info;

    private static final String CAMERA_DIR = "/dcim/";
    private final int SELECT_PHOTO = 457;
    private final int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 456;

    private String mCurrentPhotoPath;
    private String photoPath = "";
    private LinearLayout ll_notification,ll_myProfile;
    CircularImageView imgProfile;
    // flag for Internet connection status
    Boolean isInternetPresent = false;
    private String school_id,school_name,school_address,school_principle_name,school_email,school_mobile;
    private ImageView emergency_icon;
    private final static int GET_CAMERA_RESULT = 102;
    private SharedPreferences sharedPreferences;
    private ArrayList<String> permissionsToRequest;
    private ArrayList<String> permissionsRejected;
    private boolean status = false;
    private LinearLayout qrcode_image_LL;
    private ImageView qrcode_image;

    // Connection detector class
    ConnectionDetector cd;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        setDrawerbackIcon("My Profile");

        bannerImage = (ImageView) findViewById(R.id.bannerImage);

        init();

        if (isInternetPresent){
            ll_myProfile.setVisibility(View.VISIBLE);
            setProfileInfo();
            setApi();
        } else {
            customAlertDialog("BullyBeware",BullyConstants.NO_INTERNET_CONNECTED);

            ll_myProfile.setVisibility(View.GONE);
        }

        edtxt_school_id.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                // TODO Auto-generated method stub
                if (count == 0)
                {
                    enableEdittext();
                }
                    status = false;
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {

                // TODO Auto-generated method stub
            }
        });

        setOnClickListner();
    }


    private void init(){
        inputlayout_fName = (TextInputLayout) findViewById(R.id.profile_input_layout_fname);
        inputlayout_lName = (TextInputLayout) findViewById(R.id.profile_input_layout_lname);
        inputlayout_schoolEmail = (TextInputLayout) findViewById(R.id.profile_inputlayout_school_email);

        edtxt_fName = (EditText) findViewById(R.id.profile_edtxt_fname);
        edtxt_lName = (EditText) findViewById(R.id.profile_edtxt_lname);
        edtxt_email = (EditText) findViewById(R.id.profile_edtxt_email);
        edtxt_schoolEmail = (EditText) findViewById(R.id.profile_school_email);
        edtxt_cnt_fname = (EditText) findViewById(R.id.profile_cnt_fname);
        edtxt_cnt_lName = (EditText) findViewById(R.id.profile_cnt_lname);
        edtxt_telephone = (EditText) findViewById(R.id.profile_edtxt_telephone);

        profile_school_name = (EditText) findViewById(R.id.profile_school_name);

        emergency_icon = (ImageView) findViewById(R.id.profile_iv_emergency_alarm);

        ll_myProfile = (LinearLayout) findViewById(R.id.ll_myProfile);

        qrcode_image_LL = (LinearLayout) findViewById(R.id.qrcode_image_LL);
        qrcode_image = (ImageView) findViewById(R.id.qrcode_image);

//        edtxt_telephone.addTextChangedListener(new PhoneNumberTextWatcher(edtxt_telephone));
        UsPhoneNumberFormatter lineNumberFormatter = new UsPhoneNumberFormatter(
                new WeakReference<EditText>(edtxt_telephone));
        edtxt_telephone.addTextChangedListener(lineNumberFormatter);
        tv_title_name = (TextView) findViewById(R.id.profile_title_name);
        tv_title_city = (TextView) findViewById(R.id.profile_title_city);

        profile_edit_check = (ImageView) findViewById(R.id.profile_edit_check);
        imgProfile = (CircularImageView) findViewById(R.id.circleView);
        state_spineer = (Spinner) findViewById(R.id.profile_state_spinner);
        city_spineer = (Spinner) findViewById(R.id.profile_city_spinner);
        relation_spinner = (Spinner) findViewById(R.id.Profile_relation_spinner);

        edtxt_school_id = (EditText) findViewById(R.id.profile_school_id);
        edtxt_schoolAddress = (EditText) findViewById(R.id.profile_school_add);
        edtxt_principleName = (EditText) findViewById(R.id.profile_principle_name);
        edtxt_mobile = (EditText) findViewById(R.id.profile_edtxt_mobile);

//        edtxt_mobile.addTextChangedListener(new PhoneNumberTextWatcher(edtxt_mobile));
        UsPhoneNumberFormatter addLineNumberFormatter = new UsPhoneNumberFormatter(
                new WeakReference<EditText>(edtxt_mobile));
        edtxt_mobile.addTextChangedListener(addLineNumberFormatter);
        school_search = (ImageView) findViewById(R.id.profile_school_search);

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        arrayCityList = new ArrayList<CityListPojo>();
        arrayStateList = new ArrayList<StateListPojo>();
        arrayRelationList = new ArrayList<RelationListPojo>();

        preferences = new BullyPreferences(MyProfileActivity.this);

        ll_notification = (LinearLayout) findViewById(R.id.ll_Notification);
        ll_notification.setVisibility(View.GONE);

        cd = new ConnectionDetector(getApplicationContext());
        // get Internet status
        isInternetPresent = cd.isConnectingToInternet();


    }

    private void setOnClickListner(){
        state_spineer.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                if (i == 0) {

                    LinearLayout parent = (LinearLayout) view;

                    ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));

                } else {
                    state_id = arrayStateList.get(i).getId();

                    System.out.println("id" + state_id);

                    LinearLayout parent = (LinearLayout) view;

                    ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));

                    setCityData();

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        city_spineer.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i == 0) {
      //              city_id = "1";

                    LinearLayout parent = (LinearLayout) view;

                    ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));
                } else {
                    city_id = arrayCityList.get(i).getId();

                    LinearLayout parent = (LinearLayout) view;

                    ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        relation_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i == 0) {

                   relation_id = "0";

                    LinearLayout parent = (LinearLayout) view;

                    ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.song_name_text_color));

                } else {
                    relation_id = arrayRelationList.get(i).getId();

                    LinearLayout parent = (LinearLayout) view;

                    ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        profile_edit_check.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                isInternetPresent = cd.isConnectingToInternet();

                if (isInternetPresent){
                    Submitform();
                } else {
                    customAlertDialog("BullyBeware",BullyConstants.NO_INTERNET_CONNECTED);
                }
            }
        });

        imgProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    addPermissionDialogCamera();
                } else {
                    CameraDialog();
                }
            }
        });

        school_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setSchoolData();
            }
        });

        edtxt_school_id.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    setSchoolData();

                    return true;
                }
                return false;
            }
        });

        emergency_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    playStopClickListner();
                } catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
    }


    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    public  boolean validateEmail(TextInputLayout layoutTxt,EditText edtTxt) {
        if (edtTxt.getText().toString().trim().isEmpty())  {

            customAlertDialog("Report Incident",getResources().getString(R.string.err_school_email));
            requestFocus(edtTxt);
            return false;
        } else {
            layoutTxt.setErrorEnabled(false);
        }

        return true;
    }

    protected static boolean isValidEmail(CharSequence target) {
        return target != null && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }
    public  boolean validEmail(TextInputLayout layoutTxt,EditText edtTxt) {
        if (!isValidEmail(edtTxt.getText().toString().trim()))  {

            customAlertDialog("My Profile",getResources().getString(R.string.err_valid_school_email));
            requestFocus(edtTxt);
            return false;
        } else {
            layoutTxt.setErrorEnabled(false);
        }

        return true;
    }

    public  boolean validateFirstname(TextInputLayout layoutTxt,EditText edtTxt) {
        if (edtTxt.getText().toString().trim().isEmpty()) {

            customAlertDialog("My Profile",getResources().getString(R.string.err_fname));
            requestFocus(edtTxt);
            return false;
        } else {
            layoutTxt.setErrorEnabled(false);
        }

        return true;
    }
    public  boolean validateFirstnamelength(TextInputLayout layoutTxt,EditText edtTxt) {
        if (edtTxt.getText().toString().trim().length() < 2 || edtTxt.getText().toString().trim().length() > 30) {

            customAlertDialog("My Profile",getResources().getString(R.string.err_fname_length));
            requestFocus(edtTxt);
            return false;
        } else {
            layoutTxt.setErrorEnabled(false);
        }

        return true;
    }
    public  boolean validateLastname(TextInputLayout layoutTxt,EditText edtTxt) {
        if (edtTxt.getText().toString().trim().isEmpty()) {

            customAlertDialog("My Profile",getResources().getString(R.string.err_lname));
            requestFocus(edtTxt);
            return false;
        } else {
            layoutTxt.setErrorEnabled(false);
        }

        return true;
    }

    public  boolean validateLastnamelength(TextInputLayout layoutTxt,EditText edtTxt) {
        if (edtTxt.getText().toString().trim().length() < 2 || edtTxt.getText().toString().trim().length() > 30) {

            customAlertDialog("My Profile",getResources().getString(R.string.err_lname_length));
            requestFocus(edtTxt);
            return false;
        } else {
            layoutTxt.setErrorEnabled(false);
        }

        return true;
    }


    private void setSchoolData(){
        if (isInternetPresent){
            final ProgressDialog d = BullyDialogs.showLoading(MyProfileActivity.this);
            d.setCanceledOnTouchOutside(false);
            RestClient.get().getSchoolList(edtxt_school_id.getText().toString(), new Callback<SchoolPojo>() {
                @Override
                public void success(SchoolPojo schoolPojo, Response response) {

                    if (schoolPojo != null){
                        if (schoolPojo.getStatus()){
                            System.out.println("xxx right");

                            school_id_info = schoolPojo.getObject().getId();
                            school_name = schoolPojo.getObject().getSchool_name();
                            school_address = schoolPojo.getObject().getSchool_address();
                            school_principle_name = schoolPojo.getObject().getPrinciple_name();
                            school_mobile = schoolPojo.getObject().getSchool_mobile();
                            school_email = schoolPojo.getObject().getSchool_email();



                            SchoolListDialog();
                        }  else {
                            customAlertDialog("School Information",schoolPojo.getMessage());
                        }
                        d.dismiss();
                    }
                    else
                    {
                        d.dismiss();
                        BullyToast.showToast(MyProfileActivity.this, BullyConstants.SERVER_NOT_RESPOND);
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    System.out.println("xxx fail");
                    d.dismiss();
                }
            });
        }
    }

    private void SchoolListDialog() {
        final Dialog dialogLogout = new Dialog(MyProfileActivity.this, android.R.style.Theme_Translucent_NoTitleBar);
        dialogLogout.setContentView(R.layout.schoollistdialog);
        dialogLogout.show();

        TextView dialog_header = (TextView) dialogLogout.findViewById(R.id.school_dialog_header);
        dialog_header.setVisibility(View.VISIBLE);
        dialog_header.setText("SCHOOL INFO");
        ImageView iView_cross = (ImageView) dialogLogout.findViewById(R.id.dialog_header_cross);
        TextView tv_submit = (TextView) dialogLogout.findViewById(R.id.schooldialog_submit);
        tv_submit.setText("YES");
        TextView tv_cancel = (TextView) dialogLogout.findViewById(R.id.schooldialog_cancel);
        TextView dialog_text = (TextView) dialogLogout.findViewById(R.id.schooldialog_text);
        dialog_text.setVisibility(View.VISIBLE);
        dialog_text.setText("Is this Correct?");

        final TextView edtxt_school_name = (TextView) dialogLogout.findViewById(R.id.dialog_school_name);
        final TextView school_add = (TextView) dialogLogout.findViewById(R.id.dialog_school_add);

        edtxt_school_name.setText(school_name);
        school_add.setText(school_address);

        tv_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                edtxt_schoolEmail.setText(school_email);
                profile_school_name.setText(school_name);
                edtxt_schoolAddress.setText(school_address);
                edtxt_principleName.setText(school_principle_name);
                edtxt_mobile.setText(school_mobile);

//                edtxt_schoolEmail.setFocusable(false);
//                edtxt_schoolEmail.setFocusableInTouchMode(false);
//                profile_school_name.setFocusable(false);
//                profile_school_name.setFocusableInTouchMode(false);
//                edtxt_schoolAddress.setFocusable(false);
//                edtxt_schoolAddress.setFocusableInTouchMode(false);
//                edtxt_principleName.setFocusable(false);
//                edtxt_principleName.setFocusableInTouchMode(false);
//                edtxt_mobile.setFocusable(false);
//                edtxt_mobile.setFocusableInTouchMode(false);

                  disableEdittext();

                school_id = school_id_info;
                status = true;
                dialogLogout.dismiss();

            }
        });
        iView_cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogLogout.dismiss();
            }
        });

        tv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogLogout.dismiss();
            }
        });
    }

    private Boolean Submitform(){
        if (!validateFirstname(inputlayout_fName, edtxt_fName)) {
            return true;
        }
        if (!validateFirstnamelength(inputlayout_fName, edtxt_fName)){
            return true;
        }
        if (!validateLastname(inputlayout_lName, edtxt_lName)) {
            return true;
        }
        if (!validateLastnamelength(inputlayout_lName, edtxt_fName)){
            return true;
        }
        else if (edtxt_school_id.getText().toString().length() == 0 & edtxt_schoolEmail.getText().toString().length() == 0){
            customAlertDialog("Report Incident",getResources().getString(R.string.err_school_email));
        }else if (edtxt_schoolEmail.getText().toString().length() > 0 && !(checkValidEmail(edtxt_schoolEmail.getText().toString()))){
            customAlertDialog("Report Incident",getResources().getString(R.string.err_school_email));
        }
       /*if (!validateEmail(inputlayout_schoolEmail,edtxt_schoolEmail))
            return true;
        if (!validEmail(inputlayout_schoolEmail,edtxt_schoolEmail))
            return true;
        if (!validateEmail(inputlayout_schoolEmail,edtxt_schoolEmail)){
            return true;
        }*/
         else {
            if (edtxt_school_id.getText().toString().length() > 0 && !status)
            {
                customAlertDialog("School Information","Please search with valid School Code.");
            }
            else {
                updateProfile();
            }
        }
        return true;
    }

    private void setProfileInfo(){

        final ProgressDialog d = BullyDialogs.showLoading(MyProfileActivity.this);
        d.setCanceledOnTouchOutside(false);

        RestClient.get().getProfileInfo(preferences.getStudentId(), new Callback<GetProfilePojo>() {
            @Override
            public void success(final GetProfilePojo getProfilePojo, Response response) {
                if (getProfilePojo != null){
                    if (getProfilePojo.getStatus()){

                        setStateData();
                        setCityData();

                        setRelationData();

                        edtxt_fName.setText(getProfilePojo.getObject().getName());
                        edtxt_lName.setText(getProfilePojo.getObject().getLast_name());
                        edtxt_email.setText(getProfilePojo.getObject().getEmail());
                        edtxt_schoolEmail.setText(getProfilePojo.getObject().getSchool_email());
                        edtxt_cnt_fname.setText(getProfilePojo.getObject().getCon_name());
                        edtxt_cnt_lName.setText(getProfilePojo.getObject().getCon_lastname());
                        edtxt_telephone.setText(getProfilePojo.getObject().getCon_mobile());

                        edtxt_school_id.setText(getProfilePojo.getObject().getSchool_code());
                        edtxt_schoolEmail.setText(getProfilePojo.getObject().getSchool_email());

                        edtxt_schoolAddress.setText(getProfilePojo.getObject().getSchool_address());
                        edtxt_principleName.setText(getProfilePojo.getObject().getPrincipal_name());
                        edtxt_mobile.setText(getProfilePojo.getObject().getMobile());
                        profile_school_name.setText(getProfilePojo.getObject().getSchool_name());


            //            state_id = getProfilePojo.getObject().getState();
                        city_id = getProfilePojo.getObject().getCity();
                        school_id = getProfilePojo.getObject().getSchool_id();

                        state_name = getProfilePojo.getObject().getState_name();
                        city_name = getProfilePojo.getObject().getCity_name();
                        school_name = getProfilePojo.getObject().getSchool_name();
                        relation_name = getProfilePojo.getObject().getRelation_name();

                        tv_title_name.setText(getProfilePojo.getObject().getName() + " " + getProfilePojo.getObject().getLast_name() );
                        tv_title_city.setText(getProfilePojo.getObject().getCity_name() + ", " + getProfilePojo.getObject().getState_name() );

                            try {
                                largeIcon = new GetBitmapTask().execute(getProfilePojo.getObject().getProfile_image()).get();
                                int width = largeIcon.getWidth();
                                int height = largeIcon.getHeight();
                                int newWidth = largeIcon.getWidth();
                                int newHeight = 80;

                                float scaleWidth = ((float) newWidth) / width;
                                float scaleHeight = ((float) newHeight) / height;

                                Matrix matrix = new Matrix();
                                matrix.postScale(scaleWidth, scaleHeight);

                                Bitmap resizedBitmap = Bitmap.createBitmap(largeIcon, 0, 0,
                                        width, height, matrix, true);

                                bannerImage.setImageBitmap(createBitmap_ScriptIntrinsicBlur(resizedBitmap, 100.0f));
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            if (getProfilePojo.getObject().getSchool_code() != null){
                                if (getProfilePojo.getObject().getSchool_code().length() > 0){

                                    edtxt_schoolEmail.setFocusable(false);
                                    edtxt_schoolEmail.setFocusableInTouchMode(false);
                                    profile_school_name.setFocusable(false);
                                    profile_school_name.setFocusableInTouchMode(false);
                                    edtxt_schoolAddress.setFocusable(false);
                                    edtxt_schoolAddress.setFocusableInTouchMode(false);
                                    edtxt_principleName.setFocusable(false);
                                    edtxt_principleName.setFocusableInTouchMode(false);
                                    edtxt_mobile.setFocusable(false);
                                    edtxt_mobile.setFocusableInTouchMode(false);
                                    status = true;
                                }

                          }else {
                                edtxt_schoolEmail.setFocusable(true);
                                edtxt_schoolEmail.setFocusableInTouchMode(true);
                                profile_school_name.setFocusable(true);
                                profile_school_name.setFocusableInTouchMode(true);
                                edtxt_schoolAddress.setFocusable(true);
                                edtxt_schoolAddress.setFocusableInTouchMode(true);
                                edtxt_principleName.setFocusable(true);
                                edtxt_principleName.setFocusableInTouchMode(true);
                                edtxt_mobile.setFocusable(true);
                                edtxt_mobile.setFocusableInTouchMode(true);
                            }
                        d.dismiss();

                         if (getProfilePojo.getObject().getThumbnail() == null){

                         } else {
                             Glide.with(MyProfileActivity.this).load(getProfilePojo.getObject().getThumbnail())
                                     .crossFade()
                                     .diskCacheStrategy(DiskCacheStrategy.ALL)
                                     .into(imgProfile);
                         }

                    }else {
                       d.dismiss();
                    }
                }
                else
                {
                    d.dismiss();
                    BullyToast.showToast(MyProfileActivity.this, BullyConstants.SERVER_NOT_RESPOND);
                }
            }
            @Override
            public void failure(RetrofitError error) {
            }
        });
    }

    private void updateProfile() {

        if (isInternetPresent) {
            final ProgressDialog d = BullyDialogs.showLoading(MyProfileActivity.this);
            d.setCanceledOnTouchOutside(false);

            if (edtxt_school_id.getText().toString().trim().isEmpty()){
                school_id = "";
            }

            FileUploadSer service = ServiceGenerator.createService(FileUploadSer.class, FileUploadSer.BASE_URL);
            TypedFile typedFile = null;
            if (photoPath.length() > 0)
                typedFile = new TypedFile("multipart/form-data", new File(photoPath));

            service.updateProfile(preferences.getStudentId(),edtxt_fName.getText().toString(), school_id, state_id, city_id, edtxt_schoolEmail.getText().toString(), edtxt_lName.getText().toString(), edtxt_email.getText().toString(),edtxt_cnt_fname.getText().toString(),edtxt_cnt_lName.getText().toString(),relation_id, edtxt_telephone.getText().toString(), "1", typedFile,edtxt_principleName.getText().toString(),edtxt_mobile.getText().toString(), new Callback<SignUpPojo>() {
                        @Override
                        public void success(SignUpPojo signUpPojo, Response response) {
                            if (signUpPojo != null) {
                                if (signUpPojo.getStatus()) {
                                    Toast.makeText(MyProfileActivity.this, signUpPojo.getMessage(), Toast.LENGTH_LONG).show();
                                    System.out.println("xxxx success");

                                    preferences.setStudentId(signUpPojo.getObject().getStudent_id());

                                    preferences.setSchool_id(signUpPojo.getObject().getSchool_id());

                                    preferences.setSchool_email(signUpPojo.getObject().getSchool_email());

            //                        preferences.setSchool_email(signUpPojo.getObject());

                                    Intent intent = new Intent(MyProfileActivity.this, MainActivity.class);
                                    startActivity(intent);
                                    finish();

                                } else {
                                   alerdialog(signUpPojo.getMessage());
                                }

                            } else {
//                               alerdialog(BullyConstants.REGISTRATION_FAILED);
                               alerdialog(BullyConstants.SERVER_NOT_RESPOND);
                            }
                            d.dismiss();
                        }

                        @Override
                        public void failure(RetrofitError error) {
                            System.out.println("xxxx " + "failure");
                            d.dismiss();
                        }
                    });
        } else{
            BullyToast.showToast(MyProfileActivity.this, "Sorry! We could not Update your account.Please check your internet connection and try again.");
        }
    }

    private void alerdialog(String message){
        new AlertDialog.Builder(MyProfileActivity.this)
                .setTitle("PROFILE UPDATE FAILED!")
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // Whatever...
                    }
                }).create().show();
    }

    private void setStateData() {
        if (isInternetPresent) {
            RestClient.get().getState(new Callback<StatePojo>() {
                @Override
                public void success(StatePojo statePojo, Response response) {
                    System.out.println("xxx" + "Sucess");
                    if (statePojo != null) {
                        if (statePojo.getStatus()) {
                            if (statePojo.getObject().size() > 0){
                                arrayStateList = statePojo.getObject();
                                int pos = -1;
                                for (int j=0;j<arrayStateList.size();j++) {
                                    if (state_name != null) {
                                        if (state_name.equalsIgnoreCase(arrayStateList.get(j).getState_name())) {

                                            pos = j;
                                            break;
                                        }
                                    }
                                }
                                state_adapter = new StateListAdapter(MyProfileActivity.this, arrayStateList);
                                state_spineer.setAdapter(state_adapter);
                                if(pos != -1){
                                    state_spineer.setSelection(pos);
                                }
                            } else {
                                BullyToast.showToast(MyProfileActivity.this, BullyConstants.NO_DATA);
                            }
                        } else {
                            BullyToast.showToast(MyProfileActivity.this,statePojo.getMessage());
                        }
                    } else {
                        StateListPojo slp = new StateListPojo();
                        slp.setState_name("State");
                        arrayStateList.add(0, slp);
                        state_adapter = new StateListAdapter(MyProfileActivity.this, arrayStateList);
                        state_spineer.setAdapter(state_adapter);
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    System.out.println("xxx" + "Failure");
                }
            });
        } else {
            StateListPojo slp = new StateListPojo();
            slp.setState_name("State");
            arrayStateList.add(0, slp);
            state_adapter = new StateListAdapter(MyProfileActivity.this, arrayStateList);
            state_spineer.setAdapter(state_adapter);
        }
    }
    private void setCityData() {
        if (isInternetPresent) {
            RestClient.get().getCity(state_id, new Callback<CityPojo>() {
                @Override
                public void success(CityPojo cityPojo, Response response) {
                    if (cityPojo != null) {
                        if (cityPojo.getStatus()) {
                            arrayCityList = cityPojo.getObject();

                            int pos = -1;
                            for (int j=0;j<arrayCityList.size();j++) {
                                if (city_name != null) {
                                    if (city_name.equalsIgnoreCase(arrayCityList.get(j).getCity_name())) {

                                        pos = j;
                                        break;
                                    }
                                }
                            }
                            city_adapter = new CityListAdapter(MyProfileActivity.this, arrayCityList);
                            city_spineer.setAdapter(city_adapter);
                            if(pos != -1){
                                city_spineer.setSelection(pos);
                            }

                        } else {
                            //                       BullyToast.showToast(getActivity(),cityPojo.getMessage());
                        }
                    } else {
                        BullyToast.showToast(MyProfileActivity.this, BullyConstants.SERVER_NOT_RESPOND);
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    System.out.println("xxx" + "failure");
                }
            });
        } else {
            CityListPojo clp = new CityListPojo();
            clp.setCity_name("City");
            arrayCityList.add(0, clp);
            city_adapter = new CityListAdapter(MyProfileActivity.this, arrayCityList);
            city_spineer.setAdapter(city_adapter);
        }
    }

    private void setRelationData() {
        if (isInternetPresent) {
            RestClient.get().getRelation(new Callback<RelationPojo>() {
                @Override
                public void success(RelationPojo relationPojo, Response response) {
                    if (relationPojo != null) {
                        if (relationPojo.getStatus()) {
                            arrayRelationList = relationPojo.getObject();
                            RelationListPojo rlp = new RelationListPojo();
                            rlp.setRelation("Relation");
                            arrayRelationList.add(0, rlp);
                            int pos = -1;
                            for (int j=0;j<arrayRelationList.size();j++) {
                                if (relation_name != null) {
                                    if (relation_name.equalsIgnoreCase(arrayRelationList.get(j).getRelation())) {

                                        pos = j;
                                        break;
                                    }
                                }
                            }

                            relation_adapter = new RelationListAdapter(MyProfileActivity.this, arrayRelationList);
                            relation_spinner.setAdapter(relation_adapter);
                            if(pos != -1){
                                relation_spinner.setSelection(pos);
                            }
                        }
                    }
                    else
                    {
                        BullyToast.showToast(MyProfileActivity.this, BullyConstants.SERVER_NOT_RESPOND);
                    }
                }
                @Override
                public void failure(RetrofitError error) {

                }
            });
        }else {
            RelationListPojo rlp = new RelationListPojo();
            rlp.setRelation("Relation");
            arrayRelationList.add(0, rlp);
            relation_adapter = new RelationListAdapter(MyProfileActivity.this, arrayRelationList);
            relation_spinner.setAdapter(relation_adapter);
        }
    }

    private void CameraDialog() {
        dialogLogout = new Dialog(MyProfileActivity.this, android.R.style.Theme_Translucent_NoTitleBar);
        dialogLogout.setContentView(R.layout.camera_dialog);
        dialogLogout.show();

        LinearLayout ll_dialog = (LinearLayout) dialogLogout.findViewById(R.id.ll_dialog);

        TextView tv_camera = (TextView) dialogLogout.findViewById(R.id.tv_camera);
        TextView tv_gallety = (TextView) dialogLogout.findViewById(R.id.tv_gallery);

        ll_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogLogout.dismiss();
            }
        });

        tv_camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                try {

                    String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
                    String imageFileName = "bmh" + timeStamp + "_";
                    File albumF = getAlbumDir();
                    File imageF = File.createTempFile(imageFileName, "bmh", albumF);


                    mCurrentPhotoPath = imageF.getAbsolutePath();
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(imageF));
                } catch (IOException e) {
                    e.printStackTrace();
                }

                startActivityForResult(takePictureIntent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);

                dialogLogout.dismiss();
            }
        });

        tv_gallety.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = new Intent(
                        Intent.ACTION_PICK,
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                startActivityForResult(i, SELECT_PHOTO);

                dialogLogout.dismiss();
            }
        });
    }


    private File getAlbumDir() {
        File storageDir = null;

        if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.FROYO) {
                storageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "CameraPicture");
            } else {
                storageDir = new File(Environment.getExternalStorageDirectory() + CAMERA_DIR + "CameraPicture");
            }

            if (storageDir != null) {
                if (!storageDir.mkdirs()) {
                    if (!storageDir.exists()) {
//		Log.d("CameraSample", "failed to create directory");
                        return null;
                    }
                }
            }

        } else {
//		Log.v(getString(R.string.app_name), "External storage is not mounted READ/WRITE.");
        }

        return storageDir;
    }

    public void onActivityResult(final int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data == null) {
            System.out.println("hh data null");
        }

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_PHOTO) {

                Uri selectedImage = data.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};

                Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                String imagePath1 = cursor.getString(columnIndex);
                System.out.println("hh image path =" + imagePath1);

                File f = new File(compressImage(imagePath1, 1));
                Bitmap myBitmap = BitmapFactory.decodeFile(f.getAbsolutePath());

                imgProfile.setImageBitmap(myBitmap);
      //          bannerImage.setBackgroundDrawable(new BitmapDrawable(myBitmap));
       //         bannerImage.setImageBitmap(createBitmap_ScriptIntrinsicBlur(myBitmap, 100.0f));

                int width = myBitmap.getWidth();
                int height = myBitmap.getHeight();
                int newWidth = myBitmap.getWidth();
                int newHeight = 80;

                float scaleWidth = ((float) newWidth) / width;
                float scaleHeight = ((float) newHeight) / height;

                Matrix matrix = new Matrix();
                matrix.postScale(scaleWidth, scaleHeight);

                Bitmap resizedBitmap = Bitmap.createBitmap(myBitmap, 0, 0,
                        width, height, matrix, true);

                bannerImage.setImageBitmap(createBitmap_ScriptIntrinsicBlur(resizedBitmap, 100.0f));

                photoPath = f.getAbsolutePath();

            } else if (requestCode == CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE) {

                System.out.println("hh image path = " + mCurrentPhotoPath);

                 File f =new File(compressImage(mCurrentPhotoPath, 0));
       //         File f = new File(mCurrentPhotoPath);
                Bitmap myBitmap = BitmapFactory.decodeFile(f.getAbsolutePath());

                imgProfile.setImageBitmap(myBitmap);
      //          bannerImage.setBackgroundDrawable(new BitmapDrawable(myBitmap));
     //           bannerImage.setImageBitmap(createBitmap_ScriptIntrinsicBlur(myBitmap, 100.0f));

                int width = myBitmap.getWidth();
                int height = myBitmap.getHeight();
                int newWidth = myBitmap.getWidth();
                int newHeight = 80;

                float scaleWidth = ((float) newWidth) / width;
                float scaleHeight = ((float) newHeight) / height;

                Matrix matrix = new Matrix();
                matrix.postScale(scaleWidth, scaleHeight);

                Bitmap resizedBitmap = Bitmap.createBitmap(myBitmap, 0, 0,
                        width, height, matrix, true);

                bannerImage.setImageBitmap(createBitmap_ScriptIntrinsicBlur(resizedBitmap, 100.0f));
                photoPath = f.getAbsolutePath();
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        finish();
    }

    public class PhoneNumberTextWatcher implements TextWatcher {

        private final String TAG = PhoneNumberTextWatcher.class
                .getSimpleName();
        private EditText edTxt;
        private boolean isDelete;

        public PhoneNumberTextWatcher(EditText edTxtPhone) {
            this.edTxt = edTxtPhone;
            edTxt.setOnKeyListener(new View.OnKeyListener() {

                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    if (keyCode == KeyEvent.KEYCODE_DEL) {
                        isDelete = true;
                    }
                    return false;
                }
            });
        }


        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        public void beforeTextChanged(CharSequence s, int start, int count,
                                      int after) {
        }

        public void afterTextChanged(Editable s) {

            if (isDelete) {
                isDelete = false;
                return;
            }
            String val = s.toString();
            String a = "";
            String b = "";
            String c = "";
            if (val != null && val.length() > 0) {
                val = val.replace("-", "");
                if (val.length() >= 3) {
                    a = val.substring(0, 3);
                } else if (val.length() < 3) {
                    a = val.substring(0, val.length());
                }
                if (val.length() >= 6) {
                    b = val.substring(3, 6);
                    c = val.substring(6, val.length());
                } else if (val.length() > 3 && val.length() < 6) {
                    b = val.substring(3, val.length());
                }
                StringBuffer stringBuffer = new StringBuffer();
                if (a != null && a.length() > 0) {
                    stringBuffer.append(a);
                    if (a.length() == 3) {
                        stringBuffer.append("-");
                    }
                }
                if (b != null && b.length() > 0) {
                    stringBuffer.append(b);
                    if (b.length() == 3) {
                        stringBuffer.append("-");
                    }
                }
                if (c != null && c.length() > 0) {
                    stringBuffer.append(c);
                }
                edTxt.removeTextChangedListener(this);
                edTxt.setText(stringBuffer.toString());
                edTxt.setSelection(edTxt.getText().toString().length());
                edTxt.addTextChangedListener(this);
            } else {
                edTxt.removeTextChangedListener(this);
                edTxt.setText("");
                edTxt.addTextChangedListener(this);
            }
        }
    }

    private void addPermissionDialogCamera() {
        ArrayList<String> permissions = new ArrayList<>();
        int resultCode;

        permissions.add(CAMERA);
        permissions.add(WRITE_EXTERNAL_STORAGE);
        resultCode = GET_CAMERA_RESULT;


        //filter out the permissions we have already accepted
        permissionsToRequest = findUnAskedPermissions(permissions, sharedPreferences);
        //get the permissions we have asked for before but are not granted..
        //we will store this in a global list to access later.
        permissionsRejected = findRejectedPermissions(permissions, sharedPreferences);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            if (permissionsToRequest.size() > 0) {//we need to ask for permissions
                //but have we already asked for them?
                requestPermissions(permissionsToRequest.toArray(new String[permissionsToRequest.size()]), resultCode);
                //mark all these as asked..
                for (String perm : permissionsToRequest) {
                    markAsAsked(perm, sharedPreferences);
                }
            } else {
                //show the success banner
                if (permissionsRejected.size() < permissions.size()) {
                    //this means we can show success because some were already accepted.
                    //permissionSuccess.setVisibility(View.VISIBLE);
                    CameraDialog();
                }

                if (permissionsRejected.size() > 0) {
                    //we have none to request but some previously rejected..tell the user.
                    //It may be better to show a dialog here in a prod application

                    try {
                        requestPermissions(permissionsToRequest.toArray(new String[permissionsToRequest.size()]), resultCode);
                        //mark all these as asked..
                        for (String perm : permissionsToRequest) {
                            markAsAsked(perm, sharedPreferences);
                        }
                    } catch (Exception e){
                        e.printStackTrace();
                    }
                }
            }
        }
    }
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {
            case GET_CAMERA_RESULT:
                if (hasPermission(WRITE_EXTERNAL_STORAGE)  && hasPermission(CAMERA)) {

                    CameraDialog();

                } else {
                    permissionsRejected.add(WRITE_EXTERNAL_STORAGE);
                    permissionsRejected.add(CAMERA);
                    clearMarkAsAsked(WRITE_EXTERNAL_STORAGE, sharedPreferences);
                    clearMarkAsAsked(CAMERA,sharedPreferences);
                 /*   String message = "permissions was rejected. Please allow to run the app.";
                    makePostRequestSnack(message, permissionsRejected.size());*/
                }
                break;
        }
    }

    private void enableEdittext()
    {
        edtxt_schoolEmail.setFocusable(true);
        edtxt_schoolEmail.setFocusableInTouchMode(true);

        profile_school_name.setFocusable(true);
        profile_school_name.setFocusableInTouchMode(true);

        edtxt_schoolAddress.setFocusable(true);
        edtxt_schoolAddress.setFocusableInTouchMode(true);

        edtxt_principleName.setFocusable(true);
        edtxt_principleName.setFocusableInTouchMode(true);

        edtxt_mobile.setFocusable(true);
        edtxt_mobile.setFocusableInTouchMode(true);
    }

    private void disableEdittext()
    {
        edtxt_schoolEmail.setFocusable(false);
        edtxt_schoolEmail.setFocusableInTouchMode(false);

        profile_school_name.setFocusable(false);
        profile_school_name.setFocusableInTouchMode(false);

        edtxt_schoolAddress.setFocusable(false);
        edtxt_schoolAddress.setFocusableInTouchMode(false);

        edtxt_principleName.setFocusable(false);
        edtxt_principleName.setFocusableInTouchMode(false);

        edtxt_mobile.setFocusable(false);
        edtxt_mobile.setFocusableInTouchMode(false);
    }

    private void setApi(){
        if (isInternetPresent){
            RestClient.get().getQrImage(preferences.getStudentId(), new Callback<QRCodeImagePojo>() {
                @Override
                public void success(QRCodeImagePojo qrCodeImagePojo, Response response) {

                    qrcode_image_LL.setVisibility(View.VISIBLE);

                    if (qrCodeImagePojo != null){
                        if (qrCodeImagePojo.getStatus()){
                            Glide.clear(qrcode_image);
                            Glide.with(MyProfileActivity.this).load(qrCodeImagePojo.getObject())
                                    .thumbnail(0.5f)
                                    .crossFade()
                                    .placeholder(R.mipmap.ic_launcher)
                                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                                    .into(qrcode_image);
                        }
                    }
                    else
                    {
                        BullyToast.showToast(MyProfileActivity.this, BullyConstants.SERVER_NOT_RESPOND);
                    }
                }

                @Override
                public void failure(RetrofitError error) {

                }
            });
        }

    }
}
