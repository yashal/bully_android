/**
 * Copyright 2016 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.nexgen.bullybeware.activity;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.nexgen.bullybeware.R;
//import com.mirchimurga.ui.R;
//import com.mirchimurga.ui.utils.AppConstants;
//import com.mirchimurga.ui.utils.Preferences;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Random;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";
    private String message,type,action,tittle;
    Bitmap bitmapVideo,bitmapAudio;
//    Preferences preference;
    /**
     * Called when message is received.
     *
     * @param data Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage data) {
        try{
        // [START_EXCLUDE]
        // There are two types of messages data messages and notification messages. Data messages are handled
        // here in onMessageReceived whether the app is in the foreground or background. Data messages are the type
        // traditionally used with GCM. Notification messages are only received here in onMessageReceived when the app
        // is in the foreground. When the app is in the background an automatically generated notification is displayed.
        // When the user taps on the notification they are returned to the app. Messages containing both notification
        // and data payloads are treated as notification messages. The Firebase console always sends notification
        // messages. For more see: https://firebase.google.com/docs/cloud-messaging/concept-options
        // [END_EXCLUDE]

        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
//        preference= Preferences.getInstance(this);
//            message = data.getData().get("message");
//            type= data.getData().get("type");
//            action= data.getData().get("action");
//            System.out.println("message:"+message);
//            System.out.println("type:"+type);
//            System.out.println("action:"+action);
//
//        Log.d(TAG, "From: " + data.getFrom());

        // Check if message contains a data payload.
            System.out.println("Message data payload: "+data.getData().size());
        if (data.getData().size() > 0) {
            Log.d(TAG, "Message data payload: " + data.getData());
        }

        // Check if message contains a notification payload.
        if (data.getNotification() != null) {
            Log.d(TAG, "Message Notification Body: " + data.getNotification().getTitle());
        }

//        JSONArray array= new JSONArray(data.getData().get("details"));
//        JSONObject object= array.getJSONObject(0);
//        Log.d(TAG, "object: " + object);
//        if(preference.getString(AppConstants.NOTIFICATION).equals("true")) {
//            if ((action.equalsIgnoreCase("add") || action.equalsIgnoreCase("murga")) && type.equalsIgnoreCase("video")) {
//                bitmapVideo = getBitmapFromURL(object.getString("thumbnail"));
//                notify(message, object, "Video");
//            } else if ((action.equalsIgnoreCase("add") || action.equalsIgnoreCase("murga")) && type.equalsIgnoreCase("audio")) {
//                bitmapAudio = getBitmapFromURL(object.getString("imagefile"));
//                notify(message, object, "Audio");
//            }
//        }else{
//            System.out.println("Notification are off from application");
//        }




        sendNotification(data.getNotification().getTitle(), data.getNotification().getBody());
        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    // [END receive_message]

    /**
     * Create and show a simple notification containing the received FCM message.
     *
     * @param title FCM message body received.
     * @param body FCM message body received.
     */
    private void sendNotification(String title, String body) {
        Intent intent = new Intent(this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(title)
                .setContentText(body)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
    }

  /* private void notify(String tittle, JSONObject object, String type) {

       String ns = Context.NOTIFICATION_SERVICE;
      NotificationManager mNotificationManager = (NotificationManager) getSystemService(ns);

     try {
           CharSequence contentTitle = tittle ;

               Intent intent = new Intent(this, LoginActivity.class);
               PendingIntent contentIntent = PendingIntent.getActivity(this, new Random().nextInt(), intent, PendingIntent.FLAG_ONE_SHOT);
               Notification notification;
               android.support.v7.app.NotificationCompat.Builder builder = new android.support.v7.app.NotificationCompat.Builder(this);
               builder.setOnlyAlertOnce(true);
              notification = builder.setContentIntent(contentIntent).setSmallIcon(R.mipmap.murga123).setAutoCancel(true).setContentTitle(contentTitle).setStyle(new
                     NotificationCompat.BigPictureStyle(builder).bigPicture(bitmapVideo).setSummaryText(tittle)).setSound(Uri.parse("android.resource://" + this.getPackageName() + "/" + R.raw.a1_ascendent)).setContentIntent(contentIntent).build();
             mNotificationManager.notify(new Random().nextInt(), notification);


       } catch (Exception e) {
          e.printStackTrace();
       }
  }*/
//    public Bitmap getBitmapFromURL(String strURL) {
//        try {
//            URL url = new URL(strURL);
//            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
//            connection.setDoInput(true);
//            connection.connect();
//            InputStream input = connection.getInputStream();
//            Bitmap myBitmap = BitmapFactory.decodeStream(input);
//            // sendBigPictureStyleNotification();
//            return myBitmap;
//        } catch (IOException e) {
//            e.printStackTrace();
//            return null;
//        }
//    }
}
