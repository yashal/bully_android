package com.nexgen.bullybeware.activity;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nexgen.bullybeware.R;
import com.nexgen.bullybeware.adapter.AudioGridAdapter;
import com.nexgen.bullybeware.pojo.CheckboxStatusPojo;
import com.nexgen.bullybeware.util.BullyPreferences;
import com.nexgen.bullybeware.util.BullyToast;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by quepplin1 on 9/27/2016.
 */
public class SavedAudioActivity extends BaseActivity {

    private String imageUri;
    private GridView gridview;
    private TextView tv_reportIncident;
    private String audio_path;
    private ArrayList<CheckboxStatusPojo> audiostatus = new ArrayList<CheckboxStatusPojo>();
    private ArrayList<String> checkedimagelist = new ArrayList<String>();
    private AudioGridAdapter adapter;
    private LinearLayout ll_audio_main;

    private static final long DOUBLE_CLICK_TIME_DELTA = 2000;//milliseconds

    long lastClickTime = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_saved_audio);

        setDrawerbackIcon("Preview");
        imageUri = "android.resource://com.nexgen.bullybeware/" + R.mipmap.icon_camara;

        init();
        setOnClickListner();
        gridView();
    }

    private void init() {
        gridview = (GridView) findViewById(R.id.audio_gridview);
        tv_reportIncident = (TextView) findViewById(R.id.audio_tv_report_incident);
        ll_audio_main = (LinearLayout) findViewById(R.id.ll_audio_main);

        audio_path = getIntent().getStringExtra("audio_path");
        CheckboxStatusPojo statusPojo = new CheckboxStatusPojo(true, audio_path);
        CheckboxStatusPojo statusPojo1 = new CheckboxStatusPojo(false, imageUri);
        audiostatus.add(statusPojo);
        audiostatus.add(statusPojo1);

        BullyToast.showToast(SavedAudioActivity.this, getResources().getString(R.string.emergency_alaram));
    }

    private void setOnClickListner() {
        tv_reportIncident.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkedimagelist.clear();
                for (int i = 0; i < audiostatus.size(); i++) {
                    Boolean status = audiostatus.get(i).getStatus();

                    if (status)
                        checkedimagelist.add(audiostatus.get(i).getPath());
                }
                Intent intent = new Intent(SavedAudioActivity.this, AudioReportActivity.class);
                Bundle args = new Bundle();
                args.putSerializable("ARRAYLIST", (Serializable) checkedimagelist);
                intent.putExtra("BUNDLE", args);
                startActivity(intent);

            }
        });

        ll_audio_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                long clickTime = System.currentTimeMillis();
                if (clickTime - lastClickTime < DOUBLE_CLICK_TIME_DELTA) {
                    try {
                        playStopClickListner();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    lastClickTime = 0;
                } else {
                    lastClickTime = clickTime;
                }
            }
        });
    }

    private void gridView() {
        adapter = new AudioGridAdapter(this, audiostatus);
        gridview.setAdapter(adapter);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            audiostatus.remove(audiostatus.size() - 1);
            audio_path = data.getStringExtra("audio_path");
            CheckboxStatusPojo statusPojo = new CheckboxStatusPojo(false, audio_path);
            CheckboxStatusPojo statusPojo1 = new CheckboxStatusPojo(false, imageUri);

            audiostatus.add(statusPojo);
            audiostatus.add(statusPojo1);
            gridView();
        }
        if (resultCode == Activity.RESULT_CANCELED) {
            //Write your code if there's no result
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        Intent intent = new Intent(SavedAudioActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }
}
