package com.nexgen.bullybeware.activity;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.internal.Utility;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.auth.GoogleAuthException;
import com.google.android.gms.auth.GoogleAuthUtil;
import com.google.android.gms.auth.UserRecoverableAuthException;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;
import com.nexgen.bullybeware.R;
import com.nexgen.bullybeware.fragment.FirstSignUpFragment;
import com.nexgen.bullybeware.model.RestClient;
import com.nexgen.bullybeware.pojo.FbProfileInfoPojo;
import com.nexgen.bullybeware.pojo.ForgotPasswordPojo;
import com.nexgen.bullybeware.pojo.GooglePlusProfilePojo;
import com.nexgen.bullybeware.pojo.LogInPojo;
import com.nexgen.bullybeware.util.BullyApplication;
import com.nexgen.bullybeware.util.BullyConstants;
import com.nexgen.bullybeware.util.BullyDialogs;
import com.nexgen.bullybeware.util.BullyPreferences;
import com.nexgen.bullybeware.util.BullyToast;
import com.nexgen.bullybeware.util.ConnectionDetector;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import static android.Manifest.permission.GET_ACCOUNTS;


/**
 * Created by quepplin1 on 8/9/2016.
 */

public class LoginActivity extends BaseActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private CallbackManager callbackManager;
    private String  uid;
    private ImageView iView_login, iView_register, iView_fblogin, iView_googlelogin;
    private EditText edtxt_email, edtxt_password;
    private TextView tv_forgotpassword;
    private static String password;
    EditText fp_edtxt_email;
    private String click = "no_click";

    // for external permission
    private final static int GET_ACCOUNTS_RESULT = 102;
    private SharedPreferences sharedPreferences;
    private ArrayList<String> permissionsToRequest;
    private ArrayList<String> permissionsRejected;

    private BullyPreferences preference;
    private Dialog dialogLogout;
    private ProgressDialog d;

    // flag for Internet connection status
    Boolean isInternetPresent = false;

    // Connection detector class
    ConnectionDetector cd;

    // for google plus
    private String googleId, token;
    private static final int RC_SIGN_IN = 0;
    private GoogleApiClient mGoogleApiClient;
    private boolean mIntentInProgress;
    private boolean mSignInClicked = false;
    private ConnectionResult mConnectionResult;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        preference = new BullyPreferences(this);

        iView_fblogin = (ImageView) findViewById(R.id.iView_fblogin);
        iView_googlelogin = (ImageView) findViewById(R.id.iView_googlelogin);
        iView_login = (ImageView) findViewById(R.id.iView_login);
        iView_register = (ImageView) findViewById(R.id.iView_register);

        tv_forgotpassword = (TextView) findViewById(R.id.tv_forgotpassword);

        edtxt_email = (EditText) findViewById(R.id.edtxt_email);
        edtxt_password = (EditText) findViewById(R.id.edtxt_password);

        cd = new ConnectionDetector(getApplicationContext());
        // get Internet status
        isInternetPresent = cd.isConnectingToInternet();

        // google login code
        mGoogleApiClient = new GoogleApiClient.Builder(this).addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this).addApi(Plus.API, Plus.PlusOptions.builder().build())
                .addScope(Plus.SCOPE_PLUS_LOGIN).build();
        mGoogleApiClient.connect();


        // Login button click
        iView_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (edtxt_email.getText().toString().length() == 0) {
                    customAlertDialog(getResources().getString(R.string.login_failed), getResources().getString(R.string.err_email));
                } else if (edtxt_password.getText().toString().length() == 0) {
                    customAlertDialog(getResources().getString(R.string.login_failed), getResources().getString(R.string.err_password));
                } else if (!isValidEmail(edtxt_email.getText().toString().trim())) {
                    customAlertDialog(getResources().getString(R.string.login_failed), getResources().getString(R.string.err_valid_email));
                } else {
                    dologinWithEmail();
                }
            }
        });

        // Register button click
        iView_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginActivity.this, SignUpActivity.class);
                startActivity(intent);
                finish();
                overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
            }
        });

        //facebook integration starts

        iView_fblogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isInternetPresent = cd.isConnectingToInternet();

                if (isInternetPresent) {
                    LoginManager.getInstance().logInWithReadPermissions(LoginActivity.this, Arrays.asList("public_profile", "email"));
                } else {
                    customAlertDialog("BullyBeware", BullyConstants.NO_INTERNET_CONNECTED);
                }
            }
        });

        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {

                token = loginResult.getAccessToken().getToken();
                uid = loginResult.getAccessToken().getUserId();

                System.out.println("token" + token);
                System.out.println("uid" + uid);
                preference.setSocial_id(uid);

                facebookSocialLogin();

            }

            @Override
            public void onCancel() {
                Toast.makeText(LoginActivity.this, "Login cancelled by user!", Toast.LENGTH_LONG).show();
                System.out.println("Facebook Login failed!!");
            }

            @Override
            public void onError(FacebookException error) {
                Toast.makeText(LoginActivity.this, "Login unsuccessful!", Toast.LENGTH_LONG).show();
//                System.out.println("Facebook Login failed!! because of " + error.getCause().toString());
            }
        });

        //facebook integration ends

        //Gmail integration starts


        iView_googlelogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isInternetPresent = cd.isConnectingToInternet();

                click = "google_click";

                if (isInternetPresent) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        addPermissionDialogMarshMallow();
                    } else {
                        signInWithGplus();
                    }
                } else {
                    customAlertDialog("BullyBeware", BullyConstants.NO_INTERNET_CONNECTED);
                }
            }
        });




        tv_forgotpassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                forgotpasswordDialog();
            }
        });
    }

    protected static boolean isValidEmail(CharSequence target) {
        return target != null && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        }
        else if (requestCode == RC_SIGN_IN) {
            if (resultCode != RESULT_OK) {
                mSignInClicked = false;
            }
            mIntentInProgress = false;
            if (!mGoogleApiClient.isConnecting()) {
                mGoogleApiClient.connect();
            }

        } else if (requestCode == 125 && resultCode == RESULT_OK) {
            Bundle extra = data.getExtras();
            token = extra.getString("authtoken");
            getProfileInformation();
        }

        else {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }


    // Facebook login api call
    private void facebookSocialLogin() {
        isInternetPresent = cd.isConnectingToInternet();
        if (isInternetPresent) {
            final ProgressDialog d = BullyDialogs.showLoading(LoginActivity.this);
            d.setCanceledOnTouchOutside(false);

            RestClient.get().getfbprofile(token, uid, new Callback<FbProfileInfoPojo>() {
                @Override
                public void success(FbProfileInfoPojo basepojo, Response response) {

                    System.out.println("xxxx" + "Sucess");
                    if (basepojo != null) {
                        if (basepojo.getStatus()) {
                            String f_name = "";
                            String l_name = "";
                            String email = "";
                            if (basepojo.getObject().getFirst_name()!= null)
                            {
                                f_name = basepojo.getObject().getFirst_name();
                            }
                            if (basepojo.getObject().getLast_name()!= null)
                            {
                                l_name = basepojo.getObject().getLast_name();
                            }
                            if (basepojo.getObject().getEmail()!= null)
                            {
                                email = basepojo.getObject().getEmail();
                            }

                            preference.setLOGGEDINFROM("fb");
                            preference.setStudentId(basepojo.getObject().getStudent_id());


                            System.out.println("hhhhh f_name "+f_name);
                            System.out.println("hhhhh l_name "+l_name);
                            System.out.println("hhhhh email "+email);

                            if ((f_name.equals("null") || f_name.isEmpty()) || (l_name.equals("null") || l_name.isEmpty()) ||
                               (email.equals("null") || email.isEmpty()))
                            {
                                customAlertDialog("Registration Failed",getResources().getString(R.string.fb_error));
                            }
                            else
                            {
                                if (basepojo.getObject().getStudent_id() != null) {
                                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                                    preference.setFname(f_name);
                                    preference.setLastName(l_name);
                                    preference.setEmail(email);
                                    if (basepojo.getObject().getSchool_email() != null && !basepojo.getObject().getSchool_email().isEmpty())
                                    {
                                        preference.setSchool_email(basepojo.getObject().getSchool_email());
                                    }
                                    if (basepojo.getObject().getSchool_id() != null && !basepojo.getObject().getSchool_id().isEmpty())
                                    {
                                        preference.setSchool_id(basepojo.getObject().getSchool_id());
                                    }
                                    System.out.println("hh yashal setSchool_id "+basepojo.getObject().getSchool_id());
                                    System.out.println("hh yashal setSchool_email "+basepojo.getObject().getSchool_email());
                                    startActivity(intent);
                                    overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
                                    finish();

                                    TrackEvent("Login", "facebook", email);

                                } else {
                                    Intent intent = new Intent(LoginActivity.this, SocialSignUpActivity.class);
                                    intent.putExtra("social", "facebook");
                                    preference.setFname(f_name);
                                    preference.setLastName(l_name);
                                    preference.setEmail(email);
                                    startActivity(intent);
                                    overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
                                    finish();
                                }
                            }

                        } else {
                            BullyToast.showToast(LoginActivity.this, BullyConstants.LOGIN_FAILED);
                        }
                    } else {
//                        BullyToast.showToast(LoginActivity.this, BullyConstants.LOGIN_FAILED);
                        BullyToast.showToast(LoginActivity.this, BullyConstants.SERVER_NOT_RESPOND);
                    }
                    d.dismiss();
                }

                @Override
                public void failure(RetrofitError error) {
                    System.out.println("xxxx" + "failure");
                    d.dismiss();
                }
            });
        } else {
            BullyToast.showToast(LoginActivity.this, BullyConstants.NO_INTERNET_CONNECTED);
        }
    }

    // google plus api call

    private void googleplusLogin(String token, String googleId) {
        isInternetPresent = cd.isConnectingToInternet();
        if (isInternetPresent) {
            if (!isFinishing()) {
                d = BullyDialogs.showLoading(LoginActivity.this);
                d.setCanceledOnTouchOutside(false);
            }

            RestClient.get().getgooglePlusprofile(token, googleId, new Callback<GooglePlusProfilePojo>() {
                @Override
                public void success(GooglePlusProfilePojo basepojo, Response response) {

                    System.out.println("xxx" + "Succeess");
                    if (basepojo != null) {
                        if (basepojo.getStatus()) {

                            String f_name = "";
                            String l_name = "";
                            String email = "";
                            if (basepojo.getObject().getGiven_name()!= null)
                            {
                                f_name = basepojo.getObject().getGiven_name();
                            }
                            if (basepojo.getObject().getFamily_name()!= null)
                            {
                                l_name = basepojo.getObject().getFamily_name();
                            }
                            if (basepojo.getObject().getEmail()!= null)
                            {
                                email = basepojo.getObject().getEmail();
                            }

                            preference.setLOGGEDINFROM("gmail");
                            preference.setStudentId(basepojo.getObject().getStudent_id());

                            System.out.println("hhhhh f_name "+f_name);
                            System.out.println("hhhhh l_name "+l_name);
                            System.out.println("hhhhh email "+email);

                            if ((f_name.equals("null") || f_name.isEmpty()) || (l_name.equals("null") || l_name.isEmpty()) ||
                                    (email.equals("null") || email.isEmpty()))
                            {
                                customAlertDialog("Registration Failed",getResources().getString(R.string.g_plus_error));
                            }
                            else
                            {
                                if (basepojo.getObject().getStudent_id() != null) {
                                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                                    preference.setFname(f_name);
                                    preference.setLastName(l_name);
                                    preference.setEmail(email);
                                    if (basepojo.getObject().getSchool_email() != null && !basepojo.getObject().getSchool_email().isEmpty())
                                    {
                                        preference.setSchool_email(basepojo.getObject().getSchool_email());
                                    }
                                    if (basepojo.getObject().getSchool_id() != null && !basepojo.getObject().getSchool_id().isEmpty())
                                    {
                                        preference.setSchool_id(basepojo.getObject().getSchool_id());
                                    }
                                    System.out.println("hh yashal setSchool_id "+basepojo.getObject().getSchool_id());
                                    System.out.println("hh yashal setSchool_email "+basepojo.getObject().getSchool_email());
                                    startActivity(intent);
                                    overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
                                    finish();

                                    TrackEvent("Login", "google plus", email);

                                } else {
                                    Intent intent = new Intent(LoginActivity.this, SocialSignUpActivity.class);
                                    intent.putExtra("social", "google plus");
                                    preference.setFname(f_name);
                                    preference.setLastName(l_name);
                                    preference.setEmail(email);
                                    startActivity(intent);
                                    overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
                                    finish();
                                }
                            }


                            googlePlusLogout();

                        } else {
                            BullyToast.showToast(LoginActivity.this, basepojo.getMessage());
                            googlePlusLogout();
                        }
                    } else {
//                        BullyToast.showToast(LoginActivity.this, BullyConstants.LOGIN_FAILED);
                        BullyToast.showToast(LoginActivity.this, BullyConstants.SERVER_NOT_RESPOND);
                    }
                    if (d != null) {
                        d.dismiss();
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    System.out.println("xxx" + "failure");
                    d.dismiss();
                }
            });
        } else {
            BullyToast.showToast(LoginActivity.this, BullyConstants.NO_INTERNET_CONNECTED);
        }
    }


    // email login api call

    private void dologinWithEmail() {
        isInternetPresent = cd.isConnectingToInternet();
        if (isInternetPresent) {
            final ProgressDialog d = BullyDialogs.showLoading(LoginActivity.this);
            d.setCanceledOnTouchOutside(false);

            password = edtxt_password.getText().toString().trim();

            md5(password);

            RestClient.get().login(edtxt_email.getText().toString().trim(), password, new Callback<LogInPojo>() {
                @Override
                public void success(LogInPojo logInPojo, Response response) {

                    if (logInPojo != null) {
                        if (logInPojo.getStatus()) {

                            preference.setLOGGEDINFROM("normal");
                            preference.setStudentId(logInPojo.getObject().getStudent_id());
                            preference.setEmail(logInPojo.getObject().getEmail());
                            preference.setSchool_id(logInPojo.getObject().getSchool_id());
                            preference.setSchool_email(logInPojo.getObject().getSchool_email());
                            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                            startActivity(intent);
                            overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
                            finish();

                            TrackEvent("Login", "email", logInPojo.getObject().getEmail());

                        } else {
                            customAlertDialog(getResources().getString(R.string.login_failed), logInPojo.getMessage());
                        }
                    } else {
//                        BullyToast.showToast(LoginActivity.this, BullyConstants.LOGIN_FAILED);
                        BullyToast.showToast(LoginActivity.this, BullyConstants.SERVER_NOT_RESPOND);
                    }
                    d.dismiss();
                }

                @Override
                public void failure(RetrofitError error) {
                    d.dismiss();
                }
            });
        } else {
            customAlertDialog("BullyBeware", BullyConstants.NO_INTERNET_CONNECTED);
        }
    }


    private void addPermissionDialogMarshMallow() {
        ArrayList<String> permissions = new ArrayList<>();
        int resultCode = 0;

        permissions.add(GET_ACCOUNTS);
        resultCode = GET_ACCOUNTS_RESULT;

        //filter out the permissions we have already accepted
        permissionsToRequest = findUnAskedPermissions(permissions, sharedPreferences);
        //get the permissions we have asked for before but are not granted..
        //we will store this in a global list to access later.
        permissionsRejected = findRejectedPermissions(permissions, sharedPreferences);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            if (permissionsToRequest.size() > 0) {//we need to ask for permissions
                //but have we already asked for them?
                requestPermissions(permissionsToRequest.toArray(new String[permissionsToRequest.size()]), resultCode);
                //mark all these as asked..
                for (String perm : permissionsToRequest) {
                    markAsAsked(perm, sharedPreferences);
                }
            } else {
                //show the success banner
                if (permissionsRejected.size() < permissions.size()) {
                    //this means we can show success because some were already accepted.
                    //permissionSuccess.setVisibility(View.VISIBLE);
                    signInWithGplus();
                }

                if (permissionsRejected.size() > 0) {
                    //we have none to request but some previously rejected..tell the user.
                    //It may be better to show a dialog here in a prod application

                    requestPermissions(permissionsToRequest.toArray(new String[permissionsToRequest.size()]), resultCode);
                    //mark all these as asked..
                    for (String perm : permissionsToRequest) {
                        markAsAsked(perm, sharedPreferences);
                    }
                }
            }
        }
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {
            case GET_ACCOUNTS_RESULT:
                if (hasPermission(GET_ACCOUNTS)) {
                    signInWithGplus();
                } else {
                    permissionsRejected.add(GET_ACCOUNTS);
                    clearMarkAsAsked(GET_ACCOUNTS, sharedPreferences);
                    String message = "permission for contact access was rejected. Please allow to run the app.";
                    makePostRequestSnack(message, permissionsRejected.size());
                }
                break;
        }
    }

    // password change md5 format

    public static final String md5(final String s) {
        final String MD5 = "MD5";
        try {
            // Create MD5 Hash
            MessageDigest digest = java.security.MessageDigest
                    .getInstance(MD5);
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuilder hexString = new StringBuilder();
            for (byte aMessageDigest : messageDigest) {
                String h = Integer.toHexString(0xFF & aMessageDigest);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
                password = hexString.toString();
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

    // Forgot password

    private void forgotpasswordDialog() {
        dialogLogout = new Dialog(LoginActivity.this, android.R.style.Theme_Translucent_NoTitleBar);
        dialogLogout.setContentView(R.layout.custom_dialog);
        dialogLogout.show();

        fp_edtxt_email = (EditText) dialogLogout.findViewById(R.id.fp_edtxt_email);

        TextView dialog_header = (TextView) dialogLogout.findViewById(R.id.dialog_header);
        dialog_header.setVisibility(View.VISIBLE);
        dialog_header.setText("Forgot Password");
        ImageView iView_cross = (ImageView) dialogLogout.findViewById(R.id.dialog_header_cross);
        TextView tv_submit = (TextView) dialogLogout.findViewById(R.id.dialog_submit);
        tv_submit.setText("Submit");


        tv_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (fp_edtxt_email.getText().toString().length() == 0) {
                    customAlertDialog("FORGOT PASSWORD!", getResources().getString(R.string.err_email));
                } else if (!isValidEmail(fp_edtxt_email.getText().toString().trim())) {
                    customAlertDialog("FORGOT PASSWORD!", getResources().getString(R.string.err_valid_email));
                } else {
                    if (isInternetPresent) {
                        ForgotpasswordApi();
                    } else {
                        BullyToast.showToast(LoginActivity.this, BullyConstants.NO_INTERNET_CONNECTED);
                    }
                }
            }
        });
        iView_cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogLogout.dismiss();
            }
        });
    }

    // Forgot password api call

    private void ForgotpasswordApi() {
        isInternetPresent = cd.isConnectingToInternet();

        if (isInternetPresent) {
            final ProgressDialog d = BullyDialogs.showLoading(LoginActivity.this);
            d.setCanceledOnTouchOutside(false);

            RestClient.get().forgotpassword(fp_edtxt_email.getText().toString(), new Callback<ForgotPasswordPojo>() {
                @Override
                public void success(ForgotPasswordPojo forgotPasswordPojo, Response response) {
                    if (forgotPasswordPojo != null) {
                        if (forgotPasswordPojo.getStatus()) {
                            Toast.makeText(LoginActivity.this, forgotPasswordPojo.getMessage(), Toast.LENGTH_LONG).show();

                            preference.setStudentId(forgotPasswordPojo.getObject().getStudent_id());

                            Intent intent = new Intent(LoginActivity.this, ChangePasswordActivity.class);
                            intent.putExtra("from", "login");
                            intent.putExtra("otp", forgotPasswordPojo.getObject().getOTP());
                            startActivity(intent);
                            finish();
                            overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
                            dialogLogout.dismiss();
                        } else {
                            customAlertDialog("FORGOT PASSWORD!", forgotPasswordPojo.getMessage());
                        }
                        d.dismiss();
                    }
                    else
                    {
                        d.dismiss();
                        BullyToast.showToast(LoginActivity.this, BullyConstants.SERVER_NOT_RESPOND);
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    d.dismiss();
                }
            });
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();

        finish();
    }

    public void customAlertDialog(String title, String text) {
        final Dialog dialogLogout = new Dialog(LoginActivity.this, android.R.style.Theme_Translucent_NoTitleBar);
        dialogLogout.setContentView(R.layout.custom_dialog);
        dialogLogout.show();

        EditText fp_edtxt_email = (EditText) dialogLogout.findViewById(R.id.fp_edtxt_email);
        fp_edtxt_email.setVisibility(View.GONE);

        TextView dialog_header = (TextView) dialogLogout.findViewById(R.id.dialog_header);
        dialog_header.setVisibility(View.VISIBLE);
        dialog_header.setText(title);
        ImageView iView_cross = (ImageView) dialogLogout.findViewById(R.id.dialog_header_cross);
        TextView tv_submit = (TextView) dialogLogout.findViewById(R.id.dialog_submit);
        tv_submit.setText("OK");
        TextView dialog_text = (TextView) dialogLogout.findViewById(R.id.dialog_text);
        dialog_text.setVisibility(View.VISIBLE);
        dialog_text.setText(text);

        tv_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialogLogout.dismiss();
            }
        });
        iView_cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogLogout.dismiss();
            }
        });
    }

    // google plus login

    private void signInWithGplus() {
        if (!mGoogleApiClient.isConnecting()) {
            mSignInClicked = true;
            resolveSignInError();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        // make sure to initiate connection
        mGoogleApiClient.connect();
    }

    private void resolveSignInError() {
        if (mConnectionResult != null && mConnectionResult.hasResolution()) {
            try {
                mIntentInProgress = true;
                mConnectionResult.startResolutionForResult(this, RC_SIGN_IN);
            } catch (IntentSender.SendIntentException e) {
                mIntentInProgress = false;
                mGoogleApiClient.connect();
            }
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        mSignInClicked = false;
        getProfileInformation();
    }

    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        if (!connectionResult.hasResolution()) {
            GooglePlayServicesUtil.getErrorDialog(connectionResult.getErrorCode(), this,
                    0).show();
            return;
        }

        if (!mIntentInProgress) {
            // Store the ConnectionResult for later usage
            mConnectionResult = connectionResult;
            if (mSignInClicked) {
                // The user has already clicked 'sign-in' so we attempt to
                // resolve all
                // errors until the user is signed in, or they cancel.
                resolveSignInError();
            }
        }
    }

    private void getProfileInformation() {
        new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... params) {

                try {
                    if (Plus.PeopleApi.getCurrentPerson(mGoogleApiClient) != null) {
                        final String SCOPES = "oauth2:profile email";
//                        final String SCOPES = "oauth2:" +Scopes.PLUS_LOGIN+" "/*+Scopes.PROFILE+" "*/+Scopes.EMAIL;
//                        final String SCOPES = "oauth2:https://www.googleapis.com/auth/plus.login";

                        try {
                            token = GoogleAuthUtil.getToken(
                                    LoginActivity.this,
                                    Plus.AccountApi.getAccountName(mGoogleApiClient),
                                    SCOPES);

                            Person currentPerson = Plus.PeopleApi.getCurrentPerson(mGoogleApiClient);
                            googleId = currentPerson.getId();

                        } catch (SecurityException e) {
                            System.out.println("hh PERMISSION_NOT_GRANTED");
                        }

                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    System.out.println("hh IOException ");
                } catch (UserRecoverableAuthException e) {
                    e.printStackTrace();
                    Intent recover = e.getIntent();
                    startActivityForResult(recover, 125);
                    System.out.println("hh UserRecoverableAuthException ");
                } catch (GoogleAuthException authEx) {
                    // The call is not ever expected to succeed
                    // assuming you have already verified that
                    // Google Play services is installed.
                }

                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                preference.setSocial_id(googleId);
                if (token != null)
                    googleplusLogin(token, googleId);
            }
        }.execute();

    }

    // google plus logout
    private void googlePlusLogout() {
        System.out.println("hh logout");
        if (mGoogleApiClient.isConnected()) {
            Plus.AccountApi.clearDefaultAccount(mGoogleApiClient);
            mGoogleApiClient.disconnect();
            mGoogleApiClient.connect();
        }
    }
}
