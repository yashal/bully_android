package com.nexgen.bullybeware.activity;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.text.InputType;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import com.nexgen.bullybeware.R;
import com.nexgen.bullybeware.adapter.AudioReportAdapter;
import com.nexgen.bullybeware.adapter.IncidentTypeAdapter;
import com.nexgen.bullybeware.database.DatabaseHandler;
import com.nexgen.bullybeware.model.RestClient;
import com.nexgen.bullybeware.pojo.ImagelistStatusPojo;
import com.nexgen.bullybeware.pojo.IncidentTypeListPojo;
import com.nexgen.bullybeware.pojo.IncidentTypePojo;
import com.nexgen.bullybeware.pojo.ReportSubmissionFilesPojo;
import com.nexgen.bullybeware.pojo.ReportSubmissionPojo;
import com.nexgen.bullybeware.service.AudioUploadService;
import com.nexgen.bullybeware.util.BullyConstants;
import com.nexgen.bullybeware.util.BullyDialogs;
import com.nexgen.bullybeware.util.BullyPreferences;
import com.nexgen.bullybeware.util.BullyToast;
import com.nexgen.bullybeware.util.ConnectionDetector;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


/**
 * Created by quepplin1 on 9/29/2016.
 */
public class AudioReportActivity extends BaseActivity implements View.OnClickListener {

    private AudioReportActivity ctx = this;
    private EditText edtxt_title,edtxt_date,edtext_bullyName,edtxt_victinName,edtxt_nearestAdult,edtxt_description;
    private ViewPager viewPager;
    private Spinner type_spineer;
    private ArrayList<IncidentTypeListPojo> arraylist;
    private IncidentTypeAdapter adapter;
    private AudioReportAdapter audioAdapter;
    private String incident_id= "0" ;
    private TextView tv_submit;
    private Calendar calendar;
    private int year, month, day;
    private String date,incident_type;
    private ProgressDialog d;
    private DatabaseHandler db;
    private String type,title,victim_name,nearest_adult,description,incident,Submit_id,bully_name,school_id;
    private Dialog dialogLogout;

    // flag for Internet connection status
    Boolean isInternetPresent = false;

    // Connection detector class
    ConnectionDetector cd;
    private BullyPreferences preferences;
    ArrayList<String> object;
    private ArrayList<ImagelistStatusPojo> audiostatus;
    private ImagelistStatusPojo statusPojo;
    private static final long DOUBLE_CLICK_TIME_DELTA = 2000;//milliseconds
    long lastClickTime = 0;
    private LinearLayout ll_Audio_preview;

    private ImageView terms_checkbox;
    private boolean status_check = false;
    private boolean isChecked = true;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_incident);

        setDrawerDataCross("Report Incident");

        init();
        setData();
        setIncidentTypeData();
        setOnClickListner();

    }
    private void init(){
        edtxt_title = (EditText) findViewById(R.id.report_edtxt_title);
        edtxt_date  = (EditText) findViewById(R.id.report_edtxt_date);
        edtxt_victinName = (EditText) findViewById(R.id.report_edtxt_victim_name);
        edtext_bullyName = (EditText) findViewById(R.id.report_edtxt_Bully_name);
        edtxt_nearestAdult = (EditText) findViewById(R.id.report_edtxt_nearest_adult);
        edtxt_description = (EditText) findViewById(R.id.report_edtxt_description);
        tv_submit = (TextView) findViewById(R.id.report_tv_submit);

        ll_Audio_preview = (LinearLayout) findViewById(R.id.ll_image_report);

        terms_checkbox = (ImageView) findViewById(R.id.terms_checkbox);

        edtxt_title.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
        edtxt_victinName.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);

        type_spineer = (Spinner) findViewById(R.id.type_spinner);

        viewPager = (ViewPager) findViewById(R.id.report_pager);

        arraylist = new ArrayList<IncidentTypeListPojo>();

        preferences = new BullyPreferences(AudioReportActivity.this);

        cd = new ConnectionDetector(getApplicationContext());
        // get Internet status
        isInternetPresent = cd.isConnectingToInternet();

        audiostatus = new ArrayList<ImagelistStatusPojo>();

        db = new DatabaseHandler(AudioReportActivity.this);

        // set date and time

        calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);

        month = calendar.get(Calendar.MONTH) +1;
        day = calendar.get(Calendar.DAY_OF_MONTH);
        showDate(year, month, day);

        LinearLayout drawer_cross = (LinearLayout) findViewById(R.id.drawerCross);

        drawer_cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Submit_id != null){
                    finish();
                } else {
                    NotSubmitedData();
                }
            }
        });

        terms_checkbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isChecked)
                {
                    terms_checkbox.setImageDrawable(ContextCompat.getDrawable(ctx, R.mipmap.check_box_unchecked));
                    isChecked = false;
                    status_check = true;
                    System.out.println("jhvbjhvjhvjhvjhvhuyj A"); // unchecked
                    showMessageDialog(ctx);
                }
                else
                {
                    terms_checkbox.setImageDrawable(ContextCompat.getDrawable(ctx, R.mipmap.check_box_checked));
                    isChecked = true;
                    status_check = false;
                    System.out.println("jhvbjhvjhvjhvjhvhuyj B");  // checked

                }
            }
        });

    }

    private void setOnClickListner(){
        type_spineer.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if (position == 0){

                    incident_id = "0";

                    LinearLayout parent1 = (LinearLayout) view;

                    ((TextView) parent1.getChildAt(0)).setTextColor(getResources().getColor(R.color.song_name_text_color));

                } else {
                    incident_id = arraylist.get(position).getId();

                    incident_type = arraylist.get(position).getIncident();

                    LinearLayout paren= (LinearLayout) view;
                    ((TextView) paren.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        tv_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (preferences.getSchool_id().length() > 0){
                    school_id = preferences.getSchool_id();
                } else {
                    school_id = preferences.getSchool_email();
                }
                // get Internet status
                isInternetPresent = cd.isConnectingToInternet();

                if (isInternetPresent){
                    if (incident_id.equalsIgnoreCase("0")){
                        customAlertDialog(getResources().getString(R.string.submission_failed),getResources().getString(R.string.err_incident_type));
                    } else if (date.length() == 0){
                        customAlertDialog(getResources().getString(R.string.submission_failed),getResources().getString(R.string.err_incident_date));
                    } else if (edtxt_victinName.getText().toString().length() == 0){
                        customAlertDialog(getResources().getString(R.string.submission_failed),getResources().getString(R.string.err_victim_name));
                    }  else if (school_id.length() == 0){
                        customAlertDialog(getResources().getString(R.string.submission_failed), getResources().getString(R.string.err_school_id));
                    } else {

                        try {
                            for (int i=0;i<audiostatus.size();i++){
                                audiostatus.get(i).setRequeststaus(true);
                            }
                            Calendar cal = Calendar.getInstance();
                            Date currentLocalTime = cal.getTime();
                            DateFormat date2 = new SimpleDateFormat("hh:mm:ss");
                            date2.setTimeZone(TimeZone.getTimeZone("EST"));

                            final String time = date2.format(currentLocalTime);

                            Intent uploadIntent = new Intent(AudioReportActivity.this, AudioUploadService.class);
                            uploadIntent.putExtra("student_id", preferences.getStudentId());
                            uploadIntent.putExtra("incident_id", incident_id);
                            uploadIntent.putExtra("date", date);
                            uploadIntent.putExtra("time", time);
                            uploadIntent.putExtra("title", edtxt_title.getText().toString().trim());
                            uploadIntent.putExtra("bully_name", edtext_bullyName.getText().toString());
                            uploadIntent.putExtra("victim_name", edtxt_victinName.getText().toString().trim());
                            uploadIntent.putExtra("nearest_adult", edtxt_nearestAdult.getText().toString().trim());
                            uploadIntent.putExtra("description", edtxt_description.getText().toString().trim());
                            uploadIntent.putExtra("Submit_id",Submit_id);
                            uploadIntent.putExtra("audio_status", audiostatus);
                            uploadIntent.putExtra("school_id",school_id);
                            uploadIntent.putExtra("status_check", status_check);
                            startService(uploadIntent);

                            BullyToast.showToast(AudioReportActivity.this,"Upload Started");

                            Intent intent = new Intent(AudioReportActivity.this, MainActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                        } catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                } else {
                    audioAdapter.onPageChanged();
                    NetworkDialog();
                }

            }
        });
        edtxt_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog(999);
            }
        });
        ll_Audio_preview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                long clickTime = System.currentTimeMillis();
                if (clickTime - lastClickTime < DOUBLE_CLICK_TIME_DELTA){
                    try {
                        playStopClickListner();
                    } catch (Exception e){
                        e.printStackTrace();
                    }
                    lastClickTime = 0;
                } else {
                    lastClickTime = clickTime;
                }
            }
        });
    }

    // Date picker dialog

    @Override
    protected Dialog onCreateDialog(int id) {
        // TODO Auto-generated method stub
        if (id == 999) {
            DatePickerDialog dialog = new DatePickerDialog(this,myDateListener, year, month, day);
            dialog.getDatePicker().setMaxDate(System.currentTimeMillis());
            return dialog;
        }
        return null;
    }

    private DatePickerDialog.OnDateSetListener myDateListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker arg0, int arg1, int arg2, int arg3) {
            // TODO Auto-generated method stub
            // arg1 = year
            // arg2 = month
            // arg3 = day
            showDate(arg1, arg2+1, arg3);
            arg0.setMaxDate(System.currentTimeMillis());
        }
    };

    private void showDate(int year, Integer month, Integer day) {

        edtxt_date.setText(new StringBuilder().append((month.toString().length() == 1 ? "0"+month.toString():month.toString())).append("/")
                .append((day.toString().length() == 1 ? "0"+day.toString():day.toString())).append("/").append(year));

        date = String.valueOf(new StringBuilder().append(year).append("-")
                .append((month.toString().length() == 1 ? "0"+month.toString():month.toString())).append("-").append((day.toString().length() == 1 ? "0"+day.toString():day.toString())));
    }


    private void setData(){

        Intent intent = getIntent();
        Bundle args = intent.getBundleExtra("BUNDLE");
        object = (ArrayList<String>) args.getSerializable("ARRAYLIST");

        audiostatus.clear();

        for (int i = 0; i < object.size(); i++) {
            statusPojo = new ImagelistStatusPojo();
            statusPojo.setPath(object.get(i));
            audiostatus.add(statusPojo);
        }

        if (getIntent().getStringExtra("type") !=null){
            type = getIntent().getStringExtra("type");
        } if (getIntent().getStringExtra("incident") != null){
            incident = getIntent().getStringExtra("incident");
        }if (getIntent().getStringExtra("title") != null){
            title = getIntent().getStringExtra("title");
            edtxt_title.setText(title);
        } if (getIntent().getStringExtra("Submit_id") != null){
            Submit_id = getIntent().getStringExtra("Submit_id");
        }if (getIntent().getStringExtra("bully_name") != null){
            bully_name = getIntent().getStringExtra("bully_name");
            edtext_bullyName.setText(bully_name);}
        if (getIntent().getStringExtra("date") !=null){
            date = getIntent().getStringExtra("date");
            edtxt_date.setText(date);
        } if (getIntent().getStringExtra("Victim_name") != null){
            victim_name = getIntent().getStringExtra("Victim_name");
            edtxt_victinName.setText(victim_name);
        } if (getIntent().getStringExtra("nearest_adult") != null){
            nearest_adult = getIntent().getStringExtra("nearest_adult");
            edtxt_nearestAdult.setText(nearest_adult);
        } if (getIntent().getStringExtra("description") != null){
            description = getIntent().getStringExtra("description");
            edtxt_description.setText(description);
        }


        audioAdapter = new AudioReportAdapter(AudioReportActivity.this, audiostatus);
        viewPager.setAdapter(audioAdapter);
        audioAdapter.notifyDataSetChanged();

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                audioAdapter.onPageChanged();
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private void setIncidentTypeData() {
        if (isInternetPresent){
            RestClient.get().getIncidentType(new Callback<IncidentTypePojo>() {
                @Override
                public void success(IncidentTypePojo basepojo, Response response) {
                    if (basepojo != null){
                        if (basepojo.getStatus()){
                            arraylist = basepojo.getObject();
                            IncidentTypeListPojo typepojo = new IncidentTypeListPojo();
                            typepojo.setIncident("Select Incident Type");
                            arraylist.add(0, typepojo);

                            int pos = -1;
                            for (int j=0;j<arraylist.size();j++) {
                                if (type != null) {
                                    if (type.equalsIgnoreCase(arraylist.get(j).getIncident())) {

                                        pos = j;
                                        break;
                                    }
                                }
                            }
                            adapter = new IncidentTypeAdapter(AudioReportActivity.this, arraylist);
                            type_spineer.setAdapter(adapter);
                            if(pos != -1){
                                type_spineer.setSelection(pos);
                            } else {
                                type_spineer.setSelection(0);
                            }
                        }
                    }
                    else
                    {
                        d.dismiss();
                        BullyToast.showToast(AudioReportActivity.this, BullyConstants.SERVER_NOT_RESPOND);
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    System.out.println("xxx" + "failure");
                }
            });
        }else {
            IncidentTypeListPojo typepojo = new IncidentTypeListPojo();
            typepojo.setIncident("Select Incident Type");
            arraylist.add(0,typepojo);
            adapter = new IncidentTypeAdapter(AudioReportActivity.this,arraylist);
            type_spineer.setAdapter(adapter);
        }
    }

    // Networkdialog show when user offline

    private void NetworkDialog() {
        dialogLogout = new Dialog(AudioReportActivity.this, android.R.style.Theme_Translucent_NoTitleBar);
        dialogLogout.setContentView(R.layout.custom_dialog);
        dialogLogout.show();

        EditText fp_edtxt_email = (EditText) dialogLogout.findViewById(R.id.fp_edtxt_email);
        fp_edtxt_email.setVisibility(View.GONE);

        TextView dialog_header = (TextView) dialogLogout.findViewById(R.id.dialog_header);
        dialog_header.setVisibility(View.VISIBLE);
        dialog_header.setText(R.string.out_of_network);
        ImageView iView_cross = (ImageView) dialogLogout.findViewById(R.id.dialog_header_cross);
        TextView tv_submit = (TextView) dialogLogout.findViewById(R.id.dialog_submit);
        tv_submit.setText("OK");
        TextView dialog_text = (TextView) dialogLogout.findViewById(R.id.dialog_text);
        dialog_text.setVisibility(View.VISIBLE);


        Calendar cal = Calendar.getInstance(TimeZone.getDefault());
        Date currentLocalTime = cal.getTime();
        DateFormat date1 = new SimpleDateFormat("hh:mm aa");
        date1.setTimeZone(TimeZone.getTimeZone("EST"));

        final String time = date1.format(currentLocalTime);

        Long tsLong = System.currentTimeMillis()/1000;
        final String localTime = tsLong.toString();

        if (Submit_id != null){
            dialog_text.setText(R.string.offline_submit_title);

            tv_submit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                     finish();
                    audioAdapter.onPageChanged();
                }
            });
        } else {
            dialog_text.setText(R.string.offline_submit_message);
            tv_submit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    audioAdapter.onPageChanged();

                    preferences.setSubmit_time(localTime);

                    ReportSubmissionPojo pojo = new ReportSubmissionPojo();
                    pojo.setUSER_ID(preferences.getStudentId());
                    pojo.setFile_type("2");
                    pojo.setSUBMIT_ID(localTime);
                    pojo.setType("");
                    pojo.setSubject(edtxt_title.getText().toString().trim());
                    pojo.setBully_name(edtext_bullyName.getText().toString());
                    pojo.setDate(date);
                    pojo.setTime(time);
                    pojo.setName(edtxt_victinName.getText().toString().trim());
                    pojo.setAdult(edtxt_nearestAdult.getText().toString().trim());
                    pojo.setDescription(edtxt_description.getText().toString().trim());

                    audioupload();

                    db.addDetails(pojo);

                    Intent intent = new Intent(AudioReportActivity.this, MyRecordActivity.class);
                    intent.putExtra("from_activity","Report_incident");
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                }
            });
        }

        iView_cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogLogout.dismiss();
            }
        });
    }

    private void NotSubmitedData(){

        Calendar cal = Calendar.getInstance(TimeZone.getDefault());
        Date currentLocalTime = cal.getTime();
        DateFormat date1 = new SimpleDateFormat("hh:mm aa");
        date1.setTimeZone(TimeZone.getTimeZone("EST"));

        final String time = date1.format(currentLocalTime);

        Long tsLong = System.currentTimeMillis()/1000;
        final String localTime = tsLong.toString();

        preferences.setSubmit_time(localTime);

        ReportSubmissionPojo pojo = new ReportSubmissionPojo();
        pojo.setUSER_ID(preferences.getStudentId());
        pojo.setFile_type("2");
        pojo.setSUBMIT_ID(localTime);
        pojo.setType(incident_type);
        pojo.setSubject(edtxt_title.getText().toString().trim());
        pojo.setBully_name(edtext_bullyName.getText().toString());
        pojo.setDate(date);
        pojo.setTime(time);
        pojo.setName(edtxt_victinName.getText().toString().trim());
        pojo.setAdult(edtxt_nearestAdult.getText().toString().trim());
        pojo.setDescription(edtxt_description.getText().toString().trim());

        audioupload();

        db.addDetails(pojo);

        BullyToast.showToast(AudioReportActivity.this,getResources().getString(R.string.back_submit_message));

        Intent intent = new Intent(AudioReportActivity.this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.tv_status_audio:

                d = BullyDialogs.showLoading(AudioReportActivity.this);
                d.setCanceledOnTouchOutside(false);

                audioupload();

                break;
        }
    }
    private void audioupload(){

            for (int i = 0; i < audiostatus.size(); i++) {
                ReportSubmissionFilesPojo pojo = new ReportSubmissionFilesPojo();
                pojo.setSubmit_id(preferences.getSubmit_time());
                pojo.setImage_url("");
                pojo.setAudio_url(audiostatus.get(i).getPath());
                pojo.setVideo_url("");

                ArrayList<ReportSubmissionFilesPojo> arrayimage = new ArrayList<ReportSubmissionFilesPojo>();
                arrayimage.add(pojo);

                db.addFiles(arrayimage);
            }
    }

  /*  @Override
    protected void onPause() {
        super.onPause();
        if (audioReciver != null) {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(
                    audioReciver);
        }
        if (isFinishing()) {
            // mHandler.removeCallbacksAndMessages(null);
        }
    }*/

    @Override
    protected void onStop() {
        super.onStop();

        audioAdapter.onPageChanged();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        audioAdapter.onPageChanged();

    }

    public void showMessageDialog(final Context ctx) {
        final Dialog dialog_message = new Dialog(ctx, android.R.style.Theme_Translucent_NoTitleBar);
        dialog_message.setContentView(R.layout.report_dialog);

        ImageView close_dialog = (ImageView) dialog_message.findViewById(R.id.dialog_header_cross);
        close_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_message.dismiss();
            }
        });

        LinearLayout ll_dialog_submit = (LinearLayout)dialog_message.findViewById(R.id.ll_dialog_submit);
        ll_dialog_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_message.dismiss();
                isChecked = false;
                status_check = true;
            }
        });

        LinearLayout ll_dialog_cancel = (LinearLayout)dialog_message.findViewById(R.id.ll_dialog_cancel);
        ll_dialog_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog_message.dismiss();
                isChecked = true;
                status_check = false;
                terms_checkbox.setImageDrawable(ContextCompat.getDrawable(ctx, R.mipmap.check_box_checked));
            }
        });

        dialog_message.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        dialog_message.show();
    }
}
