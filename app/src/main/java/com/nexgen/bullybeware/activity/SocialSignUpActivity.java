package com.nexgen.bullybeware.activity;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.plus.People;
import com.google.android.gms.plus.Plus;
import com.nexgen.bullybeware.R;
import com.nexgen.bullybeware.adapter.CityListAdapter;
import com.nexgen.bullybeware.adapter.StateListAdapter;
import com.nexgen.bullybeware.fragment.SecondSignUpFragment;
import com.nexgen.bullybeware.model.FileUploadSer;
import com.nexgen.bullybeware.model.RestClient;
import com.nexgen.bullybeware.model.ServiceGenerator;
import com.nexgen.bullybeware.pojo.CityListPojo;
import com.nexgen.bullybeware.pojo.CityPojo;
import com.nexgen.bullybeware.pojo.SchoolPojo;
import com.nexgen.bullybeware.pojo.SignUpPojo;
import com.nexgen.bullybeware.pojo.StateListPojo;
import com.nexgen.bullybeware.pojo.StatePojo;
import com.nexgen.bullybeware.util.BullyConstants;
import com.nexgen.bullybeware.util.BullyDialogs;
import com.nexgen.bullybeware.util.BullyPreferences;
import com.nexgen.bullybeware.util.BullyToast;
import com.nexgen.bullybeware.util.ConnectionDetector;
import com.nexgen.bullybeware.util.UsPhoneNumberFormatter;

import java.io.File;
import java.lang.ref.WeakReference;
import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedFile;

/**
 * Created by umax desktop on 2/15/2017.
 */

public class SocialSignUpActivity extends BaseActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, ResultCallback<People.LoadPeopleResult> {

    private EditText edtxt_schoolemail;
    private ImageView school_search,iView_terms;
    private BullyPreferences preference;
    private Spinner state_spinner, city_spinner;
    private EditText edtxt_school_id,school_addres,principle_name,school_number;
    private String school_id,school_name,school_address,school_principle_name,school_email,school_mobile,school_id_info;

    private ArrayList<CityListPojo> arrayCityList;
    private ArrayList<StateListPojo> arrayStateList;

    private StateListAdapter state_adapter;
    private CityListAdapter city_adapter;

    private String state_id, city_id;
    private TextView tv_err_state,tv_err_city,tv_err_school_email,tv_err_principal_name;
    private static String fname,lname,email,age;

    private TextInputLayout inputlayoutschoolemail;

    // flag for Internet connection status
    Boolean isInternetPresent = false;

    // Connection detector class
    ConnectionDetector cd;
    private TextView lblContinue,tv_termsCondition;
    private String checkbox_status = "check";

    private GoogleApiClient mGoogleApiClient;
    private String social;
    private boolean status = false;
    private TextView detailstext;
    private String social_type = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.social_signup_activity);

        setDrawerbackIcon("Complete Your profile");

        init();
        getData();
        setStateData();
        setOnClickListner();
//        detailstext.setText("First Name is "+preference.getFname()+"\nLast Name is "+preference.getLastName()+"\nEmail is "+preference.getEmail());

        edtxt_school_id.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                // TODO Auto-generated method stub
                if (count == 0)
                {
                    enableEdittext();
                }
                status = false;
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {

                // TODO Auto-generated method stub
            }
        });
    }

    private void init(){
        edtxt_schoolemail = (EditText) findViewById(R.id.signup_school_email);

        detailstext = (TextView) findViewById(R.id.detailstext);


        lblContinue = (TextView) findViewById(R.id.social_signup_tv_submit);
        tv_err_state = (TextView) findViewById(R.id.social_signup_tv_err_state);
        tv_err_city  = (TextView) findViewById(R.id.social_signup_tv_err_city);
        tv_err_school_email = (TextView) findViewById(R.id.social_signup_tv_err_school_email);
        tv_termsCondition = (TextView) findViewById(R.id.social_signup_tv_terms_condition);

        tv_err_principal_name = (TextView) findViewById(R.id.social_signup_tv_err_principle_name);

        state_spinner = (Spinner) findViewById(R.id.social_signup_state_spinner);
        city_spinner = (Spinner) findViewById(R.id.social_signup_city_spinner);

        edtxt_school_id = (EditText) findViewById(R.id.social_signup_school_id);
        school_addres = (EditText) findViewById(R.id.social_signup_school_add);
        principle_name = (EditText) findViewById(R.id.social_signup_principle_name);
        school_number = (EditText) findViewById(R.id.social_signup_edtxt_telephone);

        edtxt_schoolemail.addTextChangedListener(new MyTextWatcher(edtxt_schoolemail));
        principle_name.addTextChangedListener(new MyTextWatcher(principle_name));

//        school_number.addTextChangedListener(new PhoneNumberTextWatcher(school_number));
        UsPhoneNumberFormatter addLineNumberFormatter = new UsPhoneNumberFormatter(
                new WeakReference<EditText>(school_number));
        school_number.addTextChangedListener(addLineNumberFormatter);

        inputlayoutschoolemail = (TextInputLayout) findViewById(R.id.social_signup_input_layout_school_email);
        school_search = (ImageView) findViewById(R.id.signup_social_school_search);

        iView_terms = (ImageView) findViewById(R.id.signup_social_terms_checkbox);

        cd = new ConnectionDetector(getApplicationContext());
        // get Internet status
        isInternetPresent = cd.isConnectingToInternet();

        arrayCityList = new ArrayList<CityListPojo>();
        arrayStateList = new ArrayList<StateListPojo>();

        preference = new BullyPreferences(this);
        social = getIntent().getStringExtra("social");

        if (social.equalsIgnoreCase("facebook"))
        {
            social_type = "Facebook";
        }
        else
        {
            social_type = "Google";
        }

        // google plus api client

        mGoogleApiClient = new GoogleApiClient.Builder(this).addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this).addApi(Plus.API, Plus.PlusOptions.builder().build())
                .addScope(Plus.SCOPE_PLUS_LOGIN).build();
        mGoogleApiClient.connect();
    }

    private void getData(){

        preference = new BullyPreferences(SocialSignUpActivity.this);

        fname = preference.getFname();
        lname = preference.getLastName();
        email = preference.getEmail();

    }

    // Click listner method
    private void setOnClickListner(){

        lblContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tv_err_state.setVisibility(View.GONE);
                tv_err_city.setVisibility(View.GONE);
                tv_err_principal_name.setVisibility(View.GONE);
                tv_err_school_email.setVisibility(View.GONE);
                submitform();
            }
        });

        state_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                if (i == 0) {

                    LinearLayout parent = (LinearLayout) view;

                    ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.song_name_text_color));

                    state_id = "0";

                    CityListPojo clp = new CityListPojo();
                    clp.setCity_name("City");
                    arrayCityList.clear();
                    arrayCityList.add(0, clp);
                    city_adapter = new CityListAdapter(SocialSignUpActivity.this, arrayCityList);
                    city_spinner.setAdapter(city_adapter);

                } else {
                    state_id = arrayStateList.get(i).getId();

                    System.out.println("id" + state_id);

                    LinearLayout parent = (LinearLayout) view;

                    ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));
                    tv_err_state.setVisibility(View.GONE);

                    setCityData();

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        city_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i == 0) {
                    city_id = "city";

                    LinearLayout parent = (LinearLayout) view;

                    ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.song_name_text_color));
                } else {
                    city_id = arrayCityList.get(i).getId();

                    tv_err_city.setVisibility(View.GONE);

                    LinearLayout parent = (LinearLayout) view;

                    ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        school_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setSchoolData();

            }
        });

        edtxt_school_id.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    setSchoolData();

                    return true;
                }
                return false;
            }
        });

        iView_terms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkbox_status.equalsIgnoreCase("check")){
                    iView_terms.setImageResource(R.mipmap.check_box_unchecked);
                    checkbox_status = "uncheck";
                } else {
                    iView_terms.setImageResource(R.mipmap.check_box_checked);
                    checkbox_status = "check";
                }
            }
        });

        tv_termsCondition.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SocialSignUpActivity.this, TermsConditionsActivity.class);
                startActivity(intent);
            }
        });
    }
    public void submitform(){
        if (state_id.equalsIgnoreCase("0")){
            tv_err_state.setVisibility(View.VISIBLE);
        } else if (city_id.equalsIgnoreCase("city")){
            tv_err_city.setVisibility(View.VISIBLE);
        } else if (principle_name.getText().toString().isEmpty()){
            tv_err_principal_name.setVisibility(View.VISIBLE);
            return;
        } /*else if (!validateEmail(inputlayoutschoolemail,edtxt_schoolemail)){
            return;
        } else if(checkbox_status.equalsIgnoreCase("uncheck")){
            customAlertDialog("Registration Failed","Please accept terms and conditions");
        }
        else {
            doSignUp();
        }*/
        else if (edtxt_school_id.getText().toString().length() == 0 & edtxt_schoolemail.getText().toString().length() == 0){

            tv_err_school_email.setText(getResources().getString(R.string.err_school_email));
            tv_err_school_email.setVisibility(View.VISIBLE);

        }else if (edtxt_schoolemail.getText().toString().length() > 0 && !(checkValidEmail(edtxt_schoolemail.getText().toString()))){
            tv_err_school_email.setText(getResources().getString(R.string.err_valid_school_email));
            tv_err_school_email.setVisibility(View.VISIBLE);
        }else if(checkbox_status.equalsIgnoreCase("uncheck")){
            customAlertDialog("Registration Failed","Please accept terms and conditions");
        }
        else {
            if (edtxt_school_id.getText().toString().length() > 0 && !status)
            {
                customAlertDialog("School Information","Please search with valid School Code.");
            }
            else {
                doSignUp();
            }
        }
    }

    // SignUp form


    private void doSignUp() {
        isInternetPresent = cd.isConnectingToInternet();
        if (isInternetPresent) {
            final ProgressDialog d = BullyDialogs.showLoading(SocialSignUpActivity.this);
            d.setCanceledOnTouchOutside(false);

            FileUploadSer service = ServiceGenerator.createService(FileUploadSer.class, FileUploadSer.BASE_URL);
            TypedFile typedFile = null;

            String social_id = preference.getSocial_id();

            String principle = principle_name.getText().toString();
            System.out.println("hhhhhhh fname "+fname);
            System.out.println("hhhhhhh lname "+lname);
            System.out.println("hhhhhhh school_id "+school_id);
            System.out.println("hhhhhhh state_id "+state_id);
            System.out.println("hhhhhhh city_id "+city_id);
            System.out.println("hhhhhhh school_email "+edtxt_schoolemail.getText().toString());
            System.out.println("hhhhhhh email "+email.trim());
            System.out.println("hhhhhhh password "+"123456");
            System.out.println("hhhhhhh country "+"1");
            System.out.println("hhhhhhh profile_image "+typedFile);
            System.out.println("hhhhhhh socialId "+social_id);
            System.out.println("hhhhhhh age "+age);
            System.out.println("hhhhhhh principle "+principle);
            System.out.println("hhhhhhh school_number "+school_number.getText().toString());
            System.out.println("hhhhhhh social_type "+social_type);

            service.signup(social_type, fname, school_id, state_id, city_id,edtxt_schoolemail.getText().toString(), lname, email.trim(),
                    "123456", "1", typedFile,social_id,age,principle,school_number.getText().toString(),  new Callback<SignUpPojo>() {
                @Override
                public void success(SignUpPojo signUpPojo, Response response) {
                    if (signUpPojo != null) {
                        if (signUpPojo.getStatus()) {
                            BullyToast.showToast(SocialSignUpActivity.this,signUpPojo.getMessage());
                            System.out.println("xxxx success");

                            preference.setStudentId(signUpPojo.getObject().getStudent_id());

                            preference.setSchool_id(signUpPojo.getObject().getSchool_id());

                            preference.setSchool_email(signUpPojo.getObject().getSchool_email());

                            Intent intent = new Intent(SocialSignUpActivity.this, MainActivity.class);
                            startActivity(intent);
                            finish();

                            TrackEvent("Signup",social,signUpPojo.getObject().getEmail());

                            overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);

                        } else {
                            System.out.println("xxxx success but registration failed "+signUpPojo.getMessage());
                            customAlertDialog("Registration Failed",signUpPojo.getMessage());
                        }

                    } else {
                        customAlertDialog("", BullyConstants.REGISTRATION_FAILED);
                    }
                    d.dismiss();
                }

                @Override
                public void failure(RetrofitError error) {
                    System.out.println("xxxx " + "failure");
                    d.dismiss();
                }
            });
        } else
            BullyToast.showToast(SocialSignUpActivity.this, "Sorry! We could not create your account. Please check your internet connection and try again.");
    }
    public void customAlertDialog(String title, String text) {
        final Dialog dialogLogout = new Dialog(SocialSignUpActivity.this, android.R.style.Theme_Translucent_NoTitleBar);
        dialogLogout.setContentView(R.layout.custom_dialog);
        dialogLogout.show();

        EditText fp_edtxt_email = (EditText) dialogLogout.findViewById(R.id.fp_edtxt_email);
        fp_edtxt_email.setVisibility(View.GONE);

        TextView dialog_header = (TextView) dialogLogout.findViewById(R.id.dialog_header);
        dialog_header.setVisibility(View.VISIBLE);
        dialog_header.setText(title);
        ImageView iView_cross = (ImageView) dialogLogout.findViewById(R.id.dialog_header_cross);
        TextView tv_submit = (TextView) dialogLogout.findViewById(R.id.dialog_submit);
        tv_submit.setText("OK");
        TextView dialog_text = (TextView) dialogLogout.findViewById(R.id.dialog_text);
        dialog_text.setVisibility(View.VISIBLE);
        dialog_text.setText(text);

        tv_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialogLogout.dismiss();
            }
        });
        iView_cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogLogout.dismiss();
            }
        });
    }

    // School dialog open

    private void SchoolListDialog() {
        final Dialog dialogLogout = new Dialog(SocialSignUpActivity.this, android.R.style.Theme_Translucent_NoTitleBar);
        dialogLogout.setContentView(R.layout.schoollistdialog);
        dialogLogout.show();


        TextView dialog_header = (TextView) dialogLogout.findViewById(R.id.school_dialog_header);
        dialog_header.setVisibility(View.VISIBLE);
        dialog_header.setText("School Information");
        ImageView iView_cross = (ImageView) dialogLogout.findViewById(R.id.dialog_header_cross);
        TextView tv_submit = (TextView) dialogLogout.findViewById(R.id.schooldialog_submit);
        tv_submit.setText("YES");
        TextView tv_cancel = (TextView) dialogLogout.findViewById(R.id.schooldialog_cancel);
        TextView dialog_text = (TextView) dialogLogout.findViewById(R.id.schooldialog_text);
        dialog_text.setVisibility(View.VISIBLE);
        dialog_text.setText("Is this Correct?");

        TextView edtxt_school_name = (TextView) dialogLogout.findViewById(R.id.dialog_school_name);
        final TextView school_add = (TextView) dialogLogout.findViewById(R.id.dialog_school_add);

        edtxt_school_name.setText(school_name);
        school_add.setText(school_address);

        tv_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                edtxt_schoolemail.setText(school_email);
                school_addres.setText(school_address);
                principle_name.setText(school_principle_name);
                school_number.setText(school_mobile);

                school_id = school_id_info;
                status = true;
                disableEdittext();
                dialogLogout.dismiss();

            }
        });
        iView_cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogLogout.dismiss();
            }
        });

        tv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogLogout.dismiss();
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
        lblContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                submitform();
            }
        });
    }

    // State data show
    private void setStateData() {
        if (isInternetPresent) {
            RestClient.get().getState(new Callback<StatePojo>() {
                @Override
                public void success(StatePojo statePojo, Response response) {
                    System.out.println("xxx" + "Sucess");
                    if (statePojo != null) {
                        if (statePojo.getStatus()) {
                            if (statePojo.getObject().size() > 0){
                                arrayStateList = statePojo.getObject();
                                StateListPojo slp = new StateListPojo();
                                slp.setState_name("State");
                                arrayStateList.add(0, slp);
                                state_adapter = new StateListAdapter(SocialSignUpActivity.this, arrayStateList);
                                state_spinner.setAdapter(state_adapter);

                            } else {
                                BullyToast.showToast(SocialSignUpActivity.this, BullyConstants.NO_DATA);
                            }
                        } else {
                            BullyToast.showToast(SocialSignUpActivity.this,statePojo.getMessage());
                        }
                    } else {
                        StateListPojo slp = new StateListPojo();
                        slp.setState_name("State");
                        arrayStateList.add(0, slp);
                        state_adapter = new StateListAdapter(SocialSignUpActivity.this, arrayStateList);
                        state_spinner.setAdapter(state_adapter);
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    System.out.println("xxx" + "Failure");
                }
            });
        } else {
            StateListPojo slp = new StateListPojo();
            slp.setState_name("State");
            arrayStateList.add(0, slp);
            state_adapter = new StateListAdapter(SocialSignUpActivity.this, arrayStateList);
            state_spinner.setAdapter(state_adapter);
        }
    }

    // City data show
    private void setCityData() {
        if (isInternetPresent) {
            RestClient.get().getCity(state_id, new Callback<CityPojo>() {
                @Override
                public void success(CityPojo cityPojo, Response response) {
                    if (cityPojo != null) {
                        if (cityPojo.getStatus()) {
                            arrayCityList = cityPojo.getObject();
                            CityListPojo clp = new CityListPojo();
                            clp.setCity_name("City");
                            arrayCityList.add(0, clp);
                            city_adapter = new CityListAdapter(SocialSignUpActivity.this, arrayCityList);
                            city_spinner.setAdapter(city_adapter);
                        } else {
                            //  BullyToast.showToast(getActivity(),cityPojo.getMessage());
                        }
                    } else {
                           BullyToast.showToast(SocialSignUpActivity.this,BullyConstants.SERVER_NOT_RESPOND);
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    System.out.println("xxx" + "failure");
                }
            });
        } else {
            CityListPojo clp = new CityListPojo();
            clp.setCity_name("City");
            arrayCityList.add(0, clp);
            city_adapter = new CityListAdapter(SocialSignUpActivity.this, arrayCityList);
            city_spinner.setAdapter(city_adapter);
        }
    }

    // School data show
    private void setSchoolData(){
        if (isInternetPresent){
            final ProgressDialog d = BullyDialogs.showLoading(SocialSignUpActivity.this);
            d.setCanceledOnTouchOutside(false);

            RestClient.get().getSchoolList(edtxt_school_id.getText().toString(), new Callback<SchoolPojo>() {
                @Override
                public void success(SchoolPojo schoolPojo, Response response) {

                    if (schoolPojo != null){
                        if (schoolPojo.getStatus()){
                            System.out.println("xxx right");

                            school_id_info = schoolPojo.getObject().getId();
                            school_name = schoolPojo.getObject().getSchool_name();
                            school_address = schoolPojo.getObject().getSchool_address();
                            school_principle_name = schoolPojo.getObject().getPrinciple_name();
                            school_mobile = schoolPojo.getObject().getSchool_mobile();
                            school_email = schoolPojo.getObject().getSchool_email();

                            SchoolListDialog();
                        } else {
                            customAlertDialog("School Information",schoolPojo.getMessage());
                        }
                        d.dismiss();
                    }
                    else
                    {
                        d.dismiss();
                        BullyToast.showToast(SocialSignUpActivity.this, BullyConstants.SERVER_NOT_RESPOND);
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    System.out.println("xxx fail");
                    d.dismiss();
                }
            });
        }
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        }
    }
    public  boolean validateEmail(TextInputLayout layoutTxt,EditText edtTxt) {
        if (edtTxt.getText().toString().trim().isEmpty())  {
            tv_err_school_email.setText(getResources().getString(R.string.err_school_email));
            tv_err_school_email.setVisibility(View.VISIBLE);
            requestFocus(edtTxt);
            return false;
        } else {
            layoutTxt.setErrorEnabled(false);
        }

        return true;
    }

    // Google plus logout
    private void googlePlusLogout() {
        System.out.println("hh logout");
        if (mGoogleApiClient.isConnected()) {
            Plus.AccountApi.clearDefaultAccount(mGoogleApiClient);
            mGoogleApiClient.disconnect();
            mGoogleApiClient.connect();
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Plus.PeopleApi.loadVisible(mGoogleApiClient, null).setResultCallback(
                this);
    }

    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onResult(@NonNull People.LoadPeopleResult loadPeopleResult) {

    }

    // Mobile edit textWatcher added

    public class PhoneNumberTextWatcher implements TextWatcher {
        private EditText edTxt;
        private boolean isDelete;

        public PhoneNumberTextWatcher(EditText edTxtPhone) {
            this.edTxt = edTxtPhone;
            edTxt.setOnKeyListener(new View.OnKeyListener() {

                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    if (keyCode == KeyEvent.KEYCODE_DEL) {
                        isDelete = true;
                    }
                    return false;
                }
            });
        }

        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        public void beforeTextChanged(CharSequence s, int start, int count,
                                      int after) {
        }

        public void afterTextChanged(Editable s) {

            if (isDelete) {
                isDelete = false;
                return;
            }
            String val = s.toString();
            String a = "";
            String b = "";
            String c = "";
            if (val != null && val.length() > 0) {
                val = val.replace("-", "");
                if (val.length() >= 3) {
                    a = val.substring(0, 3);
                } else if (val.length() < 3) {
                    a = val.substring(0, val.length());
                }
                if (val.length() >= 6) {
                    b = val.substring(3, 6);
                    c = val.substring(6, val.length());
                } else if (val.length() > 3 && val.length() < 6) {
                    b = val.substring(3, val.length());
                }
                StringBuffer stringBuffer = new StringBuffer();
                if (a != null && a.length() > 0) {
                    stringBuffer.append(a);
                    if (a.length() == 3) {
                        stringBuffer.append("-");
                    }
                }
                if (b != null && b.length() > 0) {
                    stringBuffer.append(b);
                    if (b.length() == 3) {
                        stringBuffer.append("-");
                    }
                }
                if (c != null && c.length() > 0) {
                    stringBuffer.append(c);
                }
                edTxt.removeTextChangedListener(this);
                edTxt.setText(stringBuffer.toString());
                edTxt.setSelection(edTxt.getText().toString().length());
                edTxt.addTextChangedListener(this);
            } else {
                edTxt.removeTextChangedListener(this);
                edTxt.setText("");
                edTxt.addTextChangedListener(this);
            }
        }
    }

    //  Edittext change listner added

    private class MyTextWatcher implements TextWatcher {

        private View view;
        private MyTextWatcher(View view) {
            this.view = view;
        }

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.signup_school_email:

                    tv_err_school_email.setVisibility(View.GONE);

                    break;

                case R.id.social_signup_principle_name:
                    System.out.println("hhhhhh ahahahah");
                    tv_err_principal_name.setVisibility(View.GONE);
            }
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();

        if (preference.getLOGGEDINFROM().equals("gmail")) {
            googlePlusLogout();
            preference.setLOGGEDINFROM("");
        } else if (preference.getLOGGEDINFROM().equals("fb")) {
            FacebookSdk.sdkInitialize(getApplicationContext());
            LoginManager.getInstance().logOut();

            preference.setLOGGEDINFROM("");
        }

        // Preference clear

        SharedPreferences settings = getSharedPreferences("Bully_PREFS", Activity.MODE_PRIVATE);
        settings.edit().clear().commit();

        Intent intent = new Intent(SocialSignUpActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    private void enableEdittext()
    {
        principle_name.setFocusable(true);
        principle_name.setFocusableInTouchMode(true);

        school_number.setFocusable(true);
        school_number.setFocusableInTouchMode(true);

        school_addres.setFocusable(true);
        school_addres.setFocusableInTouchMode(true);

        edtxt_schoolemail.setFocusable(true);
        edtxt_schoolemail.setFocusableInTouchMode(true);
    }

    private void disableEdittext()
    {
        principle_name.setFocusable(false);
        principle_name.setFocusableInTouchMode(false);

        school_number.setFocusable(false);
        school_number.setFocusableInTouchMode(false);

        school_addres.setFocusable(false);
        school_addres.setFocusableInTouchMode(false);

        edtxt_schoolemail.setFocusable(false);
        edtxt_schoolemail.setFocusableInTouchMode(false);
    }
}
