package com.nexgen.bullybeware.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;

import com.nexgen.bullybeware.R;
import com.nexgen.bullybeware.adapter.ExpandableListAdapter;
import com.nexgen.bullybeware.adapter.SafeListAdapter;
import com.nexgen.bullybeware.model.RestClient;
import com.nexgen.bullybeware.pojo.RequestResponsePojo;
import com.nexgen.bullybeware.util.BullyConstants;
import com.nexgen.bullybeware.util.BullyDialogs;
import com.nexgen.bullybeware.util.BullyPreferences;
import com.nexgen.bullybeware.util.BullyToast;
import com.nexgen.bullybeware.util.ConnectionDetector;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class SituationResponseActivity extends BaseActivity {

    private SituationResponseActivity ctx = this;
    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerView.Adapter mAdapter;
    private BullyPreferences preferences;
    private String school_id;
    private ConnectionDetector cd;
    private LinearLayout no_safe_list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_situation_response);

        setDrawerbackIcon("Situation and Response");

        preferences = new BullyPreferences(ctx);
        cd = new ConnectionDetector(getApplicationContext());
        no_safe_list = (LinearLayout)findViewById(R.id.no_safe_list);
        mRecyclerView = (RecyclerView)findViewById(R.id.safeplace_recycler_view);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(ctx);
        mRecyclerView.setLayoutManager(mLayoutManager);
        if (preferences.getSchool_id().length() > 0) {
            school_id = preferences.getSchool_id();
        } else {
            school_id = preferences.getSchool_email();
        }
        System.out.println("hhhh yashal school is is "+school_id);
        if (school_id.length() >0) {
            getSafeFriendList();
        }
        else
        {
            customAlertDialog(getResources().getString(R.string.what_should_i_do), getResources().getString(R.string.err_school_id));
        }

    }


    public void getSafeFriendList() {
        if (cd.isConnectingToInternet()) {
            final ProgressDialog d = BullyDialogs.showLoading(ctx);
            d.setCanceledOnTouchOutside(false);
            RestClient.get().getResponseRequest(school_id, new Callback<RequestResponsePojo>() {
                @Override
                public void success(RequestResponsePojo basePojo, Response response) {

                    if (basePojo != null) {
                        if (basePojo.getStatus()) {
                            if (basePojo.getObject() != null) {
                                if (basePojo.getObject().size() > 0) {
                                    mAdapter = new ExpandableListAdapter(ctx, basePojo.getObject());
                                    mRecyclerView.setAdapter(mAdapter);
                                    no_safe_list.setVisibility(View.GONE);
                                } else {
                                    no_safe_list.setVisibility(View.VISIBLE);
                                }
                            } else {
                                no_safe_list.setVisibility(View.VISIBLE);
                            }

                        }
                        else
                        {
                            if (basePojo.getMessage()!= null)
                            {
                                customAlertDialog(getResources().getString(R.string.what_should_i_do), basePojo.getMessage());
                            }
                        }
                    } else {
                        BullyToast.showToast(ctx, BullyConstants.SERVER_NOT_RESPOND);
                    }
                    d.dismiss();
                }

                @Override
                public void failure(RetrofitError error) {
                    d.dismiss();
                }

            });
        } else {
            customAlertDialog("BullyBeware", BullyConstants.NO_INTERNET_CONNECTED);
        }
    }
}
