package com.nexgen.bullybeware.activity;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;

import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.plus.People;
import com.google.android.gms.plus.Plus;
import com.nexgen.bullybeware.R;
import com.nexgen.bullybeware.adapter.BullyPageAdapter;
import com.nexgen.bullybeware.adapter.SingleBullypagerAdapter;
import com.nexgen.bullybeware.fragment.FirstSignUpFragment;
import com.nexgen.bullybeware.fragment.SecondSignUpFragment;
import com.nexgen.bullybeware.util.BullyConstants;
import com.nexgen.bullybeware.util.BullyPreferences;
import com.nexgen.bullybeware.util.ConnectionDetector;
import com.nexgen.bullybeware.util.CustomViewPager;

import java.util.ArrayList;

import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

/**
 * Created by quepplin1 on 8/11/2016.
 */

public class SignUpActivity extends BaseActivity  {

    // flag for Internet connection status
    Boolean isInternetPresent = false;

    // Connection detector class
    ConnectionDetector cd;

    private CustomViewPager viewPager;
    private BullyPageAdapter adapter;

    private ImageView first_view, second_view;

    private LinearLayout ll_pager_view;


    BroadcastReceiver reciever = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            int flag = intent.getIntExtra("Flag", 0);

            if (viewPager.getCurrentItem() != flag) {
                viewPager.setCurrentItem(flag);
            }
        }
    };

    BroadcastReceiver age_reciever = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            String age_limit = intent.getStringExtra("age_limit");

            if (age_limit.equalsIgnoreCase("under")) {
                ll_pager_view.setVisibility(View.GONE);
            } else {
                ll_pager_view.setVisibility(View.VISIBLE);
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        LocalBroadcastManager.getInstance(this).registerReceiver(reciever, new IntentFilter("Action_Send"));
        LocalBroadcastManager.getInstance(this).registerReceiver(age_reciever, new IntentFilter("Action_age"));

        setDrawerbackIcon("Complete Your profile");

        init();
        setOnClickListner();
    }

    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(reciever, new IntentFilter("Action_Send"));
    }

    private void init() {

        first_view = (ImageView) findViewById(R.id.signup_first_view);
        second_view = (ImageView) findViewById(R.id.signup_second_view);

        ll_pager_view = (LinearLayout) findViewById(R.id.ll_pager_view);


        cd = new ConnectionDetector(getApplicationContext());
        // get Internet status
        isInternetPresent = cd.isConnectingToInternet();

        // adapter add

        adapter = new BullyPageAdapter(getSupportFragmentManager());
        viewPager = (CustomViewPager) findViewById(R.id.signupPager);
        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(2);

    }

    private boolean firstRun = true;

    private void setOnClickListner() {
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                System.out.println("xhxhx " + position);
                if (position == 0) {
                    FirstSignUpFragment fragment = (FirstSignUpFragment) adapter.getFragment(position);
                    if (fragment != null) {
                        fragment.setData();
                        boolean status;
                        if (firstRun) {
                            status = true;
                            firstRun = false;
                        } else {
                            status = fragment.submitContinue();
                        }
                        Handler handler = new Handler();
                        if (status)
                            viewPager.setPagingEnabled(false);
                        Runnable myRunnable = new Runnable() {
                            public void run() {
                                viewPager.setPagingEnabled(true);
                            }
                        };
                        handler.postDelayed(myRunnable, 2000);
                    }
                }
                setStyle(position);
            }

            @Override
            public void onPageSelected(int position) {
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

        first_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (viewPager.getCurrentItem() != 0) {
                    viewPager.setCurrentItem(0);
                }
            }
        });

        second_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (viewPager.getCurrentItem() == 0) {
                    FirstSignUpFragment fragment = (FirstSignUpFragment) adapter.getFragment(viewPager.getCurrentItem());
                    if (fragment != null) {
                        fragment.setData();
                        boolean status = fragment.submitContinue();
                        Handler handler = new Handler();
                        if (status) {
                            viewPager.setPagingEnabled(false);
                        } else {
                            if (viewPager.getCurrentItem() != 1) {
                                viewPager.setCurrentItem(1);
                            }
                        }
                        Runnable myRunnable = new Runnable() {
                            public void run() {
                                viewPager.setPagingEnabled(true);
                            }
                        };
                        handler.postDelayed(myRunnable, 2000);
                    }
                } else {
                    if (viewPager.getCurrentItem() != 1) {
                        viewPager.setCurrentItem(1);
                    }
                }
            }
        });
    }

    private void setStyle(int position) {
        if (position == 0) {
            first_view.setImageResource(R.mipmap.active_1);
            second_view.setImageResource(R.mipmap.in_active_2);
        } else if (position == 1) {
            first_view.setImageResource(R.mipmap.in_active_1);
            second_view.setImageResource(R.mipmap.active_2);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(reciever);
    }

    @Override
    protected void onStop() {
        super.onStop();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(reciever);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        Intent intent = new Intent(SignUpActivity.this,LoginActivity.class);
        startActivity(intent);
        finish();

        // Preference clear

        SharedPreferences settings = getSharedPreferences("Bully_PREFS", Activity.MODE_PRIVATE);
        settings.edit().clear().commit();


    }
}
