package com.nexgen.bullybeware.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nexgen.bullybeware.R;
import com.nexgen.bullybeware.adapter.RecordPageAdapter;

/**
 * Created by quepplin1 on 9/28/2016.
 */
public class MyRecordActivity extends BaseActivity {

    private ViewPager viewPager;
    private RecordPageAdapter adapter;
    private int fragment_pos = 0;
    private TextView tv_myRecording, tv_mySubmission;
    private LinearLayout myRecordings_selector, mySubmissions_selecter;
    private LinearLayout ll_notification;
    private ImageView iv_emergency_alarm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_record);

        setDrawerbackIcon("My Activity");

        init();
        setOnClickListner();

    }

    private void init() {
        adapter = new RecordPageAdapter(getSupportFragmentManager());
        viewPager = (ViewPager) findViewById(R.id.recordPager);
        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(1);

        String from_activity = getIntent().getStringExtra("from_activity");

        if (from_activity.equalsIgnoreCase("Report_incident")) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    viewPager.setCurrentItem(1);
                }
            }, 1000);
        } else {
            viewPager.setCurrentItem(0);
        }

        tv_myRecording = (TextView) findViewById(R.id.tv_myRecordings);
        tv_mySubmission = (TextView) findViewById(R.id.tv_mySubmissions);

        ll_notification = (LinearLayout) findViewById(R.id.ll_Notification);
        ll_notification.setVisibility(View.GONE);

        myRecordings_selector = (LinearLayout) findViewById(R.id.myRecordings_selector);
        mySubmissions_selecter = (LinearLayout) findViewById(R.id.mySubmissions_selecter);

        iv_emergency_alarm = (ImageView) findViewById(R.id.iv_emergency_alarm);

    }

    private void setOnClickListner() {
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                setStyle(position);
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        tv_myRecording.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (viewPager.getCurrentItem() != 1) {
                    viewPager.setCurrentItem(1);
                }
            }
        });
        tv_mySubmission.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (viewPager.getCurrentItem() != 0) {
                    viewPager.setCurrentItem(0);
                }
            }
        });
        iv_emergency_alarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    playStopClickListner();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

    }

    private void setStyle(int position) {
        fragment_pos = position;
        if (position == 0) {

            tv_myRecording.setTextColor(getResources().getColor(R.color.record_selected_pager_color));
            tv_mySubmission.setTextColor(getResources().getColor(R.color.white));

            mySubmissions_selecter.setVisibility(View.VISIBLE);
            myRecordings_selector.setVisibility(View.GONE);

        } else if (position == 1) {
            tv_myRecording.setTextColor(getResources().getColor(R.color.white));
            tv_mySubmission.setTextColor(getResources().getColor(R.color.record_selected_pager_color));

            myRecordings_selector.setVisibility(View.VISIBLE);
            mySubmissions_selecter.setVisibility(View.GONE);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        Intent intent = new Intent(MyRecordActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }
}
