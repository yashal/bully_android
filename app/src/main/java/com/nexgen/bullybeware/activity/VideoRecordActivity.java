package com.nexgen.bullybeware.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.Camera;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.PowerManager;
import android.preference.PreferenceManager;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nexgen.bullybeware.R;
import com.nexgen.bullybeware.util.BullyPreferences;
import com.nexgen.bullybeware.util.BullyToast;
import com.nexgen.bullybeware.util.CameraView;
import com.nexgen.bullybeware.util.InitMediaRecorder;

import java.util.ArrayList;

import static android.Manifest.permission.CAMERA;

/**
 * Created by quepplin1 on 8/31/2016.
 */
public class VideoRecordActivity extends BaseActivity {

    private Camera mCamera;
    private CameraView mCameraView = null;
    private InitMediaRecorder mInitMediaRecorder;
    private MediaRecorder mMediaRecoder;
    private ImageView iView_start, iView_stop;
    private TextView tv_timer;
    private int cnt;
    private String for_val;
    CountDownTimer t;
    private LinearLayout video_record_ll;
    private static final long DOUBLE_CLICK_TIME_DELTA = 2000;//milliseconds
    long lastClickTime = 0;

    @Override
    protected void onResume() {
        super.onResume();
        mCameraView.resumePreview();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_videorecord);

        setDrawerCrossIcon("Record Video");

        init();
        opencamera();

        setOnClickListner();

        for_val = getIntent().getStringExtra("for");
    }

    private void init() {

        tv_timer = (TextView) findViewById(R.id.tv_timer);
        iView_start = (ImageView) findViewById(R.id.iView_start_video);
        iView_stop = (ImageView) findViewById(R.id.iView_stop_video);

        video_record_ll = (LinearLayout) findViewById(R.id.video_record_ll);

        BullyToast.showToast(VideoRecordActivity.this, getResources().getString(R.string.emergency_alaram));

    }

    private void setOnClickListner() {
        iView_stop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (t != null) {
                    t.cancel();
                }
                mInitMediaRecorder.releaseMediaRecorder(for_val);

                releaseCamera();
            }
        });
        iView_start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    mInitMediaRecorder = new InitMediaRecorder(VideoRecordActivity.this, mCamera);
                    mMediaRecoder = mInitMediaRecorder.getPreparedMediaRecorder();

                    statTimer();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        video_record_ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                long clickTime = System.currentTimeMillis();
                if (clickTime - lastClickTime < DOUBLE_CLICK_TIME_DELTA) {
                    try {
                        playStopClickListner();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    lastClickTime = 0;
                } else {
                    lastClickTime = clickTime;
                }
            }
        });
    }

    private void statTimer() {
        t = new CountDownTimer(120000, 1000) {

            @Override
            public void onTick(long millisUntilFinished) {

                cnt++;

                long millis = cnt;
                int seconds = (int) (millis);
                int minutes = seconds / 60;
                seconds = seconds % 60;

                tv_timer.setText(String.format("%02d:%02d", minutes, seconds) + "Recording");

            }

            @Override
            public void onFinish() {
                tv_timer.setText("Done");

                mInitMediaRecorder.releaseMediaRecorder(for_val);

                releaseCamera();
            }
        }.start();
    }


    private void opencamera() {

        mCamera = Camera.open();

        if (mCamera != null) {
            mCameraView = new CameraView(this, mCamera);
            FrameLayout camera_view = (FrameLayout) findViewById(R.id.camerapreview);
            camera_view.addView(mCameraView);
        }
        mCameraView.setOnClickListener(new View.OnClickListener() {
            private boolean click = false;

            @Override
            public void onClick(View view) {
                if (click) {
                    //lock screen
                    // Source = https://github.com/hyy12345678/LockScreen
                    PowerManager manager = (PowerManager) getSystemService(Context.POWER_SERVICE);
                    PowerManager.WakeLock wl = manager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, VideoRecordActivity.class.getSimpleName());
                    wl.acquire();
                    wl.release();
                    click = false;
                } else {
                    click = true;
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            click = false;
                        }
                    }, 1000);
                }
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mCamera != null) {
            //        releaseCamera();
            mCameraView.getHolder().removeCallback(mCameraView);
            //mCamera.stopPreview();
            //          mCameraView = null;
        }
    }

    private void releaseCamera() {
        if (mCamera != null) {
            mCamera.release();
            mCamera = null;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        releaseCamera();
    }

    /*  private void releaseMediaRecorder(){
        mMediaRecoder.stop();
        mMediaRecoder.reset();
        mMediaRecoder.release();
        mMediaRecoder = null;

       *//* Intent intent = new Intent(VideoRecordActivity.this, LoginActivity.class);
        startActivity(intent);*//*
    }*/
}
