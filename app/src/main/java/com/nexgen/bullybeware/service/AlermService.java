package com.nexgen.bullybeware.service;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;

import com.nexgen.bullybeware.R;
import com.nexgen.bullybeware.util.BullyPreferences;

import java.io.IOException;

/**
 * Created by quepplin1 on 11/14/2016.
 */
public class AlermService extends Service {
    public static final int IS_PLAYING = 1;
    public static final int START = 6;
    public static final int STOP = 7;
    public static final String ACTION_IS_PLAYING = "is_playing";
    private BullyPreferences preferences;

    Handler msgHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case IS_PLAYING:
                    is_playing();
                    break;
                case START:
                    start_player();
                    break;
                case STOP:
                    stop();
                    break;

            }
            super.handleMessage(msg);
        }
    };

    MediaPlayer mediaPlayer = new MediaPlayer();
    Messenger messenger = new Messenger(msgHandler);

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return messenger.getBinder();
    }

    @Override
    public void onCreate() {
        super.onCreate();

        preferences = new BullyPreferences(getApplicationContext());
    }
    private void is_playing() {
        Intent message = new Intent(ACTION_IS_PLAYING);
        if (mediaPlayer.isPlaying()) {
            message.putExtra("is_playing", true);
        } else {
                message.putExtra("is_playing", false);
        }
        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(message);
    }

    private void stop() {
        mediaPlayer.reset();
        Intent message = new Intent(ACTION_IS_PLAYING);
        message.putExtra("is_playing", false);
        message.putExtra("is_stopped", true);
        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(message);
    }

    public void start_player() {
        try {

            mediaPlayer.reset();
            setSound();
            mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    mediaPlayer.setLooping(true);
                    mediaPlayer.start();
                    Intent message = new Intent(ACTION_IS_PLAYING);
                    message.putExtra("is_playing", true);
                    message.putExtra("is_stopped", false);
                    LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(message);

                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setSound(){
        if ( preferences.getSound_config().equalsIgnoreCase("Emergency")){
            mediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.emergency1);
        } else if (preferences.getSound_config().equalsIgnoreCase("Alert Sound")){
            mediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.emergency2);
        }  else {
            mediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.emergency1);
        }
    }

    @Override
    public boolean onUnbind(Intent intent) {
        return super.onUnbind(intent);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return Service.START_STICKY;
    }
}
