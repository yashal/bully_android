package com.nexgen.bullybeware.service;

import android.app.Dialog;
import android.app.IntentService;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.nexgen.bullybeware.R;
import com.nexgen.bullybeware.activity.MainActivity;
import com.nexgen.bullybeware.activity.ReportIncidentActivity;
import com.nexgen.bullybeware.database.DatabaseHandler;
import com.nexgen.bullybeware.model.FileUploadSer;
import com.nexgen.bullybeware.model.RestClient;
import com.nexgen.bullybeware.model.ServiceGenerator;
import com.nexgen.bullybeware.pojo.ImageUploadPojo;
import com.nexgen.bullybeware.pojo.ImagelistStatusPojo;
import com.nexgen.bullybeware.pojo.IncidentSubmissionPojo;
import com.nexgen.bullybeware.pojo.ReportSubmissionFilesPojo;
import com.nexgen.bullybeware.util.BullyApplication;
import com.nexgen.bullybeware.util.BullyDialogs;
import com.nexgen.bullybeware.util.BullyPreferences;
import com.nexgen.bullybeware.util.BullyToast;

import java.io.File;
import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedFile;

/**
 * Created by quepplin1 on 11/21/2016.
 */
public class AudioUploadService extends Service {

    private String student_id, incident_id, date, title, bully_name, victim_name, nearest_adult, description, form_id, message, Submit_id, time, school_id;
    private ArrayList<ImagelistStatusPojo> audiostatus;
    private ArrayList<ImagelistStatusPojo> remaningstatuslist = new ArrayList<ImagelistStatusPojo>();
    private final IBinder mBinder = new MyBinder();
    private BullyPreferences preferences;

    private DatabaseHandler db;
    private boolean status_check;

    public AudioUploadService() {

    }

    @Override
    public void onCreate() {
        super.onCreate();

        db = new DatabaseHandler(this);

        preferences = new BullyPreferences(this);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {

        return mBinder;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        try {
            student_id = intent.getStringExtra("student_id");
            incident_id = intent.getStringExtra("incident_id");
            date = intent.getStringExtra("date");
            time = intent.getStringExtra("time");
            title = intent.getStringExtra("title");
            bully_name = intent.getStringExtra("bully_name");
            victim_name = intent.getStringExtra("victim_name");
            nearest_adult = intent.getStringExtra("nearest_adult");
            description = intent.getStringExtra("description");
            Submit_id = intent.getStringExtra("Submit_id");
            school_id = intent.getStringExtra("school_id");

            form_id = intent.getStringExtra("form_id");
            status_check = intent.getBooleanExtra("status_check", false);

            audiostatus = (ArrayList<ImagelistStatusPojo>) intent.getSerializableExtra("audio_status");

            if (form_id == null) {
                incidentFormApi(getApplicationContext(), student_id, incident_id, date, time, title, bully_name, victim_name, nearest_adult, description);
            } else {
                audioupload();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return super.onStartCommand(intent, flags, startId);
    }


    private void incidentFormApi(final Context context, String student_id, String incident_id, String date, String time, String title, String bully_name, String victim_name, String nearest_adult, String description) {

        System.out.println("rrrr " + preferences.getSchool_id());

        RestClient.get().postIncidentSubmissionNew(status_check, student_id, incident_id, date, time, title, bully_name, victim_name,
                nearest_adult, description, "2", school_id, new Callback<IncidentSubmissionPojo>() {
                    @Override
                    public void success(final IncidentSubmissionPojo basepojo, Response response) {
                        if (basepojo != null) {
                            if (basepojo.getStatus()) {
                                form_id = basepojo.getObject().getFrom_id();
                                audioupload();

                                message = basepojo.getMessage();

                            } else {
                                new android.os.Handler(Looper.getMainLooper()).post(new Runnable() {
                                    @Override
                                    public void run() {
                                        Toast.makeText(context, basepojo.getMessage(), Toast.LENGTH_LONG).show();
                                    }
                                });
                            }
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        System.out.println("xxx failed");

                        //               customAlertDialog("Report Incident", "Whoops! Looks like there is a problem with our server. Please try again soon!");

                    }
                });
    }

    private void audioupload() {

        new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... voids) {
                FileUploadSer service = ServiceGenerator.createService(FileUploadSer.class, FileUploadSer.BASE_URL);
                TypedFile typedFile = null;
                boolean allUploaded = false;
                for (int i = 0; i < audiostatus.size(); i++) {
                    typedFile = new TypedFile("multipart/form-data", new File(audiostatus.get(i).getPath()));

                    final int finalI = i;
                    try {
                        ImageUploadPojo audiouploadpojo = service.audioUploadAsync(form_id, typedFile);

                        if (audiouploadpojo != null) {
                            if (audiouploadpojo.getStatus()) {
                                System.out.println("yyy Sucees");
                                audiostatus.get(finalI).setStatus(true);
                            }
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                for (int i = 0; i < audiostatus.size(); i++) {
                    if (audiostatus.get(i).getStatus()) {
                        allUploaded = true;
                    } else {
                        allUploaded = false;
                        remaningstatuslist.add(audiostatus.get(i));
                    }
                }

                if (allUploaded) {

                    new android.os.Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                        }
                    });

                    TrackEvent("Submission","audio",preferences.getEmail());

                    db.deleteList(Submit_id);
                    db.deletefile(Submit_id);
                } else {
                    Intent intent = new Intent("Audio_Service");
                    intent.putExtra("form_id", form_id);
                    intent.putExtra("Submit_id", Submit_id);
                    intent.putExtra("audio_status", remaningstatuslist);
                    LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
                }
                return null;
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

    }

    public void customAlertDialog(String title, String text) {
        final Dialog dialogLogout = new Dialog(getApplicationContext(), android.R.style.Theme_Translucent_NoTitleBar);
        dialogLogout.setContentView(R.layout.custom_dialog);
        dialogLogout.show();

        EditText fp_edtxt_email = (EditText) dialogLogout.findViewById(R.id.fp_edtxt_email);
        fp_edtxt_email.setVisibility(View.GONE);

        TextView dialog_header = (TextView) dialogLogout.findViewById(R.id.dialog_header);
        dialog_header.setVisibility(View.VISIBLE);
        dialog_header.setText(title);
        ImageView iView_cross = (ImageView) dialogLogout.findViewById(R.id.dialog_header_cross);
        TextView tv_submit = (TextView) dialogLogout.findViewById(R.id.dialog_submit);
        tv_submit.setText("OK");
        TextView dialog_text = (TextView) dialogLogout.findViewById(R.id.dialog_text);
        dialog_text.setVisibility(View.VISIBLE);
        dialog_text.setText(text);

        tv_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialogLogout.dismiss();

                Intent intent = new Intent(getApplicationContext(), ReportIncidentActivity.class);
                startActivity(intent);
            }
        });
        iView_cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogLogout.dismiss();
            }
        });
    }

    public void TrackEvent(String category,String action,String label) {

        Tracker t = ((BullyApplication) this.getApplication())
                .getTracker(BullyApplication.TrackerName.APP_TRACKER);
        t.send(new HitBuilders.EventBuilder()
                .setCategory(category)
                .setAction(action)
                .setLabel(label)
                .build());
    }


    public class MyBinder extends Binder {
        AudioUploadService getService() {
            return AudioUploadService.this;
        }
    }
}
