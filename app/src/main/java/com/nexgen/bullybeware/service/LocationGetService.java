package com.nexgen.bullybeware.service;

import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;

import com.nexgen.bullybeware.model.RestClient;
import com.nexgen.bullybeware.pojo.GetLatLong;

import java.io.Serializable;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by umax desktop on 2/9/2017.
 */

public class LocationGetService extends Service {

    private String friend_email;


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        try {
            friend_email = intent.getStringExtra("friend_email");

            getlatlong(friend_email);

        } catch (Exception e){
            e.printStackTrace();
        }

        return super.onStartCommand(intent, flags, startId);
    }

    private void getlatlong(String friend_email){

        RestClient.get().getLatLong(friend_email, new Callback<GetLatLong>() {
            @Override
            public void success(GetLatLong getLatLong, Response response) {

                if (getLatLong != null){
                    if (getLatLong.getStatus()){

                        try {
                         /*   System.out.println("yy lat"+getLatLong.getObject());
                            System.out.println("yy long"+getLatLong.getObject().getLongitude());

                            Double la = Double.valueOf(getLatLong.getObject().getLatitude());
                            Double lo = Double.valueOf(getLatLong.getObject().getLongitude());
*/
                            Intent intent = new Intent("Location_Service");
                            intent.putExtra("LatLongobject",getLatLong.getObject());
                            LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);

                        } catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                }
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });

    }
}
