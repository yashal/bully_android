package com.nexgen.bullybeware.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.IBinder;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.nexgen.bullybeware.database.DatabaseHandler;
import com.nexgen.bullybeware.model.FileUploadSer;
import com.nexgen.bullybeware.model.RestClient;
import com.nexgen.bullybeware.model.ServiceGenerator;
import com.nexgen.bullybeware.pojo.ImageUploadPojo;
import com.nexgen.bullybeware.pojo.ImagelistStatusPojo;
import com.nexgen.bullybeware.pojo.IncidentSubmissionPojo;
import com.nexgen.bullybeware.util.BullyApplication;
import com.nexgen.bullybeware.util.BullyPreferences;
import com.nexgen.bullybeware.util.ConnectionDetector;

import java.io.File;
import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedFile;

/**
 * Created by hp on 11/19/2016.
 */

public class TextUploadService extends Service {

    private String student_id,incident_id,date,title,bully_name,victim_name,nearest_adult,description,form_id,message,Submit_id,time,school_id;
    private ArrayList<ImagelistStatusPojo>remaningstatuslist= new ArrayList<ImagelistStatusPojo>();
    private DatabaseHandler db;
   /* HandlerThread handlerThread = new HandlerThread(ImageUploadService.class.getSimpleName());
    Looper looper;*/
    private final IBinder mBinder = new MyBinder();
    // flag for Internet connection status
    Boolean isInternetPresent = false;

    // Connection detector class
    ConnectionDetector cd;

    private BullyPreferences preferences;
    private boolean status_check;

    public TextUploadService() {

       /* handlerThread.start();
        looper = handlerThread.getLooper();*/
    }

    @Override
    public void onCreate() {
        super.onCreate();

        preferences = new BullyPreferences(this);

        db = new DatabaseHandler(this);

        cd = new ConnectionDetector(getApplicationContext());
        // get Internet status
        isInternetPresent = cd.isConnectingToInternet();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {

        return mBinder;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        try {
            student_id = intent.getStringExtra("student_id");
            incident_id = intent.getStringExtra("incident_id");
            date = intent.getStringExtra("date");
            time = intent.getStringExtra("time");
            title = intent.getStringExtra("title");
            bully_name = intent.getStringExtra("bully_name");
            victim_name = intent.getStringExtra("victim_name");
            nearest_adult = intent.getStringExtra("nearest_adult");
            description = intent.getStringExtra("description");
            Submit_id = intent.getStringExtra("Submit_id");

            school_id = intent.getStringExtra("school_id");

            form_id = intent.getStringExtra("form_id");
            status_check = intent.getBooleanExtra("status_check", false);


            if (form_id == null){
                incidentFormApi(getApplicationContext(),student_id,incident_id,date,time,title,bully_name,victim_name,nearest_adult,description);

            }
        } catch (Exception e){
            e.printStackTrace();
        }

        return super.onStartCommand(intent, flags, startId);

    }

    private void incidentFormApi(final Context context, String student_id, String incident_id, String date,String time, String title, String bully_name, String victim_name, String nearest_adult, String description) {

        System.out.println("rrrrrrr"+school_id);

            RestClient.get().postIncidentSubmissionNew(status_check, student_id, incident_id, date, time, title,bully_name, victim_name,
                    nearest_adult, description, "4",school_id, new Callback<IncidentSubmissionPojo>() {
                        @Override
                        public void success(final IncidentSubmissionPojo basepojo, Response response) {
                            if (basepojo != null) {
                                if (basepojo.getStatus()) {
                                    form_id = basepojo.getObject().getFrom_id();
                                    message = basepojo.getMessage();

                                } else {

                                    new android.os.Handler(Looper.getMainLooper()).post(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(context, basepojo.getMessage(), Toast.LENGTH_LONG).show();
                                        }
                                    });
                                }
                            }
                        }

                        @Override
                        public void failure(RetrofitError error) {
                            System.out.println("xxx failed");

    //                   customAlertDialog("Report Incident", "Whoops! Looks like there is a problem with our server. Please try again soon!");

                        }
                    });
        }

    public void TrackEvent(String category,String action,String label) {

        Tracker t = ((BullyApplication) this.getApplication())
                .getTracker(BullyApplication.TrackerName.APP_TRACKER);
        t.send(new HitBuilders.EventBuilder()
                .setCategory(category)
                .setAction(action)
                .setLabel(label)
                .build());
    }

    public class MyBinder extends Binder {
        TextUploadService getService() {
            return TextUploadService.this;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
