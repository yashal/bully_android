package com.nexgen.bullybeware.service;

import android.app.Dialog;
import android.app.IntentService;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.Bundle;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.gson.JsonElement;
import com.nexgen.bullybeware.R;
import com.nexgen.bullybeware.activity.MainActivity;
import com.nexgen.bullybeware.activity.ReportIncidentActivity;
import com.nexgen.bullybeware.database.DatabaseHandler;
import com.nexgen.bullybeware.model.FileUploadSer;
import com.nexgen.bullybeware.model.RestClient;
import com.nexgen.bullybeware.model.ServiceGenerator;
import com.nexgen.bullybeware.pojo.CheckboxStatusPojo;
import com.nexgen.bullybeware.pojo.ImageUploadPojo;
import com.nexgen.bullybeware.pojo.ImagelistStatusPojo;
import com.nexgen.bullybeware.pojo.IncidentSubmissionPojo;
import com.nexgen.bullybeware.pojo.ReportSubmissionFilesPojo;
import com.nexgen.bullybeware.util.BullyApplication;
import com.nexgen.bullybeware.util.BullyDialogs;
import com.nexgen.bullybeware.util.BullyPreferences;
import com.nexgen.bullybeware.util.BullyToast;
import com.nexgen.bullybeware.util.ConnectionDetector;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedFile;

/**
 * Created by hp on 11/19/2016.
 */

public class ImageUploadService extends Service {

    private String student_id,incident_id,date,title,bully_name,victim_name,nearest_adult,description,form_id,message,Submit_id,time,school_id;
    private ArrayList<ImagelistStatusPojo> imagestatus;
    private ArrayList<ImagelistStatusPojo>remaningstatuslist= new ArrayList<ImagelistStatusPojo>();
    private DatabaseHandler db;
   /* HandlerThread handlerThread = new HandlerThread(ImageUploadService.class.getSimpleName());
    Looper looper;*/
    private final IBinder mBinder = new MyBinder();
    // flag for Internet connection status
    Boolean isInternetPresent = false;

    // Connection detector class
    ConnectionDetector cd;

    private BullyPreferences preferences;
    private boolean status_check;

    public ImageUploadService() {

       /* handlerThread.start();
        looper = handlerThread.getLooper();*/
    }

    @Override
    public void onCreate() {
        super.onCreate();

        preferences = new BullyPreferences(this);

        db = new DatabaseHandler(this);

        cd = new ConnectionDetector(getApplicationContext());
        // get Internet status
        isInternetPresent = cd.isConnectingToInternet();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {

        return mBinder;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        try {
            student_id = intent.getStringExtra("student_id");
            incident_id = intent.getStringExtra("incident_id");
            date = intent.getStringExtra("date");
            time = intent.getStringExtra("time");
            title = intent.getStringExtra("title");
            bully_name = intent.getStringExtra("bully_name");
            victim_name = intent.getStringExtra("victim_name");
            nearest_adult = intent.getStringExtra("nearest_adult");
            description = intent.getStringExtra("description");
            Submit_id = intent.getStringExtra("Submit_id");

            school_id = intent.getStringExtra("school_id");

            form_id = intent.getStringExtra("form_id");
            status_check = intent.getBooleanExtra("status_check", false);

            imagestatus = (ArrayList<ImagelistStatusPojo>)intent.getSerializableExtra("image_status");

            if (form_id == null){
                incidentFormApi(getApplicationContext(),student_id,incident_id,date,time,title,bully_name,victim_name,nearest_adult,description);

            } else {
                imageupload();
            }
        } catch (Exception e){
            e.printStackTrace();
        }

        return super.onStartCommand(intent, flags, startId);

    }

    private void incidentFormApi(final Context context, String student_id, String incident_id, String date,String time, String title, String bully_name, String victim_name, String nearest_adult, String description) {

        System.out.println("rrrrrrr"+school_id);

            RestClient.get().postIncidentSubmissionNew(status_check, student_id, incident_id, date, time, title,bully_name, victim_name,
                    nearest_adult, description, "1",school_id, new Callback<IncidentSubmissionPojo>() {
                        @Override
                        public void success(final IncidentSubmissionPojo basepojo, Response response) {
                            if (basepojo != null) {
                                if (basepojo.getStatus()) {
                                    form_id = basepojo.getObject().getFrom_id();
                                    imageupload();
                                    message = basepojo.getMessage();

                                } else {

                                    new android.os.Handler(Looper.getMainLooper()).post(new Runnable() {
                                        @Override
                                        public void run() {
                                            Toast.makeText(context, basepojo.getMessage(), Toast.LENGTH_LONG).show();
                                        }
                                    });
                                }
                            }
                        }

                        @Override
                        public void failure(RetrofitError error) {
                            System.out.println("xxx failed");

    //                   customAlertDialog("Report Incident", "Whoops! Looks like there is a problem with our server. Please try again soon!");

                        }
                    });
        }

    private void imageupload() {

        new AsyncTask<Void,Void,Void>(){

            @Override
            protected Void doInBackground(Void... voids) {
                FileUploadSer service = ServiceGenerator.createService(FileUploadSer.class, FileUploadSer.BASE_URL);
                TypedFile typedFile;
                boolean allUploaded = false;
                for (int i = 0; i < imagestatus.size(); i++) {
                    typedFile = new TypedFile("multipart/form-data", new File(imagestatus.get(i).getPath()));

                    final int finalI = i;
                    try {
                        ImageUploadPojo imageUploadPojo = service.imageUploadAsync(form_id, typedFile);
                        Log.d("API", imageUploadPojo.toString());
                        if (imageUploadPojo != null) {
                            if (imageUploadPojo.getStatus()) {

                                System.out.println("yyy Sucees");

                                imagestatus.get(finalI).setStatus(true);
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                 remaningstatuslist.clear();
                for (int j = 0; j < imagestatus.size(); j++) {
                    if (!imagestatus.get(j).getStatus()) {
                        allUploaded = false;
                        remaningstatuslist.add(imagestatus.get(j));
                    } else
                        allUploaded = true;
                }

                if (allUploaded) {

                    new android.os.Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            TrackEvent("Submission","image",preferences.getEmail());
                            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                        }
                    });

                    db.deleteList(Submit_id);
                    db.deletefile(Submit_id);
                } else {
                    Intent intent = new Intent("Image_Service");
                    intent.putExtra("form_id", form_id);
                    intent.putExtra("image_status", remaningstatuslist);
                    intent.putExtra("Submit_id",Submit_id);
                    LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
                }

                return null;
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    public void TrackEvent(String category,String action,String label) {

        Tracker t = ((BullyApplication) this.getApplication())
                .getTracker(BullyApplication.TrackerName.APP_TRACKER);
        t.send(new HitBuilders.EventBuilder()
                .setCategory(category)
                .setAction(action)
                .setLabel(label)
                .build());
    }

    public class MyBinder extends Binder {
        ImageUploadService getService() {
            return ImageUploadService.this;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
