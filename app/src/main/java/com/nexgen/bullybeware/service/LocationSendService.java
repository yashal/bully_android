package com.nexgen.bullybeware.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

import com.nexgen.bullybeware.model.RestClient;
import com.nexgen.bullybeware.pojo.DeleteLatLongPojo;
import com.nexgen.bullybeware.pojo.SendLatLong;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by umax desktop on 2/9/2017.
 */

public class LocationSendService extends Service {

    private String student_id,student_email;
    private Double latitude,longitude;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        try {

            student_id = intent.getStringExtra("student_id");
            student_email = intent.getStringExtra("student_email");
            latitude = intent.getDoubleExtra("lat",12.00);
            longitude = intent.getDoubleExtra("long",12.87);

            String lati = String.valueOf(latitude);
            String longi = String.valueOf(longitude);

            locationSendApi(student_id,lati,longi);

        } catch (Exception e){
            e.printStackTrace();
        }
        return super.onStartCommand(intent, flags, startId);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private void locationSendApi(String student_id,String lati,String longi){

        RestClient.get().sendLatLong(student_id,student_email,lati, longi, new Callback<SendLatLong>() {
            @Override
            public void success(SendLatLong sendLatLong, Response response) {

                if (sendLatLong != null){
                    if (sendLatLong.getStatus()){
                        System.out.println("xxx latlong send");
                    }
                }
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
    }
}
