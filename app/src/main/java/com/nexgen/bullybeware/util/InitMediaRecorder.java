package com.nexgen.bullybeware.util;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.hardware.Camera;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;

import com.nexgen.bullybeware.activity.SavedVideoActivity;
import com.nexgen.bullybeware.activity.VideoRecordActivity;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by quepplin1 on 8/31/2016.
 */
public class InitMediaRecorder {

    private File mOutputFile;
    private MediaRecorder mMediaRecorder;
    private Camera mCamera;
    private Context context;
    private String videopath;

    public InitMediaRecorder(Context context,Camera camera) {
        mCamera = camera;
        this.context = context;
        if(prepare(mCamera)){
            Log.d("Done", "Media Reocrder prepared");
        } else {
            Log.d("Error", "Media Recorder not prepared");
        }
    }

    public MediaRecorder getPreparedMediaRecorder(){

        return mMediaRecorder;
    }

    public boolean prepare(Camera camera) {

        mCamera = camera;

        mMediaRecorder = new MediaRecorder();


        // Step 1: Unlock and set camera to MediaRecorder
        mCamera.unlock();
        mMediaRecorder.setCamera(mCamera);


        // Step 2: Set sources
        mMediaRecorder.setAudioSource(MediaRecorder.AudioSource.CAMCORDER);
        mMediaRecorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);

        // Step 3: Set a CamcorderProfile
        mMediaRecorder.setProfile(CamcorderProfile.get(CamcorderProfile.QUALITY_LOW));

        String filepath = Environment.getExternalStorageDirectory().getPath();

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File file = new File(filepath,"/BullyApp/Video");


        if(!file.exists()){
            file.mkdirs();
        }
        mOutputFile = new File(file,"/Bully_" + timeStamp + ".mp4");

        // Step 4: Set output file
        mMediaRecorder.setOutputFile(mOutputFile.getAbsolutePath());
        mMediaRecorder.setOrientationHint(90);


        videopath = mOutputFile.getAbsolutePath();


        new SingleMediaScanner(context, mOutputFile);

        // Step 5.
        try {
            mMediaRecorder.prepare();
            mMediaRecorder.start();

        } catch(IllegalStateException e) {
            Log.d("ERROR", "IllegalStateException preparing MediaRecorder: " + e.getMessage());
            releaseMediaRecorder("");
            return false;
        } catch (IOException e) {
            Log.d("ERROR", "IOException preparing MediaRecorder: " + e.getMessage());
            releaseMediaRecorder("");
            return false;
        }
        return true;
    }
    /** Create a file Uri for saving an image or video */
    private Uri getOutputMediaFileUri(){
        return Uri.fromFile(mOutputFile);
    }


    public void releaseMediaRecorder(String for_val) {
        if(mMediaRecorder != null){
            mMediaRecorder.reset();
            mMediaRecorder.release();
            mMediaRecorder = null;
            mCamera.stopPreview();
            mCamera.lock();
            mCamera.release();

            Uri uri = getOutputMediaFileUri();

            System.out.println("xx" + uri);

            if (for_val == null){
                Intent intent = new Intent(context, SavedVideoActivity.class);
                intent.putExtra("video_file",mOutputFile.getAbsolutePath());
                 context.startActivity(intent);
                ((Activity)context).finish();
            } else {
                Intent returnIntent = new Intent();
                returnIntent.putExtra("video_file", mOutputFile.getAbsolutePath());
                ((Activity)context).setResult(Activity.RESULT_OK, returnIntent);
                ((Activity)context).finish();
            }
        }
    }
}
