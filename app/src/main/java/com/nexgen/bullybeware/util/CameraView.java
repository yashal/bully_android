package com.nexgen.bullybeware.util;

import android.content.Context;
import android.hardware.Camera;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.io.IOException;
import java.util.List;

/**
 * Created by quepplin1 on 8/31/2016.
 */
public class CameraView extends SurfaceView implements SurfaceHolder.Callback {

    private Camera mCamera;
    private SurfaceHolder mHolder;

    public CameraView(Context context, Camera camera) {
        super(context);

        try {
            mCamera = camera;
            mCamera.setDisplayOrientation(90);
        } catch (Exception e){
            e.printStackTrace();
        }

        mHolder = getHolder();
        mHolder.addCallback(this);

        mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
    }

    public void resumePreview(){
        if(mCamera!=null && mHolder != null){
            try {
                mCamera.setPreviewDisplay(mHolder);
                //mCamera.startPreview();

            } catch (Exception e) {
                Log.d("ERROR","Camera error on surfaceCreated" + e.getMessage());
            }
        }
    }

    @Override
    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        this.mHolder = surfaceHolder;
        try {
            mCamera.setPreviewDisplay(surfaceHolder);
            mCamera.startPreview();

        } catch (Exception e) {
            Log.d("ERROR","Camera error on surfaceCreated" + e.getMessage());
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i1, int i2) {
// start preview with new settings
        try {
            mCamera.setPreviewDisplay(surfaceHolder);
            mCamera.startPreview();
        } catch (Exception e) {
            // intentionally left blank for a test
        }
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
       /* mCamera.stopPreview();
        mCamera.release();*/
    }
}
