package com.nexgen.bullybeware.util;

import android.app.Activity;
import android.app.Application;
import android.view.Gravity;
import android.widget.Toast;

/**
 * Created by quepplin1 on 8/30/2016.
 */
public class BullyToast  {

    public static void showToast(Activity acti, String message) {
        Toast toast = Toast.makeText(acti, message, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }
}
