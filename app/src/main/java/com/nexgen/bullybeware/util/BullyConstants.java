package com.nexgen.bullybeware.util;

/**
 * Created by quepplin1 on 8/11/2016.
 */

public class BullyConstants {

//    public static String BASE_URL = "http://139.162.60.231/bully/module"; // Live URL
    public static String BASE_URL = "http://139.162.60.231/quepp_bully/module"; // Test URL
    public final static String PREF_NAME = "Bully_PREFS";
    public static String LOGIN_FAILED = "LOGIN FAILED";
    public final static String NO_INTERNET_CONNECTED = "No Internet Connection is detected. Please check your internet connection and try again.\n";
    public static String REGISTRATION_FAILED = "REGISTRATION FAILED";
    public static String NO_DATA = "No Data Available";
    public final static String SERVER_NOT_RESPOND = "Server is not responding. Please try again later.";
//    public final static String SERVER_NOT_RESPOND = "Whoops. Looks like there is a problem with your internet connection. Please try again once your connection to internet is re-established";
    public final static String DEFAULT_VALUE = "";
}
