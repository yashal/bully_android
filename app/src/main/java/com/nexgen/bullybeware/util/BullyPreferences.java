package com.nexgen.bullybeware.util;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

/**
 * Created by quepplin1 on 8/26/2016.
 */
public class BullyPreferences {

    private static final String USER_PREFS = "Bully_PREFS";
    private SharedPreferences appSharedPrefs;
    private SharedPreferences.Editor prefsEditor;

    private Context ctx;

    private String LOGGEDINFROM = "LOGGEDINFROM";
    private String studentId = "StudentID";
    private String image = "Image";
    private String firstName = "FirstName";
    private String lastName = "Lastname";
    private String email = "Email";
    private String password = "password";
    private String state = "State";
    private String city = "City";
    private String school = "School";
    private String school_email = "School_email";
    private String secQuestion = "sec_question";
    private String secAnswer = "sec_answer";
    private String contact_relation = "Contact_relation";
    private String phone_number = "Phone_number";
    private String for_activity = "For_Activity";
    private String sound_config = "Emergency";
    private String account_key = "Account_key";
    private String social_id = "Social_id";
    private String Submit_time = "Submit_Time";
    private String school_id = "School_id";
    private String user_age = "User_age";
    private String device_id = "Device_id";
    private String device_type = "Device_type";
    private String imei = "imei";
    private String user_img = "User_img";


    public BullyPreferences(Context context) {

        ctx = context;
        this.appSharedPrefs = context.getSharedPreferences(USER_PREFS, Activity.MODE_PRIVATE);
        this.prefsEditor = appSharedPrefs.edit();
    }

    public void setString(String key,String value)
    {
        try{
            SharedPreferences.Editor editor = appSharedPrefs.edit();
            editor.putString(key, value);
            editor.commit();
        }catch(Exception e){
   //         Log.i(TAG,e.getMessage());
        }
    }
    public String getString(String key){
        try{
            return appSharedPrefs.getString(key, null);
        }catch(Exception e){
       //     Log.i(TAG,e.getMessage());
        }
        return null;
    }

    public void clearSharedPref(){
        try{
            SharedPreferences.Editor editor = appSharedPrefs.edit();
            editor.clear();
            editor.commit();
        }catch(Exception e){
      //      Log.i(TAG,e.getMessage());
        }
    }


    public void setLOGGEDINFROM(String login_from){
        prefsEditor.putString(LOGGEDINFROM,login_from).commit();
    }
    public String getLOGGEDINFROM(){
        return appSharedPrefs.getString(LOGGEDINFROM,"");
    }


    /* --------------Student ID-----------------------*/

    public void setStudentId(String Student_id){
        prefsEditor.putString(studentId,Student_id).commit();
    }

    public String getStudentId(){
        return appSharedPrefs.getString(studentId, "");
    }
    public void setImage(String Image){
        prefsEditor.putString(image,Image).commit();
    }
    public String getImage(){
        return appSharedPrefs.getString(image,"");
    }

    public void setFname(String f_name){
        prefsEditor.putString(firstName,f_name).commit();
    }
    public String getFname(){
        return appSharedPrefs.getString(firstName,"");
    }
    public void setLastName(String l_name){
        prefsEditor.putString(lastName,l_name).commit();
    }
    public String getLastName(){
        return appSharedPrefs.getString(lastName, "");
    }
    public void setEmail(String Email){
        prefsEditor.putString(email,Email).commit();
    }
    public String getEmail(){
        return appSharedPrefs.getString(email,"");
    }
    public void setPassword(String Password){
        prefsEditor.putString(password, Password).commit();
    }
    public String getPassword(){
        return appSharedPrefs.getString(password,"");
    }
    public void setState(String State){
        prefsEditor.putString(state, State).commit();
    }
    public String getState(){
        return appSharedPrefs.getString(state,"");
    }

    public void setCity(String City){
        prefsEditor.putString(city,City).commit();
    }
    public String getCity(){
        return appSharedPrefs.getString(city,"");
    }
    public void setSchool(String School){
        prefsEditor.putString(school,School).commit();
    }
    public String getSchool(){
        return appSharedPrefs.getString(school,"");
    }
    public void setSchool_email(String School_name){
        prefsEditor.putString(school_email, School_name).commit();
    }
    public String getSchool_email(){
        return appSharedPrefs.getString(school_email,"");
    }
    public void setSecquestion(String sec_question){
        prefsEditor.putString(secQuestion, sec_question).commit();
    }
    public String getSecquestion(){
        return appSharedPrefs.getString(secQuestion,"");
    }
    public void setSecanswer(String sec_answer){
        prefsEditor.putString(secAnswer, sec_answer).commit();
    }
    public String getSecanswer(){
        return appSharedPrefs.getString(secAnswer,"");
    }
    public void setContact_relation(String Contact_relation){
        prefsEditor.putString(contact_relation,Contact_relation).commit();
    }
    public String getContact_relation(){
        return appSharedPrefs.getString(contact_relation,"");
    }
    public void setFor_activity(String for_Activity){
        prefsEditor.putString(for_activity, for_Activity).commit();
    }
    public String getFor_activity(){
        return appSharedPrefs.getString(for_activity,"");
    }

    public void setPhone_number(String Phone_Number){
        prefsEditor.putString(phone_number,Phone_Number).commit();
    }

    public String getPhone_number(){
        return appSharedPrefs.getString(phone_number,"");
    }
    public void setSound_config(String Sound_config){
        prefsEditor.putString(sound_config,Sound_config).commit();
    }
    public String getSound_config(){
        return appSharedPrefs.getString(sound_config,"");
    }
    public void setAccount_key(String Account_key){
        prefsEditor.putString(account_key,Account_key).commit();
    }
    public String getAccount_key(){
        return appSharedPrefs.getString(account_key,"");
    }
    public void setSocial_id(String Social_id){
        prefsEditor.putString(social_id,Social_id).commit();
    }
    public String getSocial_id(){
        return appSharedPrefs.getString(social_id,"");
    }

    public void setSubmit_time(String Submit_Time){
        prefsEditor.putString(Submit_time,Submit_Time).commit();
    }

    public String getSubmit_time(){
        return appSharedPrefs.getString(Submit_time,"");
    }

    public void setSchool_id(String School_id){
        prefsEditor.putString(school_id,School_id).commit();
    }

    public String getSchool_id(){
        return appSharedPrefs.getString(school_id,"");
    }

    public void setUser_age(String User_age){
        prefsEditor.putString(user_age,User_age).commit();
    }

    public String getUser_age(){
        return appSharedPrefs.getString(user_age,"");
    }

    public void setDevice_id(String Device_id){
        prefsEditor.putString(device_id,Device_id).commit();
    }
    public String getDevice_id(){
        return appSharedPrefs.getString(device_id,"");
    }

    public void setDevice_type(String Device_type){
        prefsEditor.putString(device_type,Device_type).commit();

    }

    public String getDevice_type(){
        return appSharedPrefs.getString(device_type,"");
    }

    public void setImei(String Imei){
        prefsEditor.putString(imei, Imei).commit();
    }

    public String getImei(){
        return appSharedPrefs.getString(imei,"");
    }

    public void setUser_img(String User_img){
        prefsEditor.putString(user_img,User_img).commit();
    }

    public String getUser_img(){
        return appSharedPrefs.getString(user_img,"");
    }
  }
