package com.nexgen.bullybeware.util;

import android.graphics.Bitmap;

/**
 * Created by quepplin1 on 9/26/2016.
 */
public class ScaleBitmap {

    public static Bitmap ScaleBitmap(Bitmap bm, float scalingFactor) {
        int scaleHeight = (int) (bm.getHeight() * scalingFactor);
        int scaleWidth = (int) (bm.getWidth() * scalingFactor);

        return Bitmap.createScaledBitmap(bm, scaleWidth, scaleHeight, true);
    }
}
