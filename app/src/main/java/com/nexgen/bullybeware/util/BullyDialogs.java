package com.nexgen.bullybeware.util;

import android.app.Activity;
import android.app.ProgressDialog;

/**
 * Created by quepplin1 on 8/24/2016.
 */
public class BullyDialogs {

    public static ProgressDialog showLoading(Activity activity){
        ProgressDialog mProgressDialog = new ProgressDialog(activity);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Loading...");
        if (mProgressDialog != null){
            mProgressDialog.show();
        }
        return mProgressDialog;
    }
}
