package com.nexgen.bullybeware.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.nexgen.bullybeware.pojo.ReportSubmissionFilesPojo;
import com.nexgen.bullybeware.pojo.ReportSubmissionPojo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by quepplin1 on 11/2/2016.
 */
public class DatabaseHandler extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 18;
    private static final String DATABASE_NAME = "BullyApp_db";
    private static final String TABLE_SUBMISSION = "Report_form";
    private static final String FILE_SUBMISSION = "File_Submission";
    private static final String Column_Id = "id";
    private static final String USER_ID = "User_id";
    private static final String INCIDENT_TYPE = "IncidentType";
    private static final String INCIDENT_TITLE = "IncidentTitle";
    private static final String INCIDENT_DATE = "IncidentDate";
    private static final String BULLY_NAME = "BullyName";
    private static final String VICTIM_NAME = "VictimName";
    private static final String NEAREST_ADULT = "NearestAdult";
    private static final String INCIDENT_DESCRIPTION = "Incident_Description";
    private static final String FILE_TYPE = "File_Type";
    private static final String INCIDENT_TIME = "Incident_Time";
    private static final String SUBMIT_ID = "Submit_Id";

    private static final String FORM_ID = "Form_id";
    private static final String IMAGE_URL = "Image_url";
    private static final String AUDIO_URL = "Audio_url";
    private static final String VIDEO_URL = "Video_url";


    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);

    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL("create table " + TABLE_SUBMISSION + "("
                + " id integer primary key AUTOINCREMENT, "
                + "User_id text,"
                + "File_Type text,"
                + "Submit_Id text,"
                + "IncidentType text,"
                + "IncidentTitle text,"
                + "IncidentDate text,"
                + "Incident_Time text,"
                + "BullyName text,"
                + "VictimName text,"
                + "NearestAdult text,"
                + "Incident_Description text" + ");");

        db.execSQL("create table " + FILE_SUBMISSION + "("
                + "Submit_Id text,"
                + "Image_url text,"
                + "Audio_url text,"
                + "Video_url text" + ");");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SUBMISSION);
        db.execSQL("DROP TABLE IF EXISTS " + FILE_SUBMISSION);
        onCreate(db);
    }

    public void addDetails(ReportSubmissionPojo list) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(USER_ID,list.getUSER_ID());
        values.put(FILE_TYPE,list.getFile_type());
        values.put(SUBMIT_ID,list.getSUBMIT_ID());
        values.put(INCIDENT_TYPE, list.getType());
        values.put(INCIDENT_TITLE, list.getSubject());
        values.put(INCIDENT_DATE, list.getDate());
        values.put(INCIDENT_TIME,list.getTime());
        values.put(BULLY_NAME,list.getBully_name());
        values.put(VICTIM_NAME, list.getName());
        values.put(NEAREST_ADULT, list.getAdult());
        values.put(INCIDENT_DESCRIPTION, list.getDescription());
        // Inserting Row

        db.insert(TABLE_SUBMISSION, null, values);
        db.close(); // Closing database connection

    }

    public void addFiles(ArrayList<ReportSubmissionFilesPojo> list) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(SUBMIT_ID, list.get(0).getSubmit_id());
        values.put(IMAGE_URL, list.get(0).getImage_url());
        values.put(AUDIO_URL, list.get(0).getAudio_url());
        values.put(VIDEO_URL, list.get(0).getVideo_url());
        // Inserting Row

        db.insert(FILE_SUBMISSION, null, values);
        db.close(); // Closing database connection

    }

    public ArrayList<ReportSubmissionPojo> getAllList() {
        ArrayList<ReportSubmissionPojo> submissionList = new ArrayList<ReportSubmissionPojo>();

        String selectQuery = "SELECT  * FROM " + TABLE_SUBMISSION + " ORDER BY "+ Column_Id + " DESC";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

           if (cursor.moveToFirst()){
               do{
                   ReportSubmissionPojo pojo = new ReportSubmissionPojo();
                   pojo.setUSER_ID(cursor.getString(1));
                   pojo.setFile_type(cursor.getString(2));
                   pojo.setSUBMIT_ID(cursor.getString(3));
                   pojo.setType(cursor.getString(4));
                   pojo.setSubject(cursor.getString(5));
                   pojo.setDate(cursor.getString(6));
                   pojo.setTime(cursor.getString(7));
                   pojo.setBully_name(cursor.getString(8));
                   pojo.setName(cursor.getString(9));
                   pojo.setAdult(cursor.getString(10));
                   pojo.setDescription(cursor.getString(11));

                   submissionList.add(pojo);

               }while (cursor.moveToNext());
    }
    return submissionList;
}
    public void deleteList(String name) {

        // TODO Auto-generated method stub
        SQLiteDatabase db = this.getWritableDatabase();
        if(db != null){

            db.execSQL("DELETE FROM " + TABLE_SUBMISSION+ " WHERE "+SUBMIT_ID+"='"+name+"'");
            db.close();
        }
    }

    public void deletefile(String name) {

        // TODO Auto-generated method stub
        SQLiteDatabase db = this.getWritableDatabase();
        if(db != null){

            db.execSQL("DELETE FROM " + FILE_SUBMISSION+ " WHERE "+SUBMIT_ID+"='"+name+"'");
            db.close();
        }
    }

    public ArrayList<ReportSubmissionFilesPojo> getAllFiles() {
        ArrayList<ReportSubmissionFilesPojo> filesList = new ArrayList<ReportSubmissionFilesPojo>();

        String selectQuery = "SELECT  * FROM " + FILE_SUBMISSION;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()){
            do{
                ReportSubmissionFilesPojo pojo = new ReportSubmissionFilesPojo();
                pojo.setSubmit_id(cursor.getString(0));
                pojo.setImage_url(cursor.getString(1));
                pojo.setAudio_url(cursor.getString(2));
                pojo.setVideo_url(cursor.getString(3));

                filesList.add(pojo);

            }while (cursor.moveToNext());
        }

        return filesList;
    }

}
