package com.nexgen.bullybeware.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.nexgen.bullybeware.R;
import com.nexgen.bullybeware.activity.SafePlaceActivity;
import com.nexgen.bullybeware.pojo.SafeListDataPojo;

import java.util.ArrayList;

/**
 * Created by Yash on 5/13/2017.
 */

public class SafeListAdapter extends RecyclerView.Adapter<SafeListAdapter.ViewHolder> {
    private ArrayList<SafeListDataPojo> android;
    private Context context;

    public SafeListAdapter(Context context, ArrayList<SafeListDataPojo> android) {
        this.android = android;
        this.context = context;
    }

    @Override
    public SafeListAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.safe_list_row, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(SafeListAdapter.ViewHolder viewHolder, int i) {
        SafeListDataPojo data = android.get(i);
        viewHolder.place_name.setText(data.getName());

        viewHolder.rootLayout.setTag(R.string.key_state, data.getName());
        viewHolder.rootLayout.setTag(R.string.key, data.getContact_name());
        viewHolder.rootLayout.setTag(R.string.key_delete, data.getMobile());
        viewHolder.rootLayout.setTag(R.string.delete_position, data.getEmail());
        viewHolder.rootLayout.setOnClickListener((SafePlaceActivity) context);

    }

    @Override
    public int getItemCount() {
        return android.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView place_name;
        private LinearLayout rootLayout;
        public ViewHolder(View view) {
            super(view);

            place_name = (TextView)view.findViewById(R.id.place_name);
            rootLayout = (LinearLayout) view.findViewById(R.id.rootLayout);
        }
    }
}
