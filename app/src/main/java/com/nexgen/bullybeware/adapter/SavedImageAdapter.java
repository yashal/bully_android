package com.nexgen.bullybeware.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.nexgen.bullybeware.R;
import com.nexgen.bullybeware.activity.SavedImageActivity;
import com.nexgen.bullybeware.pojo.CheckboxStatusPojo;
import com.nexgen.bullybeware.util.ScaleBitmap;

import java.util.ArrayList;

/**
 * Created by quepplin1 on 9/26/2016.
 */
public class SavedImageAdapter extends PagerAdapter {

    Activity mContext;
    LayoutInflater mLayoutInflater;
    private ArrayList<CheckboxStatusPojo> imagelist1;
    private SavedImageActivity activity= null;

    public void setDialog(Dialog dialog) {
        this.dialog = dialog;
    }

    private Dialog dialog = null;

    public SavedImageAdapter(Activity context, ArrayList<CheckboxStatusPojo> imagelist){
        this.imagelist1 = imagelist;
        this.mContext = context;

        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
         return imagelist1.size() -1;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View itemView = mLayoutInflater.inflate(R.layout.image_pager_item, container, false);

        ImageView imageView = (ImageView) itemView.findViewById(R.id.image_iView_pager);
        LinearLayout ll_image = (LinearLayout) itemView.findViewById(R.id.ll_image);

        ll_image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(dialog!=null){
                    dialog.dismiss();
                }
            }
        });

        try {
        /*    Bitmap myBitmap = BitmapFactory.decodeFile(imagelist1.get(position).getPath());
            float scalingFactor = getBitmapScalingFactor(imageView,myBitmap);
            Bitmap newBitmap = ScaleBitmap.ScaleBitmap(myBitmap, scalingFactor);
            imageView.setImageBitmap(newBitmap);*/

            Glide.with(mContext).load(imagelist1.get(position).getPath())
         //           .thumbnail(0.5f)
                    .crossFade()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(imageView);
        } catch (Exception e){
            e.printStackTrace();
        }

        container.addView(itemView);

        return itemView;
    }
    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }
    public float getBitmapScalingFactor(ImageView imageView, Bitmap bm) {
        // Get display width from device
        int displayWidth = mContext.getWindowManager().getDefaultDisplay().getWidth();

        // Get margin to use it for calculating to max width of the ImageView
        LinearLayout.LayoutParams layoutParams =
                (LinearLayout.LayoutParams)imageView.getLayoutParams();
        int leftMargin = layoutParams.leftMargin;
        int rightMargin = layoutParams.rightMargin;

        // Calculate the max width of the imageView
        int imageViewWidth = displayWidth - (leftMargin + rightMargin);

        // Calculate scaling factor and return it
        return ( (float) imageViewWidth / (float) bm.getWidth() );
    }
}
