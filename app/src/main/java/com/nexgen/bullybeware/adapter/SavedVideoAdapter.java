package com.nexgen.bullybeware.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;

import com.nexgen.bullybeware.R;
import com.nexgen.bullybeware.activity.SavedImageActivity;
import com.nexgen.bullybeware.pojo.CheckboxStatusPojo;
import com.nexgen.bullybeware.util.ScaleBitmap;
import com.nexgen.bullybeware.views.VideoView;

import java.util.ArrayList;

/**
 * Created by quepplin1 on 9/28/2016.
 */
public class SavedVideoAdapter extends PagerAdapter {

    Context mContext;
    LayoutInflater mLayoutInflater;
    private ArrayList<CheckboxStatusPojo> videolist;
    private SavedImageActivity activity = null;
    private VideoView videoReport = null;
    private ImageView iView_audio_play;

    private Dialog dialog = null;
    Uri videoUri;

    public void setDialog(Dialog dialog) {
        this.dialog = dialog;
    }

    public SavedVideoAdapter(Context context, ArrayList<CheckboxStatusPojo> videolist) {
        this.videolist = videolist;
        this.mContext = context;

        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return videolist.size() - 1;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View itemView = mLayoutInflater.inflate(R.layout.video_dialog, container, false);

        final VideoView videoview = (VideoView) itemView.findViewById(R.id.Videoview);
        final FrameLayout fl_video = (FrameLayout) itemView.findViewById(R.id.fl_video);
        final ImageView iView_play = (ImageView) itemView.findViewById(R.id.video_play);
        final ImageView iView_video = (ImageView) itemView.findViewById(R.id.iView_video);
        LinearLayout ll_video = (LinearLayout) itemView.findViewById(R.id.video_ll);



        Bitmap thumb = ThumbnailUtils.createVideoThumbnail(videolist.get(position).getPath(), MediaStore.Video.Thumbnails.MINI_KIND);
        iView_video.setImageBitmap(thumb);

        fl_video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                videoview.setVisibility(View.VISIBLE);
                videoReport = videoview;
                iView_audio_play = iView_play;
                if (videoview.isPlaying()) {
                    videoview.stop();
                    iView_play.setVisibility(View.VISIBLE);
                } else {
                    videoReport.resume();
               //     videoUri = Uri.parse(videolist.get(position).getPath());
                    videoview.setVideoPath(videolist.get(position).getPath());
                    //            videoview.setZOrderOnTop(true);
                    videoview.start();

                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            // Do something after 5s = 5000ms
                //            iView_video.setVisibility(View.GONE);

                        }
                    }, 2000);
                    iView_play.setVisibility(View.GONE);
                }
            }
        });

        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                if (dialog != null) {
                    dialog.dismiss();
                }
            }
        });


        ll_video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dialog != null) {
                    dialog.dismiss();
                    videoview.stop();
                }
            }
        });

        container.addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
        notifyDataSetChanged();
    }

    public void onPageChanged() {
         if (videoReport != null)
          videoReport.stop();


        if (iView_audio_play != null)
            iView_audio_play.setVisibility(View.VISIBLE);

    }
}
