package com.nexgen.bullybeware.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.media.session.MediaController;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.VideoView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.nexgen.bullybeware.R;
import com.nexgen.bullybeware.activity.SavedImageActivity;
import com.nexgen.bullybeware.activity.SavedVideoActivity;
import com.nexgen.bullybeware.activity.VideoRecordActivity;
import com.nexgen.bullybeware.pojo.CheckboxStatusPojo;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by quepplin1 on 9/27/2016.
 */
public class VideoGridAdapter extends BaseAdapter {

    private ArrayList<CheckboxStatusPojo> videostatus;
    private String videoUri = "android.resource://com.nexgen.bullybeware/" + R.mipmap.record_video;

    private Activity mContext;
    private Dialog dialogLogout;
    private int done_count = 1;
    private final int REQUEST_TAKE_GALLERY_VIDEO = 2;
    private SavedVideoAdapter adapter;
    MediaController mc;
    Dialog builder;

    public VideoGridAdapter(Activity Context,ArrayList<CheckboxStatusPojo> videolist) {

        this.mContext = Context;
        this.videostatus = videolist;

    }

    @Override
    public int getCount() {
        return videostatus.size();

        // return imagelist.size()+1;
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {

        final boolean isLastItem = (i == (videostatus.size() - 1));

        if (view == null) {
            LayoutInflater mInflater = (LayoutInflater) mContext
                    .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            view = mInflater.inflate(R.layout.grid_row, null);
        }
            ImageView iView_saved = (ImageView) view.findViewById(R.id.iView_saved);
        final ImageView checkbox_unselected = (ImageView) view.findViewById(R.id.checkbox_unselected);
        final ImageView checkbox_selected = (ImageView) view.findViewById(R.id.checkbox_selected);

        ImageView play_list = (ImageView) view.findViewById(R.id.play_list);


        if(videostatus.get(i).getStatus()){
            checkbox_selected.setVisibility(View.VISIBLE);
            checkbox_unselected.setVisibility(View.GONE);
        }else{
            checkbox_selected.setVisibility(View.GONE);
            checkbox_unselected.setVisibility(View.VISIBLE);
        }
        View.OnClickListener clickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int selectCount = 0;
                for(CheckboxStatusPojo statusPojo : videostatus){
                    if(statusPojo.getStatus())
                        selectCount++;
                }
                if(videostatus.get(i).getStatus()){
                    if(selectCount!=1) {
                        checkbox_unselected.setVisibility(View.VISIBLE);
                        checkbox_selected.setVisibility(View.GONE);
                        videostatus.get(i).setStatus(false);
                    }
                }
                else{
                    checkbox_selected.setVisibility(View.VISIBLE);
                    checkbox_unselected.setVisibility(View.GONE);
                    videostatus.get(i).setStatus(true);
                }
            }
        };

        checkbox_unselected.setOnClickListener(clickListener);

        checkbox_selected.setOnClickListener(clickListener);


        if (isLastItem) {
            iView_saved.setImageURI(Uri.parse(videoUri));
            checkbox_unselected.setVisibility(View.GONE);

            checkbox_unselected.setVisibility(View.GONE);
            play_list.setVisibility(View.GONE);
        } else {

            Bitmap thumb = ThumbnailUtils.createVideoThumbnail(videostatus.get(i).getPath(), MediaStore.Video.Thumbnails.MINI_KIND);
            iView_saved.setImageBitmap(thumb);
        }

        iView_saved.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isLastItem) {
                    ((SavedVideoActivity)mContext).video();

                } else {
                    showVideoDialog(videostatus,i);
                }
            }
        });
        return view;
    }

    public void showVideoDialog(ArrayList<CheckboxStatusPojo> video,int pos) {
        builder = new Dialog(mContext,android.R.style.Theme_Translucent_NoTitleBar);
        builder.requestWindowFeature(Window.FEATURE_NO_TITLE);
        builder.setContentView(R.layout.pager_video_dialog);
        builder.show();

        adapter = new SavedVideoAdapter(mContext,video);

        ViewPager mViewPager = (ViewPager) builder.findViewById(R.id.video_dialog_pager);
        mViewPager.setAdapter(adapter);
        adapter.setDialog(builder);
        mViewPager.setCurrentItem(pos);
        adapter.notifyDataSetChanged();

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                 adapter.onPageChanged();
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }
}
