package com.nexgen.bullybeware.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.net.Uri;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.nexgen.bullybeware.R;
import com.nexgen.bullybeware.activity.SavedImageActivity;
import com.nexgen.bullybeware.pojo.CheckboxStatusPojo;
import java.util.ArrayList;

/**
 * Created by quepplin1 on 9/6/2016.
 */
public class GridAdapter extends BaseAdapter {

    private Activity mcontext;
    private ArrayList<CheckboxStatusPojo> imagestatus;
    private Dialog dialogLogout;
    private String imageUri;
    private SavedImageAdapter imageAdapter;

    public GridAdapter(Activity context, ArrayList<CheckboxStatusPojo> imagestatus, String imageUri) {
        this.mcontext = context;
        this.imagestatus = imagestatus;
        this.imageUri = imageUri;
    }

    @Override
    public int getCount() {
        return imagestatus.size();

    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {

        final boolean isLastItem = (i == (imagestatus.size() - 1));

        if (view == null) {
            LayoutInflater mInflater = (LayoutInflater) mcontext
                    .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            view = mInflater.inflate(R.layout.image_grid_row, null);
        }
        ImageView iView_saved = (ImageView) view.findViewById(R.id.grid_iView_saved);
        final ImageView checkbox_unselected = (ImageView) view.findViewById(R.id.checkbox_unselected);
        final ImageView checkbox_selected = (ImageView) view.findViewById(R.id.checkbox_selected);


        if (imagestatus.get(i).getStatus()) {
            checkbox_selected.setVisibility(View.VISIBLE);
            checkbox_unselected.setVisibility(View.GONE);
        } else {
            checkbox_selected.setVisibility(View.GONE);
            checkbox_unselected.setVisibility(View.VISIBLE);
        }

        View.OnClickListener clickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int selectCount = 0;
                for (CheckboxStatusPojo statusPojo : imagestatus) {
                    if (statusPojo.getStatus())
                        selectCount++;
                }
                if (imagestatus.get(i).getStatus()) {
                    if (selectCount != 1) {
                        checkbox_unselected.setVisibility(View.VISIBLE);
                        checkbox_selected.setVisibility(View.GONE);
                        imagestatus.get(i).setStatus(false);
                    }
                } else {
                    checkbox_selected.setVisibility(View.VISIBLE);
                    checkbox_unselected.setVisibility(View.GONE);
                    imagestatus.get(i).setStatus(true);
        }
    }
        };

        checkbox_unselected.setOnClickListener(clickListener);

        checkbox_selected.setOnClickListener(clickListener);

        if (isLastItem) {

            iView_saved.setImageURI(Uri.parse(imageUri));
            checkbox_unselected.setVisibility(View.GONE);

        } else {
            Glide.with(mcontext).load(imagestatus.get(i).getPath())
                    .crossFade()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(iView_saved);

        }

        iView_saved.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isLastItem) {

                    ((SavedImageActivity) mcontext).Camera();

                } else {
                    showImageDialog(imagestatus, i);
                }
            }
        });
        return view;
    }


    public void showImageDialog(final ArrayList<CheckboxStatusPojo> image, int pos) {
        dialogLogout = new Dialog(mcontext, android.R.style.Theme_Translucent_NoTitleBar);
        dialogLogout.setContentView(R.layout.image_dialog);
        dialogLogout.show();

        ViewPager mViewPager = (ViewPager) dialogLogout.findViewById(R.id.pager);

        dialogLogout.setCanceledOnTouchOutside(true);
        imageAdapter = new SavedImageAdapter(mcontext, image);
        imageAdapter.setDialog(dialogLogout);
        mViewPager.setAdapter(imageAdapter);
        mViewPager.setCurrentItem(pos);

    }
}
