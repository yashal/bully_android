package com.nexgen.bullybeware.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.View;

import com.nexgen.bullybeware.fragment.FirstSignUpFragment;
import com.nexgen.bullybeware.fragment.SecondSignUpFragment;

import java.util.ArrayList;

/**
 * Created by umax desktop on 1/6/2017.
 */

public class SingleBullypagerAdapter extends FragmentStatePagerAdapter {

    private ArrayList<Fragment> mPageReferenceMap = new ArrayList<>();

    public SingleBullypagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0: {
                FirstSignUpFragment fragment_first = new FirstSignUpFragment();
                mPageReferenceMap.add(position, fragment_first);
                return fragment_first;
            }
        }

        return null;
    }

    @Override
    public int getCount() {
        return 1;
    }

    public Fragment getFragment(int key) {
        if (key < mPageReferenceMap.size())
            return mPageReferenceMap.get(key);
        else
            return null;
    }

    public void destroyItem(View container, int position, Object object) {
        super.destroyItem(container, position, object);
        mPageReferenceMap.remove(position);
    }
}
