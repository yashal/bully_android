package com.nexgen.bullybeware.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ThumbnailUtils;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.nexgen.bullybeware.R;
import com.nexgen.bullybeware.activity.AudioReportActivity;
import com.nexgen.bullybeware.activity.MyRecordActivity;
import com.nexgen.bullybeware.activity.MySubmissionDetaileActivity;
import com.nexgen.bullybeware.activity.ReportIncidentActivity;
import com.nexgen.bullybeware.activity.TextReportIncident;
import com.nexgen.bullybeware.activity.VideoReportActivity;
import com.nexgen.bullybeware.database.DatabaseHandler;
import com.nexgen.bullybeware.fragment.MyRecordingsFragment;
import com.nexgen.bullybeware.pojo.GetSubmissionObjectPojo;
import com.nexgen.bullybeware.pojo.ReportSubmissionFilesPojo;
import com.nexgen.bullybeware.pojo.ReportSubmissionPojo;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by quepplin1 on 11/2/2016.
 */
public class MyRecordingsAdapter extends RecyclerView.Adapter<MyRecordingsAdapter.ViewHolder> {

    private Activity context;
    private ArrayList<ReportSubmissionPojo> arraysubmissionlist;
    private ArrayList<String> checkedimagelist = new ArrayList<String>();
    private DatabaseHandler database;
    String date_time;


    public MyRecordingsAdapter(Activity context,ArrayList<ReportSubmissionPojo> arraysubmissionlist){

        this.context = context;
        this.arraysubmissionlist = arraysubmissionlist;

        database = new DatabaseHandler(context);

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.myrecording_layout, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final ReportSubmissionPojo data = arraysubmissionlist.get(position);

        holder.record_type.setText(data.getType());

        holder.victim_name.setText(data.getName());

        final String date = data.getDate();
        String time = data.getTime();

        String str = date;
        String[] splited = str.split("-");


            String split_one= splited[0];
            String split_second= splited[1];
            String split_three= splited[2];

            holder.record_date.setText( new StringBuilder().append(split_second).append("/")
                    .append(split_three).append("/").append(split_one)+", " + time);


        if (data.getFile_type().equalsIgnoreCase("2")){
            holder.record_icon.setVisibility(View.GONE);
            holder.record_video_thumbnail.setVisibility(View.GONE);
            holder.record_audio.setVisibility(View.VISIBLE);
        } else if (data.getFile_type().equalsIgnoreCase("1")){
            holder.record_audio.setVisibility(View.GONE);
            holder.record_video_thumbnail.setVisibility(View.GONE);
            holder.record_icon.setVisibility(View.VISIBLE);

            if (data.getFilelist().size() > 0){
                Glide.with(context).load(data.getFilelist().get(0).getImage_url())
                        .thumbnail(0.5f)
                        .crossFade()
                        .placeholder(R.mipmap.ic_launcher)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(holder.record_icon);
            }



        } else if (data.getFile_type().equalsIgnoreCase("3")){
            holder.record_icon.setVisibility(View.GONE);
            holder.record_audio.setVisibility(View.GONE);
            holder.record_video_thumbnail.setVisibility(View.VISIBLE);

            Bitmap thumb = ThumbnailUtils.createVideoThumbnail(data.getFilelist().get(0).getVideo_url(), MediaStore.Video.Thumbnails.MINI_KIND);
            holder.record_video_thumbnail.setImageBitmap(thumb);

        }
        else if (data.getFile_type().equalsIgnoreCase("4")){
            holder.record_icon.setVisibility(View.GONE);
            holder.record_audio.setVisibility(View.GONE);
            holder.record_video_thumbnail.setVisibility(View.INVISIBLE);
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= null;
                if (data.getFile_type().equalsIgnoreCase("1")){
                    intent = new Intent(context, ReportIncidentActivity.class);
                    Bundle args = new Bundle();
                    checkedimagelist.clear();
                    if (data.getFilelist().size() > 0){
                        for (int i = 0; i< data.getFilelist().size(); i++){
                            checkedimagelist.add(data.getFilelist().get(i).getImage_url());
                        }

                        args.putSerializable("ARRAYLIST",(Serializable)checkedimagelist);
                        intent.putExtra("BUNDLE",args);
                    } else {
                        intent.putExtra("file","1");
                    }

                } else if(data.getFile_type().equalsIgnoreCase("2")){
                    intent = new Intent(context, AudioReportActivity.class);
                    Bundle args = new Bundle();
                    checkedimagelist.clear();
                    if (data.getFilelist().size() > 0){
                        for (int i = 0; i< data.getFilelist().size(); i++){
                            checkedimagelist.add(data.getFilelist().get(i).getAudio_url());
                        }

                        args.putSerializable("ARRAYLIST",(Serializable)checkedimagelist);
                        intent.putExtra("BUNDLE",args);
                    } else {
                        intent.putExtra("file","1");
                    }

                } else if (data.getFile_type().equalsIgnoreCase("3")){
                    intent = new Intent(context, VideoReportActivity.class);
                    Bundle args = new Bundle();
                    checkedimagelist.clear();
                    if (data.getFilelist().size() > 0){
                        for (int i = 0; i< data.getFilelist().size(); i++){
                            checkedimagelist.add(data.getFilelist().get(i).getVideo_url());
                        }

                        args.putSerializable("ARRAYLIST",(Serializable)checkedimagelist);
                        intent.putExtra("BUNDLE",args);
                    } else {
                        intent.putExtra("file","1");
                    }
                }
                else if (data.getFile_type().equalsIgnoreCase("4")){
                    intent = new Intent(context, TextReportIncident.class);
                    Bundle args = new Bundle();

                }
                intent.putExtra("type",data.getType());
                intent.putExtra("incident","");
                intent.putExtra("title",data.getSubject());
                intent.putExtra("bully_name",data.getBully_name());
                intent.putExtra("Submit_id",data.getSUBMIT_ID());
                intent.putExtra("date",date_time);
                intent.putExtra("Victim_name",data.getName());
                intent.putExtra("nearest_adult",data.getAdult());
                intent.putExtra("description",data.getDescription());

                context.startActivity(intent);

            }
        });
    }

    @Override
    public int getItemCount() {
        return arraysubmissionlist.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder{

        private TextView record_type,record_date,victim_name;
        private ImageView record_icon,record_video_thumbnail;
        private FrameLayout record_audio;
        private LinearLayout recoding_layout;

        public ViewHolder(View itemView) {
            super(itemView);

            record_type = (TextView) itemView.findViewById(R.id.recording_incident_type);
            record_date = (TextView) itemView.findViewById(R.id.recording_incident_date);
            victim_name = (TextView) itemView.findViewById(R.id.recording_victim_name);

            record_icon = (ImageView) itemView.findViewById(R.id.recording_iView_icon);
            record_audio = (FrameLayout) itemView.findViewById(R.id.recording_audio);
            record_video_thumbnail = (ImageView) itemView.findViewById(R.id.recording_video_thumbnail);

            recoding_layout = (LinearLayout) itemView.findViewById(R.id.recoding_layout);
        }
    }
}
