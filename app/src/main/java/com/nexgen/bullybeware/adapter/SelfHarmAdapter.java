package com.nexgen.bullybeware.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nexgen.bullybeware.R;
import com.nexgen.bullybeware.activity.SelfHarmListActivity;
import com.nexgen.bullybeware.pojo.FriendListObjectPojo;
import com.nexgen.bullybeware.pojo.SelfHarmObjectPojo;

import java.util.ArrayList;

/**
 * Created by umax desktop on 2/16/2017.
 */

public class SelfHarmAdapter extends RecyclerView.Adapter<SelfHarmAdapter.ViewHolder> {

    private Activity context;
    private ArrayList<SelfHarmObjectPojo> selfharmlist;

    public SelfHarmAdapter(Activity context,ArrayList<SelfHarmObjectPojo> selfharmlist){

        this.context = context;
        this.selfharmlist = selfharmlist;

    }



    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.self_harm_list_layout, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        holder.friend_name.setText(selfharmlist.get(position).getFriend_name());
        holder.description.setText(selfharmlist.get(position).getHarm_description());
        holder.submitted_on.setText(selfharmlist.get(position).getSubmitted_on());

        holder.iv_delete.setOnClickListener((SelfHarmListActivity)context);
        holder.ll_selfharm.setOnClickListener((SelfHarmListActivity)context);
        holder.iv_delete.setTag(R.string.self_harm_id, selfharmlist.get(position).getId());
        holder.iv_delete.setTag(R.string.self_harm_position, position);

    }

    @Override
    public int getItemCount() {
        return selfharmlist.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private TextView friend_name,description,submitted_on;
        private ImageView iv_delete;
        private LinearLayout ll_selfharm;


        public ViewHolder(View itemView) {
            super(itemView);

            friend_name = (TextView) itemView.findViewById(R.id.friend_name);
            description = (TextView) itemView.findViewById(R.id.sh_description);
            submitted_on = (TextView) itemView.findViewById(R.id.sh_submitted_on);

            iv_delete = (ImageView) itemView.findViewById(R.id.selfharm_delete);

            ll_selfharm = (LinearLayout) itemView.findViewById(R.id.selfharm);
        }
    }
}
