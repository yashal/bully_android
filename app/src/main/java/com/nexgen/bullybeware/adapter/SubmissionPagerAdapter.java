package com.nexgen.bullybeware.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.StrictMode;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.VideoView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.nexgen.bullybeware.R;
import com.nexgen.bullybeware.activity.SavedImageActivity;
import com.nexgen.bullybeware.pojo.CheckboxStatusPojo;
import com.nexgen.bullybeware.pojo.GetSubmissionFilesPojo;
import com.nexgen.bullybeware.util.BullyDialogs;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

/**
 * Created by quepplin1 on 10/20/2016.
 */
public class SubmissionPagerAdapter extends PagerAdapter {

    private MediaPlayer mp;
    Activity mContext;
    LayoutInflater mLayoutInflater;
    private ArrayList<GetSubmissionFilesPojo> filelist;
    private SavedImageActivity activity= null;
    private String audio,image,type,video;
    private ImageView iView_audio_play;

    public void setDialog(Dialog dialog) {
        this.dialog = dialog;
    }

    private Dialog dialog = null;

    public SubmissionPagerAdapter(Activity context, ArrayList<GetSubmissionFilesPojo> filelist,String type){
        this.filelist = filelist;
        this.mContext = context;
        this.type = type;

        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return filelist.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View itemView = mLayoutInflater.inflate(R.layout.submission_pager_adapter, container, false);

        FrameLayout fl_record_audio = (FrameLayout) itemView.findViewById(R.id.fl_record_audio);
        FrameLayout fl_record_video = (FrameLayout) itemView.findViewById(R.id.fl_record_video);
        ImageView iv_incident = (ImageView)itemView.findViewById(R.id.incident_photo);
        final WebView videoView = (WebView) itemView.findViewById(R.id.pager_video);
        final ImageView audio_play = (ImageView) itemView.findViewById(R.id.audio_iView_play);
        final ImageView video_play = (ImageView) itemView.findViewById(R.id.video_iView_play);


        mp = new MediaPlayer();

        if (filelist.size() > 0){
            //      audio_play.setVisibility(View.VISIBLE);
            if (type.equalsIgnoreCase("2")){
                audio = filelist.get(position).getFile();
            } else if (type.equalsIgnoreCase("1")){
                image = filelist.get(position).getFile();
            } else if (type.equalsIgnoreCase("3")){
                video = filelist.get(position).getFile();
            }
        }else {
            //     audio_play.setVisibility(View.GONE);
        }

        if (type.equalsIgnoreCase("2")){
            iv_incident.setVisibility(View.GONE);
            fl_record_video.setVisibility(View.GONE);
            fl_record_audio.setVisibility(View.VISIBLE);
            audio_play.setVisibility(View.VISIBLE);
        } else if (type.equalsIgnoreCase("1")){
            fl_record_audio.setVisibility(View.GONE);
            fl_record_video.setVisibility(View.GONE);
            iv_incident.setVisibility(View.VISIBLE);

            Glide.with(mContext).load(filelist.get(position).getFile())
                    .thumbnail(0.5f)
                    .crossFade()
                    .placeholder(R.mipmap.ic_launcher)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(iv_incident);

        } else if (type.equalsIgnoreCase("3")){
            iv_incident.setVisibility(View.GONE);
            fl_record_audio.setVisibility(View.GONE);
            audio_play.setVisibility(View.VISIBLE);

            fl_record_video.setVisibility(View.VISIBLE);

           final ProgressDialog alertDialog = BullyDialogs.showLoading(mContext);

       //     String url = "http://www.youtube.com/watch?v="+video;
       //     String videoUrl = getUrlVideoRTSP(url);

            videoView.getSettings().setJavaScriptEnabled(true);
            videoView.getSettings().setPluginState(WebSettings.PluginState.ON);
            final String mimeType = "text/html";
            final String encoding = "UTF-8";
            String html = getHTML();
            videoView.setWebChromeClient(new WebChromeClient() {
                public void onProgressChanged(WebView view, int progress) {
                    if (progress >= 85) {
                        if (alertDialog != null && alertDialog.isShowing())
                            alertDialog.dismiss();
                    }
                }
            });
            videoView.loadDataWithBaseURL("", html, mimeType, encoding, "");
            alertDialog.dismiss();
        }

        fl_record_audio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                iView_audio_play = audio_play;
                if (mp != null && mp.isPlaying()) {
                    mp.stop();
                    mp.reset();
                    audio_play.setVisibility(View.VISIBLE);
                } else {
                    try {

                        mp.setDataSource(filelist.get(position).getFile());
                        audio_play.setVisibility(View.GONE);
                        mp.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                            @Override
                            public void onPrepared(MediaPlayer mediaPlayer) {
                                mp.start();
                            }
                        });
                        mp.prepareAsync();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            public void onCompletion(MediaPlayer mp) {

                mp.reset();
                iView_audio_play.setVisibility(View.VISIBLE);

            }
        });

        container.addView(itemView);

        return itemView;
    }
    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }
    public void onPageChanged(){

        if (mp != null || mp.isPlaying()){
            mp.stop();
            mp.reset();
        }
        if (iView_audio_play != null)
            iView_audio_play.setVisibility(View.VISIBLE);
    }

    public String getHTML() {
        String html = "<iframe class=\"youtube-player\" style=\"border: 0; width: 100%; height: 95%; padding:0px; margin:0px\" id=\"ytplayer\" type=\"text/html\" src=\"http://www.youtube.com/embed/"
                + video
                + "?fs=0\" frameborder=\"0\">\n"
                + "</iframe>\n";
        return html;
    }
}
