package com.nexgen.bullybeware.adapter;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.nexgen.bullybeware.R;
import com.nexgen.bullybeware.activity.AddfriendInfoActivity;
import com.nexgen.bullybeware.activity.FriendListActivity;
import com.nexgen.bullybeware.fragment.MySubmissionsFragment;
import com.nexgen.bullybeware.pojo.FriendListObjectPojo;

import java.util.ArrayList;

/**
 * Created by umax desktop on 12/22/2016.
 */

public class FriendListAdapter extends RecyclerView.Adapter<FriendListAdapter.ViewHolder> {

    private Activity context;
    private ArrayList<FriendListObjectPojo> friendlist;
    private String from;


    public FriendListAdapter(Activity context, ArrayList<FriendListObjectPojo> friendlist, String from) {

        this.context = context;
        this.friendlist = friendlist;
        this.from = from;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.friendlist_layout, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        holder.friend_name.setText(friendlist.get(position).getFirst_name());

        if (friendlist.get(position).getThumbnail().length() > 0){
            Glide.with(context).load(friendlist.get(position).getThumbnail())
                    .crossFade()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .placeholder(R.mipmap.user_pic)
                    .error(R.mipmap.user_pic)
                    .into(holder.friend_view);
        } else {
            Glide.with(context).load(R.mipmap.user_pic)
                    .crossFade()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .placeholder(R.mipmap.user_pic)
                    .error(R.mipmap.user_pic)
                    .into(holder.friend_view);
        }

        holder.video_call.setOnClickListener((FriendListActivity) context);
        holder.dial_call.setOnClickListener((FriendListActivity)context);
        holder.edit.setOnClickListener((FriendListActivity)context);
        holder.delete.setOnClickListener((FriendListActivity)context);
        holder.ll_layout.setOnClickListener((FriendListActivity)context);

        if (from != null){
            if (from.equalsIgnoreCase("follow_me")){
                holder.edit.setVisibility(View.GONE);
                holder.delete.setVisibility(View.GONE);
                holder.video_call.setVisibility(View.VISIBLE);
                holder.dial_call.setVisibility(View.VISIBLE);

                if (friendlist.get(position).getMobile().length() > 0){
                    holder.dial_call.setVisibility(View.VISIBLE);
                } else {
                    holder.dial_call.setVisibility(View.INVISIBLE);
                }

            } else {
                holder.edit.setVisibility(View.VISIBLE);
                holder.delete.setVisibility(View.VISIBLE);
                holder.video_call.setVisibility(View.GONE);
                holder.dial_call.setVisibility(View.GONE);
            }
        }

        holder.ll_layout.setTag(R.string.key,friendlist.get(position).getEmail());
        holder.delete.setTag(R.string.key_delete,friendlist.get(position).getId());
        holder.delete.setTag(R.string.delete_position,position);
        holder.edit.setTag(R.string.edit_position,position);
        holder.dial_call.setTag(R.string.dial_number,friendlist.get(position).getMobile());


    }

    @Override
    public int getItemCount() {
        return friendlist.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private TextView friend_name;
        private ImageView dial_call,delete,edit,video_call;
        private CircularImageView friend_view;
        private LinearLayout ll_layout;

        public ViewHolder(View itemView) {
            super(itemView);

            friend_name = (TextView) itemView.findViewById(R.id.friendlist_name);
            friend_view = (CircularImageView) itemView.findViewById(R.id.friendlist_circleView);

            ll_layout = (LinearLayout) itemView.findViewById(R.id.ll_friendlist);

            dial_call = (ImageView) itemView.findViewById(R.id.dial);
            delete = (ImageView) itemView.findViewById(R.id.delete);
            edit = (ImageView) itemView.findViewById(R.id.edit);
            video_call = (ImageView) itemView.findViewById(R.id.video_call);
        }
    }
}
