package com.nexgen.bullybeware.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.nexgen.bullybeware.R;
import com.nexgen.bullybeware.pojo.CityListPojo;
import com.nexgen.bullybeware.pojo.SchoolListpojo;

import java.util.ArrayList;

/**
 * Created by quepplin1 on 8/23/2016.
 */
public class SchoolListAdapter extends BaseAdapter {

    private ArrayList<SchoolListpojo> arrayList;
    private Context context;

    public SchoolListAdapter(Context context, ArrayList<SchoolListpojo> arrayList){
        this.context = context;
        this.arrayList = arrayList;
    }

    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public Object getItem(int i) {
        return arrayList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater mInflater = (LayoutInflater) context
                .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        convertView = mInflater.inflate(R.layout.state_row_layout, null);

        TextView tv_school = (TextView) convertView.findViewById(R.id.tv_state);

        tv_school.setText(arrayList.get(position).getSchool_name());

        return convertView;

    }
}
