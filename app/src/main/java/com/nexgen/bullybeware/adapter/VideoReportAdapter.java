package com.nexgen.bullybeware.adapter;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.media.Image;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.VideoView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.nexgen.bullybeware.R;
import com.nexgen.bullybeware.activity.ReportIncidentActivity;
import com.nexgen.bullybeware.activity.VideoReportActivity;
import com.nexgen.bullybeware.pojo.ImagelistStatusPojo;
import com.nexgen.bullybeware.pojo.VideolistStatusPojo;

import java.util.ArrayList;

/**
 * Created by quepplin1 on 10/22/2016.
 */
public class VideoReportAdapter extends PagerAdapter {

    Activity mContext;
    LayoutInflater mLayoutInflater;
    private ArrayList<VideolistStatusPojo> videolist = new ArrayList<VideolistStatusPojo>();
    private ReportIncidentActivity activity= null;
    private VideoView videoReportView;
    private ImageView iview_reportVideo;

    public VideoReportAdapter(Activity context, ArrayList<VideolistStatusPojo> videolist){

        this.videolist = videolist;
        this.mContext = context;

        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return videolist.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((FrameLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View itemView = mLayoutInflater.inflate(R.layout.video_pager_adapter, container, false);

        final ImageView imageView = (ImageView) itemView.findViewById(R.id.video_iView_pager);
        final ImageView iview_play = (ImageView) itemView.findViewById(R.id.video_iView_play);
        TextView tView_staus = (TextView) itemView.findViewById(R.id.tv_status_videos);
        final VideoView videoview_report = (VideoView) itemView.findViewById(R.id.report_Videoview);
        FrameLayout fl_video_report = (FrameLayout) itemView.findViewById(R.id.fl_video_report);

        String path = videolist.get(position).getPath();

        Bitmap thumb = ThumbnailUtils.createVideoThumbnail(path, MediaStore.Video.Thumbnails.MINI_KIND);
        imageView.setImageBitmap(thumb);

        if (videolist.get(position).getRequeststaus()) {
            System.out.println("yyy" + videolist.get(position).getStatus());
            if (videolist.get(position).getStatus()) {
                tView_staus.setVisibility(View.GONE);
            } else {
                tView_staus.setVisibility(View.VISIBLE);
                tView_staus.setOnClickListener((VideoReportActivity) mContext);
            }
        }
        fl_video_report.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                videoview_report.setVisibility(View.VISIBLE);
                videoReportView = videoview_report;
                iview_reportVideo = iview_play;
                if (videoview_report.isPlaying()) {
                    videoview_report.stopPlayback();
                    iview_play.setVisibility(View.VISIBLE);
                } else {
                    Uri videoUri = Uri.parse(videolist.get(position).getPath());
                    videoview_report.setVideoURI(videoUri);
                    //            videoview.setZOrderOnTop(true);
                    videoview_report.start();

                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            // Do something after 5s = 5000ms
                            imageView.setVisibility(View.GONE);

                        }
                    }, 3000);

                    iview_play.setVisibility(View.GONE);
                }
            }
        });

        if (videoview_report.isPlaying()){
            iview_play.setVisibility(View.GONE);
        } else {
            iview_play.setVisibility(View.VISIBLE);
        }


        container.addView(itemView);

        return itemView;
    }
    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((FrameLayout) object);
        notifyDataSetChanged();
    }

    public void onPageChanged() {
        if (videoReportView != null)
            videoReportView.stopPlayback();

        if (iview_reportVideo != null){
            iview_reportVideo.setVisibility(View.VISIBLE);
        }
    }
}
