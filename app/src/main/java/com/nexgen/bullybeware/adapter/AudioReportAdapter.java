package com.nexgen.bullybeware.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nexgen.bullybeware.R;
import com.nexgen.bullybeware.activity.AudioReportActivity;
import com.nexgen.bullybeware.activity.ReportIncidentActivity;
import com.nexgen.bullybeware.pojo.ImagelistStatusPojo;
import com.nexgen.bullybeware.util.ScaleBitmap;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by quepplin1 on 9/29/2016.
 */
public class AudioReportAdapter extends PagerAdapter {

    Activity mContext;
    LayoutInflater mLayoutInflater;
    private ArrayList<ImagelistStatusPojo> audiolist = new ArrayList<ImagelistStatusPojo>();
    private ReportIncidentActivity activity= null;
    MediaPlayer mp;
    private ImageView iView_audio_play;

    public AudioReportAdapter(Activity context, ArrayList<ImagelistStatusPojo> audiolist){

        this.audiolist = audiolist;
        this.mContext = context;

        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);


    }

    @Override
    public int getCount() {
        return audiolist.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View itemView = mLayoutInflater.inflate(R.layout.audio_image_adapter, container, false);

        mp = new MediaPlayer();

        final ImageView audio_play = (ImageView) itemView.findViewById(R.id.audioReport_play);
        TextView tv_status_audio = (TextView) itemView.findViewById(R.id.tv_status_audio);
        FrameLayout audioReport_layout = (FrameLayout) itemView.findViewById(R.id.audiosubmitt_layout);

        if (audiolist.get(position).getRequeststaus()) {
            System.out.println("yyy" + audiolist.get(position).getStatus());
            if (audiolist.get(position).getStatus()) {
                tv_status_audio.setVisibility(View.GONE);
            } else {
                tv_status_audio.setVisibility(View.VISIBLE);
                tv_status_audio.setOnClickListener((AudioReportActivity) mContext);
            }
        }
        audioReport_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("xxx click");

                iView_audio_play = audio_play;

                if (mp != null && mp.isPlaying()) {
                    mp.stop();
                    mp.reset();
                    audio_play.setVisibility(View.VISIBLE);
                } else {
                    try {
                        mp.setDataSource(audiolist.get(position).getPath());
                        audio_play.setVisibility(View.GONE);
                        mp.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                            @Override
                            public void onPrepared(MediaPlayer mediaPlayer) {
                                mp.start();
                            }
                        });
                        mp.prepareAsync();

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            public void onCompletion(MediaPlayer mp) {
                mp.reset();
                iView_audio_play.setVisibility(View.VISIBLE);
            }
        });

        container.addView(itemView);

        return itemView;
    }
    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }

    public void onPageChanged(){

        if (mp != null && mp.isPlaying()){
            mp.stop();
            mp.reset();
        }
        if (iView_audio_play != null)
            iView_audio_play.setVisibility(View.VISIBLE);
    }
}
