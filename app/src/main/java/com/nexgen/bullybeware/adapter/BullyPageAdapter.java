package com.nexgen.bullybeware.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.View;

import com.nexgen.bullybeware.fragment.FirstSignUpFragment;
import com.nexgen.bullybeware.fragment.SecondSignUpFragment;

import java.util.ArrayList;

/**
 * Created by quepplin1 on 9/7/2016.
 */
public class BullyPageAdapter extends FragmentStatePagerAdapter {

    private ArrayList<Fragment> mPageReferenceMap = new ArrayList<>();

    public BullyPageAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0: {
                FirstSignUpFragment fragment_first = new FirstSignUpFragment();
                mPageReferenceMap.add(position, fragment_first);
                return fragment_first;
            }
            case 1: {
                SecondSignUpFragment fragment_second = new SecondSignUpFragment();
                mPageReferenceMap.add(position, fragment_second);
                return fragment_second;
            }
        }

        return null;
    }

    @Override
    public int getCount() {
        return 2;
    }

    public Fragment getFragment(int key) {
        if (key < mPageReferenceMap.size())
            return mPageReferenceMap.get(key);
        else
            return null;
    }

    public void destroyItem(View container, int position, Object object) {
        super.destroyItem(container, position, object);
        mPageReferenceMap.remove(position);
    }
}
