package com.nexgen.bullybeware.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.nexgen.bullybeware.R;
import com.nexgen.bullybeware.activity.SavedAudioActivity;
import com.nexgen.bullybeware.activity.SavedImageActivity;
import com.nexgen.bullybeware.pojo.CheckboxStatusPojo;
import com.nexgen.bullybeware.util.ScaleBitmap;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by quepplin1 on 9/27/2016.
 */
public class SavedAudioAdapter extends PagerAdapter {

    Activity mContext;
    LayoutInflater mLayoutInflater;
    private ArrayList<CheckboxStatusPojo> audiolist1;
    private SavedAudioActivity activity= null;
    MediaPlayer m;
    private ImageView iView_play_audio;
    private Dialog dialog = null;
    private FrameLayout fl_audio;

    public void setDialog(Dialog dialog) {
        this.dialog = dialog;
    }

    public SavedAudioAdapter(Activity context, ArrayList<CheckboxStatusPojo> audiolist){
        this.audiolist1 = audiolist;
        this.mContext = context;

        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        m = new MediaPlayer();

    }
    @Override
    public int getCount() {
        return audiolist1.size()-1;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        View itemView = mLayoutInflater.inflate(R.layout.pager_item, container, false);

        final ImageView iView_play = (ImageView) itemView.findViewById(R.id.audio_play);
        fl_audio = (FrameLayout) itemView.findViewById(R.id.fl_audio);
        LinearLayout ll_main_audio = (LinearLayout) itemView.findViewById(R.id.ll_main_audio);

        fl_audio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                iView_play_audio = iView_play;
                try {
                    if (m != null && m.isPlaying()) {
                        m.stop();
                        m.reset();
                        iView_play.setVisibility(View.VISIBLE);
                    } else {
                        iView_play.setVisibility(View.GONE);
                        m.setDataSource(audiolist1.get(position).getPath());
                        m.prepare();
                        m.start();

                    }
                }
                catch (IOException e){

                }
            }
        });

        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                if(dialog!=null){
                    dialog.dismiss();
                    m.reset();
                }
            }
        });

        ll_main_audio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(dialog!=null){
                    dialog.dismiss();
                    m.reset();
                }
            }
        });

        m.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            public void onCompletion(MediaPlayer mp) {
                m.reset();
                iView_play_audio.setVisibility(View.VISIBLE);
            }
        });

        container.addView(itemView);

        return itemView;
    }
    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
        notifyDataSetChanged();
    }
    public void onPageChanged(){

        if (m != null && m.isPlaying()){
            m.stop();
            m.reset();
        }

        if (iView_play_audio != null)
            iView_play_audio.setVisibility(View.VISIBLE);
    }
}
