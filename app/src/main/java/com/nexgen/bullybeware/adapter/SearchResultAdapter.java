package com.nexgen.bullybeware.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nexgen.bullybeware.R;
import com.nexgen.bullybeware.activity.SafePlaceActivity;
import com.nexgen.bullybeware.activity.SearchByNameActivity;
import com.nexgen.bullybeware.pojo.SafeListDataPojo;
import com.nexgen.bullybeware.pojo.SearchFriendDataPojo;

import java.util.ArrayList;

/**
 * Created by yash on 7/19/2017.
 */

public class SearchResultAdapter extends RecyclerView.Adapter<SearchResultAdapter.ViewHolder> {
    private ArrayList<SearchFriendDataPojo> android;
    private Context context;

    public SearchResultAdapter(Context context, ArrayList<SearchFriendDataPojo> android) {
        this.android = android;
        this.context = context;
    }

    @Override
    public SearchResultAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.search_result_row, viewGroup, false);
        return new SearchResultAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(SearchResultAdapter.ViewHolder viewHolder, int i) {
        SearchFriendDataPojo data = android.get(i);
        if (data.getName()!= null || data.getLast_name() != null)
        {
            viewHolder.friendName.setText(data.getName()+" "+data.getLast_name());
        }
        if (data.getEmail()!= null )
        {
            viewHolder.friendEmail.setText(data.getEmail());
        }

        viewHolder.rootLayout.setTag(R.string.key_state, data.getName());
        viewHolder.rootLayout.setTag(R.string.key, data.getLast_name());
        viewHolder.rootLayout.setTag(R.string.key_delete, data.getMobile());
        viewHolder.rootLayout.setTag(R.string.delete_position, data.getEmail());
        viewHolder.rootLayout.setTag(R.string.edit_position, data.getThumbnail());
        viewHolder.rootLayout.setTag(R.string.dial_number, data.getId());
        viewHolder.rootLayout.setOnClickListener((SearchByNameActivity) context);

    }

    @Override
    public int getItemCount() {
        return android.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView friendName,friendEmail;
        private LinearLayout rootLayout;

        public ViewHolder(View view) {
            super(view);

            friendName = (TextView) view.findViewById(R.id.friendName);
            friendEmail = (TextView) view.findViewById(R.id.friendEmail);
            rootLayout = (LinearLayout) view.findViewById(R.id.rootLayout);
        }
    }
}