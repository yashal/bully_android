package com.nexgen.bullybeware.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nexgen.bullybeware.R;
import com.nexgen.bullybeware.pojo.RequestResponseDataPojo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ExpandableListAdapter extends RecyclerView.Adapter<ExpandableListAdapter.ViewHolder> {
    private ArrayList<RequestResponseDataPojo> android;
    private Context context;
    private static final float INITIAL_POSITION = 0.0f;
    private static final float ROTATED_POSITION = 180f;

    public ExpandableListAdapter(Context context, ArrayList<RequestResponseDataPojo> android) {
        this.android = android;
        this.context = context;
    }

    @Override
    public ExpandableListAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_group, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ExpandableListAdapter.ViewHolder viewHolder, int i) {
        final RequestResponseDataPojo data = android.get(i);
        viewHolder.lblListHeader.setText(data.getSituation());

        if (data.getResponse() != null)
        {
            viewHolder.lblListItem.setText(data.getResponse());
        }
        else
        {
            viewHolder.lblListItem.setText(context.getResources().getString(R.string.no_response_yet));
        }

        viewHolder.arrows.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (data.isExpanded) {
                    viewHolder.arrows.setRotation(INITIAL_POSITION);
                    viewHolder.lblListItem.setVisibility(View.GONE);
                    data.setExpanded(false);
                } else {
                    viewHolder.arrows.setRotation(ROTATED_POSITION);
                    viewHolder.lblListItem.setVisibility(View.VISIBLE);
                    data.setExpanded(true);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return android.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView lblListHeader, lblListItem;
        private ImageView arrows;
        private LinearLayout rootLayout;

        public ViewHolder(View view) {
            super(view);

            lblListHeader = (TextView) view.findViewById(R.id.lblListHeader);
            lblListItem = (TextView) view.findViewById(R.id.lblListItem);
            arrows = (ImageView) view.findViewById(R.id.arrows);
            rootLayout = (LinearLayout) view.findViewById(R.id.rootLayout);
        }
    }


}
