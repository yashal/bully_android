package com.nexgen.bullybeware.adapter;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.nexgen.bullybeware.R;
import com.nexgen.bullybeware.activity.MyRecordActivity;
import com.nexgen.bullybeware.activity.MySubmissionDetaileActivity;
import com.nexgen.bullybeware.fragment.MySubmissionsFragment;
import com.nexgen.bullybeware.model.RestClient;
import com.nexgen.bullybeware.pojo.DeleteSubmissionData;
import com.nexgen.bullybeware.pojo.GetSubmissionFilesPojo;
import com.nexgen.bullybeware.pojo.GetSubmissionObjectPojo;
import com.nexgen.bullybeware.util.BullyDialogs;

import java.io.IOException;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by quepplin1 on 9/28/2016.
 */
public class MyRecordAdapter extends RecyclerView.Adapter<MyRecordAdapter.ViewHolder> {

    private Activity context;
    private ArrayList<GetSubmissionObjectPojo> arraysubmissionlist;
    private ArrayList<GetSubmissionFilesPojo>arrayfilelist;
    private MySubmissionsFragment fragment;


    public MyRecordAdapter(Activity context,ArrayList<GetSubmissionObjectPojo> arraysubmissionlist,MySubmissionsFragment fragment){

        this.context = context;
        this.arraysubmissionlist = arraysubmissionlist;
        this.fragment = fragment;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.myrecord_layout, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final GetSubmissionObjectPojo data = arraysubmissionlist.get(position);

            holder.record_title.setText(data.getIncident());

            holder.victim_name.setText(data.getVictim());

        String date = data.getDate();
        String time = data.getTime();

        String str = date;
        String[] splited = str.split("-");

        String split_one= splited[0];
        String split_second= splited[1];
        String split_three= splited[2];

        final String date_time = new StringBuilder().append(split_second).append("/")
                .append(split_three).append("/").append(split_one) +", "+time;


        holder.record_date.setText(new StringBuilder().append(split_second).append("/")
                .append(split_three).append("/").append(split_one) +", "+ time);


        if (data.getType().equalsIgnoreCase("2")){
            holder.record_icon.setVisibility(View.GONE);
            holder.record_video_thumbnail.setVisibility(View.GONE);
            holder.record_audio.setVisibility(View.VISIBLE);
        } else if (data.getType().equalsIgnoreCase("1")){
            holder.record_audio.setVisibility(View.GONE);
            holder.record_video_thumbnail.setVisibility(View.GONE);
            holder.record_icon.setVisibility(View.VISIBLE);

            if (data.getFiles().size() > 0) {
                Glide.with(context).load(data.getFiles().get(0).getThumbnail())
                        .thumbnail(0.5f)
                        .crossFade()
                        .placeholder(R.mipmap.ic_launcher)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(holder.record_icon);
            } else {
                holder.record_icon.setImageResource(R.mipmap.ic_launcher);
            }
        } else if (data.getType().equalsIgnoreCase("3")){
            holder.record_icon.setVisibility(View.GONE);
            holder.record_audio.setVisibility(View.GONE);
            holder.record_video_thumbnail.setVisibility(View.VISIBLE);

            if (data.getFiles().size() > 0) {
                Glide.with(context).load(data.getFiles().get(0).getThumbnail())
                        .thumbnail(0.5f)
                        .crossFade()
                        .placeholder(R.mipmap.ic_launcher)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(holder.record_video_thumbnail);
            } else {
                holder.record_video_thumbnail.setImageResource(R.mipmap.ic_launcher);
            }
        }
        else if (data.getType().equalsIgnoreCase("4")){
            holder.record_icon.setVisibility(View.GONE);
            holder.record_audio.setVisibility(View.GONE);
            holder.record_video_thumbnail.setVisibility(View.INVISIBLE);
        }
        holder.submission_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fragment.DeleteSubmissiondialog(arraysubmissionlist.get(position).getId(),position);
            }
        });

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, MySubmissionDetaileActivity.class);
                Bundle args = new Bundle();
                if (arraysubmissionlist.get(position).getFiles().size() > 0){
                    arrayfilelist = arraysubmissionlist.get(position).getFiles();
                    args.putSerializable("file",(Serializable)arrayfilelist);
                    intent.putExtra("BUNDLE",args);
                } else {
                    intent.putExtra("file","1");
                }

                intent.putExtra("type",arraysubmissionlist.get(position).getType());
                intent.putExtra("incident",arraysubmissionlist.get(position).getIncident());
                intent.putExtra("title",arraysubmissionlist.get(position).getSubject());
                intent.putExtra("bully_name",arraysubmissionlist.get(position).getBully_name());
                intent.putExtra("date",date_time);
                intent.putExtra("Victim_name",arraysubmissionlist.get(position).getVictim());
                intent.putExtra("nearest_adult",arraysubmissionlist.get(position).getAdult_name());
                intent.putExtra("description",arraysubmissionlist.get(position).getDescription());

                context.startActivity(intent);
            }
        });
    }



    @Override
    public int getItemCount() {
        return arraysubmissionlist.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        private TextView record_title,record_date,victim_name;
        private ImageView record_icon,record_video_thumbnail,submission_delete;
        private FrameLayout record_audio;

        public ViewHolder(View itemView) {
            super(itemView);
            record_title = (TextView) itemView.findViewById(R.id.record_incident_title);
            record_date = (TextView) itemView.findViewById(R.id.record_incident_date);
            victim_name = (TextView) itemView.findViewById(R.id.record_victim_name);

            submission_delete = (ImageView) itemView.findViewById(R.id.submission_delete);

            record_icon = (ImageView) itemView.findViewById(R.id.record_iView_icon);
            record_audio = (FrameLayout) itemView.findViewById(R.id.record_audio);
            record_video_thumbnail = (ImageView) itemView.findViewById(R.id.record_video_thumbnail);
        }
    }
}
