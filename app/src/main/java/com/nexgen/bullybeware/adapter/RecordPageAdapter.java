package com.nexgen.bullybeware.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.View;
import com.nexgen.bullybeware.fragment.MyRecordingsFragment;
import com.nexgen.bullybeware.fragment.MySubmissionsFragment;


import java.util.ArrayList;

/**
 * Created by quepplin1 on 9/28/2016.
 */
public class RecordPageAdapter extends FragmentStatePagerAdapter {

    private ArrayList<Fragment> mPageReferenceMap = new ArrayList<>();

    public RecordPageAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0: {
                MySubmissionsFragment fragment_submission = new MySubmissionsFragment();
                mPageReferenceMap.add(position, fragment_submission);
                return fragment_submission;
            }
            case 1: {
                MyRecordingsFragment fragment_recording = new MyRecordingsFragment();
                mPageReferenceMap.add(position, fragment_recording);
                return fragment_recording;
            }
        }

        return null;
    }

    @Override
    public int getCount() {
        return 2;
    }
    public void destroyItem(View container, int position, Object object) {
        super.destroyItem(container, position, object);
        mPageReferenceMap.remove(position);
    }
}
