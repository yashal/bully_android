package com.nexgen.bullybeware.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nexgen.bullybeware.R;
import com.nexgen.bullybeware.activity.AudioRecordActivity;
import com.nexgen.bullybeware.activity.CaptureImageActivity;
import com.nexgen.bullybeware.activity.SavedImageActivity;
import com.nexgen.bullybeware.pojo.CheckboxStatusPojo;
import com.nexgen.bullybeware.util.ScaleBitmap;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by quepplin1 on 9/27/2016.
 */
public class AudioGridAdapter  extends BaseAdapter {

    private Activity mcontext;
    private ArrayList<CheckboxStatusPojo> audiostatus;
    private Dialog dialogLogout;
    private int done_count = 1;
    private String imageUri = "android.resource://com.nexgen.bullybeware/" + R.mipmap.record_audio;
    private SavedAudioAdapter audioAdapter;
    private MediaPlayer m;

    public AudioGridAdapter(Activity context,ArrayList<CheckboxStatusPojo> audiostatus) {

        this.mcontext = context;
        this.audiostatus = audiostatus;

    }

    @Override
    public int getCount() {
        return audiostatus.size();

    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {

        final boolean isLastItem = (i == (audiostatus.size() - 1));

        if (view == null) {
            LayoutInflater mInflater = (LayoutInflater) mcontext
                    .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            view = mInflater.inflate(R.layout.audio_grid_row, null);
        }
        ImageView iView_saved = (ImageView) view.findViewById(R.id.iView_saved);
        final ImageView checkbox_unselected = (ImageView) view.findViewById(R.id.checkbox_unselected);
        final ImageView checkbox_selected = (ImageView) view.findViewById(R.id.checkbox_selected);
        FrameLayout framelayout = (FrameLayout) view.findViewById(R.id.framelayout);


        if(audiostatus.get(i).getStatus()){
            checkbox_selected.setVisibility(View.VISIBLE);
            checkbox_unselected.setVisibility(View.GONE);
        }else{
            checkbox_selected.setVisibility(View.GONE);
            checkbox_unselected.setVisibility(View.VISIBLE);
        }

        View.OnClickListener clickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int selectCount = 0;
                for(CheckboxStatusPojo statusPojo : audiostatus){
                    if(statusPojo.getStatus())
                        selectCount++;
                }
                if(audiostatus.get(i).getStatus()){
                    if(selectCount!=1) {
                        checkbox_unselected.setVisibility(View.VISIBLE);
                        checkbox_selected.setVisibility(View.GONE);
                        audiostatus.get(i).setStatus(false);
                    }
                }
                else{
                    checkbox_selected.setVisibility(View.VISIBLE);
                    checkbox_unselected.setVisibility(View.GONE);
                    audiostatus.get(i).setStatus(true);
                }
            }
        };

        checkbox_unselected.setOnClickListener(clickListener);

        checkbox_selected.setOnClickListener(clickListener);

        if (isLastItem) {

            iView_saved.setImageURI(Uri.parse(imageUri));
                /*holder.iView_saved.getLayoutParams().width = 120;
                holder.iView_saved.getLayoutParams().height = 120;*/
            checkbox_unselected.setVisibility(View.GONE);
            framelayout.setBackgroundColor(Color.TRANSPARENT);
    //        iView_saved.setLayoutParams(layoutParams);


        } else {
            try {
                iView_saved.setImageResource(R.mipmap.mic);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        framelayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isLastItem) {
                    done_count++;
                    Intent intent = new Intent(mcontext, AudioRecordActivity.class);
                    intent.putExtra("for", "result");
                    mcontext.startActivityForResult(intent, 3);

                } else {
                    showAudioDialog(audiostatus,i);
                }
            }
        });
        return view;
    }


    public void showAudioDialog(final ArrayList<CheckboxStatusPojo> audio, final int pos) {
        dialogLogout = new Dialog(mcontext, android.R.style.Theme_Translucent_NoTitleBar);
        dialogLogout.setContentView(R.layout.image_dialog);
        dialogLogout.show();

        dialogLogout.setCanceledOnTouchOutside(true);

        final ViewPager mViewPager = (ViewPager) dialogLogout.findViewById(R.id.pager);

        audioAdapter = new SavedAudioAdapter(mcontext, audio);
        mViewPager.setAdapter(audioAdapter);
        audioAdapter.setDialog(dialogLogout);
        mViewPager.setCurrentItem(pos);
        audioAdapter.notifyDataSetChanged();

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                    audioAdapter.onPageChanged();


            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }

}
