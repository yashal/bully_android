package com.nexgen.bullybeware.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.nexgen.bullybeware.R;
import com.nexgen.bullybeware.activity.ReportIncidentActivity;
import com.nexgen.bullybeware.pojo.ImagelistStatusPojo;
import com.nexgen.bullybeware.util.ScaleBitmap;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by quepplin1 on 9/16/2016.
 */
public class CustomImageAdapter extends PagerAdapter {

    Activity mContext;
    LayoutInflater mLayoutInflater;
    private ArrayList<ImagelistStatusPojo> imagelist1 = new ArrayList<ImagelistStatusPojo>();
    private ReportIncidentActivity activity= null;

    public CustomImageAdapter(Activity context, ArrayList<ImagelistStatusPojo> imagelist){

        this.imagelist1 = imagelist;
        this.mContext = context;

        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {
        return imagelist1.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((FrameLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View itemView = mLayoutInflater.inflate(R.layout.image_adapter, container, false);

        ImageView imageView = (ImageView) itemView.findViewById(R.id.image_pager);
        TextView tView_staus = (TextView) itemView.findViewById(R.id.tv_status_image);

    /*    Bitmap myBitmap = BitmapFactory.decodeFile(imagelist1.get(position).getPath());
        float scalingFactor = getBitmapScalingFactor(imageView,myBitmap);
        Bitmap newBitmap = ScaleBitmap.ScaleBitmap(myBitmap, scalingFactor);
        imageView.setImageBitmap(newBitmap);*/

        Glide.with(mContext).load(imagelist1.get(position).getPath())
                .thumbnail(0.5f)
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(imageView);

        System.out.println("yyy re"+imagelist1.get(position).getRequeststaus());

        if (imagelist1.get(position).getRequeststaus()){
            System.out.println("yyy" +imagelist1.get(position).getStatus());
            if (imagelist1.get(position).getStatus())
            {
                tView_staus.setVisibility(View.GONE);
            }else {
                tView_staus.setVisibility(View.VISIBLE);
                tView_staus.setOnClickListener((ReportIncidentActivity) mContext);
            }
        }


        container.addView(itemView);

        return itemView;
    }
    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((FrameLayout) object);
    }
}
