package com.nexgen.bullybeware.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.nexgen.bullybeware.R;
import com.nexgen.bullybeware.pojo.StateListPojo;

import java.util.ArrayList;

/**
 * Created by umax desktop on 1/5/2017.
 */

public class  AgeListAdapter extends BaseAdapter {

    private ArrayList<String> arrayList;
    private Context context;

    public AgeListAdapter(Context context, ArrayList<String> arrayList){
        this.context = context;
        this.arrayList = arrayList;
    }

    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public Object getItem(int i) {
        return arrayList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        LayoutInflater mInflater = (LayoutInflater) context
                .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        view = mInflater.inflate(R.layout.state_row_layout, null);


        TextView tv_state = (TextView) view.findViewById(R.id.tv_state);

        if (arrayList.get(i).equalsIgnoreCase("age")){
            tv_state.setText(arrayList.get(i));
        } else {
            tv_state.setText(arrayList.get(i) + " years ");
        }

        return view;
    }
}
