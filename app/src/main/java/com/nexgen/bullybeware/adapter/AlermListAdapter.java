package com.nexgen.bullybeware.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.nexgen.bullybeware.R;
import com.nexgen.bullybeware.pojo.StateListPojo;

import java.util.ArrayList;

/**
 * Created by umax desktop on 5/25/2017.
 */

public class AlermListAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<String> arrayList;


    public AlermListAdapter(Context context, ArrayList<String> arrayList){
        this.context = context;
        this.arrayList = arrayList;
    }


    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return arrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater mInflater = (LayoutInflater) context
                .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        convertView = mInflater.inflate(R.layout.alerm_row_layout, null);

        TextView tv_state = (TextView) convertView.findViewById(R.id.tv_state);
        tv_state.setText(arrayList.get(position));

        return convertView;
    }
}
