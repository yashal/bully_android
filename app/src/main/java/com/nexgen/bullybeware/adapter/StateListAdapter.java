package com.nexgen.bullybeware.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.nexgen.bullybeware.R;
import com.nexgen.bullybeware.pojo.StateListPojo;
import com.nexgen.bullybeware.pojo.StatePojo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by quepplin1 on 8/12/2016.
 */

public class StateListAdapter extends BaseAdapter {

    private ArrayList<StateListPojo> arrayList;
    private Context context;

    public StateListAdapter(Context context, ArrayList<StateListPojo> arrayList){
        this.context = context;
        this.arrayList = arrayList;
    }

    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public Object getItem(int i) {
        return arrayList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater mInflater = (LayoutInflater) context
                .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        convertView = mInflater.inflate(R.layout.state_row_layout, null);

         TextView tv_state = (TextView) convertView.findViewById(R.id.tv_state);
         tv_state.setText(arrayList.get(position).getState_name());

         return convertView;

    }

}
