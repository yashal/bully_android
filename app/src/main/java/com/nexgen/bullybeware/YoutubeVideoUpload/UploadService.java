/*
 * Copyright (c) 2013 Google Inc.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package com.nexgen.bullybeware.YoutubeVideoUpload;

import android.app.Dialog;
import android.app.IntentService;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.Bundle;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.plus.Plus;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.client.util.ExponentialBackOff;
import com.google.api.services.youtube.YouTube;
import com.google.api.services.youtube.YouTubeScopes;
import com.google.common.collect.Lists;
import com.newventuresoftware.waveform.utils.TextUtils;
import com.nexgen.bullybeware.R;
import com.nexgen.bullybeware.activity.MainActivity;
import com.nexgen.bullybeware.activity.ReportIncidentActivity;
import com.nexgen.bullybeware.activity.VideoReportActivity;
import com.nexgen.bullybeware.database.DatabaseHandler;
import com.nexgen.bullybeware.model.RestClient;
import com.nexgen.bullybeware.pojo.ImageUploadPojo;
import com.nexgen.bullybeware.pojo.ImagelistStatusPojo;
import com.nexgen.bullybeware.pojo.IncidentSubmissionPojo;
import com.nexgen.bullybeware.pojo.VideolistStatusPojo;
import com.nexgen.bullybeware.util.BullyApplication;
import com.nexgen.bullybeware.util.BullyPreferences;
import com.nexgen.bullybeware.util.BullyToast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * @author Ibrahim Ulukaya <ulukaya@google.com>
 *         <p/>
 *         Intent service to handle uploads.
 */
public class UploadService extends Service {

    private String student_id,incident_id,date,title,bully_name,victim_name,nearest_adult,description,form_id,message,Submit_id,chosenAccountName,time,school_id;

    /**
     * defines how long we'll wait for a video to finish processing
     */
    private static final int PROCESSING_TIMEOUT_SEC = 60 * 20; // 20 minutes

    /**
     * controls how often to poll for video processing status
     */
    private static final int PROCESSING_POLL_INTERVAL_SEC = 20;

    private DatabaseHandler db;

    private static final String TAG = "UploadService";

    private ArrayList<VideolistStatusPojo> videostatus;
    private ArrayList<VideolistStatusPojo>remainingvideostatus= new ArrayList<VideolistStatusPojo>();

    private final IBinder mBinder = new MyBinder();

    /**
     * processing start time
     */
    private static long mStartTime;
    final HttpTransport transport = AndroidHttp.newCompatibleTransport();
    final JsonFactory jsonFactory = new GsonFactory();
    GoogleAccountCredential credential;
    private BullyPreferences preferences;
    private ImagelistStatusPojo statusPojo;
    String videoId;
    private boolean status_check;
    /**
     * tracks the number of upload attempts
     */
    private int mUploadAttemptCount;

    public UploadService() {

    }

    @Override
    public void onCreate() {
        super.onCreate();

        preferences = new BullyPreferences(this);

        db = new DatabaseHandler(this);

    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    private boolean flag = true;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        try {
            chosenAccountName = intent.getStringExtra("ACCOUNT_KEY");

            student_id = intent.getStringExtra("student_id");
            incident_id = intent.getStringExtra("incident_id");
            date = intent.getStringExtra("date");
            time = intent.getStringExtra("time");
            title = intent.getStringExtra("title");
            bully_name = intent.getStringExtra("bully_name");
            victim_name = intent.getStringExtra("victim_name");
            nearest_adult = intent.getStringExtra("nearest_adult");
            description = intent.getStringExtra("description");
            Submit_id = intent.getStringExtra("Submit_id");
            school_id = intent.getStringExtra("school_id");

            form_id = intent.getStringExtra("form_id");

            status_check = intent.getBooleanExtra("status_check", false);

            videostatus = (ArrayList<VideolistStatusPojo>)intent.getSerializableExtra("video_status");
        } catch (Exception e){
            e.printStackTrace();
        }

        if(flag){
            if (form_id == null){
                incidentFormApi(getApplicationContext(),student_id,incident_id,date,time,title,bully_name,victim_name,nearest_adult,description);
            } else {
                uploadVideo();
            }
            flag = false;
            new android.os.Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    flag = true;
                }
            },1000);
        }

        return super.onStartCommand(intent, flags, startId);
    }

    private static void zzz(int duration) throws InterruptedException {
        Log.d(TAG, String.format("Sleeping for [%d] ms ...", duration));
        Thread.sleep(duration);
        Log.d(TAG, String.format("Sleeping for [%d] ms ... done", duration));
    }

    private static boolean timeoutExpired(long startTime, int timeoutSeconds) {
        long currTime = System.currentTimeMillis();
        long elapsed = currTime - startTime;
        if (elapsed >= timeoutSeconds * 1000) {
            return true;
        } else {
            return false;
        }
    }

    private void incidentFormApi(final Context context, String student_id, String incident_id, String date,String time, String title, String bully_name, String victim_name, String nearest_adult, String description) {

        RestClient.get().postIncidentSubmissionNew(status_check, student_id, incident_id, date, time, title,bully_name, victim_name,
                nearest_adult, description, "3",school_id, new Callback<IncidentSubmissionPojo>() {
                    @Override
                    public void success(final IncidentSubmissionPojo basepojo, Response response) {
                        if (basepojo != null) {
                            if (basepojo.getStatus()) {

                                form_id = basepojo.getObject().getFrom_id();
                                message = basepojo.getMessage();
                                uploadVideo();

                            } else {
                                new android.os.Handler(Looper.getMainLooper()).post(new Runnable() {
                                    @Override
                                    public void run() {
       //                                 Toast.makeText(context, basepojo.getMessage(), Toast.LENGTH_LONG).show();
                                    }
                                });
                            }
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        System.out.println("xxx failed");

                        //                      customAlertDialog(context,"Report Incident", "Whoops! Looks like there is a problem with our server. Please try again soon!");
                    }
                });
    }

    private void uploadVideo(){

        new AsyncTask<Void,Void,Void>(){

            @Override
            protected void onPreExecute() {
                super.onPreExecute();

                credential =
                        GoogleAccountCredential.usingOAuth2(getApplicationContext(), Lists.newArrayList(Auth.SCOPES));
                credential.setSelectedAccountName(chosenAccountName);
                credential.setBackOff(new ExponentialBackOff());
            }

            @Override
            protected Void doInBackground(Void... voids) {
                String appName = getResources().getString(R.string.app_name);
                final YouTube youtube =
                        new YouTube.Builder(transport, jsonFactory, credential).setApplicationName(
                                appName).build();

                boolean allUploaded = false;
                for (int i = 0; i < videostatus.size(); i++) {

                    if (!videostatus.get(i).getStatus()) {

                        File f = new File(videostatus.get(i).getPath());
                        Uri uri1 = Uri.fromFile(f);

                        try {
                            videoId = tryUploadAndShowSelectableNotification(uri1, youtube, i);

                            if (videoId != null) {
                                videostatus.get(i).setStatus(true);
                                videostatus.get(i).setVideo_id(videoId);
                            } else {
                                videostatus.get(i).setStatus(false);
                            }
                        } catch (InterruptedException e) {
                            // ignore
                        }
                    }
                }
                for (int j=0;j< videostatus.size();j++){
                    if (!videostatus.get(j).getStatus()) {
                        allUploaded = false;
                        remainingvideostatus.add(videostatus.get(j));

                    }else {
                        allUploaded = true;
                    }
                }
                if (allUploaded){
                    for (int k=0;k< videostatus.size(); k++ ){
                        videoupload(videostatus.get(k).getVideo_id());

                    }
                } else {
                    System.out.println("yyy failed");
                    Intent intent = new Intent("Video_Service");
                    intent.putExtra("message","fail");
                    intent.putExtra("form_id",form_id);
                    intent.putExtra("Submit_id",Submit_id);
                    intent.putExtra("video_status",remainingvideostatus);
                    intent.putExtra("ACCOUNT_KEY",chosenAccountName);
                    LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
                }

                return null;
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private String tryUploadAndShowSelectableNotification(final Uri fileUri, final YouTube youtube,int pos) throws InterruptedException {
        statusPojo = new ImagelistStatusPojo();

        Log.i(TAG, String.format("Uploading [%s] to YouTube", fileUri.toString()));
        videoId = tryUploadFromFile(fileUri, youtube,pos);
        if (videoId != null) {
            Log.i(TAG, String.format("Uploaded video with ID: %s", videoId));

            tryShowSelectableNotification(videoId, youtube);
        }
        return videoId;
    }

    private void tryShowSelectableNotification(final String videoId, final YouTube youtube)
            throws InterruptedException {
        mStartTime = System.currentTimeMillis();
        boolean processed = false;
        while (!processed) {
            processed = ResumableUpload.checkIfProcessed(videoId, youtube);
            if (!processed) {
                // wait a while
                Log.d(TAG, String.format("Video [%s] is not processed yet, will retry after [%d] seconds",
                        videoId, PROCESSING_POLL_INTERVAL_SEC));
                if (!timeoutExpired(mStartTime, PROCESSING_TIMEOUT_SEC)) {
                    zzz(PROCESSING_POLL_INTERVAL_SEC * 1000);
                } else {
                    Log.d(TAG, String.format("Bailing out polling for processing status after [%d] seconds",
                            PROCESSING_TIMEOUT_SEC));
                    return;
                }
            } else {
                ResumableUpload.showSelectableNotification(videoId, getApplicationContext());
                return;
            }
        }
    }
    private String tryUploadFromFile(Uri mFileUri, YouTube youtube,int pos) {
        long fileSize;
        InputStream fileInputStream = null;
        String videoId = null;
        try {
            File file = new File(new URI(mFileUri.toString()));
            fileSize = file.length();
            fileInputStream = new FileInputStream(file);

            videoId = ResumableUpload.upload(youtube, fileInputStream, fileSize, mFileUri, file.getAbsolutePath(),title,description,getApplicationContext());
            if(!android.text.TextUtils.isEmpty(videoId)){

            }else{

            }

        } catch (FileNotFoundException e) {
            Log.e(getApplicationContext().toString(), e.getMessage());
        } catch (URISyntaxException e) {
            e.printStackTrace();
        } finally {
            try {
                fileInputStream.close();
            } catch (IOException e) {
                // ignore
            }
        }
        return videoId;
    }

    private void videoupload(String video_id){

        RestClient.get().uploadVideo(form_id, video_id, new Callback<ImageUploadPojo>() {
            @Override
            public void success(ImageUploadPojo imageUploadPojo, Response response) {

                System.out.println("xxx Sucess" );

                db.deleteList(Submit_id);
                db.deletefile(Submit_id);

                preferences.setSocial_id("");

                TrackEvent("Submission","video",preferences.getEmail());

                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();

            }

            @Override
            public void failure(RetrofitError error) {
                System.out.println("xxx fail" );
            }
        });
    }

    public void TrackEvent(String category,String action,String label) {

        Tracker t = ((BullyApplication) this.getApplication())
                .getTracker(BullyApplication.TrackerName.APP_TRACKER);
        t.send(new HitBuilders.EventBuilder()
                .setCategory(category)
                .setAction(action)
                .setLabel(label)
                .build());
    }


    public class MyBinder extends Binder {
        UploadService getService() {
            return UploadService.this;
        }
    }
}
