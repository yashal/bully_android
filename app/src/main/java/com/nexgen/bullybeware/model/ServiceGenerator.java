package com.nexgen.bullybeware.model;

import com.squareup.okhttp.OkHttpClient;

import java.util.concurrent.TimeUnit;

import retrofit.RestAdapter;
import retrofit.client.OkClient;

/**
 * Created by yesh on 10/14/2015.
 */
public class ServiceGenerator {

    // No need to instantiate this class.
    private ServiceGenerator() {
    }

    public static <S> S createService(Class<S> serviceClass, String baseUrl) {
        OkHttpClient okHttpClient = new OkHttpClient();
        okHttpClient.setWriteTimeout(10, TimeUnit.MINUTES);
        okHttpClient.setReadTimeout(10,TimeUnit.MINUTES);

        RestAdapter.Builder builder = new RestAdapter.Builder()
                .setEndpoint(baseUrl)
                .setClient(new OkClient(okHttpClient));
        builder.setLogLevel(RestAdapter.LogLevel.FULL);

        RestAdapter adapter = builder.build();

        return adapter.create(serviceClass);
    }
}
