package com.nexgen.bullybeware.model;

import com.google.gson.JsonElement;
import com.nexgen.bullybeware.pojo.AddFriendInfoPojo;
import com.nexgen.bullybeware.pojo.ImageUploadPojo;
import com.nexgen.bullybeware.pojo.SignUpPojo;
import com.nexgen.bullybeware.util.BullyConstants;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;
import retrofit.http.Query;
import retrofit.mime.MultipartTypedOutput;
import retrofit.mime.TypedFile;

/**
 * Created by quepplin1 on 8/26/2016.
 */
public interface FileUploadSer {

    public static final String BASE_URL = BullyConstants.BASE_URL;

    @Multipart
    @POST("/api.php?action=SignUp")
    void signup( @Part("login_type") String login_type, @Part("name") String name, @Part("school_id") String school_id, @Part("state") String state, @Part("city") String city, @Part("school_email") String school_email, @Part("last_name") String last_name,
                @Part("email") String email, @Part("password") String password, @Part("country") String country, @Part("profile_image") TypedFile profile_image,@Part("socialId") String socialId, @Part("age") String age, @Part("principal_name") String principal_name, @Part("mobile") String mobile, Callback<SignUpPojo> callback);


    @Multipart
    @POST("/api.php?action=ImageUpload")
    ImageUploadPojo imageUploadAsync(@Part("From_id") String From_id, @Part("image_url") TypedFile image_url);

    @Multipart
    @POST("/api.php?action=AudioUpload")
    void audioUpload(@Part("From_id") String From_id, @Part("audio_url") TypedFile audio_url, Callback<ImageUploadPojo> callback);

    @Multipart
    @POST("/api.php?action=AudioUpload")
    ImageUploadPojo audioUploadAsync(@Part("From_id") String From_id, @Part("audio_url") TypedFile audio_url);

    @POST("/api.php?action=ImageUpload")
    void multipleimageUpload(@Body MultipartTypedOutput attachments, Callback<ImageUploadPojo> callback);

    @Multipart
    @POST("/api.php?action=UpdateProfile")
    void updateProfile(@Part("StudentId") String StudentId, @Part("name") String name, @Part("school_id") String school_id, @Part("state") String state, @Part("city") String city, @Part("school_email") String school_email, @Part("last_name") String last_name,
                @Part("email") String email, @Part("con_name") String con_name, @Part("con_lastname") String con_lastname, @Part("con_relation") String con_relation,
                @Part("con_mobile") String con_mobile, @Part("country") String country, @Part("profile_image") TypedFile profile_image,@Part("principal_name") String principal_name, @Part("mobile") String mobile, Callback<SignUpPojo> callback);

/*    @Multipart
    @POST("/api.php?action=addFriend")
    void addfriend(@Part("student_id") String student_id, @Part("first_name") String first_name, @Part("last_name") String last_name, @Part("email") String email, @Part("mobile") String mobile, @Part("thumbnail") TypedFile thumbnail, Callback<AddFriendInfoPojo> callback);*/
}
