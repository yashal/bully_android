package com.nexgen.bullybeware.model;

import com.nexgen.bullybeware.pojo.AddFriendInfoPojo;
import com.nexgen.bullybeware.pojo.ChangePasswordPojo;
import com.nexgen.bullybeware.pojo.CityPojo;
import com.nexgen.bullybeware.pojo.DeleteLatLongPojo;
import com.nexgen.bullybeware.pojo.DeleteSelfharmPojo;
import com.nexgen.bullybeware.pojo.DeleteSubmissionData;
import com.nexgen.bullybeware.pojo.FbProfileInfoPojo;
import com.nexgen.bullybeware.pojo.ForgotPasswordPojo;
import com.nexgen.bullybeware.pojo.FriendListPojo;
import com.nexgen.bullybeware.pojo.GetLatLong;
import com.nexgen.bullybeware.pojo.GetProfilePojo;
import com.nexgen.bullybeware.pojo.GetSubmissionPojo;
import com.nexgen.bullybeware.pojo.GooglePlusProfilePojo;
import com.nexgen.bullybeware.pojo.ImageUploadPojo;
import com.nexgen.bullybeware.pojo.IncidentSubmissionPojo;
import com.nexgen.bullybeware.pojo.IncidentTypePojo;
import com.nexgen.bullybeware.pojo.LogInPojo;
import com.nexgen.bullybeware.pojo.ParentsEmailpojo;
import com.nexgen.bullybeware.pojo.QRCodeImagePojo;
import com.nexgen.bullybeware.pojo.RelationPojo;
import com.nexgen.bullybeware.pojo.RemoveFriendPojo;
import com.nexgen.bullybeware.pojo.RequestResponsePojo;
import com.nexgen.bullybeware.pojo.SafeListPojo;
import com.nexgen.bullybeware.pojo.SafePlaceEmailPojo;
import com.nexgen.bullybeware.pojo.SchoolPojo;
import com.nexgen.bullybeware.pojo.SearchFriendListPojo;
import com.nexgen.bullybeware.pojo.SecurityQuePojo;
import com.nexgen.bullybeware.pojo.SelfHarmListPojo;
import com.nexgen.bullybeware.pojo.SelfHarmPojo;
import com.nexgen.bullybeware.pojo.SendLatLong;
import com.nexgen.bullybeware.pojo.SignUpPojo;
import com.nexgen.bullybeware.pojo.StatePojo;

import retrofit.Callback;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;
import retrofit.http.Query;
import retrofit.mime.TypedFile;

/**
 * Created by quepplin1 on 8/11/2016.
 */

public interface Api {

    @GET("/api.php?action=Login")
    void login(@Query("email") String email, @Query("password") String password, Callback<LogInPojo> callback);

    @GET("/api.php?action=ForgotPassword")
    void forgotpassword(@Query("EmailId") String EmailId, Callback<ForgotPasswordPojo> callback);

    @GET("/api.php?action=ChangePassword")
    void otpChangepassword(@Query("StudentId") String StudentId,@Query("Otp")String Otp ,@Query("NewPassword") String NewPassword, Callback<ChangePasswordPojo> callback);

    @GET("/api.php?action=ChangePassword")
    void oldChangePassword(@Query("StudentId") String StudentId,@Query("OldPassword") String OldPassword , @Query("NewPassword") String NewPassword, Callback<ChangePasswordPojo> callback);

    @GET("/api.php?action=GetfbProfile")
    void getfbprofile(@Query("access_token") String access_token, @Query("socialId") String socialId, Callback<FbProfileInfoPojo> callback);

    @GET("/api.php?action=GetgmailProfile")
    void getgooglePlusprofile(@Query("access_token") String access_token, @Query("socialId") String socialId, Callback<GooglePlusProfilePojo> callback);

    @GET("/api.php?action=GetState")
    void getState(Callback<StatePojo> callback);

    @GET("/api.php?action=GetCity")
    void getCity(@Query("StateId") String StateId, Callback<CityPojo> callback);

    @GET("/api.php?action=GetSchools")
    void getSchool(@Query("school_code") String school_code, Callback<SchoolPojo> callback);

    @GET("/api.php?action=GetSchools")
    void getSchoolList(@Query("school_code") String school_code, Callback<SchoolPojo> callback);

    @GET("/api.php?action=GetRelations")
    void getRelation(Callback<RelationPojo> callback);

    @GET("/api.php?action=SecurityQuestions")
    void getSecurityQue(Callback<SecurityQuePojo> callback);

    @GET("/api.php?action=IncidentTypes")
    void getIncidentType(Callback<IncidentTypePojo> callback);

    @GET("/api.php?action=GetSubmission")
    void getSubmissionInfo(@Query("StudentId") String StudentId, Callback<GetSubmissionPojo> callback);

    @GET("/api.php?action=DeleteSubmission")
    void deleteSubmissiondata(@Query("SubmissionId") String SubmissionId, Callback<DeleteSubmissionData> callback);

    @GET("/api.php?action=GetProfile")
    void getProfileInfo(@Query("StudentId") String StudentId, Callback<GetProfilePojo> callback);

    /*@FormUrlEncoded
    @POST("/api.php?action=InsertSubmission")
    void postIncidentSubmission(@Field("user_id") String user_id, @Field("incident_id") String incident_id, @Field("date") String date, @Field("time") String time,
                                @Field("subject") String subject,@Field("bully_name") String bully_name, @Field("victim") String victim, @Field("adult_name") String adult_name, @Field("description")String description,
                               @Field("type") String type, @Field("school_id") String school_id, Callback<IncidentSubmissionPojo> callback);
*/
    @FormUrlEncoded
    @POST("/api.php?action=InsertSubmission")
    void postIncidentSubmissionNew(@Field("is_name_visible") boolean is_name_visible,@Field("user_id") String user_id, @Field("incident_id") String incident_id, @Field("date") String date, @Field("time") String time,
                                @Field("subject") String subject,@Field("bully_name") String bully_name, @Field("victim") String victim, @Field("adult_name") String adult_name, @Field("description")String description,
                                @Field("type") String type, @Field("school_id") String school_id, Callback<IncidentSubmissionPojo> callback);


    @FormUrlEncoded
    @POST("/api.php?action=VideoUpload")
    void uploadVideo(@Field("From_id") String From_id, @Field("video_url") String video_url,Callback<ImageUploadPojo> callback);

    @GET("/api.php?action=InsertSelfHarm")
    void selfharm(@Query("student_id") String student_id, @Query("school_id") String school_id, @Query("friend_name") String friend_name, @Query("harm_desc") String harm_desc, Callback<SelfHarmPojo> callback);

    @GET("/api.php?action=sendMailToParents")
    void sendmail(@Query("parents_email") String parents_email, @Query("age") String age, @Query("device_id") String device_id, @Query("device_type") String device_type, @Query("imei") String imei, Callback<ParentsEmailpojo> callback);

    @GET("/api.php?action=sendQrCode")
    void getQrImage(@Query("student_id") String student_id, Callback<QRCodeImagePojo> callback);

    @GET("/api.php?action=friendList")
    void getFriendlist(@Query("student_id") String student_id, Callback<FriendListPojo> callback);

    @GET("/api.php?action=removeFriend")
    void removeFriend(@Query("student_id") String student_id, @Query("id") String id, Callback<RemoveFriendPojo> callback);

    @GET("/api.php?action=addFriend")
    void addfriend(@Query("student_id") String student_id,@Query("friend_id") String friend_id, @Query("mobile") String mobile, Callback<AddFriendInfoPojo> callback);

    @GET("/api.php?action=deleteLatLong")
    void deletelatlong(@Query("student_id")String student_id, Callback<DeleteLatLongPojo> callback);

    @GET("/api.php?action=sendLatLong")
    void sendLatLong(@Query("student_id") String student_id,@Query("student_email")String student_email, @Query("latitude") String latitude, @Query("longitude")String longitude, Callback<SendLatLong>callback);

    @GET("/api.php?action=getLatLong")
    void getLatLong(@Query("student_email") String student_email, Callback<GetLatLong> callback);

    @GET("/api.php?action=getSelfHarmList")
    void getSelfharmList(@Query("student_id") String student_id, Callback<SelfHarmListPojo>callback);

    @GET("/api.php?action=delete_self_harm")
    void deleteSelfHarm(@Query("id") String id, Callback<DeleteSelfharmPojo> callback);

    @FormUrlEncoded
    @POST("/api.php?action=safe_place_list")
    void getSafePlace(@Field("school_id") String school_id, Callback<SafeListPojo> callback);

    @FormUrlEncoded
    @POST("/api.php?action=what_should_i_do_questions")
    void getResponseRequest(@Field("school_id") String school_id, Callback<RequestResponsePojo> callback);

    @FormUrlEncoded
    @POST("/api.php?action=safe_place_email")
    void sendSafePlaceEmail(@Field("name") String name, @Field("contact_name") String contact_name, @Field("email") String email,
                            @Field("content") String content, @Field("student_id") String student_id,
                            Callback<SafePlaceEmailPojo> callback);


    @FormUrlEncoded
    @POST("/api.php?action=search_friend_by_name")
    void searchFriendByName(@Field("name") String name,@Field("student_id") String student_id, Callback<SearchFriendListPojo> callback);

}