package com.nexgen.bullybeware.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.nexgen.bullybeware.R;
import com.nexgen.bullybeware.adapter.MyRecordingsAdapter;
import com.nexgen.bullybeware.database.DatabaseHandler;
import com.nexgen.bullybeware.pojo.GetSubmissionObjectPojo;
import com.nexgen.bullybeware.pojo.ReportSubmissionFilesPojo;
import com.nexgen.bullybeware.pojo.ReportSubmissionPojo;
import com.nexgen.bullybeware.util.BullyPreferences;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by quepplin1 on 9/28/2016.
 */
public class MyRecordingsFragment extends Fragment implements View.OnClickListener {

    private View view;
    private ImageView iView_filter_all,iView_filter_video,iView_filter_audio,iView_filter_photo, filter_iView_text;
    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private MyRecordingsAdapter adapter;
    private DatabaseHandler database;
    private ArrayList<ReportSubmissionPojo> arraysubmissionlist;
    private ArrayList<ReportSubmissionFilesPojo> arrayfilelist;
    private LinearLayout nodata_found;
    private boolean fragmentResume = false;
    private boolean fragmentVisible = false;
    private boolean fragmentOnCreated = false;
    private String user_id;
    private BullyPreferences preferences;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.recording_fragment, container, false);

        init();

        iView_filter_all.setOnClickListener(this);
        iView_filter_video.setOnClickListener(this);
        iView_filter_audio.setOnClickListener(this);
        iView_filter_photo.setOnClickListener(this);
        filter_iView_text.setOnClickListener(this);

        return view;

    }

    private void init(){
        iView_filter_all = (ImageView) view.findViewById(R.id.filter_iView_all);
        iView_filter_video = (ImageView) view.findViewById(R.id.filter_iView_videos);
        iView_filter_audio = (ImageView) view.findViewById(R.id.filter_iView_audio);
        iView_filter_photo = (ImageView) view.findViewById(R.id.filter_iView_photo);
        filter_iView_text = (ImageView) view.findViewById(R.id.filter_iView_text);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.recordings_recycler_view);

        nodata_found = (LinearLayout) view.findViewById(R.id.recording_ll_NoData);

        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);

        preferences = new BullyPreferences(getActivity());

        database = new DatabaseHandler(getActivity());

        arraysubmissionlist = database.getAllList();
        arrayfilelist = database.getAllFiles();

        for (int i=0; i< arraysubmissionlist.size();i++){
            ArrayList<ReportSubmissionFilesPojo> listFiles = new ArrayList<>();
            for(ReportSubmissionFilesPojo filesPojo : arrayfilelist){
                if (arraysubmissionlist.get(i).getSUBMIT_ID().equalsIgnoreCase(filesPojo.getSubmit_id())){
                    /*checkedimagelist.add(arrayfilelist.get(i).getImage_url());*/
                    listFiles.add(filesPojo);
                }
            }
            arraysubmissionlist.get(i).setFilelist(listFiles);
        }

        if (!fragmentResume && fragmentVisible &&  arraysubmissionlist != null && arraysubmissionlist.size() == 0) { //only when first time fragment is created
            if (arraysubmissionlist.size() > 0){
                nodata_found.setVisibility(View.GONE);
            } else {
                nodata_found.setVisibility(View.VISIBLE);
            }
        }

        ArrayList<ReportSubmissionPojo> arr_friendList = new ArrayList<>();

        for (int j=0; j< arraysubmissionlist.size();j++){

            if (arraysubmissionlist.get(j).getUSER_ID().equalsIgnoreCase(preferences.getStudentId())){

                arr_friendList.add(arraysubmissionlist.get(j));

                adapter = new MyRecordingsAdapter(getActivity(), arr_friendList);
                mRecyclerView.setAdapter(adapter);
                adapter.notifyDataSetChanged();
            }
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        if (isVisibleToUser && isResumed() && arraysubmissionlist != null && arraysubmissionlist.size() == 0){ // only at fragment screen is resumed
            fragmentResume = true;
            fragmentVisible = false;
            fragmentOnCreated = true;
            if (arraysubmissionlist.size() > 0){
                mRecyclerView.setVisibility(View.VISIBLE);
                nodata_found.setVisibility(View.GONE);
            } else {
                nodata_found.setVisibility(View.VISIBLE);
                mRecyclerView.setVisibility(View.GONE);
            }
        } else if (isVisibleToUser) {        // only at fragment onCreated
            fragmentResume = false;
            fragmentVisible = true;
            fragmentOnCreated = true;
        } else if (!isVisibleToUser && fragmentOnCreated) {// only when you go out of fragment screen
            fragmentVisible = false;
            fragmentResume = false;
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.filter_iView_all:

                iView_filter_all.setImageResource(R.mipmap.check_box_checked);
                iView_filter_video.setImageResource(R.mipmap.check_box_unchecked);
                iView_filter_audio.setImageResource(R.mipmap.check_box_unchecked);
                iView_filter_photo.setImageResource(R.mipmap.check_box_unchecked);
                filter_iView_text.setImageResource(R.mipmap.check_box_unchecked);

                adapter = new MyRecordingsAdapter(getActivity(), arraysubmissionlist);
                mRecyclerView.setAdapter(adapter);
                adapter.notifyDataSetChanged();

                break;
            case R.id.filter_iView_videos:

                iView_filter_all.setImageResource(R.mipmap.check_box_unchecked);
                iView_filter_video.setImageResource(R.mipmap.check_box_checked);
                iView_filter_audio.setImageResource(R.mipmap.check_box_unchecked);
                iView_filter_photo.setImageResource(R.mipmap.check_box_unchecked);
                filter_iView_text.setImageResource(R.mipmap.check_box_unchecked);

                ArrayList<ReportSubmissionPojo> arr_video = new ArrayList<>();

                for (int i=0;i<arraysubmissionlist.size();i++){
                    if (arraysubmissionlist.get(i).getFile_type().equalsIgnoreCase("3"))
                        arr_video.add(arraysubmissionlist.get(i));
                }
                adapter = new MyRecordingsAdapter(getActivity(), arr_video);
                mRecyclerView.setAdapter(adapter);
                adapter.notifyDataSetChanged();


                break;

            case R.id.filter_iView_audio:

                iView_filter_all.setImageResource(R.mipmap.check_box_unchecked);
                iView_filter_video.setImageResource(R.mipmap.check_box_unchecked);
                iView_filter_audio.setImageResource(R.mipmap.check_box_checked);
                iView_filter_photo.setImageResource(R.mipmap.check_box_unchecked);
                filter_iView_text.setImageResource(R.mipmap.check_box_unchecked);

                ArrayList<ReportSubmissionPojo> arr_audio = new ArrayList<>();
                for(int i =0; i< arraysubmissionlist.size(); i++)
                {
                    if(arraysubmissionlist.get(i).getFile_type().equalsIgnoreCase("2"))
                        arr_audio.add(arraysubmissionlist.get(i));
                }

                adapter = new MyRecordingsAdapter(getActivity(), arr_audio);
                mRecyclerView.setAdapter(adapter);
                adapter.notifyDataSetChanged();
                break;

            case R.id.filter_iView_photo:

                iView_filter_all.setImageResource(R.mipmap.check_box_unchecked);
                iView_filter_video.setImageResource(R.mipmap.check_box_unchecked);
                iView_filter_audio.setImageResource(R.mipmap.check_box_unchecked);
                iView_filter_photo.setImageResource(R.mipmap.check_box_checked);
                filter_iView_text.setImageResource(R.mipmap.check_box_unchecked);

                ArrayList<ReportSubmissionPojo> arr_image = new ArrayList<>();
                for(int i =0; i< arraysubmissionlist.size(); i++)
                {
                    if(arraysubmissionlist.get(i).getFile_type().equalsIgnoreCase("1"))
                        arr_image.add(arraysubmissionlist.get(i));
                }

                adapter = new MyRecordingsAdapter(getActivity(), arr_image);
                mRecyclerView.setAdapter(adapter);
                adapter.notifyDataSetChanged();
                break;

            case R.id.filter_iView_text:

                iView_filter_all.setImageResource(R.mipmap.check_box_unchecked);
                iView_filter_video.setImageResource(R.mipmap.check_box_unchecked);
                iView_filter_audio.setImageResource(R.mipmap.check_box_unchecked);
                iView_filter_photo.setImageResource(R.mipmap.check_box_unchecked);
                filter_iView_text.setImageResource(R.mipmap.check_box_checked);

                ArrayList<ReportSubmissionPojo> arr_text = new ArrayList<>();
                for(int i =0; i< arraysubmissionlist.size(); i++)
                {
                    if(arraysubmissionlist.get(i).getFile_type().equalsIgnoreCase("4"))
                        arr_text.add(arraysubmissionlist.get(i));
                }

                adapter = new MyRecordingsAdapter(getActivity(), arr_text);
                mRecyclerView.setAdapter(adapter);
                adapter.notifyDataSetChanged();
                break;
        }
    }
}
