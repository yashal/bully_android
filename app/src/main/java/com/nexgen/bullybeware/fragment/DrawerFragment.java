package com.nexgen.bullybeware.fragment;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.TypedArray;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.plus.People;
import com.google.android.gms.plus.Plus;
import com.google.api.services.youtube.YouTubeScopes;
import com.nexgen.bullybeware.R;
import com.nexgen.bullybeware.activity.AboutUsActivity;
import com.nexgen.bullybeware.activity.AudioReportActivity;
import com.nexgen.bullybeware.activity.BaseActivity;
import com.nexgen.bullybeware.activity.ChangePasswordActivity;
import com.nexgen.bullybeware.activity.ContactUsActivity;
import com.nexgen.bullybeware.activity.FriendListActivity;
import com.nexgen.bullybeware.activity.GeneralActivity;
import com.nexgen.bullybeware.activity.LoginActivity;
import com.nexgen.bullybeware.activity.MainActivity;
import com.nexgen.bullybeware.activity.MyProfileActivity;
import com.nexgen.bullybeware.activity.MyRecordActivity;
import com.nexgen.bullybeware.activity.QRCodeImageActivity;
import com.nexgen.bullybeware.activity.QRCodeScanActivity;
import com.nexgen.bullybeware.activity.ReportIncidentActivity;
import com.nexgen.bullybeware.activity.SafePlaceActivity;
import com.nexgen.bullybeware.activity.SelfHarmActivity;
import com.nexgen.bullybeware.activity.SelfHarmListActivity;
import com.nexgen.bullybeware.activity.SoundConfigActivity;
import com.nexgen.bullybeware.activity.TermsConditionsActivity;
import com.nexgen.bullybeware.activity.VideoReportActivity;
import com.nexgen.bullybeware.adapter.NavDrawerListAdapter;
import com.nexgen.bullybeware.model.NavDrawerItem;
import com.nexgen.bullybeware.pojo.ReportSubmissionPojo;
import com.nexgen.bullybeware.util.BullyConstants;
import com.nexgen.bullybeware.util.BullyPreferences;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import java.util.concurrent.ExecutionException;

/**
 * Created by quepplin1 on 8/29/2016.
 */
public class DrawerFragment extends Fragment implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, ResultCallback<People.LoadPeopleResult> {

    private DrawerLayout drawerlayout;
    private String[] navMenuTitles;
    private ArrayList<NavDrawerItem> navDrawerItems;
    private BullyPreferences preferences;
    private GoogleApiClient mGoogleApiClient;
    private ListView list_slidermenu;
    public NavDrawerListAdapter adapter;
    public int selectedPos = -1;
    public ActionBarDrawerToggle mDrawerToggle;
    private ImageView iView_cross;
    private Dialog dialogLogout;
    public static final String ACCOUNT_KEY = "accountName";
    private TextView tv_setup_menu;
    private LinearLayout ll_slide_menu;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.left_slide_list_layout, container, false);

        selectedPos = -1;

        preferences = new BullyPreferences(getActivity());

        // load slide menu items
        navMenuTitles = getResources().getStringArray(R.array.nav_drawer_items);

        navDrawerItems = new ArrayList<NavDrawerItem>();

        navDrawerItems.add(new NavDrawerItem(navMenuTitles[0]));
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[1]));
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[2]));
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[3]));
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[4]));
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[5]));
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[6]));
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[7]));
        navDrawerItems.add(new NavDrawerItem(navMenuTitles[8]));


        list_slidermenu = (ListView) v.findViewById(R.id.list_slidermenu);
        list_slidermenu.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                selectedPos = i;
                drawerlayout.closeDrawers();
            }
        });

        tv_setup_menu = (TextView) v.findViewById(R.id.setup_menu);

        tv_setup_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedPos = -1;
            }
        });

        ll_slide_menu = (LinearLayout) v.findViewById(R.id.ll_slide_menu);

        ll_slide_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedPos = -1;
            }
        });

        iView_cross = (ImageView) v.findViewById(R.id.iview_cross);

        iView_cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                selectedPos = -1;
                drawerlayout.closeDrawers();
            }
        });

        // setting the nav drawer list adapter
        adapter = new NavDrawerListAdapter(getActivity(),
                navDrawerItems);
        list_slidermenu.setAdapter(adapter);

        mGoogleApiClient = new GoogleApiClient.Builder(getActivity()).addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this).addApi(Plus.API, Plus.PlusOptions.builder().build())
                .addScope(Plus.SCOPE_PLUS_LOGIN)
                .addScope(new Scope(YouTubeScopes.YOUTUBE)).build();
        mGoogleApiClient.connect();
        return v;

    }

    @SuppressLint("NewApi")
    public void setUp(final DrawerLayout drawerlayout) {

        this.drawerlayout = drawerlayout;
        if (adapter != null)
            adapter.notifyDataSetChanged();

        mDrawerToggle = new ActionBarDrawerToggle(getActivity(), drawerlayout, R.string.open_drawer, R.string.close_drawer) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                getActivity().invalidateOptionsMenu();
            }

            @SuppressLint("NewApi")
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                switch (selectedPos) {

                    case 0:
                        Intent soundConfigIntent = new Intent(getActivity(), SoundConfigActivity.class);
                        startActivity(soundConfigIntent);
                        getActivity().overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
                        break;
                    case 1:
                        Intent intent = new Intent(getActivity(), MyProfileActivity.class);
                        startActivity(intent);
                        getActivity().overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
                        break;

                    case 2:
                        Intent aboutIntent = new Intent(getActivity(), GeneralActivity.class);
                        startActivity(aboutIntent);
                        getActivity().overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
                        break;

                    case 3:
                        Intent selfharm = new Intent(getActivity(), SelfHarmListActivity.class);
                        startActivity(selfharm);
                        getActivity().overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
                        break;

                    case 4:
                        Intent safePlace = new Intent(getActivity(), SafePlaceActivity.class);
                        startActivity(safePlace);
                        getActivity().overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
                        Toast.makeText(getActivity(), "Clicked on safe friend list", Toast.LENGTH_SHORT).show();
                        break;

                    case 5:
                        Intent activityIntent = new Intent(getActivity(), FriendListActivity.class);
                        activityIntent.putExtra("from", "follow_me");
                        startActivity(activityIntent);
                        getActivity().overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
                        break;

                    case 6:
                        Intent scanQrcode = new Intent(getActivity(), FriendListActivity.class);
                        scanQrcode.putExtra("from", "main");
                        startActivity(scanQrcode);
                        getActivity().overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);

                        break;

                    case 7:
                        Intent changeIntent = new Intent(getActivity(), ChangePasswordActivity.class);
                        changeIntent.putExtra("from", "main");
                        startActivity(changeIntent);
                        getActivity().overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
                        break;

                    case 8:
                        LogoutDialog();
                        break;
                }
            }
        };

        drawerlayout.setDrawerListener(mDrawerToggle);
        drawerlayout.post(new Runnable() {

            @Override
            public void run() {
                mDrawerToggle.syncState();
            }
        });
    }

    private void LogoutDialog() {
        dialogLogout = new Dialog(getActivity(), android.R.style.Theme_Translucent_NoTitleBar);
        dialogLogout.setContentView(R.layout.logout_dialog);
        dialogLogout.show();
        TextView dialog_header = (TextView) dialogLogout.findViewById(R.id.logout_dialog_header);
        dialog_header.setVisibility(View.VISIBLE);
        dialog_header.setText("Log Out");
        LinearLayout ll_submit = (LinearLayout) dialogLogout.findViewById(R.id.ll_dialog_submit);
        LinearLayout ll_cancel = (LinearLayout) dialogLogout.findViewById(R.id.ll_dialog_cancel);
        ImageView iView_cross = (ImageView) dialogLogout.findViewById(R.id.dialog_header_cross);
        TextView tv_submit = (TextView) dialogLogout.findViewById(R.id.dialog_submit);
        tv_submit.setText("YES");
        TextView dialog_text = (TextView) dialogLogout.findViewById(R.id.dialog_text);
        dialog_text.setVisibility(View.VISIBLE);
        dialog_text.setText("Are you sure you want to logout?");

        ll_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (preferences.getString(ACCOUNT_KEY) != null) {
                    googlePlusLogout();
                } else if (preferences.getLOGGEDINFROM().equalsIgnoreCase("fb")) {
                    FacebookSdk.sdkInitialize(getActivity().getApplicationContext());
                    LoginManager.getInstance().logOut();
                }

                preferences.clearSharedPref();

                preferences.setStudentId("");
                preferences.setFname("");
                preferences.setLastName("");
                preferences.setEmail("");
                ((BaseActivity) getActivity()).stopClickListner();
                ((MainActivity) getActivity()).sinchStop();

                Intent intent = new Intent(getActivity(), LoginActivity.class);
                startActivity(intent);
                getActivity().finish();
                getActivity().overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
                Toast.makeText(getActivity(), "You are successfully logged out", Toast.LENGTH_SHORT).show();

            }
        });
        iView_cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogLogout.dismiss();
            }
        });

        ll_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogLogout.dismiss();
            }
        });
    }

    private void googlePlusLogout() {
        if (mGoogleApiClient.isConnected()) {
            Plus.AccountApi.clearDefaultAccount(mGoogleApiClient);
            mGoogleApiClient.disconnect();
            mGoogleApiClient.connect();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (dialogLogout != null) {
            if (dialogLogout.isShowing()) {
                dialogLogout.dismiss();
            }
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        Plus.PeopleApi.loadVisible(mGoogleApiClient, null).setResultCallback(this);
    }

    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onResult(@NonNull People.LoadPeopleResult loadPeopleResult) {

    }
}
