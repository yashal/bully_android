package com.nexgen.bullybeware.fragment;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.nexgen.bullybeware.R;
import com.nexgen.bullybeware.activity.BaseActivity;
import com.nexgen.bullybeware.activity.LoginActivity;
import com.nexgen.bullybeware.activity.MainActivity;
import com.nexgen.bullybeware.activity.MyProfileActivity;
import com.nexgen.bullybeware.activity.SignUpActivity;
import com.nexgen.bullybeware.activity.TermsConditionsActivity;
import com.nexgen.bullybeware.adapter.CityListAdapter;
import com.nexgen.bullybeware.adapter.RelationListAdapter;
import com.nexgen.bullybeware.adapter.SchoolListAdapter;
import com.nexgen.bullybeware.adapter.SecurityQueListAdapter;
import com.nexgen.bullybeware.adapter.StateListAdapter;
import com.nexgen.bullybeware.model.FileUploadSer;
import com.nexgen.bullybeware.model.RestClient;
import com.nexgen.bullybeware.model.ServiceGenerator;
import com.nexgen.bullybeware.pojo.CityListPojo;
import com.nexgen.bullybeware.pojo.CityPojo;
import com.nexgen.bullybeware.pojo.RelationListPojo;
import com.nexgen.bullybeware.pojo.RelationPojo;
import com.nexgen.bullybeware.pojo.SchoolListpojo;
import com.nexgen.bullybeware.pojo.SchoolPojo;
import com.nexgen.bullybeware.pojo.SecurityQueListPojo;
import com.nexgen.bullybeware.pojo.SecurityQuePojo;
import com.nexgen.bullybeware.pojo.SignUpPojo;
import com.nexgen.bullybeware.pojo.StateListPojo;
import com.nexgen.bullybeware.pojo.StatePojo;
import com.nexgen.bullybeware.util.BullyApplication;
import com.nexgen.bullybeware.util.BullyConstants;
import com.nexgen.bullybeware.util.BullyDialogs;
import com.nexgen.bullybeware.util.BullyPreferences;
import com.nexgen.bullybeware.util.BullyToast;
import com.nexgen.bullybeware.util.ConnectionDetector;
import com.nexgen.bullybeware.util.UsPhoneNumberFormatter;

import java.io.File;
import java.lang.ref.WeakReference;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.TreeMap;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedFile;

/**
 * Created by quepplin1 on 9/7/2016.
 */
public class SecondSignUpFragment extends Fragment {

    private View view;
    private EditText edtxt_schoolemail;
    private ImageView school_search,iView_terms;
    private BullyPreferences preference;
    private Spinner state_spinner, city_spinner;
    private LinearLayout state_ll_spinner, city_ll_spineer;
    private EditText edtxt_school_id,school_addres,principle_name,school_number;
    private String school_id,school_name,school_address,school_principle_name,school_email,school_mobile,school_id_info;

    private ArrayList<CityListPojo> arrayCityList;
    private ArrayList<StateListPojo> arrayStateList;

    private StateListAdapter state_adapter;
    private CityListAdapter city_adapter;

    private String state_id, city_id;
    private TextView tv_err_state,tv_err_city,tv_err_school_email,tv_err_principal_name;
    private static String image,fname,lname,email,password,age;

    private TextInputLayout inputlayoutschoolemail;

    // flag for Internet connection status
    Boolean isInternetPresent = false;

    // Connection detector class
    ConnectionDetector cd;
    private TextView lblContinue,tv_termsCondition;
    private String checkbox_status = "check";
    private boolean status = false;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.second_signup_fragment, container, false);

        init();
        setStateData();
        setOnClickListner();

        edtxt_school_id.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                // TODO Auto-generated method stub
                if (count == 0)
                {
                    enableEdittext();
                }
                    status = false;
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {

                // TODO Auto-generated method stub
            }
        });
        return view;
    }

    private void init(){
        edtxt_schoolemail = (EditText) view.findViewById(R.id.signup_school_email);

        lblContinue = (TextView)view.findViewById(R.id.signup_tv_submit);

        tv_err_state = (TextView) view.findViewById(R.id.tv_err_state);
        tv_err_city  = (TextView) view.findViewById(R.id.tv_err_city);
        tv_err_school_email = (TextView) view.findViewById(R.id.tv_err_school_email);
        tv_termsCondition = (TextView) view.findViewById(R.id.tv_terms_condition);

        tv_err_principal_name = (TextView) view.findViewById(R.id.tv_err_principle_name);

        state_spinner = (Spinner) view.findViewById(R.id.state_spinner);
        city_spinner = (Spinner) view.findViewById(R.id.city_spinner);

        state_ll_spinner = (LinearLayout) view.findViewById(R.id.state_ll_spinner);
        city_ll_spineer = (LinearLayout) view.findViewById(R.id.city_ll_spinner);

        edtxt_school_id = (EditText) view.findViewById(R.id.signup_school_id);
        school_addres = (EditText) view.findViewById(R.id.signup_school_add);
        principle_name = (EditText) view.findViewById(R.id.signup_principle_name);
        school_number = (EditText) view.findViewById(R.id.signup_edtxt_telephone);
        UsPhoneNumberFormatter addLineNumberFormatter = new UsPhoneNumberFormatter(
                new WeakReference<EditText>(school_number));
        school_number.addTextChangedListener(addLineNumberFormatter);
//        school_number.addTextChangedListener(new PhoneNumberTextWatcher(school_number));


        inputlayoutschoolemail = (TextInputLayout) view.findViewById(R.id.input_layout_school_email);
        school_search = (ImageView) view.findViewById(R.id.school_search);

        iView_terms = (ImageView) view.findViewById(R.id.terms_checkbox);

        cd = new ConnectionDetector(getActivity().getApplicationContext());
        // get Internet status
        isInternetPresent = cd.isConnectingToInternet();

        arrayCityList = new ArrayList<CityListPojo>();
        arrayStateList = new ArrayList<StateListPojo>();

        preference = new BullyPreferences(getActivity());

        edtxt_schoolemail.addTextChangedListener(new MyTextWatcher(edtxt_schoolemail));
        principle_name.addTextChangedListener(new MyTextWatcher(principle_name));
    }

    private void getData(){

        preference = new BullyPreferences(getActivity());
        image = preference.getImage();
        fname = preference.getFname();
        lname = preference.getLastName();
        email = preference.getEmail();
        password = preference.getPassword();
        age = preference.getUser_age();

    }

    private void setOnClickListner(){

        lblContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((BaseActivity)getActivity()).hideSoftKeyboard();
                tv_err_state.setVisibility(View.GONE);
                tv_err_city.setVisibility(View.GONE);
                tv_err_principal_name.setVisibility(View.GONE);
                tv_err_school_email.setVisibility(View.GONE);
                submitform();
            }
        });

        state_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                if (i == 0) {

                   LinearLayout parent = (LinearLayout) view;

                  ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.song_name_text_color));

                    state_id = "0";

                    CityListPojo clp = new CityListPojo();
                    clp.setCity_name("Select your city");
                    arrayCityList.clear();
                    arrayCityList.add(0, clp);
                    city_adapter = new CityListAdapter(getActivity(), arrayCityList);
                    city_spinner.setAdapter(city_adapter);

                } else {
                    state_id = arrayStateList.get(i).getId();

                    System.out.println("id" + state_id);

                    LinearLayout parent = (LinearLayout) view;

                    ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));
                    tv_err_state.setVisibility(View.GONE);

                    setCityData();

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        city_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i == 0) {
                     city_id = "city";

                    LinearLayout parent = (LinearLayout) view;

               ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.song_name_text_color));
                } else {
                    city_id = arrayCityList.get(i).getId();

                    tv_err_city.setVisibility(View.GONE);

                    LinearLayout parent = (LinearLayout) view;

                    ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        school_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setSchoolData();
            }
        });

        edtxt_school_id.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    setSchoolData();
                    return true;
                }
                return false;
            }
        });

        iView_terms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkbox_status.equalsIgnoreCase("check")){
                    iView_terms.setImageResource(R.mipmap.check_box_unchecked);
                    checkbox_status = "uncheck";
                } else {
                    iView_terms.setImageResource(R.mipmap.check_box_checked);
                    checkbox_status = "check";
                }
            }
        });

        tv_termsCondition.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), TermsConditionsActivity.class);
                startActivity(intent);
            }
        });
    }

    public void submitform(){
        if (state_id.equalsIgnoreCase("0")){
            tv_err_state.setVisibility(View.VISIBLE);
        } else if (city_id.equalsIgnoreCase("city")){
            tv_err_city.setVisibility(View.VISIBLE);
        } else if (principle_name.getText().toString().isEmpty()){
            tv_err_principal_name.setVisibility(View.VISIBLE);
        } else if (edtxt_school_id.getText().toString().length() == 0 & edtxt_schoolemail.getText().toString().length() == 0){

                tv_err_school_email.setText(getResources().getString(R.string.err_school_email));
                tv_err_school_email.setVisibility(View.VISIBLE);

        }else if (edtxt_schoolemail.getText().toString().length() > 0 && !((BaseActivity)getActivity()).checkValidEmail(edtxt_schoolemail.getText().toString())){
            tv_err_school_email.setText(getResources().getString(R.string.err_valid_school_email));
            tv_err_school_email.setVisibility(View.VISIBLE);
        }else if(checkbox_status.equalsIgnoreCase("uncheck")){
            customAlertDialog("Registration Failed","Please accept terms and conditions");
        }
        else {
            if (edtxt_school_id.getText().toString().length() > 0 && !status)
            {
                customAlertDialog("School Information","Please search with valid School Code.");
            }
            else {
                doSignUp();
            }
        }
    }

    public void TrackEvent(String category,String action,String label) {

        Tracker t = ((BullyApplication) getActivity().getApplication())
                .getTracker(BullyApplication.TrackerName.APP_TRACKER);
        t.send(new HitBuilders.EventBuilder()
                .setCategory(category)
                .setAction(action)
                .setLabel(label)
                .build());
    }

    private void doSignUp() {
        md5(password);
        isInternetPresent = cd.isConnectingToInternet();
        if (isInternetPresent) {
            final ProgressDialog d = BullyDialogs.showLoading(getActivity());
            d.setCanceledOnTouchOutside(false);

            FileUploadSer service = ServiceGenerator.createService(FileUploadSer.class, FileUploadSer.BASE_URL);
            TypedFile typedFile = null;
            if (image.length() > 0)
                typedFile = new TypedFile("multipart/form-data", new File(image));

            String social_id = preference.getSocial_id();

            String principle = principle_name.getText().toString();

            service.signup("InApp", fname, school_id, state_id, city_id,edtxt_schoolemail.getText().toString(), lname, email.trim(), password.trim(), "1", typedFile,social_id,age,principle,school_number.getText().toString(), new Callback<SignUpPojo>() {
                        @Override
                        public void success(SignUpPojo signUpPojo, Response response) {
                            if (signUpPojo != null) {
                                if (signUpPojo.getStatus()) {
                                    BullyToast.showToast(getActivity(),signUpPojo.getMessage());
                                    System.out.println("xxxx success");

                                    preference.setStudentId(signUpPojo.getObject().getStudent_id());

                                    preference.setSchool_id(signUpPojo.getObject().getSchool_id());

                                    preference.setSchool_email(signUpPojo.getObject().getSchool_email());

                                    TrackEvent("Signup","signup_form",signUpPojo.getObject().getEmail());

                                    Intent intent = new Intent(getActivity(), MainActivity.class);
                                    startActivity(intent);
                                    getActivity().finish();
                                    getActivity().overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);

                                } else {
                                    customAlertDialog("Registration Failed",signUpPojo.getMessage());
                                }

                            } else {
                                customAlertDialog("",BullyConstants.REGISTRATION_FAILED);
                            }
                            d.dismiss();
                        }

                        @Override
                        public void failure(RetrofitError error) {
                            System.out.println("xxxx " + "failure");
                            d.dismiss();
                        }
                    });
        } else
            BullyToast.showToast(getActivity(), "Sorry! We could not create your account. Please check your internet connection and try again.");
    }
    public static final String md5(final String s) {
        final String MD5 = "MD5";
        try {
            // Create MD5 Hash
            MessageDigest digest = java.security.MessageDigest
                    .getInstance(MD5);
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuilder hexString = new StringBuilder();
            for (byte aMessageDigest : messageDigest) {
                String h = Integer.toHexString(0xFF & aMessageDigest);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
                password = hexString.toString();
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

    public void customAlertDialog(String title, String text) {
        final Dialog dialogLogout = new Dialog(getActivity(), android.R.style.Theme_Translucent_NoTitleBar);
        dialogLogout.setContentView(R.layout.custom_dialog);
        dialogLogout.show();

        EditText fp_edtxt_email = (EditText) dialogLogout.findViewById(R.id.fp_edtxt_email);
        fp_edtxt_email.setVisibility(View.GONE);

        TextView dialog_header = (TextView) dialogLogout.findViewById(R.id.dialog_header);
        dialog_header.setVisibility(View.VISIBLE);
        dialog_header.setText(title);
        ImageView iView_cross = (ImageView) dialogLogout.findViewById(R.id.dialog_header_cross);
        TextView tv_submit = (TextView) dialogLogout.findViewById(R.id.dialog_submit);
        tv_submit.setText("OK");
        TextView dialog_text = (TextView) dialogLogout.findViewById(R.id.dialog_text);
        dialog_text.setVisibility(View.VISIBLE);
        dialog_text.setText(text);

        tv_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dialogLogout.dismiss();
            }
        });
        iView_cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogLogout.dismiss();
            }
        });
    }

    private void SchoolListDialog() {
        final Dialog dialogLogout = new Dialog(getActivity(), android.R.style.Theme_Translucent_NoTitleBar);
        dialogLogout.setContentView(R.layout.schoollistdialog);
        dialogLogout.show();


        TextView dialog_header = (TextView) dialogLogout.findViewById(R.id.school_dialog_header);
        dialog_header.setVisibility(View.VISIBLE);
        dialog_header.setText("School Information");
        ImageView iView_cross = (ImageView) dialogLogout.findViewById(R.id.dialog_header_cross);
        TextView tv_submit = (TextView) dialogLogout.findViewById(R.id.schooldialog_submit);
        tv_submit.setText("YES");
        TextView tv_cancel = (TextView) dialogLogout.findViewById(R.id.schooldialog_cancel);
        TextView dialog_text = (TextView) dialogLogout.findViewById(R.id.schooldialog_text);
        dialog_text.setVisibility(View.VISIBLE);
        dialog_text.setText("Is this Correct?");

        TextView edtxt_school_name = (TextView) dialogLogout.findViewById(R.id.dialog_school_name);
        final TextView school_add = (TextView) dialogLogout.findViewById(R.id.dialog_school_add);

        edtxt_school_name.setText(school_name);
        school_add.setText(school_address);

        tv_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                edtxt_schoolemail.setText(school_email);
                school_addres.setText(school_address);
                principle_name.setText(school_principle_name);
                school_number.setText(school_mobile);

                school_id = school_id_info;
                disableEdittext();
                status = true;
                dialogLogout.dismiss();

            }
        });
        iView_cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogLogout.dismiss();
            }
        });

        tv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogLogout.dismiss();
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        lblContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                submitform();
            }
        });
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        if (isVisibleToUser){
            getActivity().getWindow().setSoftInputMode(
                    WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
            ((SignUpActivity)getActivity()).hideSoftKeyboard();
            getData();
        } else {

        }
    }

    private void setStateData() {
        if (isInternetPresent) {
            RestClient.get().getState(new Callback<StatePojo>() {
                @Override
                public void success(StatePojo statePojo, Response response) {
                    System.out.println("xxx" + "Sucess");
                    if (statePojo != null) {
                        if (statePojo.getStatus()) {
                            if (statePojo.getObject().size() > 0){
                                arrayStateList = statePojo.getObject();
                                StateListPojo slp = new StateListPojo();
                                slp.setState_name("Select your state");
                                arrayStateList.add(0, slp);
                                state_adapter = new StateListAdapter(getActivity(), arrayStateList);
                                state_spinner.setAdapter(state_adapter);

                            } else {
                                BullyToast.showToast(getActivity(), BullyConstants.NO_DATA);
                            }
                        } else {
                            BullyToast.showToast(getActivity(),statePojo.getMessage());
                        }
                    } else {
                        StateListPojo slp = new StateListPojo();
                        slp.setState_name("Select your state");
                        arrayStateList.add(0, slp);
                        state_adapter = new StateListAdapter(getActivity(), arrayStateList);
                        state_spinner.setAdapter(state_adapter);
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    System.out.println("xxx" + "Failure");
                }
            });
        } else {
            StateListPojo slp = new StateListPojo();
            slp.setState_name("State");
            arrayStateList.add(0, slp);
            state_adapter = new StateListAdapter(getActivity(), arrayStateList);
            state_spinner.setAdapter(state_adapter);
        }
    }
    private void setCityData() {
        if (isInternetPresent) {
            RestClient.get().getCity(state_id, new Callback<CityPojo>() {
                @Override
                public void success(CityPojo cityPojo, Response response) {
                    if (cityPojo != null) {
                        if (cityPojo.getStatus()) {
                            arrayCityList = cityPojo.getObject();
                            CityListPojo clp = new CityListPojo();
                            clp.setCity_name("Select your city");
                            arrayCityList.add(0, clp);
                            city_adapter = new CityListAdapter(getActivity(), arrayCityList);
                            city_spinner.setAdapter(city_adapter);
                        } else {
                            // BullyToast.showToast(getActivity(),cityPojo.getMessage());
                        }
                    } else {
                          BullyToast.showToast(getActivity(),BullyConstants.SERVER_NOT_RESPOND);
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    System.out.println("xxx" + "failure");
                }
            });
        } else {
            CityListPojo clp = new CityListPojo();
            clp.setCity_name("Select your city");
            arrayCityList.add(0, clp);
            city_adapter = new CityListAdapter(getActivity(), arrayCityList);
            city_spinner.setAdapter(city_adapter);
        }
    }

    private void setSchoolData(){
        if (isInternetPresent){
            final ProgressDialog d = BullyDialogs.showLoading(getActivity());
            d.setCanceledOnTouchOutside(false);

            RestClient.get().getSchoolList(edtxt_school_id.getText().toString(), new Callback<SchoolPojo>() {
                @Override
                public void success(SchoolPojo schoolPojo, Response response) {

                    if (schoolPojo != null){
                        if (schoolPojo.getStatus()){
                            System.out.println("xxx right");

                            school_id_info = schoolPojo.getObject().getId();
                            school_name = schoolPojo.getObject().getSchool_name();
                            school_address = schoolPojo.getObject().getSchool_address();
                            school_principle_name = schoolPojo.getObject().getPrinciple_name();
                            school_mobile = schoolPojo.getObject().getSchool_mobile();
                            school_email = schoolPojo.getObject().getSchool_email();

                            SchoolListDialog();
                        } else {
                            customAlertDialog("School Information",schoolPojo.getMessage());
                        }
                        d.dismiss();
                    }
                    else
                    {
                        d.dismiss();
                        BullyToast.showToast(getActivity(),BullyConstants.SERVER_NOT_RESPOND);
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    System.out.println("xxx fail");
                    d.dismiss();
                }
            });
        }
    }


    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        }
    }
    public  boolean validateEmail(TextInputLayout layoutTxt,EditText edtTxt) {
        if (edtTxt.getText().toString().trim().isEmpty())  {
            tv_err_school_email.setText(getResources().getString(R.string.err_school_email));
            tv_err_school_email.setVisibility(View.VISIBLE);
            requestFocus(edtTxt);
            return false;
        } else {
            layoutTxt.setErrorEnabled(false);
        }

        return true;
    }

    protected static boolean isValidEmail(CharSequence target) {
        return target != null && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    public class PhoneNumberTextWatcher implements TextWatcher {


        private EditText edTxt;
        private boolean isDelete;

        public PhoneNumberTextWatcher(EditText edTxtPhone) {
            this.edTxt = edTxtPhone;
            edTxt.setOnKeyListener(new View.OnKeyListener() {

                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    if (keyCode == KeyEvent.KEYCODE_DEL) {
                        isDelete = true;
                    }
                    return false;
                }
            });
        }


        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        public void beforeTextChanged(CharSequence s, int start, int count,
                                      int after) {
        }

        public void afterTextChanged(Editable s) {

            if (isDelete) {
                isDelete = false;
                return;
            }
            String val = s.toString();
            String a = "";
            String b = "";
            String c = "";
            if (val != null && val.length() > 0) {
                val = val.replace("-", "");
                if (val.length() >= 3) {
                    a = val.substring(0, 3);
                } else if (val.length() < 3) {
                    a = val.substring(0, val.length());
                }
                if (val.length() >= 6) {
                    b = val.substring(3, 6);
                    c = val.substring(6, val.length());
                } else if (val.length() > 3 && val.length() < 6) {
                    b = val.substring(3, val.length());
                }
                StringBuffer stringBuffer = new StringBuffer();
                if (a != null && a.length() > 0) {
                    stringBuffer.append(a);
                    if (a.length() == 3) {
                        stringBuffer.append("-");
                    }
                }
                if (b != null && b.length() > 0) {
                    stringBuffer.append(b);
                    if (b.length() == 3) {
                        stringBuffer.append("-");
                    }
                }
                if (c != null && c.length() > 0) {
                    stringBuffer.append(c);
                }
                edTxt.removeTextChangedListener(this);
                edTxt.setText(stringBuffer.toString());
                edTxt.setSelection(edTxt.getText().toString().length());
                edTxt.addTextChangedListener(this);
            } else {
                edTxt.removeTextChangedListener(this);
                edTxt.setText("");
                edTxt.addTextChangedListener(this);
            }
        }
    }

    private class MyTextWatcher implements TextWatcher {

        private View view;
        private MyTextWatcher(View view) {
            this.view = view;
        }

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.signup_school_email:
                   /* validateEmail(inputlayoutschoolemail,edtxt_schoolemail);
                    validEmail(inputlayoutschoolemail,edtxt_schoolemail);*/

                    tv_err_school_email.setVisibility(View.GONE);

              //      inputlayoutschoolemail.setErrorEnabled(false);
                    break;

                case R.id.signup_principle_name:

                    tv_err_principal_name.setVisibility(View.GONE);

            }
        }
    }

    private void enableEdittext()
    {
        principle_name.setFocusable(true);
        principle_name.setFocusableInTouchMode(true);

        school_number.setFocusable(true);
        school_number.setFocusableInTouchMode(true);

        school_addres.setFocusable(true);
        school_addres.setFocusableInTouchMode(true);

        edtxt_schoolemail.setFocusable(true);
        edtxt_schoolemail.setFocusableInTouchMode(true);
    }

    private void disableEdittext()
    {
        principle_name.setFocusable(false);
        principle_name.setFocusableInTouchMode(false);

        school_number.setFocusable(false);
        school_number.setFocusableInTouchMode(false);

        school_addres.setFocusable(false);
        school_addres.setFocusableInTouchMode(false);

        edtxt_schoolemail.setFocusable(false);
        edtxt_schoolemail.setFocusableInTouchMode(false);
    }
}
