package com.nexgen.bullybeware.fragment;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nexgen.bullybeware.R;
import com.nexgen.bullybeware.activity.FriendListActivity;
import com.nexgen.bullybeware.activity.MyRecordActivity;
import com.nexgen.bullybeware.adapter.MyRecordAdapter;
import com.nexgen.bullybeware.model.RestClient;
import com.nexgen.bullybeware.pojo.DeleteSubmissionData;
import com.nexgen.bullybeware.pojo.GetSubmissionObjectPojo;
import com.nexgen.bullybeware.pojo.GetSubmissionPojo;
import com.nexgen.bullybeware.util.BullyConstants;
import com.nexgen.bullybeware.util.BullyDialogs;
import com.nexgen.bullybeware.util.BullyPreferences;
import com.nexgen.bullybeware.util.BullyToast;
import com.nexgen.bullybeware.util.ConnectionDetector;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by quepplin1 on 9/28/2016.
 */
public class MySubmissionsFragment extends Fragment implements View.OnClickListener{

    private View view;
    private ImageView iView_filter_all,iView_filter_video,iView_filter_audio,iView_filter_photo, filter_iView_text;
    private BullyPreferences preferences;
    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private MyRecordAdapter adapter;
    public ArrayList<GetSubmissionObjectPojo> arraysubmissionalllist;
    private ArrayList<GetSubmissionObjectPojo> arrayselectelist;
    private String type;
    private LinearLayout nodatafound;
    // flag for Internet connection status
    Boolean isInternetPresent = false;
    private boolean fragmentResume = false;
    private boolean fragmentVisible = false;
    private boolean fragmentOnCreated = false;
    private String click = "all";
    Dialog dialogLogout;

    // Connection detector class
    ConnectionDetector cd;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.submission_fragment, container, false);

        init();

        return view;
    }
    private void init(){
        iView_filter_all = (ImageView) view.findViewById(R.id.filter_iView_all);
        iView_filter_video = (ImageView) view.findViewById(R.id.filter_iView_videos);
        iView_filter_audio = (ImageView) view.findViewById(R.id.filter_iView_audio);
        iView_filter_photo = (ImageView) view.findViewById(R.id.filter_iView_photo);
        filter_iView_text = (ImageView) view.findViewById(R.id.filter_iView_text);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.submissions_recycler_view);

        nodatafound = (LinearLayout) view.findViewById(R.id.submission_ll_NoData);

        nodatafound.setVisibility(View.VISIBLE);

        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);

        arraysubmissionalllist = new ArrayList<>();

        arrayselectelist = new ArrayList<>();

        cd = new ConnectionDetector(getActivity());
        // get Internet status
        isInternetPresent = cd.isConnectingToInternet();

        preferences = new BullyPreferences(getActivity());

        if (!fragmentResume && fragmentVisible &&  arraysubmissionalllist != null && arraysubmissionalllist.size() == 0) { //only when first time fragment is created
            getSubmissionData();
        }

        iView_filter_all.setOnClickListener(this);
        iView_filter_video.setOnClickListener(this);
        iView_filter_audio.setOnClickListener(this);
        iView_filter_photo.setOnClickListener(this);
        filter_iView_text.setOnClickListener(this);

    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        if (isVisibleToUser && isResumed() && arraysubmissionalllist != null && arraysubmissionalllist.size() == 0){ // only at fragment screen is resumed
            fragmentResume = true;
            fragmentVisible = false;
            fragmentOnCreated = true;
            getSubmissionData();
        } else if (isVisibleToUser) {        // only at fragment onCreated
            fragmentResume = false;
            fragmentVisible = true;
            fragmentOnCreated = true;
        } else if (!isVisibleToUser && fragmentOnCreated) {// only when you go out of fragment screen
            fragmentVisible = false;
            fragmentResume = false;
        }
    }

    private void getSubmissionData(){
        if (isInternetPresent) {
            final ProgressDialog d = BullyDialogs.showLoading(getActivity());
            d.setCanceledOnTouchOutside(false);
            RestClient.get().getSubmissionInfo(preferences.getStudentId(), new Callback<GetSubmissionPojo>() {
                @Override
                public void success(GetSubmissionPojo basepojo, Response response) {
                    if (basepojo != null) {
                        if (basepojo.getStatus()) {
                            System.out.println("xxx2 Sucess");

                            d.dismiss();

                            arraysubmissionalllist = basepojo.getObject();
                            if (arraysubmissionalllist.size() > 0){
                                nodatafound.setVisibility(View.GONE);
                            } else {
                                nodatafound.setVisibility(View.VISIBLE);
                            }
                            adapter = new MyRecordAdapter(getActivity(), arraysubmissionalllist,MySubmissionsFragment.this);
                            mRecyclerView.setAdapter(adapter);
                            adapter.notifyDataSetChanged();
                        } else {
                            d.dismiss();
                        }
                    }
                    else
                    {
                        d.dismiss();
                        BullyToast.showToast(getActivity(),BullyConstants.SERVER_NOT_RESPOND);
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    System.out.println("xxx2 fail");
                }
            });
        } else {
 //           BullyToast.showToast(getActivity(), BullyConstants.NO_INTERNET_CONNECTED);
            nodatafound.setVisibility(View.GONE);
            ((MyRecordActivity)getActivity()).customAlertDialog("BullyBeware", BullyConstants.NO_INTERNET_CONNECTED);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.filter_iView_all:

                iView_filter_all.setImageResource(R.mipmap.check_box_checked);
                iView_filter_video.setImageResource(R.mipmap.check_box_unchecked);
                iView_filter_audio.setImageResource(R.mipmap.check_box_unchecked);
                iView_filter_photo.setImageResource(R.mipmap.check_box_unchecked);
                filter_iView_text.setImageResource(R.mipmap.check_box_unchecked);

                if (arraysubmissionalllist.size() > 0){
                    nodatafound.setVisibility(View.GONE);
                } else {
                    nodatafound.setVisibility(View.VISIBLE);
                }

                adapter = new MyRecordAdapter(getActivity(), arraysubmissionalllist,MySubmissionsFragment.this);
                mRecyclerView.setAdapter(adapter);
                adapter.notifyDataSetChanged();

                click = "all";

                break;
            case R.id.filter_iView_videos:

                iView_filter_all.setImageResource(R.mipmap.check_box_unchecked);
                iView_filter_video.setImageResource(R.mipmap.check_box_checked);
                iView_filter_audio.setImageResource(R.mipmap.check_box_unchecked);
                iView_filter_photo.setImageResource(R.mipmap.check_box_unchecked);
                filter_iView_text.setImageResource(R.mipmap.check_box_unchecked);

                click = "other";

      //          ArrayList<GetSubmissionObjectPojo> arr_video = new ArrayList<>();
                arrayselectelist.clear();

                for (int i=0;i<arraysubmissionalllist.size();i++){
                    if (arraysubmissionalllist.get(i).getType().equalsIgnoreCase("3"))
                        arrayselectelist.add(arraysubmissionalllist.get(i));
                }

                if (arrayselectelist.size() > 0){
                    nodatafound.setVisibility(View.GONE);
                } else {
                    nodatafound.setVisibility(View.VISIBLE);
                }

                adapter = new MyRecordAdapter(getActivity(), arrayselectelist,MySubmissionsFragment.this);
                mRecyclerView.setAdapter(adapter);
                adapter.notifyDataSetChanged();


                break;

            case R.id.filter_iView_audio:

                iView_filter_all.setImageResource(R.mipmap.check_box_unchecked);
                iView_filter_video.setImageResource(R.mipmap.check_box_unchecked);
                iView_filter_audio.setImageResource(R.mipmap.check_box_checked);
                iView_filter_photo.setImageResource(R.mipmap.check_box_unchecked);
                filter_iView_text.setImageResource(R.mipmap.check_box_unchecked);

   //             ArrayList<GetSubmissionObjectPojo> arr_audio = new ArrayList<>();

                click = "other";

                arrayselectelist.clear();

                for(int i =0; i< arraysubmissionalllist.size(); i++)
                {
                    if(arraysubmissionalllist.get(i).getType().equalsIgnoreCase("2"))
                        arrayselectelist.add(arraysubmissionalllist.get(i));
                }

                if (arrayselectelist.size() > 0){
                    nodatafound.setVisibility(View.GONE);
                } else {
                    nodatafound.setVisibility(View.VISIBLE);
                }

                adapter = new MyRecordAdapter(getActivity(), arrayselectelist,MySubmissionsFragment.this);
                mRecyclerView.setAdapter(adapter);
                adapter.notifyDataSetChanged();
                break;

            case R.id.filter_iView_photo:

                iView_filter_all.setImageResource(R.mipmap.check_box_unchecked);
                iView_filter_video.setImageResource(R.mipmap.check_box_unchecked);
                iView_filter_audio.setImageResource(R.mipmap.check_box_unchecked);
                iView_filter_photo.setImageResource(R.mipmap.check_box_checked);
                filter_iView_text.setImageResource(R.mipmap.check_box_unchecked);

                click = "other";

      //          ArrayList<GetSubmissionObjectPojo> arr_image = new ArrayList<>();
                arrayselectelist.clear();


                for(int i =0; i< arraysubmissionalllist.size(); i++)
                {
                    if(arraysubmissionalllist.get(i).getType().equalsIgnoreCase("1"))
                        arrayselectelist.add(arraysubmissionalllist.get(i));
                }
                if (arrayselectelist.size() > 0){
                    nodatafound.setVisibility(View.GONE);
                } else {
                    nodatafound.setVisibility(View.VISIBLE);
                }

                adapter = new MyRecordAdapter(getActivity(), arrayselectelist,MySubmissionsFragment.this);
                mRecyclerView.setAdapter(adapter);
                adapter.notifyDataSetChanged();
                break;

            case R.id.filter_iView_text:

                iView_filter_all.setImageResource(R.mipmap.check_box_unchecked);
                iView_filter_video.setImageResource(R.mipmap.check_box_unchecked);
                iView_filter_audio.setImageResource(R.mipmap.check_box_unchecked);
                iView_filter_photo.setImageResource(R.mipmap.check_box_unchecked);
                filter_iView_text.setImageResource(R.mipmap.check_box_checked);

                click = "other";

                //          ArrayList<GetSubmissionObjectPojo> arr_image = new ArrayList<>();
                arrayselectelist.clear();


                for(int i =0; i< arraysubmissionalllist.size(); i++)
                {
                    if(arraysubmissionalllist.get(i).getType().equalsIgnoreCase("4"))
                        arrayselectelist.add(arraysubmissionalllist.get(i));
                }
                if (arrayselectelist.size() > 0){
                    nodatafound.setVisibility(View.GONE);
                } else {
                    nodatafound.setVisibility(View.VISIBLE);
                }

                adapter = new MyRecordAdapter(getActivity(), arrayselectelist,MySubmissionsFragment.this);
                mRecyclerView.setAdapter(adapter);
                adapter.notifyDataSetChanged();
                break;
        }
    }

    public void deletefile(final String submission_id, final int pos){
        final ProgressDialog d = BullyDialogs.showLoading(getActivity());
        d.setCanceledOnTouchOutside(false);

        RestClient.get().deleteSubmissiondata(submission_id, new Callback<DeleteSubmissionData>() {
            @Override
            public void success(DeleteSubmissionData deleteSubmissionData, Response response) {
                if (deleteSubmissionData != null){
                    System.out.println("dddd success");

                    try {
                        if (click.equalsIgnoreCase("all")){
                            arraysubmissionalllist.remove(pos);
                        } else {
                            for (int i=0; i< arraysubmissionalllist.size();i++){
                                if (arraysubmissionalllist.get(i).getId().equalsIgnoreCase(arrayselectelist.get(pos).getId())){

                                    arraysubmissionalllist.remove(i);
                                    break;
                                }
                            }
                            arrayselectelist.remove(pos);
                        }
                        adapter.notifyDataSetChanged();
                    } catch (Exception e){
                        e.printStackTrace();
                    }
                    d.dismiss();
                }
                else
                {
                    d.dismiss();
                    BullyToast.showToast(getActivity(),BullyConstants.SERVER_NOT_RESPOND);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                System.out.println("dddd fail");
            }
        });

        System.out.println("xxxx deletete");
    }

    public void DeleteSubmissiondialog(final String submission_id, final int pos) {
        dialogLogout = new Dialog(getActivity(), android.R.style.Theme_Translucent_NoTitleBar);
        dialogLogout.setContentView(R.layout.logout_dialog);
        dialogLogout.show();


        TextView dialog_header = (TextView) dialogLogout.findViewById(R.id.logout_dialog_header);
        dialog_header.setVisibility(View.VISIBLE);
        dialog_header.setText("My Submission");
        LinearLayout ll_submit = (LinearLayout) dialogLogout.findViewById(R.id.ll_dialog_submit);
        LinearLayout ll_cancel = (LinearLayout) dialogLogout.findViewById(R.id.ll_dialog_cancel);
        ImageView iView_cross = (ImageView) dialogLogout.findViewById(R.id.dialog_header_cross);
        TextView tv_submit = (TextView) dialogLogout.findViewById(R.id.dialog_submit);
        tv_submit.setText("YES");
        TextView tv_cancel = (TextView) dialogLogout.findViewById(R.id.dialog_cancel);
        TextView dialog_text = (TextView) dialogLogout.findViewById(R.id.dialog_text);
        dialog_text.setVisibility(View.VISIBLE);
        dialog_text.setText("Are you sure you want to delete?");


        ll_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                deletefile(submission_id,pos);

                dialogLogout.dismiss();
            }
        });
        iView_cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogLogout.dismiss();
            }
        });

        ll_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogLogout.dismiss();
            }
        });
    }
}
