package com.nexgen.bullybeware.fragment;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.nexgen.bullybeware.R;
import com.nexgen.bullybeware.activity.LoginActivity;
import com.nexgen.bullybeware.activity.MainActivity;
import com.nexgen.bullybeware.activity.SignUpActivity;
import com.nexgen.bullybeware.adapter.AgeListAdapter;
import com.nexgen.bullybeware.model.RestClient;
import com.nexgen.bullybeware.pojo.ParentsEmailpojo;
import com.nexgen.bullybeware.util.BullyConstants;
import com.nexgen.bullybeware.util.BullyDialogs;
import com.nexgen.bullybeware.util.BullyPreferences;
import com.nexgen.bullybeware.util.BullyToast;
import com.nexgen.bullybeware.util.ConnectionDetector;
import com.nexgen.bullybeware.util.ImageLoadingUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;

/**
 * Created by quepplin1 on 9/7/2016.
 */
public class FirstSignUpFragment extends Fragment {

    private View view;
    private ImageView iView_camera;
    private static final String CAMERA_DIR = "/dcim/";
    private final int SELECT_PHOTO = 457;
    private final int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 456;

    private String mCurrentPhotoPath;
    private String photoPath = "";
    private BullyPreferences preference;

    private TextView tv_err_fn, tv_err_ln, tv_err_email, tv_err_password, tv_err_conPassword;

    private EditText edtxt_fname, edtxt_lname, edtxt_email, edtxt_password, edtxt_confirm_password;
    private TextInputLayout inputlayout_fname, inputlayout_lname, inputlayout_email, inputlayout_password, inputlayout_confPassword;

    // flag for Internet connection status
    Boolean isInternetPresent = false;

    Dialog dialogLogout;

    // Connection detector class
    ConnectionDetector cd;
    private TextView lblContinue;
    private SharedPreferences sharedPreferences;
    private final static int GET_CAMERA_RESULT = 102;
    private ArrayList<String> permissionsToRequest;
    private ArrayList<String> permissionsRejected;

    private LinearLayout ll_signup_form, ll_parents_form, age_ll_spinner;
    private Spinner age_spinner;
    private AgeListAdapter adapter;
    private ArrayList<String> arrayagelist;
    private Integer age;
    private RelativeLayout rl_image;
    private EditText edtxt_age_email;
    private TextView signup_age_submit;
    private LinearLayout ll_age_spinner;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.first_signup_fragment, container, false);

        getActivity().getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        init();
        setAgeList();
        setOnClickListner();

        return view;
    }

    private void init() {

        edtxt_fname = (EditText) view.findViewById(R.id.signup_edtxt_fname);
        edtxt_lname = (EditText) view.findViewById(R.id.signup_edtxt_lname);
        edtxt_email = (EditText) view.findViewById(R.id.signup_edtxt_email);
        edtxt_password = (EditText) view.findViewById(R.id.signup_edtxt_password);
        edtxt_confirm_password = (EditText) view.findViewById(R.id.signup_edtxt_confirm_password);

        ll_age_spinner = (LinearLayout) view.findViewById(R.id.ll_age_spinner);

        edtxt_age_email = (EditText) view.findViewById(R.id.signup_edtxt_age_email);

        signup_age_submit = (TextView) view.findViewById(R.id.signup_age_submit);

        inputlayout_fname = (TextInputLayout) view.findViewById(R.id.input_layout_fname);
        inputlayout_lname = (TextInputLayout) view.findViewById(R.id.input_layout_lname);
        inputlayout_email = (TextInputLayout) view.findViewById(R.id.input_layout_email);
        inputlayout_password = (TextInputLayout) view.findViewById(R.id.input_layout_password);
        inputlayout_confPassword = (TextInputLayout) view.findViewById(R.id.input_layout_confirm_password);

        tv_err_fn = (TextView) view.findViewById(R.id.tv_err_firstname);
        tv_err_ln = (TextView) view.findViewById(R.id.tv_err_lastname);
        tv_err_email = (TextView) view.findViewById(R.id.tv_err_email);
        tv_err_password = (TextView) view.findViewById(R.id.tv_err_password);
        tv_err_conPassword = (TextView) view.findViewById(R.id.tv_err_conf_password);

        iView_camera = (ImageView) view.findViewById(R.id.iView_camera);
        lblContinue = (TextView) view.findViewById(R.id.signup_tv_first_continue);

        cd = new ConnectionDetector(getActivity().getApplicationContext());

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());

        edtxt_fname.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
        edtxt_lname.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
        // get Internet status
        isInternetPresent = cd.isConnectingToInternet();

        preference = new BullyPreferences(getActivity());

        if (preference.getLOGGEDINFROM().equalsIgnoreCase("gmail") || preference.getLOGGEDINFROM().equalsIgnoreCase("fb")) {
            if (preference.getFname() != null) {
                edtxt_fname.setText(preference.getFname());
            }
            if (preference.getLastName() != null) {
                edtxt_lname.setText(preference.getLastName());

            }
            if (preference.getEmail() != null) {
                edtxt_email.setText(preference.getEmail());
            }

            if (preference.getEmail().length() > 0) {
                edtxt_email.setFocusable(false);
            } else {
                edtxt_email.setFocusableInTouchMode(true);
            }

            ll_age_spinner.setVisibility(View.GONE);

            edtxt_fname.setFocusable(false);
            edtxt_lname.setFocusable(false);

            edtxt_password.setFocusableInTouchMode(true);
            edtxt_confirm_password.setFocusableInTouchMode(true);

            iView_camera.setEnabled(true);

        } else {

            ll_age_spinner.setVisibility(View.VISIBLE);

            edtxt_fname.setFocusable(false);
            edtxt_lname.setFocusable(false);
            edtxt_email.setFocusable(false);
            edtxt_password.setFocusable(false);
            edtxt_confirm_password.setFocusable(false);
            iView_camera.setEnabled(false);
        }

        rl_image = (RelativeLayout) view.findViewById(R.id.rl_image);

        ll_signup_form = (LinearLayout) view.findViewById(R.id.ll_age_signup_form);
        ll_parents_form = (LinearLayout) view.findViewById(R.id.ll_parents_form);

        age_ll_spinner = (LinearLayout) view.findViewById(R.id.age_ll_spinner);
        age_spinner = (Spinner) view.findViewById(R.id.signup_age_spinner);

        arrayagelist = new ArrayList<String>();

        String[] otherList = new String[]{"Age", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25"};
        arrayagelist.addAll(Arrays.asList(otherList));
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        if (getActivity() != null)
            if (isVisibleToUser) {
                ((SignUpActivity) getActivity()).hideSoftKeyboard();
            } else {
                if (preference != null) {
                    setData();
                }
            }
    }

    private void setAgeList() {
        adapter = new AgeListAdapter(getActivity(), arrayagelist);
        age_spinner.setAdapter(adapter);
    }

    public void setData() {
        try {
            if (preference != null) {
                preference.setImage(photoPath);
                preference.setFname(edtxt_fname.getText().toString());
                preference.setLastName(edtxt_lname.getText().toString());
                preference.setEmail(edtxt_email.getText().toString());
                preference.setPassword(edtxt_password.getText().toString());
                preference.setUser_age(age.toString());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setOnClickListner() {

        lblContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (age.equals(50)) {
                    BullyToast.showToast(getActivity(), "Please select age");
                } else {
                    submitForm();
                }

            }
        });
        iView_camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    addPermissionDialogCamera();
                } else {
                    CameraDialog();
                }
            }
        });

        age_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                if (i == 0) {

                    age = 50;

                    LinearLayout parent = (LinearLayout) view;

                    ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.song_name_text_color));

                } else {
                    try {
                        age = Integer.valueOf(arrayagelist.get(i));

                        System.out.println("age" + age);

                        LinearLayout parent = (LinearLayout) view;

                        ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.black));

                        if (age < 13) {
                            ll_parents_form.setVisibility(View.VISIBLE);
                            ll_signup_form.setVisibility(View.GONE);
                            lblContinue.setVisibility(View.GONE);
                            rl_image.setVisibility(View.GONE);

                            Intent intent = new Intent("Action_age");
                            intent.putExtra("age_limit", "under");
                            LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);

                        } else {
                            ll_signup_form.setVisibility(View.VISIBLE);
                            lblContinue.setVisibility(View.VISIBLE);
                            rl_image.setVisibility(View.VISIBLE);
                            ll_parents_form.setVisibility(View.GONE);

                            edtxt_fname.setFocusableInTouchMode(true);
                            edtxt_lname.setFocusableInTouchMode(true);
                            edtxt_email.setFocusableInTouchMode(true);
                            edtxt_password.setFocusableInTouchMode(true);
                            edtxt_confirm_password.setFocusableInTouchMode(true);

                            iView_camera.setEnabled(true);

                            Intent intent = new Intent("Action_age");
                            intent.putExtra("age_limit", "uper");
                            LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(intent);
                        }
                    } catch (NumberFormatException c) {
                        c.printStackTrace();
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        signup_age_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isInternetPresent = cd.isConnectingToInternet();
                if (isInternetPresent) {
                    if (edtxt_age_email.getText().toString().isEmpty()) {
                        BullyToast.showToast(getActivity(), "Email address cannot be blank");
                    } else {
                        sendemail();
                    }
                } else {
                    BullyToast.showToast(getActivity(), BullyConstants.NO_INTERNET_CONNECTED);
                }

            }
        });

        edtxt_fname.addTextChangedListener(new MyTextWatcher(edtxt_fname));
        edtxt_lname.addTextChangedListener(new MyTextWatcher(edtxt_lname));
        edtxt_email.addTextChangedListener(new MyTextWatcher(edtxt_email));
        edtxt_password.addTextChangedListener(new MyTextWatcher(edtxt_password));
        edtxt_confirm_password.addTextChangedListener(new MyTextWatcher(edtxt_confirm_password));

    }

    private void CameraDialog() {
        dialogLogout = new Dialog(getActivity(), android.R.style.Theme_Translucent_NoTitleBar);
        dialogLogout.setContentView(R.layout.camera_dialog);
        dialogLogout.show();

        LinearLayout ll_dialog = (LinearLayout) dialogLogout.findViewById(R.id.ll_dialog);

        TextView tv_camera = (TextView) dialogLogout.findViewById(R.id.tv_camera);
        TextView tv_gallety = (TextView) dialogLogout.findViewById(R.id.tv_gallery);

        ll_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogLogout.dismiss();
            }
        });

        tv_camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                try {
                    String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
                    String imageFileName = "bmh" + timeStamp + "_";
                    File albumF = getAlbumDir();
                    File imageF = File.createTempFile(imageFileName, "bmh", albumF);

                    mCurrentPhotoPath = imageF.getAbsolutePath();
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(imageF));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                startActivityForResult(takePictureIntent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);

                dialogLogout.dismiss();
            }
        });

        tv_gallety.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = new Intent(
                        Intent.ACTION_PICK,
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                startActivityForResult(i, SELECT_PHOTO);

                dialogLogout.dismiss();
            }
        });
    }

    private void sendemail() {

        if (isInternetPresent) {
            final ProgressDialog d = BullyDialogs.showLoading(getActivity());
            d.setCanceledOnTouchOutside(false);

            System.out.println("xyxy device_id" + preference.getDevice_id());
            System.out.println("xyxy imei" + preference.getImei());

            RestClient.get().sendmail(edtxt_age_email.getText().toString(), age.toString(), preference.getDevice_id(), "android", preference.getImei(), new Callback<ParentsEmailpojo>() {
                @Override
                public void success(ParentsEmailpojo parentsEmailpojo, Response response) {

                    if (parentsEmailpojo != null) {

                        if (parentsEmailpojo.getStatus()) {
                            BullyToast.showToast(getActivity(), parentsEmailpojo.getMessage());

                            Intent intent = new Intent(getActivity(), LoginActivity.class);
                            startActivity(intent);
                            getActivity().finish();
                        } else {
                            BullyToast.showToast(getActivity(), parentsEmailpojo.getMessage());
                        }
                        d.dismiss();
                    }
                    else
                    {
                        d.dismiss();
                        BullyToast.showToast(getActivity(),BullyConstants.SERVER_NOT_RESPOND);
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    d.dismiss();
                }
            });
        }
    }


    private File getAlbumDir() {
        File storageDir = null;

        if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.FROYO) {
                storageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "CameraPicture");
            } else {
                storageDir = new File(Environment.getExternalStorageDirectory() + CAMERA_DIR + "CameraPicture");
            }

            if (storageDir != null) {
                if (!storageDir.mkdirs()) {
                    if (!storageDir.exists()) {
                        //		Log.d("CameraSample", "failed to create directory");
                        return null;
                    }
                }
            }

        } else {
            //		Log.v(getString(R.string.app_name), "External storage is not mounted READ/WRITE.");
        }

        return storageDir;
    }

    public void onActivityResult(final int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data == null) {
            System.out.println("hh data null");
        }

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_PHOTO) {

                Uri selectedImage = data.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};

                Cursor cursor = getActivity().getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                String imagePath1 = cursor.getString(columnIndex);
                System.out.println("hh image path =" + imagePath1);

                preference.setUser_img(imagePath1);

                File f = new File(compressImage(imagePath1, 1));
                Bitmap myBitmap = BitmapFactory.decodeFile(f.getAbsolutePath());

                iView_camera.setImageBitmap(myBitmap);

                photoPath = f.getAbsolutePath();

            } else if (requestCode == CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE) {

                System.out.println("hh image path = " + mCurrentPhotoPath);

                preference.setUser_img(mCurrentPhotoPath);

                File f = new File(compressImage(mCurrentPhotoPath, 1));
                Bitmap myBitmap = BitmapFactory.decodeFile(f.getAbsolutePath());

                iView_camera.setImageBitmap(myBitmap);
                photoPath = f.getAbsolutePath();
            }
        }
    }

    public String compressImage(String imageUri, int flag) {
        String filePath;
        if (flag == 1) {
            filePath = getRealPathFromURI(imageUri);
        }
        filePath = imageUri;
        Bitmap scaledBitmap = null;

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        Bitmap bmp = BitmapFactory.decodeFile(filePath, options);

        int actualHeight = options.outHeight;
        int actualWidth = options.outWidth;
        float maxHeight = 816.0f;
        float maxWidth = 612.0f;
        float imgRatio = actualWidth / actualHeight;
        float maxRatio = maxWidth / maxHeight;

        if (actualHeight > maxHeight || actualWidth > maxWidth) {
            if (imgRatio < maxRatio) {
                imgRatio = maxHeight / actualHeight;
                actualWidth = (int) (imgRatio * actualWidth);
                actualHeight = (int) maxHeight;
            } else if (imgRatio > maxRatio) {
                imgRatio = maxWidth / actualWidth;
                actualHeight = (int) (imgRatio * actualHeight);
                actualWidth = (int) maxWidth;
            } else {
                actualHeight = (int) maxHeight;
                actualWidth = (int) maxWidth;

            }
        }

        ImageLoadingUtils utils = new ImageLoadingUtils(getActivity());
        options.inSampleSize = utils.calculateInSampleSize(options, actualWidth, actualHeight);
        options.inJustDecodeBounds = false;
        options.inDither = false;
//        options.inPurgeable = true;
//        options.inInputShareable = true;
        options.inTempStorage = new byte[16 * 1024];

        try {
            bmp = BitmapFactory.decodeFile(filePath, options);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();

        }
        try {
            scaledBitmap = Bitmap.createBitmap(actualWidth, actualHeight, Bitmap.Config.ARGB_8888);
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        }

        float ratioX = actualWidth / (float) options.outWidth;
        float ratioY = actualHeight / (float) options.outHeight;
        float middleX = actualWidth / 2.0f;
        float middleY = actualHeight / 2.0f;

        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(ratioX, ratioY, middleX, middleY);

        Canvas canvas = new Canvas(scaledBitmap);
        canvas.setMatrix(scaleMatrix);
        canvas.drawBitmap(bmp, middleX - bmp.getWidth() / 2, middleY - bmp.getHeight() / 2, new Paint(Paint.FILTER_BITMAP_FLAG));

        ExifInterface exif;
        try {
            exif = new ExifInterface(filePath);

            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 0);
            Log.d("EXIF", "Exif: " + orientation);
            Matrix matrix = new Matrix();
            if (orientation == 6) {
                matrix.postRotate(90);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 3) {
                matrix.postRotate(180);
                Log.d("EXIF", "Exif: " + orientation);
            } else if (orientation == 8) {
                matrix.postRotate(270);
                Log.d("EXIF", "Exif: " + orientation);
            }
            scaledBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix, true);
        } catch (IOException e) {
            e.printStackTrace();
        }
        FileOutputStream out = null;
        String filename = getFilename();
        try {
            out = new FileOutputStream(filename);
            scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 80, out);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return filename;
    }

    public String getFilename() {
        File file = new File(Environment.getExternalStorageDirectory().getPath(), "MyFolder/Images");
        if (!file.exists()) {
            file.mkdirs();
        }
        String uriSting = (file.getAbsolutePath() + "/" + System.currentTimeMillis() + ".jpg");
        return uriSting;
    }

    public String getRealPathFromURI(String contentURI) {
        Uri contentUri = Uri.parse(contentURI);
        Cursor cursor = getActivity().getContentResolver().query(contentUri, null, null, null, null);
        if (cursor == null) {
            return contentUri.getPath();
        } else {
            cursor.moveToFirst();
            int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
            return cursor.getString(idx);
        }
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    public boolean validateFirstname(TextInputLayout layoutTxt, EditText edtTxt) {
        if (edtTxt.getText().toString().trim().isEmpty()) {
            tv_err_fn.setText(getResources().getString(R.string.err_fname));
            tv_err_fn.setVisibility(View.VISIBLE);
            requestFocus(edtTxt);
            return false;
        } else {
            layoutTxt.setErrorEnabled(false);
        }

        return true;
    }

    public boolean validateFirstnamelength(TextInputLayout layoutTxt, EditText edtTxt) {
        if (edtTxt.getText().toString().trim().length() < 2 || edtTxt.getText().toString().trim().length() > 30) {
            tv_err_fn.setText(getResources().getString(R.string.err_fname_length));
            tv_err_fn.setVisibility(View.VISIBLE);
            requestFocus(edtTxt);
            return false;
        } else {
            layoutTxt.setErrorEnabled(false);
        }

        return true;
    }

    public boolean validateLastname(TextInputLayout layoutTxt, EditText edtTxt) {
        if (edtTxt.getText().toString().trim().isEmpty()) {
            tv_err_ln.setText(getResources().getString(R.string.err_lname));
            tv_err_ln.setVisibility(View.VISIBLE);
            requestFocus(edtTxt);
            return false;
        } else {
            layoutTxt.setErrorEnabled(false);
        }

        return true;
    }

    public boolean validateLastnamelength(TextInputLayout layoutTxt, EditText edtTxt) {
        if (edtTxt.getText().toString().trim().length() < 2 || edtTxt.getText().toString().trim().length() > 30) {
            tv_err_ln.setText(getResources().getString(R.string.err_lname_length));
            tv_err_ln.setVisibility(View.VISIBLE);
            requestFocus(edtTxt);
            return false;
        } else {
            layoutTxt.setErrorEnabled(false);
        }

        return true;
    }

    public boolean validateEmail(TextInputLayout layoutTxt, EditText edtTxt) {
        if (edtTxt.getText().toString().trim().isEmpty()) {
            tv_err_email.setText(getResources().getString(R.string.err_email));
            tv_err_email.setVisibility(View.VISIBLE);
            requestFocus(edtTxt);
            return false;
        } else {
            layoutTxt.setErrorEnabled(false);
        }

        return true;
    }

    public boolean validEmail(TextInputLayout layoutTxt, EditText edtTxt) {
        if (!isValidEmail(edtTxt.getText().toString().trim())) {
            tv_err_email.setText(getResources().getString(R.string.err_valid_email));
            tv_err_email.setVisibility(View.VISIBLE);
            requestFocus(edtTxt);
            return false;
        } else {
            layoutTxt.setErrorEnabled(false);
        }

        return true;
    }

    public boolean validatepassword(TextInputLayout layoutTxt, EditText edtTxt) {
        if (edtTxt.getText().toString().trim().isEmpty()) {
            tv_err_password.setText(getResources().getString(R.string.err_password));
            tv_err_password.setVisibility(View.VISIBLE);
            requestFocus(edtTxt);
            return false;
        } else {
            layoutTxt.setErrorEnabled(false);
        }

        return true;
    }

    public boolean validatepasswordlength(TextInputLayout layoutTxt, EditText edtTxt) {
        if (edtTxt.getText().toString().trim().length() < 6) {
            tv_err_password.setText(getResources().getString(R.string.err_password_length));
            tv_err_password.setVisibility(View.VISIBLE);
            requestFocus(edtTxt);
            return false;
        } else {
            layoutTxt.setErrorEnabled(false);
        }

        return true;
    }

    public boolean validateConfpassword(TextInputLayout layoutTxt, EditText edtTxt) {
        if (edtTxt.getText().toString().trim().isEmpty()) {
            tv_err_conPassword.setText(getResources().getString(R.string.err_password));
            tv_err_conPassword.setVisibility(View.VISIBLE);
            requestFocus(edtTxt);
            return false;
        } else {
            layoutTxt.setErrorEnabled(false);
        }

        return true;
    }

    public boolean validateConfpasswordlength(TextInputLayout layoutTxt, EditText edtTxt) {
        if (edtTxt.getText().toString().trim().length() < 6) {
            tv_err_conPassword.setText(getResources().getString(R.string.err_password_length));
            tv_err_conPassword.setVisibility(View.VISIBLE);
            requestFocus(edtTxt);
            return false;
        } else {
            layoutTxt.setErrorEnabled(false);
        }

        return true;
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
    }


    protected static boolean isValidEmail(CharSequence target) {
        return target != null && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    public boolean submitContinue() {
        if (!validateFirstname(inputlayout_fname, edtxt_fname)) {
            return true;
        }
        if (!validateFirstnamelength(inputlayout_fname, edtxt_fname))
            return true;
        if (!validateLastname(inputlayout_lname, edtxt_lname)) {
            return true;
        }
        if (!validateLastnamelength(inputlayout_lname, edtxt_lname))
            return true;
        if (!validateEmail(inputlayout_email, edtxt_email)) {
            return true;
        }
        if (!validEmail(inputlayout_email, edtxt_email)) {
            return true;
        }
        if (!validatepassword(inputlayout_password, edtxt_password)) {
            return true;
        }
        if (!validatepasswordlength(inputlayout_password, edtxt_password)) {
            return true;
        }
        if (!validateConfpassword(inputlayout_confPassword, edtxt_confirm_password)) {
            return true;
        }
        if (!validateConfpasswordlength(inputlayout_confPassword, edtxt_confirm_password)) {
            return true;
        }
        if (!edtxt_password.getText().toString().equalsIgnoreCase(edtxt_confirm_password.getText().toString())) {
            BullyToast.showToast(getActivity(), getResources().getString(R.string.err_password_not_match));
            return true;
        }
        return false;
    }

    public boolean submitForm() {
        if (!validateFirstname(inputlayout_fname, edtxt_fname)) {
            return true;
        }
        if (!validateFirstnamelength(inputlayout_fname, edtxt_fname))
            return true;
        if (!validateLastname(inputlayout_lname, edtxt_lname)) {
            return true;
        }
        if (!validateLastnamelength(inputlayout_lname, edtxt_lname))
            return true;
        if (!validateEmail(inputlayout_email, edtxt_email)) {
            return true;
        }
        if (!validEmail(inputlayout_email, edtxt_email)) {
            return true;
        }
        if (!validatepassword(inputlayout_password, edtxt_password)) {
            return true;
        }
        if (!validatepasswordlength(inputlayout_password, edtxt_password)) {
            return true;
        }
        if (!validateConfpassword(inputlayout_confPassword, edtxt_confirm_password)) {
            return true;
        }
        if (!validateConfpasswordlength(inputlayout_confPassword, edtxt_confirm_password)) {
            return true;
        }
        if (!edtxt_password.getText().toString().equalsIgnoreCase(edtxt_confirm_password.getText().toString())) {
            BullyToast.showToast(getActivity(), getResources().getString(R.string.err_password_not_match));
            return true;
        }
        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(new Intent("Action_Send").putExtra("Flag", 1));
        return false;
    }

    private class MyTextWatcher implements TextWatcher {

        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.signup_edtxt_fname:

                    tv_err_fn.setVisibility(View.GONE);
                    break;

                case R.id.signup_edtxt_lname:

                    tv_err_ln.setVisibility(View.GONE);

                    break;
                case R.id.signup_edtxt_email:

                    tv_err_email.setVisibility(View.GONE);

                    break;

                case R.id.signup_edtxt_password:

                    tv_err_password.setVisibility(View.GONE);

                    break;
                case R.id.signup_edtxt_confirm_password:

                    tv_err_conPassword.setVisibility(View.GONE);

                    break;

            }
        }
    }

    private void addPermissionDialogCamera() {
        ArrayList<String> permissions = new ArrayList<>();
        int resultCode;

        permissions.add(CAMERA);
        permissions.add(WRITE_EXTERNAL_STORAGE);
        resultCode = GET_CAMERA_RESULT;


        //filter out the permissions we have already accepted
        permissionsToRequest = findUnAskedPermissions(permissions, sharedPreferences);
        //get the permissions we have asked for before but are not granted..
        //we will store this in a global list to access later.
        permissionsRejected = findRejectedPermissions(permissions, sharedPreferences);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            if (permissionsToRequest.size() > 0) {//we need to ask for permissions
                //but have we already asked for them?
                requestPermissions(permissionsToRequest.toArray(new String[permissionsToRequest.size()]), resultCode);
                //mark all these as asked..
                for (String perm : permissionsToRequest) {
                    markAsAsked(perm, sharedPreferences);
                }
            } else {
                //show the success banner
                if (permissionsRejected.size() < permissions.size()) {
                    //this means we can show success because some were already accepted.
                    //permissionSuccess.setVisibility(View.VISIBLE);
                    CameraDialog();
                }

                if (permissionsRejected.size() > 0) {
                    //we have none to request but some previously rejected..tell the user.
                    //It may be better to show a dialog here in a prod application

                    try {
                        requestPermissions(permissionsToRequest.toArray(new String[permissionsToRequest.size()]), resultCode);
                        //mark all these as asked..
                        for (String perm : permissionsToRequest) {
                            markAsAsked(perm, sharedPreferences);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    public ArrayList<String> findUnAskedPermissions(ArrayList<String> wanted, SharedPreferences sharedPreferences) {
        ArrayList<String> result = new ArrayList<String>();

        for (String perm : wanted) {
            if (!hasPermission(perm) && shouldWeAsk(perm, sharedPreferences)) {
                result.add(perm);
            }
        }

        return result;
    }

    public ArrayList<String> findRejectedPermissions(ArrayList<String> wanted, SharedPreferences sharedPreferences) {
        ArrayList<String> result = new ArrayList<String>();

        for (String perm : wanted) {
            if (!hasPermission(perm) && !shouldWeAsk(perm, sharedPreferences)) {
                result.add(perm);
            }
        }

        return result;
    }

    public boolean hasPermission(String permission) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (canMakeSmores()) {
                return (getActivity().checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED);
            }
        }

        return true;
    }

    public void markAsAsked(String permission, SharedPreferences sharedPreferences) {
        sharedPreferences.edit().putBoolean(permission, false).apply();
    }

    public boolean canMakeSmores() {
        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);
    }

    public boolean shouldWeAsk(String permission, SharedPreferences sharedPreferences) {
        return (sharedPreferences.getBoolean(permission, true));
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {
            case GET_CAMERA_RESULT:
                if (hasPermission(WRITE_EXTERNAL_STORAGE) && hasPermission(CAMERA)) {

                    CameraDialog();

                } else {
                    permissionsRejected.add(WRITE_EXTERNAL_STORAGE);
                    permissionsRejected.add(CAMERA);
                    clearMarkAsAsked(WRITE_EXTERNAL_STORAGE, sharedPreferences);
                    clearMarkAsAsked(CAMERA, sharedPreferences);
                 /*   String message = "permissions was rejected. Please allow to run the app.";
                    makePostRequestSnack(message, permissionsRejected.size());*/
                }
                break;
        }
    }

    public void clearMarkAsAsked(String permission, SharedPreferences sharedPreferences) {
        sharedPreferences.edit().putBoolean(permission, true).apply();
    }

}
